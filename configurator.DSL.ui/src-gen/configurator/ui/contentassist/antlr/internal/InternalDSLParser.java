package configurator.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import configurator.services.DSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDSLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_INT", "RULE_STRING", "RULE_ID", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'not'", "'-'", "'+'", "'*'", "'/'", "'=='", "'!='", "' and '", "' or '", "'>'", "'>='", "'<'", "'<='", "'true'", "'false'", "'{'", "'}'", "'Types'", "','", "'Features'", "'Constraints'", "'['", "']'", "'..'", "'Boolean'", "'Integer'", "'...'", "'('", "')'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=6;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=4;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDSLParser.tokenNames; }
    public String getGrammarFileName() { return "../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g"; }


     
     	private DSLGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(DSLGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleConfiguration"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:60:1: entryRuleConfiguration : ruleConfiguration EOF ;
    public final void entryRuleConfiguration() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:61:1: ( ruleConfiguration EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:62:1: ruleConfiguration EOF
            {
             before(grammarAccess.getConfigurationRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleConfiguration_in_entryRuleConfiguration61);
            ruleConfiguration();

            state._fsp--;

             after(grammarAccess.getConfigurationRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleConfiguration68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConfiguration"


    // $ANTLR start "ruleConfiguration"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:69:1: ruleConfiguration : ( ( rule__Configuration__Group__0 ) ) ;
    public final void ruleConfiguration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:73:2: ( ( ( rule__Configuration__Group__0 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:74:1: ( ( rule__Configuration__Group__0 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:74:1: ( ( rule__Configuration__Group__0 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:75:1: ( rule__Configuration__Group__0 )
            {
             before(grammarAccess.getConfigurationAccess().getGroup()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:76:1: ( rule__Configuration__Group__0 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:76:2: rule__Configuration__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group__0_in_ruleConfiguration94);
            rule__Configuration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConfiguration"


    // $ANTLR start "entryRuleConstraint"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:88:1: entryRuleConstraint : ruleConstraint EOF ;
    public final void entryRuleConstraint() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:89:1: ( ruleConstraint EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:90:1: ruleConstraint EOF
            {
             before(grammarAccess.getConstraintRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleConstraint_in_entryRuleConstraint121);
            ruleConstraint();

            state._fsp--;

             after(grammarAccess.getConstraintRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleConstraint128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstraint"


    // $ANTLR start "ruleConstraint"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:97:1: ruleConstraint : ( ( rule__Constraint__Group__0 ) ) ;
    public final void ruleConstraint() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:101:2: ( ( ( rule__Constraint__Group__0 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:102:1: ( ( rule__Constraint__Group__0 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:102:1: ( ( rule__Constraint__Group__0 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:103:1: ( rule__Constraint__Group__0 )
            {
             before(grammarAccess.getConstraintAccess().getGroup()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:104:1: ( rule__Constraint__Group__0 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:104:2: rule__Constraint__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Constraint__Group__0_in_ruleConstraint154);
            rule__Constraint__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConstraintAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstraint"


    // $ANTLR start "entryRuleType"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:116:1: entryRuleType : ruleType EOF ;
    public final void entryRuleType() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:117:1: ( ruleType EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:118:1: ruleType EOF
            {
             before(grammarAccess.getTypeRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleType_in_entryRuleType181);
            ruleType();

            state._fsp--;

             after(grammarAccess.getTypeRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleType188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:125:1: ruleType : ( ( rule__Type__Alternatives ) ) ;
    public final void ruleType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:129:2: ( ( ( rule__Type__Alternatives ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:130:1: ( ( rule__Type__Alternatives ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:130:1: ( ( rule__Type__Alternatives ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:131:1: ( rule__Type__Alternatives )
            {
             before(grammarAccess.getTypeAccess().getAlternatives()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:132:1: ( rule__Type__Alternatives )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:132:2: rule__Type__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__Type__Alternatives_in_ruleType214);
            rule__Type__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleExpression"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:144:1: entryRuleExpression : ruleExpression EOF ;
    public final void entryRuleExpression() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:145:1: ( ruleExpression EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:146:1: ruleExpression EOF
            {
             before(grammarAccess.getExpressionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleExpression_in_entryRuleExpression241);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getExpressionRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleExpression248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:153:1: ruleExpression : ( ( rule__Expression__Alternatives ) ) ;
    public final void ruleExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:157:2: ( ( ( rule__Expression__Alternatives ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:158:1: ( ( rule__Expression__Alternatives ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:158:1: ( ( rule__Expression__Alternatives ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:159:1: ( rule__Expression__Alternatives )
            {
             before(grammarAccess.getExpressionAccess().getAlternatives()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:160:1: ( rule__Expression__Alternatives )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:160:2: rule__Expression__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__Expression__Alternatives_in_ruleExpression274);
            rule__Expression__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getExpressionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleEString"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:174:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:175:1: ( ruleEString EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:176:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_entryRuleEString303);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEString310); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:183:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:187:2: ( ( ( rule__EString__Alternatives ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:188:1: ( ( rule__EString__Alternatives ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:188:1: ( ( rule__EString__Alternatives ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:189:1: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:190:1: ( rule__EString__Alternatives )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:190:2: rule__EString__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__EString__Alternatives_in_ruleEString336);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleFeature"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:202:1: entryRuleFeature : ruleFeature EOF ;
    public final void entryRuleFeature() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:203:1: ( ruleFeature EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:204:1: ruleFeature EOF
            {
             before(grammarAccess.getFeatureRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_entryRuleFeature363);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getFeatureRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeature370); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:211:1: ruleFeature : ( ( rule__Feature__Group__0 ) ) ;
    public final void ruleFeature() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:215:2: ( ( ( rule__Feature__Group__0 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:216:1: ( ( rule__Feature__Group__0 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:216:1: ( ( rule__Feature__Group__0 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:217:1: ( rule__Feature__Group__0 )
            {
             before(grammarAccess.getFeatureAccess().getGroup()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:218:1: ( rule__Feature__Group__0 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:218:2: rule__Feature__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group__0_in_ruleFeature396);
            rule__Feature__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleBOOLEAN"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:230:1: entryRuleBOOLEAN : ruleBOOLEAN EOF ;
    public final void entryRuleBOOLEAN() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:231:1: ( ruleBOOLEAN EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:232:1: ruleBOOLEAN EOF
            {
             before(grammarAccess.getBOOLEANRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleBOOLEAN_in_entryRuleBOOLEAN423);
            ruleBOOLEAN();

            state._fsp--;

             after(grammarAccess.getBOOLEANRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleBOOLEAN430); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBOOLEAN"


    // $ANTLR start "ruleBOOLEAN"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:239:1: ruleBOOLEAN : ( ( rule__BOOLEAN__Group__0 ) ) ;
    public final void ruleBOOLEAN() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:243:2: ( ( ( rule__BOOLEAN__Group__0 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:244:1: ( ( rule__BOOLEAN__Group__0 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:244:1: ( ( rule__BOOLEAN__Group__0 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:245:1: ( rule__BOOLEAN__Group__0 )
            {
             before(grammarAccess.getBOOLEANAccess().getGroup()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:246:1: ( rule__BOOLEAN__Group__0 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:246:2: rule__BOOLEAN__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__BOOLEAN__Group__0_in_ruleBOOLEAN456);
            rule__BOOLEAN__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBOOLEANAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBOOLEAN"


    // $ANTLR start "entryRuleINTEGER"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:258:1: entryRuleINTEGER : ruleINTEGER EOF ;
    public final void entryRuleINTEGER() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:259:1: ( ruleINTEGER EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:260:1: ruleINTEGER EOF
            {
             before(grammarAccess.getINTEGERRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleINTEGER_in_entryRuleINTEGER483);
            ruleINTEGER();

            state._fsp--;

             after(grammarAccess.getINTEGERRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleINTEGER490); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleINTEGER"


    // $ANTLR start "ruleINTEGER"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:267:1: ruleINTEGER : ( ( rule__INTEGER__Group__0 ) ) ;
    public final void ruleINTEGER() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:271:2: ( ( ( rule__INTEGER__Group__0 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:272:1: ( ( rule__INTEGER__Group__0 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:272:1: ( ( rule__INTEGER__Group__0 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:273:1: ( rule__INTEGER__Group__0 )
            {
             before(grammarAccess.getINTEGERAccess().getGroup()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:274:1: ( rule__INTEGER__Group__0 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:274:2: rule__INTEGER__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__INTEGER__Group__0_in_ruleINTEGER516);
            rule__INTEGER__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getINTEGERAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleINTEGER"


    // $ANTLR start "entryRuleENUM"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:286:1: entryRuleENUM : ruleENUM EOF ;
    public final void entryRuleENUM() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:287:1: ( ruleENUM EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:288:1: ruleENUM EOF
            {
             before(grammarAccess.getENUMRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleENUM_in_entryRuleENUM543);
            ruleENUM();

            state._fsp--;

             after(grammarAccess.getENUMRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleENUM550); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleENUM"


    // $ANTLR start "ruleENUM"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:295:1: ruleENUM : ( ( rule__ENUM__Group__0 ) ) ;
    public final void ruleENUM() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:299:2: ( ( ( rule__ENUM__Group__0 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:300:1: ( ( rule__ENUM__Group__0 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:300:1: ( ( rule__ENUM__Group__0 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:301:1: ( rule__ENUM__Group__0 )
            {
             before(grammarAccess.getENUMAccess().getGroup()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:302:1: ( rule__ENUM__Group__0 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:302:2: rule__ENUM__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__ENUM__Group__0_in_ruleENUM576);
            rule__ENUM__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getENUMAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleENUM"


    // $ANTLR start "entryRuleEnumValue"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:314:1: entryRuleEnumValue : ruleEnumValue EOF ;
    public final void entryRuleEnumValue() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:315:1: ( ruleEnumValue EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:316:1: ruleEnumValue EOF
            {
             before(grammarAccess.getEnumValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEnumValue_in_entryRuleEnumValue603);
            ruleEnumValue();

            state._fsp--;

             after(grammarAccess.getEnumValueRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEnumValue610); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEnumValue"


    // $ANTLR start "ruleEnumValue"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:323:1: ruleEnumValue : ( ( rule__EnumValue__Group__0 ) ) ;
    public final void ruleEnumValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:327:2: ( ( ( rule__EnumValue__Group__0 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:328:1: ( ( rule__EnumValue__Group__0 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:328:1: ( ( rule__EnumValue__Group__0 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:329:1: ( rule__EnumValue__Group__0 )
            {
             before(grammarAccess.getEnumValueAccess().getGroup()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:330:1: ( rule__EnumValue__Group__0 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:330:2: rule__EnumValue__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__EnumValue__Group__0_in_ruleEnumValue636);
            rule__EnumValue__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEnumValueAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEnumValue"


    // $ANTLR start "entryRuleGroup"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:342:1: entryRuleGroup : ruleGroup EOF ;
    public final void entryRuleGroup() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:343:1: ( ruleGroup EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:344:1: ruleGroup EOF
            {
             before(grammarAccess.getGroupRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleGroup_in_entryRuleGroup663);
            ruleGroup();

            state._fsp--;

             after(grammarAccess.getGroupRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleGroup670); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGroup"


    // $ANTLR start "ruleGroup"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:351:1: ruleGroup : ( ( rule__Group__Group__0 ) ) ;
    public final void ruleGroup() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:355:2: ( ( ( rule__Group__Group__0 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:356:1: ( ( rule__Group__Group__0 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:356:1: ( ( rule__Group__Group__0 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:357:1: ( rule__Group__Group__0 )
            {
             before(grammarAccess.getGroupAccess().getGroup()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:358:1: ( rule__Group__Group__0 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:358:2: rule__Group__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__Group__0_in_ruleGroup696);
            rule__Group__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getGroupAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGroup"


    // $ANTLR start "entryRuleEInt"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:370:1: entryRuleEInt : ruleEInt EOF ;
    public final void entryRuleEInt() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:371:1: ( ruleEInt EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:372:1: ruleEInt EOF
            {
             before(grammarAccess.getEIntRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEInt_in_entryRuleEInt723);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getEIntRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEInt730); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:379:1: ruleEInt : ( RULE_INT ) ;
    public final void ruleEInt() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:383:2: ( ( RULE_INT ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:384:1: ( RULE_INT )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:384:1: ( RULE_INT )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:385:1: RULE_INT
            {
             before(grammarAccess.getEIntAccess().getINTTerminalRuleCall()); 
            match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_ruleEInt756); 
             after(grammarAccess.getEIntAccess().getINTTerminalRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "entryRuleUnary"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:398:1: entryRuleUnary : ruleUnary EOF ;
    public final void entryRuleUnary() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:399:1: ( ruleUnary EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:400:1: ruleUnary EOF
            {
             before(grammarAccess.getUnaryRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleUnary_in_entryRuleUnary782);
            ruleUnary();

            state._fsp--;

             after(grammarAccess.getUnaryRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleUnary789); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUnary"


    // $ANTLR start "ruleUnary"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:407:1: ruleUnary : ( ( rule__Unary__Group__0 ) ) ;
    public final void ruleUnary() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:411:2: ( ( ( rule__Unary__Group__0 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:412:1: ( ( rule__Unary__Group__0 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:412:1: ( ( rule__Unary__Group__0 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:413:1: ( rule__Unary__Group__0 )
            {
             before(grammarAccess.getUnaryAccess().getGroup()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:414:1: ( rule__Unary__Group__0 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:414:2: rule__Unary__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Unary__Group__0_in_ruleUnary815);
            rule__Unary__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getUnaryAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUnary"


    // $ANTLR start "entryRuleBinary"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:426:1: entryRuleBinary : ruleBinary EOF ;
    public final void entryRuleBinary() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:427:1: ( ruleBinary EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:428:1: ruleBinary EOF
            {
             before(grammarAccess.getBinaryRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleBinary_in_entryRuleBinary842);
            ruleBinary();

            state._fsp--;

             after(grammarAccess.getBinaryRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleBinary849); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBinary"


    // $ANTLR start "ruleBinary"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:435:1: ruleBinary : ( ( rule__Binary__Group__0 ) ) ;
    public final void ruleBinary() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:439:2: ( ( ( rule__Binary__Group__0 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:440:1: ( ( rule__Binary__Group__0 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:440:1: ( ( rule__Binary__Group__0 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:441:1: ( rule__Binary__Group__0 )
            {
             before(grammarAccess.getBinaryAccess().getGroup()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:442:1: ( rule__Binary__Group__0 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:442:2: rule__Binary__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Binary__Group__0_in_ruleBinary875);
            rule__Binary__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBinaryAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBinary"


    // $ANTLR start "entryRuleAtomicExpression"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:454:1: entryRuleAtomicExpression : ruleAtomicExpression EOF ;
    public final void entryRuleAtomicExpression() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:455:1: ( ruleAtomicExpression EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:456:1: ruleAtomicExpression EOF
            {
             before(grammarAccess.getAtomicExpressionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAtomicExpression_in_entryRuleAtomicExpression902);
            ruleAtomicExpression();

            state._fsp--;

             after(grammarAccess.getAtomicExpressionRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAtomicExpression909); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAtomicExpression"


    // $ANTLR start "ruleAtomicExpression"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:463:1: ruleAtomicExpression : ( ( rule__AtomicExpression__Alternatives ) ) ;
    public final void ruleAtomicExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:467:2: ( ( ( rule__AtomicExpression__Alternatives ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:468:1: ( ( rule__AtomicExpression__Alternatives ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:468:1: ( ( rule__AtomicExpression__Alternatives ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:469:1: ( rule__AtomicExpression__Alternatives )
            {
             before(grammarAccess.getAtomicExpressionAccess().getAlternatives()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:470:1: ( rule__AtomicExpression__Alternatives )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:470:2: rule__AtomicExpression__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__AtomicExpression__Alternatives_in_ruleAtomicExpression935);
            rule__AtomicExpression__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAtomicExpressionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAtomicExpression"


    // $ANTLR start "entryRuleConstants"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:482:1: entryRuleConstants : ruleConstants EOF ;
    public final void entryRuleConstants() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:483:1: ( ruleConstants EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:484:1: ruleConstants EOF
            {
             before(grammarAccess.getConstantsRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleConstants_in_entryRuleConstants962);
            ruleConstants();

            state._fsp--;

             after(grammarAccess.getConstantsRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleConstants969); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstants"


    // $ANTLR start "ruleConstants"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:491:1: ruleConstants : ( ( rule__Constants__Alternatives ) ) ;
    public final void ruleConstants() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:495:2: ( ( ( rule__Constants__Alternatives ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:496:1: ( ( rule__Constants__Alternatives ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:496:1: ( ( rule__Constants__Alternatives ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:497:1: ( rule__Constants__Alternatives )
            {
             before(grammarAccess.getConstantsAccess().getAlternatives()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:498:1: ( rule__Constants__Alternatives )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:498:2: rule__Constants__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__Constants__Alternatives_in_ruleConstants995);
            rule__Constants__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getConstantsAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstants"


    // $ANTLR start "entryRuleIntConst"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:510:1: entryRuleIntConst : ruleIntConst EOF ;
    public final void entryRuleIntConst() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:511:1: ( ruleIntConst EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:512:1: ruleIntConst EOF
            {
             before(grammarAccess.getIntConstRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleIntConst_in_entryRuleIntConst1022);
            ruleIntConst();

            state._fsp--;

             after(grammarAccess.getIntConstRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleIntConst1029); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntConst"


    // $ANTLR start "ruleIntConst"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:519:1: ruleIntConst : ( ( rule__IntConst__ValueAssignment ) ) ;
    public final void ruleIntConst() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:523:2: ( ( ( rule__IntConst__ValueAssignment ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:524:1: ( ( rule__IntConst__ValueAssignment ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:524:1: ( ( rule__IntConst__ValueAssignment ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:525:1: ( rule__IntConst__ValueAssignment )
            {
             before(grammarAccess.getIntConstAccess().getValueAssignment()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:526:1: ( rule__IntConst__ValueAssignment )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:526:2: rule__IntConst__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__IntConst__ValueAssignment_in_ruleIntConst1055);
            rule__IntConst__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getIntConstAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntConst"


    // $ANTLR start "entryRuleStringConst"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:538:1: entryRuleStringConst : ruleStringConst EOF ;
    public final void entryRuleStringConst() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:539:1: ( ruleStringConst EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:540:1: ruleStringConst EOF
            {
             before(grammarAccess.getStringConstRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleStringConst_in_entryRuleStringConst1082);
            ruleStringConst();

            state._fsp--;

             after(grammarAccess.getStringConstRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleStringConst1089); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStringConst"


    // $ANTLR start "ruleStringConst"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:547:1: ruleStringConst : ( ( rule__StringConst__Group__0 ) ) ;
    public final void ruleStringConst() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:551:2: ( ( ( rule__StringConst__Group__0 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:552:1: ( ( rule__StringConst__Group__0 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:552:1: ( ( rule__StringConst__Group__0 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:553:1: ( rule__StringConst__Group__0 )
            {
             before(grammarAccess.getStringConstAccess().getGroup()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:554:1: ( rule__StringConst__Group__0 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:554:2: rule__StringConst__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__StringConst__Group__0_in_ruleStringConst1115);
            rule__StringConst__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStringConstAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStringConst"


    // $ANTLR start "entryRuleBoolConst"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:566:1: entryRuleBoolConst : ruleBoolConst EOF ;
    public final void entryRuleBoolConst() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:567:1: ( ruleBoolConst EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:568:1: ruleBoolConst EOF
            {
             before(grammarAccess.getBoolConstRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleBoolConst_in_entryRuleBoolConst1142);
            ruleBoolConst();

            state._fsp--;

             after(grammarAccess.getBoolConstRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleBoolConst1149); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBoolConst"


    // $ANTLR start "ruleBoolConst"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:575:1: ruleBoolConst : ( ( rule__BoolConst__ValueAssignment ) ) ;
    public final void ruleBoolConst() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:579:2: ( ( ( rule__BoolConst__ValueAssignment ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:580:1: ( ( rule__BoolConst__ValueAssignment ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:580:1: ( ( rule__BoolConst__ValueAssignment ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:581:1: ( rule__BoolConst__ValueAssignment )
            {
             before(grammarAccess.getBoolConstAccess().getValueAssignment()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:582:1: ( rule__BoolConst__ValueAssignment )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:582:2: rule__BoolConst__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__BoolConst__ValueAssignment_in_ruleBoolConst1175);
            rule__BoolConst__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getBoolConstAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBoolConst"


    // $ANTLR start "entryRuleIdRef"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:594:1: entryRuleIdRef : ruleIdRef EOF ;
    public final void entryRuleIdRef() throws RecognitionException {
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:595:1: ( ruleIdRef EOF )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:596:1: ruleIdRef EOF
            {
             before(grammarAccess.getIdRefRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleIdRef_in_entryRuleIdRef1202);
            ruleIdRef();

            state._fsp--;

             after(grammarAccess.getIdRefRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleIdRef1209); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIdRef"


    // $ANTLR start "ruleIdRef"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:603:1: ruleIdRef : ( ( rule__IdRef__IdentifierAssignment ) ) ;
    public final void ruleIdRef() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:607:2: ( ( ( rule__IdRef__IdentifierAssignment ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:608:1: ( ( rule__IdRef__IdentifierAssignment ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:608:1: ( ( rule__IdRef__IdentifierAssignment ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:609:1: ( rule__IdRef__IdentifierAssignment )
            {
             before(grammarAccess.getIdRefAccess().getIdentifierAssignment()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:610:1: ( rule__IdRef__IdentifierAssignment )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:610:2: rule__IdRef__IdentifierAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__IdRef__IdentifierAssignment_in_ruleIdRef1235);
            rule__IdRef__IdentifierAssignment();

            state._fsp--;


            }

             after(grammarAccess.getIdRefAccess().getIdentifierAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIdRef"


    // $ANTLR start "ruleUnaryOperator"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:623:1: ruleUnaryOperator : ( ( rule__UnaryOperator__Alternatives ) ) ;
    public final void ruleUnaryOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:627:1: ( ( ( rule__UnaryOperator__Alternatives ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:628:1: ( ( rule__UnaryOperator__Alternatives ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:628:1: ( ( rule__UnaryOperator__Alternatives ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:629:1: ( rule__UnaryOperator__Alternatives )
            {
             before(grammarAccess.getUnaryOperatorAccess().getAlternatives()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:630:1: ( rule__UnaryOperator__Alternatives )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:630:2: rule__UnaryOperator__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__UnaryOperator__Alternatives_in_ruleUnaryOperator1272);
            rule__UnaryOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getUnaryOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUnaryOperator"


    // $ANTLR start "ruleBinaryOperator"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:642:1: ruleBinaryOperator : ( ( rule__BinaryOperator__Alternatives ) ) ;
    public final void ruleBinaryOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:646:1: ( ( ( rule__BinaryOperator__Alternatives ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:647:1: ( ( rule__BinaryOperator__Alternatives ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:647:1: ( ( rule__BinaryOperator__Alternatives ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:648:1: ( rule__BinaryOperator__Alternatives )
            {
             before(grammarAccess.getBinaryOperatorAccess().getAlternatives()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:649:1: ( rule__BinaryOperator__Alternatives )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:649:2: rule__BinaryOperator__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__BinaryOperator__Alternatives_in_ruleBinaryOperator1308);
            rule__BinaryOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBinaryOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBinaryOperator"


    // $ANTLR start "ruleBooolean"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:661:1: ruleBooolean : ( ( rule__Booolean__Alternatives ) ) ;
    public final void ruleBooolean() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:665:1: ( ( ( rule__Booolean__Alternatives ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:666:1: ( ( rule__Booolean__Alternatives ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:666:1: ( ( rule__Booolean__Alternatives ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:667:1: ( rule__Booolean__Alternatives )
            {
             before(grammarAccess.getBoooleanAccess().getAlternatives()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:668:1: ( rule__Booolean__Alternatives )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:668:2: rule__Booolean__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__Booolean__Alternatives_in_ruleBooolean1344);
            rule__Booolean__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBoooleanAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooolean"


    // $ANTLR start "rule__Type__Alternatives"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:679:1: rule__Type__Alternatives : ( ( ruleBOOLEAN ) | ( ruleINTEGER ) | ( ruleENUM ) );
    public final void rule__Type__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:683:1: ( ( ruleBOOLEAN ) | ( ruleINTEGER ) | ( ruleENUM ) )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 35:
                {
                alt1=1;
                }
                break;
            case 36:
                {
                alt1=2;
                }
                break;
            case RULE_STRING:
            case RULE_ID:
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:684:1: ( ruleBOOLEAN )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:684:1: ( ruleBOOLEAN )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:685:1: ruleBOOLEAN
                    {
                     before(grammarAccess.getTypeAccess().getBOOLEANParserRuleCall_0()); 
                    pushFollow(FollowSets000.FOLLOW_ruleBOOLEAN_in_rule__Type__Alternatives1379);
                    ruleBOOLEAN();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getBOOLEANParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:690:6: ( ruleINTEGER )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:690:6: ( ruleINTEGER )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:691:1: ruleINTEGER
                    {
                     before(grammarAccess.getTypeAccess().getINTEGERParserRuleCall_1()); 
                    pushFollow(FollowSets000.FOLLOW_ruleINTEGER_in_rule__Type__Alternatives1396);
                    ruleINTEGER();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getINTEGERParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:696:6: ( ruleENUM )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:696:6: ( ruleENUM )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:697:1: ruleENUM
                    {
                     before(grammarAccess.getTypeAccess().getENUMParserRuleCall_2()); 
                    pushFollow(FollowSets000.FOLLOW_ruleENUM_in_rule__Type__Alternatives1413);
                    ruleENUM();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getENUMParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Alternatives"


    // $ANTLR start "rule__Expression__Alternatives"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:707:1: rule__Expression__Alternatives : ( ( ruleUnary ) | ( ruleBinary ) | ( ruleIdRef ) | ( ruleIntConst ) | ( ruleBoolConst ) | ( ruleStringConst ) );
    public final void rule__Expression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:711:1: ( ( ruleUnary ) | ( ruleBinary ) | ( ruleIdRef ) | ( ruleIntConst ) | ( ruleBoolConst ) | ( ruleStringConst ) )
            int alt2=6;
            alt2 = dfa2.predict(input);
            switch (alt2) {
                case 1 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:712:1: ( ruleUnary )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:712:1: ( ruleUnary )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:713:1: ruleUnary
                    {
                     before(grammarAccess.getExpressionAccess().getUnaryParserRuleCall_0()); 
                    pushFollow(FollowSets000.FOLLOW_ruleUnary_in_rule__Expression__Alternatives1445);
                    ruleUnary();

                    state._fsp--;

                     after(grammarAccess.getExpressionAccess().getUnaryParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:718:6: ( ruleBinary )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:718:6: ( ruleBinary )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:719:1: ruleBinary
                    {
                     before(grammarAccess.getExpressionAccess().getBinaryParserRuleCall_1()); 
                    pushFollow(FollowSets000.FOLLOW_ruleBinary_in_rule__Expression__Alternatives1462);
                    ruleBinary();

                    state._fsp--;

                     after(grammarAccess.getExpressionAccess().getBinaryParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:724:6: ( ruleIdRef )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:724:6: ( ruleIdRef )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:725:1: ruleIdRef
                    {
                     before(grammarAccess.getExpressionAccess().getIdRefParserRuleCall_2()); 
                    pushFollow(FollowSets000.FOLLOW_ruleIdRef_in_rule__Expression__Alternatives1479);
                    ruleIdRef();

                    state._fsp--;

                     after(grammarAccess.getExpressionAccess().getIdRefParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:730:6: ( ruleIntConst )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:730:6: ( ruleIntConst )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:731:1: ruleIntConst
                    {
                     before(grammarAccess.getExpressionAccess().getIntConstParserRuleCall_3()); 
                    pushFollow(FollowSets000.FOLLOW_ruleIntConst_in_rule__Expression__Alternatives1496);
                    ruleIntConst();

                    state._fsp--;

                     after(grammarAccess.getExpressionAccess().getIntConstParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:736:6: ( ruleBoolConst )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:736:6: ( ruleBoolConst )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:737:1: ruleBoolConst
                    {
                     before(grammarAccess.getExpressionAccess().getBoolConstParserRuleCall_4()); 
                    pushFollow(FollowSets000.FOLLOW_ruleBoolConst_in_rule__Expression__Alternatives1513);
                    ruleBoolConst();

                    state._fsp--;

                     after(grammarAccess.getExpressionAccess().getBoolConstParserRuleCall_4()); 

                    }


                    }
                    break;
                case 6 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:742:6: ( ruleStringConst )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:742:6: ( ruleStringConst )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:743:1: ruleStringConst
                    {
                     before(grammarAccess.getExpressionAccess().getStringConstParserRuleCall_5()); 
                    pushFollow(FollowSets000.FOLLOW_ruleStringConst_in_rule__Expression__Alternatives1530);
                    ruleStringConst();

                    state._fsp--;

                     after(grammarAccess.getExpressionAccess().getStringConstParserRuleCall_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Alternatives"


    // $ANTLR start "rule__EString__Alternatives"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:754:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:758:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_STRING) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_ID) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:759:1: ( RULE_STRING )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:759:1: ( RULE_STRING )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:760:1: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__EString__Alternatives1563); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:765:6: ( RULE_ID )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:765:6: ( RULE_ID )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:766:1: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__EString__Alternatives1580); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__AtomicExpression__Alternatives"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:776:1: rule__AtomicExpression__Alternatives : ( ( ruleIdRef ) | ( ruleConstants ) | ( ( rule__AtomicExpression__Group_2__0 ) ) );
    public final void rule__AtomicExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:780:1: ( ( ruleIdRef ) | ( ruleConstants ) | ( ( rule__AtomicExpression__Group_2__0 ) ) )
            int alt4=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
            case RULE_ID:
                {
                alt4=1;
                }
                break;
            case RULE_INT:
            case 24:
            case 25:
            case 32:
                {
                alt4=2;
                }
                break;
            case 38:
                {
                alt4=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:781:1: ( ruleIdRef )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:781:1: ( ruleIdRef )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:782:1: ruleIdRef
                    {
                     before(grammarAccess.getAtomicExpressionAccess().getIdRefParserRuleCall_0()); 
                    pushFollow(FollowSets000.FOLLOW_ruleIdRef_in_rule__AtomicExpression__Alternatives1612);
                    ruleIdRef();

                    state._fsp--;

                     after(grammarAccess.getAtomicExpressionAccess().getIdRefParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:787:6: ( ruleConstants )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:787:6: ( ruleConstants )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:788:1: ruleConstants
                    {
                     before(grammarAccess.getAtomicExpressionAccess().getConstantsParserRuleCall_1()); 
                    pushFollow(FollowSets000.FOLLOW_ruleConstants_in_rule__AtomicExpression__Alternatives1629);
                    ruleConstants();

                    state._fsp--;

                     after(grammarAccess.getAtomicExpressionAccess().getConstantsParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:793:6: ( ( rule__AtomicExpression__Group_2__0 ) )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:793:6: ( ( rule__AtomicExpression__Group_2__0 ) )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:794:1: ( rule__AtomicExpression__Group_2__0 )
                    {
                     before(grammarAccess.getAtomicExpressionAccess().getGroup_2()); 
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:795:1: ( rule__AtomicExpression__Group_2__0 )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:795:2: rule__AtomicExpression__Group_2__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__AtomicExpression__Group_2__0_in_rule__AtomicExpression__Alternatives1646);
                    rule__AtomicExpression__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicExpressionAccess().getGroup_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicExpression__Alternatives"


    // $ANTLR start "rule__Constants__Alternatives"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:804:1: rule__Constants__Alternatives : ( ( ruleIntConst ) | ( ruleBoolConst ) | ( ruleStringConst ) );
    public final void rule__Constants__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:808:1: ( ( ruleIntConst ) | ( ruleBoolConst ) | ( ruleStringConst ) )
            int alt5=3;
            switch ( input.LA(1) ) {
            case RULE_INT:
                {
                alt5=1;
                }
                break;
            case 24:
            case 25:
                {
                alt5=2;
                }
                break;
            case 32:
                {
                alt5=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:809:1: ( ruleIntConst )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:809:1: ( ruleIntConst )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:810:1: ruleIntConst
                    {
                     before(grammarAccess.getConstantsAccess().getIntConstParserRuleCall_0()); 
                    pushFollow(FollowSets000.FOLLOW_ruleIntConst_in_rule__Constants__Alternatives1679);
                    ruleIntConst();

                    state._fsp--;

                     after(grammarAccess.getConstantsAccess().getIntConstParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:815:6: ( ruleBoolConst )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:815:6: ( ruleBoolConst )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:816:1: ruleBoolConst
                    {
                     before(grammarAccess.getConstantsAccess().getBoolConstParserRuleCall_1()); 
                    pushFollow(FollowSets000.FOLLOW_ruleBoolConst_in_rule__Constants__Alternatives1696);
                    ruleBoolConst();

                    state._fsp--;

                     after(grammarAccess.getConstantsAccess().getBoolConstParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:821:6: ( ruleStringConst )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:821:6: ( ruleStringConst )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:822:1: ruleStringConst
                    {
                     before(grammarAccess.getConstantsAccess().getStringConstParserRuleCall_2()); 
                    pushFollow(FollowSets000.FOLLOW_ruleStringConst_in_rule__Constants__Alternatives1713);
                    ruleStringConst();

                    state._fsp--;

                     after(grammarAccess.getConstantsAccess().getStringConstParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constants__Alternatives"


    // $ANTLR start "rule__UnaryOperator__Alternatives"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:832:1: rule__UnaryOperator__Alternatives : ( ( ( 'not' ) ) | ( ( '-' ) ) | ( ( '+' ) ) );
    public final void rule__UnaryOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:836:1: ( ( ( 'not' ) ) | ( ( '-' ) ) | ( ( '+' ) ) )
            int alt6=3;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt6=1;
                }
                break;
            case 12:
                {
                alt6=2;
                }
                break;
            case 13:
                {
                alt6=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:837:1: ( ( 'not' ) )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:837:1: ( ( 'not' ) )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:838:1: ( 'not' )
                    {
                     before(grammarAccess.getUnaryOperatorAccess().getNOTEnumLiteralDeclaration_0()); 
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:839:1: ( 'not' )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:839:3: 'not'
                    {
                    match(input,11,FollowSets000.FOLLOW_11_in_rule__UnaryOperator__Alternatives1746); 

                    }

                     after(grammarAccess.getUnaryOperatorAccess().getNOTEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:844:6: ( ( '-' ) )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:844:6: ( ( '-' ) )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:845:1: ( '-' )
                    {
                     before(grammarAccess.getUnaryOperatorAccess().getNEGATIVEEnumLiteralDeclaration_1()); 
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:846:1: ( '-' )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:846:3: '-'
                    {
                    match(input,12,FollowSets000.FOLLOW_12_in_rule__UnaryOperator__Alternatives1767); 

                    }

                     after(grammarAccess.getUnaryOperatorAccess().getNEGATIVEEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:851:6: ( ( '+' ) )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:851:6: ( ( '+' ) )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:852:1: ( '+' )
                    {
                     before(grammarAccess.getUnaryOperatorAccess().getPOSITIVEEnumLiteralDeclaration_2()); 
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:853:1: ( '+' )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:853:3: '+'
                    {
                    match(input,13,FollowSets000.FOLLOW_13_in_rule__UnaryOperator__Alternatives1788); 

                    }

                     after(grammarAccess.getUnaryOperatorAccess().getPOSITIVEEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryOperator__Alternatives"


    // $ANTLR start "rule__BinaryOperator__Alternatives"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:863:1: rule__BinaryOperator__Alternatives : ( ( ( '+' ) ) | ( ( '-' ) ) | ( ( '*' ) ) | ( ( '/' ) ) | ( ( '==' ) ) | ( ( '!=' ) ) | ( ( ' and ' ) ) | ( ( ' or ' ) ) | ( ( '>' ) ) | ( ( '>=' ) ) | ( ( '<' ) ) | ( ( '<=' ) ) );
    public final void rule__BinaryOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:867:1: ( ( ( '+' ) ) | ( ( '-' ) ) | ( ( '*' ) ) | ( ( '/' ) ) | ( ( '==' ) ) | ( ( '!=' ) ) | ( ( ' and ' ) ) | ( ( ' or ' ) ) | ( ( '>' ) ) | ( ( '>=' ) ) | ( ( '<' ) ) | ( ( '<=' ) ) )
            int alt7=12;
            switch ( input.LA(1) ) {
            case 13:
                {
                alt7=1;
                }
                break;
            case 12:
                {
                alt7=2;
                }
                break;
            case 14:
                {
                alt7=3;
                }
                break;
            case 15:
                {
                alt7=4;
                }
                break;
            case 16:
                {
                alt7=5;
                }
                break;
            case 17:
                {
                alt7=6;
                }
                break;
            case 18:
                {
                alt7=7;
                }
                break;
            case 19:
                {
                alt7=8;
                }
                break;
            case 20:
                {
                alt7=9;
                }
                break;
            case 21:
                {
                alt7=10;
                }
                break;
            case 22:
                {
                alt7=11;
                }
                break;
            case 23:
                {
                alt7=12;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:868:1: ( ( '+' ) )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:868:1: ( ( '+' ) )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:869:1: ( '+' )
                    {
                     before(grammarAccess.getBinaryOperatorAccess().getADDEnumLiteralDeclaration_0()); 
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:870:1: ( '+' )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:870:3: '+'
                    {
                    match(input,13,FollowSets000.FOLLOW_13_in_rule__BinaryOperator__Alternatives1824); 

                    }

                     after(grammarAccess.getBinaryOperatorAccess().getADDEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:875:6: ( ( '-' ) )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:875:6: ( ( '-' ) )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:876:1: ( '-' )
                    {
                     before(grammarAccess.getBinaryOperatorAccess().getMINUSEnumLiteralDeclaration_1()); 
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:877:1: ( '-' )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:877:3: '-'
                    {
                    match(input,12,FollowSets000.FOLLOW_12_in_rule__BinaryOperator__Alternatives1845); 

                    }

                     after(grammarAccess.getBinaryOperatorAccess().getMINUSEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:882:6: ( ( '*' ) )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:882:6: ( ( '*' ) )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:883:1: ( '*' )
                    {
                     before(grammarAccess.getBinaryOperatorAccess().getMULTIPLYEnumLiteralDeclaration_2()); 
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:884:1: ( '*' )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:884:3: '*'
                    {
                    match(input,14,FollowSets000.FOLLOW_14_in_rule__BinaryOperator__Alternatives1866); 

                    }

                     after(grammarAccess.getBinaryOperatorAccess().getMULTIPLYEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:889:6: ( ( '/' ) )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:889:6: ( ( '/' ) )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:890:1: ( '/' )
                    {
                     before(grammarAccess.getBinaryOperatorAccess().getDIVIDEEnumLiteralDeclaration_3()); 
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:891:1: ( '/' )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:891:3: '/'
                    {
                    match(input,15,FollowSets000.FOLLOW_15_in_rule__BinaryOperator__Alternatives1887); 

                    }

                     after(grammarAccess.getBinaryOperatorAccess().getDIVIDEEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:896:6: ( ( '==' ) )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:896:6: ( ( '==' ) )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:897:1: ( '==' )
                    {
                     before(grammarAccess.getBinaryOperatorAccess().getEQUALEnumLiteralDeclaration_4()); 
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:898:1: ( '==' )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:898:3: '=='
                    {
                    match(input,16,FollowSets000.FOLLOW_16_in_rule__BinaryOperator__Alternatives1908); 

                    }

                     after(grammarAccess.getBinaryOperatorAccess().getEQUALEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:903:6: ( ( '!=' ) )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:903:6: ( ( '!=' ) )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:904:1: ( '!=' )
                    {
                     before(grammarAccess.getBinaryOperatorAccess().getDIFFERENTEnumLiteralDeclaration_5()); 
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:905:1: ( '!=' )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:905:3: '!='
                    {
                    match(input,17,FollowSets000.FOLLOW_17_in_rule__BinaryOperator__Alternatives1929); 

                    }

                     after(grammarAccess.getBinaryOperatorAccess().getDIFFERENTEnumLiteralDeclaration_5()); 

                    }


                    }
                    break;
                case 7 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:910:6: ( ( ' and ' ) )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:910:6: ( ( ' and ' ) )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:911:1: ( ' and ' )
                    {
                     before(grammarAccess.getBinaryOperatorAccess().getANDEnumLiteralDeclaration_6()); 
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:912:1: ( ' and ' )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:912:3: ' and '
                    {
                    match(input,18,FollowSets000.FOLLOW_18_in_rule__BinaryOperator__Alternatives1950); 

                    }

                     after(grammarAccess.getBinaryOperatorAccess().getANDEnumLiteralDeclaration_6()); 

                    }


                    }
                    break;
                case 8 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:917:6: ( ( ' or ' ) )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:917:6: ( ( ' or ' ) )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:918:1: ( ' or ' )
                    {
                     before(grammarAccess.getBinaryOperatorAccess().getOREnumLiteralDeclaration_7()); 
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:919:1: ( ' or ' )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:919:3: ' or '
                    {
                    match(input,19,FollowSets000.FOLLOW_19_in_rule__BinaryOperator__Alternatives1971); 

                    }

                     after(grammarAccess.getBinaryOperatorAccess().getOREnumLiteralDeclaration_7()); 

                    }


                    }
                    break;
                case 9 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:924:6: ( ( '>' ) )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:924:6: ( ( '>' ) )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:925:1: ( '>' )
                    {
                     before(grammarAccess.getBinaryOperatorAccess().getGREATERTHANEnumLiteralDeclaration_8()); 
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:926:1: ( '>' )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:926:3: '>'
                    {
                    match(input,20,FollowSets000.FOLLOW_20_in_rule__BinaryOperator__Alternatives1992); 

                    }

                     after(grammarAccess.getBinaryOperatorAccess().getGREATERTHANEnumLiteralDeclaration_8()); 

                    }


                    }
                    break;
                case 10 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:931:6: ( ( '>=' ) )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:931:6: ( ( '>=' ) )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:932:1: ( '>=' )
                    {
                     before(grammarAccess.getBinaryOperatorAccess().getGREATERTHANOREQUALEnumLiteralDeclaration_9()); 
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:933:1: ( '>=' )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:933:3: '>='
                    {
                    match(input,21,FollowSets000.FOLLOW_21_in_rule__BinaryOperator__Alternatives2013); 

                    }

                     after(grammarAccess.getBinaryOperatorAccess().getGREATERTHANOREQUALEnumLiteralDeclaration_9()); 

                    }


                    }
                    break;
                case 11 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:938:6: ( ( '<' ) )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:938:6: ( ( '<' ) )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:939:1: ( '<' )
                    {
                     before(grammarAccess.getBinaryOperatorAccess().getLESSTHANEnumLiteralDeclaration_10()); 
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:940:1: ( '<' )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:940:3: '<'
                    {
                    match(input,22,FollowSets000.FOLLOW_22_in_rule__BinaryOperator__Alternatives2034); 

                    }

                     after(grammarAccess.getBinaryOperatorAccess().getLESSTHANEnumLiteralDeclaration_10()); 

                    }


                    }
                    break;
                case 12 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:945:6: ( ( '<=' ) )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:945:6: ( ( '<=' ) )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:946:1: ( '<=' )
                    {
                     before(grammarAccess.getBinaryOperatorAccess().getLESSTHANOREQUALEnumLiteralDeclaration_11()); 
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:947:1: ( '<=' )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:947:3: '<='
                    {
                    match(input,23,FollowSets000.FOLLOW_23_in_rule__BinaryOperator__Alternatives2055); 

                    }

                     after(grammarAccess.getBinaryOperatorAccess().getLESSTHANOREQUALEnumLiteralDeclaration_11()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryOperator__Alternatives"


    // $ANTLR start "rule__Booolean__Alternatives"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:957:1: rule__Booolean__Alternatives : ( ( ( 'true' ) ) | ( ( 'false' ) ) );
    public final void rule__Booolean__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:961:1: ( ( ( 'true' ) ) | ( ( 'false' ) ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==24) ) {
                alt8=1;
            }
            else if ( (LA8_0==25) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:962:1: ( ( 'true' ) )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:962:1: ( ( 'true' ) )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:963:1: ( 'true' )
                    {
                     before(grammarAccess.getBoooleanAccess().getTrueEnumLiteralDeclaration_0()); 
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:964:1: ( 'true' )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:964:3: 'true'
                    {
                    match(input,24,FollowSets000.FOLLOW_24_in_rule__Booolean__Alternatives2091); 

                    }

                     after(grammarAccess.getBoooleanAccess().getTrueEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:969:6: ( ( 'false' ) )
                    {
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:969:6: ( ( 'false' ) )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:970:1: ( 'false' )
                    {
                     before(grammarAccess.getBoooleanAccess().getFalseEnumLiteralDeclaration_1()); 
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:971:1: ( 'false' )
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:971:3: 'false'
                    {
                    match(input,25,FollowSets000.FOLLOW_25_in_rule__Booolean__Alternatives2112); 

                    }

                     after(grammarAccess.getBoooleanAccess().getFalseEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Booolean__Alternatives"


    // $ANTLR start "rule__Configuration__Group__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:983:1: rule__Configuration__Group__0 : rule__Configuration__Group__0__Impl rule__Configuration__Group__1 ;
    public final void rule__Configuration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:987:1: ( rule__Configuration__Group__0__Impl rule__Configuration__Group__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:988:2: rule__Configuration__Group__0__Impl rule__Configuration__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group__0__Impl_in_rule__Configuration__Group__02145);
            rule__Configuration__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group__1_in_rule__Configuration__Group__02148);
            rule__Configuration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__0"


    // $ANTLR start "rule__Configuration__Group__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:995:1: rule__Configuration__Group__0__Impl : ( () ) ;
    public final void rule__Configuration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:999:1: ( ( () ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1000:1: ( () )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1000:1: ( () )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1001:1: ()
            {
             before(grammarAccess.getConfigurationAccess().getConfigurationAction_0()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1002:1: ()
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1004:1: 
            {
            }

             after(grammarAccess.getConfigurationAccess().getConfigurationAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__0__Impl"


    // $ANTLR start "rule__Configuration__Group__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1014:1: rule__Configuration__Group__1 : rule__Configuration__Group__1__Impl rule__Configuration__Group__2 ;
    public final void rule__Configuration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1018:1: ( rule__Configuration__Group__1__Impl rule__Configuration__Group__2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1019:2: rule__Configuration__Group__1__Impl rule__Configuration__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group__1__Impl_in_rule__Configuration__Group__12206);
            rule__Configuration__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group__2_in_rule__Configuration__Group__12209);
            rule__Configuration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__1"


    // $ANTLR start "rule__Configuration__Group__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1026:1: rule__Configuration__Group__1__Impl : ( ( rule__Configuration__NameAssignment_1 ) ) ;
    public final void rule__Configuration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1030:1: ( ( ( rule__Configuration__NameAssignment_1 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1031:1: ( ( rule__Configuration__NameAssignment_1 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1031:1: ( ( rule__Configuration__NameAssignment_1 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1032:1: ( rule__Configuration__NameAssignment_1 )
            {
             before(grammarAccess.getConfigurationAccess().getNameAssignment_1()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1033:1: ( rule__Configuration__NameAssignment_1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1033:2: rule__Configuration__NameAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__NameAssignment_1_in_rule__Configuration__Group__1__Impl2236);
            rule__Configuration__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__1__Impl"


    // $ANTLR start "rule__Configuration__Group__2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1043:1: rule__Configuration__Group__2 : rule__Configuration__Group__2__Impl rule__Configuration__Group__3 ;
    public final void rule__Configuration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1047:1: ( rule__Configuration__Group__2__Impl rule__Configuration__Group__3 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1048:2: rule__Configuration__Group__2__Impl rule__Configuration__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group__2__Impl_in_rule__Configuration__Group__22266);
            rule__Configuration__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group__3_in_rule__Configuration__Group__22269);
            rule__Configuration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__2"


    // $ANTLR start "rule__Configuration__Group__2__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1055:1: rule__Configuration__Group__2__Impl : ( '{' ) ;
    public final void rule__Configuration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1059:1: ( ( '{' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1060:1: ( '{' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1060:1: ( '{' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1061:1: '{'
            {
             before(grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,26,FollowSets000.FOLLOW_26_in_rule__Configuration__Group__2__Impl2297); 
             after(grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__2__Impl"


    // $ANTLR start "rule__Configuration__Group__3"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1074:1: rule__Configuration__Group__3 : rule__Configuration__Group__3__Impl rule__Configuration__Group__4 ;
    public final void rule__Configuration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1078:1: ( rule__Configuration__Group__3__Impl rule__Configuration__Group__4 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1079:2: rule__Configuration__Group__3__Impl rule__Configuration__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group__3__Impl_in_rule__Configuration__Group__32328);
            rule__Configuration__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group__4_in_rule__Configuration__Group__32331);
            rule__Configuration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__3"


    // $ANTLR start "rule__Configuration__Group__3__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1086:1: rule__Configuration__Group__3__Impl : ( ( rule__Configuration__Group_3__0 )? ) ;
    public final void rule__Configuration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1090:1: ( ( ( rule__Configuration__Group_3__0 )? ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1091:1: ( ( rule__Configuration__Group_3__0 )? )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1091:1: ( ( rule__Configuration__Group_3__0 )? )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1092:1: ( rule__Configuration__Group_3__0 )?
            {
             before(grammarAccess.getConfigurationAccess().getGroup_3()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1093:1: ( rule__Configuration__Group_3__0 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==28) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1093:2: rule__Configuration__Group_3__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_3__0_in_rule__Configuration__Group__3__Impl2358);
                    rule__Configuration__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getConfigurationAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__3__Impl"


    // $ANTLR start "rule__Configuration__Group__4"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1103:1: rule__Configuration__Group__4 : rule__Configuration__Group__4__Impl rule__Configuration__Group__5 ;
    public final void rule__Configuration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1107:1: ( rule__Configuration__Group__4__Impl rule__Configuration__Group__5 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1108:2: rule__Configuration__Group__4__Impl rule__Configuration__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group__4__Impl_in_rule__Configuration__Group__42389);
            rule__Configuration__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group__5_in_rule__Configuration__Group__42392);
            rule__Configuration__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__4"


    // $ANTLR start "rule__Configuration__Group__4__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1115:1: rule__Configuration__Group__4__Impl : ( ( rule__Configuration__Group_4__0 )? ) ;
    public final void rule__Configuration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1119:1: ( ( ( rule__Configuration__Group_4__0 )? ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1120:1: ( ( rule__Configuration__Group_4__0 )? )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1120:1: ( ( rule__Configuration__Group_4__0 )? )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1121:1: ( rule__Configuration__Group_4__0 )?
            {
             before(grammarAccess.getConfigurationAccess().getGroup_4()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1122:1: ( rule__Configuration__Group_4__0 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==30) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1122:2: rule__Configuration__Group_4__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_4__0_in_rule__Configuration__Group__4__Impl2419);
                    rule__Configuration__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getConfigurationAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__4__Impl"


    // $ANTLR start "rule__Configuration__Group__5"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1132:1: rule__Configuration__Group__5 : rule__Configuration__Group__5__Impl rule__Configuration__Group__6 ;
    public final void rule__Configuration__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1136:1: ( rule__Configuration__Group__5__Impl rule__Configuration__Group__6 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1137:2: rule__Configuration__Group__5__Impl rule__Configuration__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group__5__Impl_in_rule__Configuration__Group__52450);
            rule__Configuration__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group__6_in_rule__Configuration__Group__52453);
            rule__Configuration__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__5"


    // $ANTLR start "rule__Configuration__Group__5__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1144:1: rule__Configuration__Group__5__Impl : ( ( rule__Configuration__Group_5__0 )? ) ;
    public final void rule__Configuration__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1148:1: ( ( ( rule__Configuration__Group_5__0 )? ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1149:1: ( ( rule__Configuration__Group_5__0 )? )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1149:1: ( ( rule__Configuration__Group_5__0 )? )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1150:1: ( rule__Configuration__Group_5__0 )?
            {
             before(grammarAccess.getConfigurationAccess().getGroup_5()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1151:1: ( rule__Configuration__Group_5__0 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==31) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1151:2: rule__Configuration__Group_5__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_5__0_in_rule__Configuration__Group__5__Impl2480);
                    rule__Configuration__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getConfigurationAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__5__Impl"


    // $ANTLR start "rule__Configuration__Group__6"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1161:1: rule__Configuration__Group__6 : rule__Configuration__Group__6__Impl ;
    public final void rule__Configuration__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1165:1: ( rule__Configuration__Group__6__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1166:2: rule__Configuration__Group__6__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group__6__Impl_in_rule__Configuration__Group__62511);
            rule__Configuration__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__6"


    // $ANTLR start "rule__Configuration__Group__6__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1172:1: rule__Configuration__Group__6__Impl : ( '}' ) ;
    public final void rule__Configuration__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1176:1: ( ( '}' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1177:1: ( '}' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1177:1: ( '}' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1178:1: '}'
            {
             before(grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_6()); 
            match(input,27,FollowSets000.FOLLOW_27_in_rule__Configuration__Group__6__Impl2539); 
             after(grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__6__Impl"


    // $ANTLR start "rule__Configuration__Group_3__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1205:1: rule__Configuration__Group_3__0 : rule__Configuration__Group_3__0__Impl rule__Configuration__Group_3__1 ;
    public final void rule__Configuration__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1209:1: ( rule__Configuration__Group_3__0__Impl rule__Configuration__Group_3__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1210:2: rule__Configuration__Group_3__0__Impl rule__Configuration__Group_3__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_3__0__Impl_in_rule__Configuration__Group_3__02584);
            rule__Configuration__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_3__1_in_rule__Configuration__Group_3__02587);
            rule__Configuration__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_3__0"


    // $ANTLR start "rule__Configuration__Group_3__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1217:1: rule__Configuration__Group_3__0__Impl : ( 'Types' ) ;
    public final void rule__Configuration__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1221:1: ( ( 'Types' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1222:1: ( 'Types' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1222:1: ( 'Types' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1223:1: 'Types'
            {
             before(grammarAccess.getConfigurationAccess().getTypesKeyword_3_0()); 
            match(input,28,FollowSets000.FOLLOW_28_in_rule__Configuration__Group_3__0__Impl2615); 
             after(grammarAccess.getConfigurationAccess().getTypesKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_3__0__Impl"


    // $ANTLR start "rule__Configuration__Group_3__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1236:1: rule__Configuration__Group_3__1 : rule__Configuration__Group_3__1__Impl rule__Configuration__Group_3__2 ;
    public final void rule__Configuration__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1240:1: ( rule__Configuration__Group_3__1__Impl rule__Configuration__Group_3__2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1241:2: rule__Configuration__Group_3__1__Impl rule__Configuration__Group_3__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_3__1__Impl_in_rule__Configuration__Group_3__12646);
            rule__Configuration__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_3__2_in_rule__Configuration__Group_3__12649);
            rule__Configuration__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_3__1"


    // $ANTLR start "rule__Configuration__Group_3__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1248:1: rule__Configuration__Group_3__1__Impl : ( '{' ) ;
    public final void rule__Configuration__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1252:1: ( ( '{' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1253:1: ( '{' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1253:1: ( '{' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1254:1: '{'
            {
             before(grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_3_1()); 
            match(input,26,FollowSets000.FOLLOW_26_in_rule__Configuration__Group_3__1__Impl2677); 
             after(grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_3__1__Impl"


    // $ANTLR start "rule__Configuration__Group_3__2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1267:1: rule__Configuration__Group_3__2 : rule__Configuration__Group_3__2__Impl rule__Configuration__Group_3__3 ;
    public final void rule__Configuration__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1271:1: ( rule__Configuration__Group_3__2__Impl rule__Configuration__Group_3__3 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1272:2: rule__Configuration__Group_3__2__Impl rule__Configuration__Group_3__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_3__2__Impl_in_rule__Configuration__Group_3__22708);
            rule__Configuration__Group_3__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_3__3_in_rule__Configuration__Group_3__22711);
            rule__Configuration__Group_3__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_3__2"


    // $ANTLR start "rule__Configuration__Group_3__2__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1279:1: rule__Configuration__Group_3__2__Impl : ( ( rule__Configuration__TypesAssignment_3_2 ) ) ;
    public final void rule__Configuration__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1283:1: ( ( ( rule__Configuration__TypesAssignment_3_2 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1284:1: ( ( rule__Configuration__TypesAssignment_3_2 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1284:1: ( ( rule__Configuration__TypesAssignment_3_2 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1285:1: ( rule__Configuration__TypesAssignment_3_2 )
            {
             before(grammarAccess.getConfigurationAccess().getTypesAssignment_3_2()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1286:1: ( rule__Configuration__TypesAssignment_3_2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1286:2: rule__Configuration__TypesAssignment_3_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__TypesAssignment_3_2_in_rule__Configuration__Group_3__2__Impl2738);
            rule__Configuration__TypesAssignment_3_2();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getTypesAssignment_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_3__2__Impl"


    // $ANTLR start "rule__Configuration__Group_3__3"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1296:1: rule__Configuration__Group_3__3 : rule__Configuration__Group_3__3__Impl rule__Configuration__Group_3__4 ;
    public final void rule__Configuration__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1300:1: ( rule__Configuration__Group_3__3__Impl rule__Configuration__Group_3__4 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1301:2: rule__Configuration__Group_3__3__Impl rule__Configuration__Group_3__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_3__3__Impl_in_rule__Configuration__Group_3__32768);
            rule__Configuration__Group_3__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_3__4_in_rule__Configuration__Group_3__32771);
            rule__Configuration__Group_3__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_3__3"


    // $ANTLR start "rule__Configuration__Group_3__3__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1308:1: rule__Configuration__Group_3__3__Impl : ( ( rule__Configuration__Group_3_3__0 )* ) ;
    public final void rule__Configuration__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1312:1: ( ( ( rule__Configuration__Group_3_3__0 )* ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1313:1: ( ( rule__Configuration__Group_3_3__0 )* )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1313:1: ( ( rule__Configuration__Group_3_3__0 )* )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1314:1: ( rule__Configuration__Group_3_3__0 )*
            {
             before(grammarAccess.getConfigurationAccess().getGroup_3_3()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1315:1: ( rule__Configuration__Group_3_3__0 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==29) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1315:2: rule__Configuration__Group_3_3__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_3_3__0_in_rule__Configuration__Group_3__3__Impl2798);
            	    rule__Configuration__Group_3_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getConfigurationAccess().getGroup_3_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_3__3__Impl"


    // $ANTLR start "rule__Configuration__Group_3__4"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1325:1: rule__Configuration__Group_3__4 : rule__Configuration__Group_3__4__Impl ;
    public final void rule__Configuration__Group_3__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1329:1: ( rule__Configuration__Group_3__4__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1330:2: rule__Configuration__Group_3__4__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_3__4__Impl_in_rule__Configuration__Group_3__42829);
            rule__Configuration__Group_3__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_3__4"


    // $ANTLR start "rule__Configuration__Group_3__4__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1336:1: rule__Configuration__Group_3__4__Impl : ( '}' ) ;
    public final void rule__Configuration__Group_3__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1340:1: ( ( '}' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1341:1: ( '}' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1341:1: ( '}' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1342:1: '}'
            {
             before(grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_3_4()); 
            match(input,27,FollowSets000.FOLLOW_27_in_rule__Configuration__Group_3__4__Impl2857); 
             after(grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_3_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_3__4__Impl"


    // $ANTLR start "rule__Configuration__Group_3_3__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1365:1: rule__Configuration__Group_3_3__0 : rule__Configuration__Group_3_3__0__Impl rule__Configuration__Group_3_3__1 ;
    public final void rule__Configuration__Group_3_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1369:1: ( rule__Configuration__Group_3_3__0__Impl rule__Configuration__Group_3_3__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1370:2: rule__Configuration__Group_3_3__0__Impl rule__Configuration__Group_3_3__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_3_3__0__Impl_in_rule__Configuration__Group_3_3__02898);
            rule__Configuration__Group_3_3__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_3_3__1_in_rule__Configuration__Group_3_3__02901);
            rule__Configuration__Group_3_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_3_3__0"


    // $ANTLR start "rule__Configuration__Group_3_3__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1377:1: rule__Configuration__Group_3_3__0__Impl : ( ',' ) ;
    public final void rule__Configuration__Group_3_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1381:1: ( ( ',' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1382:1: ( ',' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1382:1: ( ',' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1383:1: ','
            {
             before(grammarAccess.getConfigurationAccess().getCommaKeyword_3_3_0()); 
            match(input,29,FollowSets000.FOLLOW_29_in_rule__Configuration__Group_3_3__0__Impl2929); 
             after(grammarAccess.getConfigurationAccess().getCommaKeyword_3_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_3_3__0__Impl"


    // $ANTLR start "rule__Configuration__Group_3_3__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1396:1: rule__Configuration__Group_3_3__1 : rule__Configuration__Group_3_3__1__Impl ;
    public final void rule__Configuration__Group_3_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1400:1: ( rule__Configuration__Group_3_3__1__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1401:2: rule__Configuration__Group_3_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_3_3__1__Impl_in_rule__Configuration__Group_3_3__12960);
            rule__Configuration__Group_3_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_3_3__1"


    // $ANTLR start "rule__Configuration__Group_3_3__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1407:1: rule__Configuration__Group_3_3__1__Impl : ( ( rule__Configuration__TypesAssignment_3_3_1 ) ) ;
    public final void rule__Configuration__Group_3_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1411:1: ( ( ( rule__Configuration__TypesAssignment_3_3_1 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1412:1: ( ( rule__Configuration__TypesAssignment_3_3_1 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1412:1: ( ( rule__Configuration__TypesAssignment_3_3_1 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1413:1: ( rule__Configuration__TypesAssignment_3_3_1 )
            {
             before(grammarAccess.getConfigurationAccess().getTypesAssignment_3_3_1()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1414:1: ( rule__Configuration__TypesAssignment_3_3_1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1414:2: rule__Configuration__TypesAssignment_3_3_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__TypesAssignment_3_3_1_in_rule__Configuration__Group_3_3__1__Impl2987);
            rule__Configuration__TypesAssignment_3_3_1();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getTypesAssignment_3_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_3_3__1__Impl"


    // $ANTLR start "rule__Configuration__Group_4__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1428:1: rule__Configuration__Group_4__0 : rule__Configuration__Group_4__0__Impl rule__Configuration__Group_4__1 ;
    public final void rule__Configuration__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1432:1: ( rule__Configuration__Group_4__0__Impl rule__Configuration__Group_4__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1433:2: rule__Configuration__Group_4__0__Impl rule__Configuration__Group_4__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_4__0__Impl_in_rule__Configuration__Group_4__03021);
            rule__Configuration__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_4__1_in_rule__Configuration__Group_4__03024);
            rule__Configuration__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__0"


    // $ANTLR start "rule__Configuration__Group_4__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1440:1: rule__Configuration__Group_4__0__Impl : ( 'Features' ) ;
    public final void rule__Configuration__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1444:1: ( ( 'Features' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1445:1: ( 'Features' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1445:1: ( 'Features' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1446:1: 'Features'
            {
             before(grammarAccess.getConfigurationAccess().getFeaturesKeyword_4_0()); 
            match(input,30,FollowSets000.FOLLOW_30_in_rule__Configuration__Group_4__0__Impl3052); 
             after(grammarAccess.getConfigurationAccess().getFeaturesKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__0__Impl"


    // $ANTLR start "rule__Configuration__Group_4__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1459:1: rule__Configuration__Group_4__1 : rule__Configuration__Group_4__1__Impl rule__Configuration__Group_4__2 ;
    public final void rule__Configuration__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1463:1: ( rule__Configuration__Group_4__1__Impl rule__Configuration__Group_4__2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1464:2: rule__Configuration__Group_4__1__Impl rule__Configuration__Group_4__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_4__1__Impl_in_rule__Configuration__Group_4__13083);
            rule__Configuration__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_4__2_in_rule__Configuration__Group_4__13086);
            rule__Configuration__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__1"


    // $ANTLR start "rule__Configuration__Group_4__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1471:1: rule__Configuration__Group_4__1__Impl : ( '{' ) ;
    public final void rule__Configuration__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1475:1: ( ( '{' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1476:1: ( '{' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1476:1: ( '{' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1477:1: '{'
            {
             before(grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_4_1()); 
            match(input,26,FollowSets000.FOLLOW_26_in_rule__Configuration__Group_4__1__Impl3114); 
             after(grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__1__Impl"


    // $ANTLR start "rule__Configuration__Group_4__2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1490:1: rule__Configuration__Group_4__2 : rule__Configuration__Group_4__2__Impl rule__Configuration__Group_4__3 ;
    public final void rule__Configuration__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1494:1: ( rule__Configuration__Group_4__2__Impl rule__Configuration__Group_4__3 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1495:2: rule__Configuration__Group_4__2__Impl rule__Configuration__Group_4__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_4__2__Impl_in_rule__Configuration__Group_4__23145);
            rule__Configuration__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_4__3_in_rule__Configuration__Group_4__23148);
            rule__Configuration__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__2"


    // $ANTLR start "rule__Configuration__Group_4__2__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1502:1: rule__Configuration__Group_4__2__Impl : ( ( rule__Configuration__RootFeaturesAssignment_4_2 ) ) ;
    public final void rule__Configuration__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1506:1: ( ( ( rule__Configuration__RootFeaturesAssignment_4_2 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1507:1: ( ( rule__Configuration__RootFeaturesAssignment_4_2 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1507:1: ( ( rule__Configuration__RootFeaturesAssignment_4_2 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1508:1: ( rule__Configuration__RootFeaturesAssignment_4_2 )
            {
             before(grammarAccess.getConfigurationAccess().getRootFeaturesAssignment_4_2()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1509:1: ( rule__Configuration__RootFeaturesAssignment_4_2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1509:2: rule__Configuration__RootFeaturesAssignment_4_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__RootFeaturesAssignment_4_2_in_rule__Configuration__Group_4__2__Impl3175);
            rule__Configuration__RootFeaturesAssignment_4_2();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getRootFeaturesAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__2__Impl"


    // $ANTLR start "rule__Configuration__Group_4__3"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1519:1: rule__Configuration__Group_4__3 : rule__Configuration__Group_4__3__Impl rule__Configuration__Group_4__4 ;
    public final void rule__Configuration__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1523:1: ( rule__Configuration__Group_4__3__Impl rule__Configuration__Group_4__4 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1524:2: rule__Configuration__Group_4__3__Impl rule__Configuration__Group_4__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_4__3__Impl_in_rule__Configuration__Group_4__33205);
            rule__Configuration__Group_4__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_4__4_in_rule__Configuration__Group_4__33208);
            rule__Configuration__Group_4__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__3"


    // $ANTLR start "rule__Configuration__Group_4__3__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1531:1: rule__Configuration__Group_4__3__Impl : ( ( rule__Configuration__Group_4_3__0 )* ) ;
    public final void rule__Configuration__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1535:1: ( ( ( rule__Configuration__Group_4_3__0 )* ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1536:1: ( ( rule__Configuration__Group_4_3__0 )* )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1536:1: ( ( rule__Configuration__Group_4_3__0 )* )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1537:1: ( rule__Configuration__Group_4_3__0 )*
            {
             before(grammarAccess.getConfigurationAccess().getGroup_4_3()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1538:1: ( rule__Configuration__Group_4_3__0 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==29) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1538:2: rule__Configuration__Group_4_3__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_4_3__0_in_rule__Configuration__Group_4__3__Impl3235);
            	    rule__Configuration__Group_4_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getConfigurationAccess().getGroup_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__3__Impl"


    // $ANTLR start "rule__Configuration__Group_4__4"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1548:1: rule__Configuration__Group_4__4 : rule__Configuration__Group_4__4__Impl ;
    public final void rule__Configuration__Group_4__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1552:1: ( rule__Configuration__Group_4__4__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1553:2: rule__Configuration__Group_4__4__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_4__4__Impl_in_rule__Configuration__Group_4__43266);
            rule__Configuration__Group_4__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__4"


    // $ANTLR start "rule__Configuration__Group_4__4__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1559:1: rule__Configuration__Group_4__4__Impl : ( '}' ) ;
    public final void rule__Configuration__Group_4__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1563:1: ( ( '}' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1564:1: ( '}' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1564:1: ( '}' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1565:1: '}'
            {
             before(grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_4_4()); 
            match(input,27,FollowSets000.FOLLOW_27_in_rule__Configuration__Group_4__4__Impl3294); 
             after(grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_4_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__4__Impl"


    // $ANTLR start "rule__Configuration__Group_4_3__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1588:1: rule__Configuration__Group_4_3__0 : rule__Configuration__Group_4_3__0__Impl rule__Configuration__Group_4_3__1 ;
    public final void rule__Configuration__Group_4_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1592:1: ( rule__Configuration__Group_4_3__0__Impl rule__Configuration__Group_4_3__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1593:2: rule__Configuration__Group_4_3__0__Impl rule__Configuration__Group_4_3__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_4_3__0__Impl_in_rule__Configuration__Group_4_3__03335);
            rule__Configuration__Group_4_3__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_4_3__1_in_rule__Configuration__Group_4_3__03338);
            rule__Configuration__Group_4_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4_3__0"


    // $ANTLR start "rule__Configuration__Group_4_3__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1600:1: rule__Configuration__Group_4_3__0__Impl : ( ',' ) ;
    public final void rule__Configuration__Group_4_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1604:1: ( ( ',' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1605:1: ( ',' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1605:1: ( ',' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1606:1: ','
            {
             before(grammarAccess.getConfigurationAccess().getCommaKeyword_4_3_0()); 
            match(input,29,FollowSets000.FOLLOW_29_in_rule__Configuration__Group_4_3__0__Impl3366); 
             after(grammarAccess.getConfigurationAccess().getCommaKeyword_4_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4_3__0__Impl"


    // $ANTLR start "rule__Configuration__Group_4_3__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1619:1: rule__Configuration__Group_4_3__1 : rule__Configuration__Group_4_3__1__Impl ;
    public final void rule__Configuration__Group_4_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1623:1: ( rule__Configuration__Group_4_3__1__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1624:2: rule__Configuration__Group_4_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_4_3__1__Impl_in_rule__Configuration__Group_4_3__13397);
            rule__Configuration__Group_4_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4_3__1"


    // $ANTLR start "rule__Configuration__Group_4_3__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1630:1: rule__Configuration__Group_4_3__1__Impl : ( ( rule__Configuration__RootFeaturesAssignment_4_3_1 ) ) ;
    public final void rule__Configuration__Group_4_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1634:1: ( ( ( rule__Configuration__RootFeaturesAssignment_4_3_1 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1635:1: ( ( rule__Configuration__RootFeaturesAssignment_4_3_1 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1635:1: ( ( rule__Configuration__RootFeaturesAssignment_4_3_1 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1636:1: ( rule__Configuration__RootFeaturesAssignment_4_3_1 )
            {
             before(grammarAccess.getConfigurationAccess().getRootFeaturesAssignment_4_3_1()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1637:1: ( rule__Configuration__RootFeaturesAssignment_4_3_1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1637:2: rule__Configuration__RootFeaturesAssignment_4_3_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__RootFeaturesAssignment_4_3_1_in_rule__Configuration__Group_4_3__1__Impl3424);
            rule__Configuration__RootFeaturesAssignment_4_3_1();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getRootFeaturesAssignment_4_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4_3__1__Impl"


    // $ANTLR start "rule__Configuration__Group_5__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1651:1: rule__Configuration__Group_5__0 : rule__Configuration__Group_5__0__Impl rule__Configuration__Group_5__1 ;
    public final void rule__Configuration__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1655:1: ( rule__Configuration__Group_5__0__Impl rule__Configuration__Group_5__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1656:2: rule__Configuration__Group_5__0__Impl rule__Configuration__Group_5__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_5__0__Impl_in_rule__Configuration__Group_5__03458);
            rule__Configuration__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_5__1_in_rule__Configuration__Group_5__03461);
            rule__Configuration__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__0"


    // $ANTLR start "rule__Configuration__Group_5__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1663:1: rule__Configuration__Group_5__0__Impl : ( 'Constraints' ) ;
    public final void rule__Configuration__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1667:1: ( ( 'Constraints' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1668:1: ( 'Constraints' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1668:1: ( 'Constraints' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1669:1: 'Constraints'
            {
             before(grammarAccess.getConfigurationAccess().getConstraintsKeyword_5_0()); 
            match(input,31,FollowSets000.FOLLOW_31_in_rule__Configuration__Group_5__0__Impl3489); 
             after(grammarAccess.getConfigurationAccess().getConstraintsKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__0__Impl"


    // $ANTLR start "rule__Configuration__Group_5__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1682:1: rule__Configuration__Group_5__1 : rule__Configuration__Group_5__1__Impl rule__Configuration__Group_5__2 ;
    public final void rule__Configuration__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1686:1: ( rule__Configuration__Group_5__1__Impl rule__Configuration__Group_5__2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1687:2: rule__Configuration__Group_5__1__Impl rule__Configuration__Group_5__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_5__1__Impl_in_rule__Configuration__Group_5__13520);
            rule__Configuration__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_5__2_in_rule__Configuration__Group_5__13523);
            rule__Configuration__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__1"


    // $ANTLR start "rule__Configuration__Group_5__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1694:1: rule__Configuration__Group_5__1__Impl : ( '{' ) ;
    public final void rule__Configuration__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1698:1: ( ( '{' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1699:1: ( '{' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1699:1: ( '{' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1700:1: '{'
            {
             before(grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_5_1()); 
            match(input,26,FollowSets000.FOLLOW_26_in_rule__Configuration__Group_5__1__Impl3551); 
             after(grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__1__Impl"


    // $ANTLR start "rule__Configuration__Group_5__2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1713:1: rule__Configuration__Group_5__2 : rule__Configuration__Group_5__2__Impl rule__Configuration__Group_5__3 ;
    public final void rule__Configuration__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1717:1: ( rule__Configuration__Group_5__2__Impl rule__Configuration__Group_5__3 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1718:2: rule__Configuration__Group_5__2__Impl rule__Configuration__Group_5__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_5__2__Impl_in_rule__Configuration__Group_5__23582);
            rule__Configuration__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_5__3_in_rule__Configuration__Group_5__23585);
            rule__Configuration__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__2"


    // $ANTLR start "rule__Configuration__Group_5__2__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1725:1: rule__Configuration__Group_5__2__Impl : ( ( rule__Configuration__ConstraintsAssignment_5_2 ) ) ;
    public final void rule__Configuration__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1729:1: ( ( ( rule__Configuration__ConstraintsAssignment_5_2 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1730:1: ( ( rule__Configuration__ConstraintsAssignment_5_2 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1730:1: ( ( rule__Configuration__ConstraintsAssignment_5_2 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1731:1: ( rule__Configuration__ConstraintsAssignment_5_2 )
            {
             before(grammarAccess.getConfigurationAccess().getConstraintsAssignment_5_2()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1732:1: ( rule__Configuration__ConstraintsAssignment_5_2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1732:2: rule__Configuration__ConstraintsAssignment_5_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__ConstraintsAssignment_5_2_in_rule__Configuration__Group_5__2__Impl3612);
            rule__Configuration__ConstraintsAssignment_5_2();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getConstraintsAssignment_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__2__Impl"


    // $ANTLR start "rule__Configuration__Group_5__3"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1742:1: rule__Configuration__Group_5__3 : rule__Configuration__Group_5__3__Impl rule__Configuration__Group_5__4 ;
    public final void rule__Configuration__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1746:1: ( rule__Configuration__Group_5__3__Impl rule__Configuration__Group_5__4 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1747:2: rule__Configuration__Group_5__3__Impl rule__Configuration__Group_5__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_5__3__Impl_in_rule__Configuration__Group_5__33642);
            rule__Configuration__Group_5__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_5__4_in_rule__Configuration__Group_5__33645);
            rule__Configuration__Group_5__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__3"


    // $ANTLR start "rule__Configuration__Group_5__3__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1754:1: rule__Configuration__Group_5__3__Impl : ( ( rule__Configuration__Group_5_3__0 )* ) ;
    public final void rule__Configuration__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1758:1: ( ( ( rule__Configuration__Group_5_3__0 )* ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1759:1: ( ( rule__Configuration__Group_5_3__0 )* )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1759:1: ( ( rule__Configuration__Group_5_3__0 )* )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1760:1: ( rule__Configuration__Group_5_3__0 )*
            {
             before(grammarAccess.getConfigurationAccess().getGroup_5_3()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1761:1: ( rule__Configuration__Group_5_3__0 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==29) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1761:2: rule__Configuration__Group_5_3__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_5_3__0_in_rule__Configuration__Group_5__3__Impl3672);
            	    rule__Configuration__Group_5_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getConfigurationAccess().getGroup_5_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__3__Impl"


    // $ANTLR start "rule__Configuration__Group_5__4"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1771:1: rule__Configuration__Group_5__4 : rule__Configuration__Group_5__4__Impl ;
    public final void rule__Configuration__Group_5__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1775:1: ( rule__Configuration__Group_5__4__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1776:2: rule__Configuration__Group_5__4__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_5__4__Impl_in_rule__Configuration__Group_5__43703);
            rule__Configuration__Group_5__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__4"


    // $ANTLR start "rule__Configuration__Group_5__4__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1782:1: rule__Configuration__Group_5__4__Impl : ( '}' ) ;
    public final void rule__Configuration__Group_5__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1786:1: ( ( '}' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1787:1: ( '}' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1787:1: ( '}' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1788:1: '}'
            {
             before(grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_5_4()); 
            match(input,27,FollowSets000.FOLLOW_27_in_rule__Configuration__Group_5__4__Impl3731); 
             after(grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_5_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__4__Impl"


    // $ANTLR start "rule__Configuration__Group_5_3__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1811:1: rule__Configuration__Group_5_3__0 : rule__Configuration__Group_5_3__0__Impl rule__Configuration__Group_5_3__1 ;
    public final void rule__Configuration__Group_5_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1815:1: ( rule__Configuration__Group_5_3__0__Impl rule__Configuration__Group_5_3__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1816:2: rule__Configuration__Group_5_3__0__Impl rule__Configuration__Group_5_3__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_5_3__0__Impl_in_rule__Configuration__Group_5_3__03772);
            rule__Configuration__Group_5_3__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_5_3__1_in_rule__Configuration__Group_5_3__03775);
            rule__Configuration__Group_5_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5_3__0"


    // $ANTLR start "rule__Configuration__Group_5_3__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1823:1: rule__Configuration__Group_5_3__0__Impl : ( ',' ) ;
    public final void rule__Configuration__Group_5_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1827:1: ( ( ',' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1828:1: ( ',' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1828:1: ( ',' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1829:1: ','
            {
             before(grammarAccess.getConfigurationAccess().getCommaKeyword_5_3_0()); 
            match(input,29,FollowSets000.FOLLOW_29_in_rule__Configuration__Group_5_3__0__Impl3803); 
             after(grammarAccess.getConfigurationAccess().getCommaKeyword_5_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5_3__0__Impl"


    // $ANTLR start "rule__Configuration__Group_5_3__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1842:1: rule__Configuration__Group_5_3__1 : rule__Configuration__Group_5_3__1__Impl ;
    public final void rule__Configuration__Group_5_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1846:1: ( rule__Configuration__Group_5_3__1__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1847:2: rule__Configuration__Group_5_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__Group_5_3__1__Impl_in_rule__Configuration__Group_5_3__13834);
            rule__Configuration__Group_5_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5_3__1"


    // $ANTLR start "rule__Configuration__Group_5_3__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1853:1: rule__Configuration__Group_5_3__1__Impl : ( ( rule__Configuration__ConstraintsAssignment_5_3_1 ) ) ;
    public final void rule__Configuration__Group_5_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1857:1: ( ( ( rule__Configuration__ConstraintsAssignment_5_3_1 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1858:1: ( ( rule__Configuration__ConstraintsAssignment_5_3_1 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1858:1: ( ( rule__Configuration__ConstraintsAssignment_5_3_1 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1859:1: ( rule__Configuration__ConstraintsAssignment_5_3_1 )
            {
             before(grammarAccess.getConfigurationAccess().getConstraintsAssignment_5_3_1()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1860:1: ( rule__Configuration__ConstraintsAssignment_5_3_1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1860:2: rule__Configuration__ConstraintsAssignment_5_3_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Configuration__ConstraintsAssignment_5_3_1_in_rule__Configuration__Group_5_3__1__Impl3861);
            rule__Configuration__ConstraintsAssignment_5_3_1();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getConstraintsAssignment_5_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5_3__1__Impl"


    // $ANTLR start "rule__Constraint__Group__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1874:1: rule__Constraint__Group__0 : rule__Constraint__Group__0__Impl rule__Constraint__Group__1 ;
    public final void rule__Constraint__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1878:1: ( rule__Constraint__Group__0__Impl rule__Constraint__Group__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1879:2: rule__Constraint__Group__0__Impl rule__Constraint__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Constraint__Group__0__Impl_in_rule__Constraint__Group__03895);
            rule__Constraint__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Constraint__Group__1_in_rule__Constraint__Group__03898);
            rule__Constraint__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__0"


    // $ANTLR start "rule__Constraint__Group__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1886:1: rule__Constraint__Group__0__Impl : ( ( rule__Constraint__NameAssignment_0 ) ) ;
    public final void rule__Constraint__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1890:1: ( ( ( rule__Constraint__NameAssignment_0 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1891:1: ( ( rule__Constraint__NameAssignment_0 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1891:1: ( ( rule__Constraint__NameAssignment_0 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1892:1: ( rule__Constraint__NameAssignment_0 )
            {
             before(grammarAccess.getConstraintAccess().getNameAssignment_0()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1893:1: ( rule__Constraint__NameAssignment_0 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1893:2: rule__Constraint__NameAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Constraint__NameAssignment_0_in_rule__Constraint__Group__0__Impl3925);
            rule__Constraint__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getConstraintAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__0__Impl"


    // $ANTLR start "rule__Constraint__Group__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1903:1: rule__Constraint__Group__1 : rule__Constraint__Group__1__Impl rule__Constraint__Group__2 ;
    public final void rule__Constraint__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1907:1: ( rule__Constraint__Group__1__Impl rule__Constraint__Group__2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1908:2: rule__Constraint__Group__1__Impl rule__Constraint__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Constraint__Group__1__Impl_in_rule__Constraint__Group__13955);
            rule__Constraint__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Constraint__Group__2_in_rule__Constraint__Group__13958);
            rule__Constraint__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__1"


    // $ANTLR start "rule__Constraint__Group__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1915:1: rule__Constraint__Group__1__Impl : ( '[' ) ;
    public final void rule__Constraint__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1919:1: ( ( '[' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1920:1: ( '[' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1920:1: ( '[' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1921:1: '['
            {
             before(grammarAccess.getConstraintAccess().getLeftSquareBracketKeyword_1()); 
            match(input,32,FollowSets000.FOLLOW_32_in_rule__Constraint__Group__1__Impl3986); 
             after(grammarAccess.getConstraintAccess().getLeftSquareBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__1__Impl"


    // $ANTLR start "rule__Constraint__Group__2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1934:1: rule__Constraint__Group__2 : rule__Constraint__Group__2__Impl rule__Constraint__Group__3 ;
    public final void rule__Constraint__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1938:1: ( rule__Constraint__Group__2__Impl rule__Constraint__Group__3 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1939:2: rule__Constraint__Group__2__Impl rule__Constraint__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Constraint__Group__2__Impl_in_rule__Constraint__Group__24017);
            rule__Constraint__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Constraint__Group__3_in_rule__Constraint__Group__24020);
            rule__Constraint__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__2"


    // $ANTLR start "rule__Constraint__Group__2__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1946:1: rule__Constraint__Group__2__Impl : ( ( rule__Constraint__ErrorMessageAssignment_2 ) ) ;
    public final void rule__Constraint__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1950:1: ( ( ( rule__Constraint__ErrorMessageAssignment_2 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1951:1: ( ( rule__Constraint__ErrorMessageAssignment_2 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1951:1: ( ( rule__Constraint__ErrorMessageAssignment_2 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1952:1: ( rule__Constraint__ErrorMessageAssignment_2 )
            {
             before(grammarAccess.getConstraintAccess().getErrorMessageAssignment_2()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1953:1: ( rule__Constraint__ErrorMessageAssignment_2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1953:2: rule__Constraint__ErrorMessageAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Constraint__ErrorMessageAssignment_2_in_rule__Constraint__Group__2__Impl4047);
            rule__Constraint__ErrorMessageAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getConstraintAccess().getErrorMessageAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__2__Impl"


    // $ANTLR start "rule__Constraint__Group__3"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1963:1: rule__Constraint__Group__3 : rule__Constraint__Group__3__Impl rule__Constraint__Group__4 ;
    public final void rule__Constraint__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1967:1: ( rule__Constraint__Group__3__Impl rule__Constraint__Group__4 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1968:2: rule__Constraint__Group__3__Impl rule__Constraint__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Constraint__Group__3__Impl_in_rule__Constraint__Group__34077);
            rule__Constraint__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Constraint__Group__4_in_rule__Constraint__Group__34080);
            rule__Constraint__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__3"


    // $ANTLR start "rule__Constraint__Group__3__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1975:1: rule__Constraint__Group__3__Impl : ( ']' ) ;
    public final void rule__Constraint__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1979:1: ( ( ']' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1980:1: ( ']' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1980:1: ( ']' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1981:1: ']'
            {
             before(grammarAccess.getConstraintAccess().getRightSquareBracketKeyword_3()); 
            match(input,33,FollowSets000.FOLLOW_33_in_rule__Constraint__Group__3__Impl4108); 
             after(grammarAccess.getConstraintAccess().getRightSquareBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__3__Impl"


    // $ANTLR start "rule__Constraint__Group__4"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1994:1: rule__Constraint__Group__4 : rule__Constraint__Group__4__Impl rule__Constraint__Group__5 ;
    public final void rule__Constraint__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1998:1: ( rule__Constraint__Group__4__Impl rule__Constraint__Group__5 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:1999:2: rule__Constraint__Group__4__Impl rule__Constraint__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Constraint__Group__4__Impl_in_rule__Constraint__Group__44139);
            rule__Constraint__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Constraint__Group__5_in_rule__Constraint__Group__44142);
            rule__Constraint__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__4"


    // $ANTLR start "rule__Constraint__Group__4__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2006:1: rule__Constraint__Group__4__Impl : ( '{' ) ;
    public final void rule__Constraint__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2010:1: ( ( '{' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2011:1: ( '{' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2011:1: ( '{' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2012:1: '{'
            {
             before(grammarAccess.getConstraintAccess().getLeftCurlyBracketKeyword_4()); 
            match(input,26,FollowSets000.FOLLOW_26_in_rule__Constraint__Group__4__Impl4170); 
             after(grammarAccess.getConstraintAccess().getLeftCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__4__Impl"


    // $ANTLR start "rule__Constraint__Group__5"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2025:1: rule__Constraint__Group__5 : rule__Constraint__Group__5__Impl rule__Constraint__Group__6 ;
    public final void rule__Constraint__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2029:1: ( rule__Constraint__Group__5__Impl rule__Constraint__Group__6 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2030:2: rule__Constraint__Group__5__Impl rule__Constraint__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Constraint__Group__5__Impl_in_rule__Constraint__Group__54201);
            rule__Constraint__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Constraint__Group__6_in_rule__Constraint__Group__54204);
            rule__Constraint__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__5"


    // $ANTLR start "rule__Constraint__Group__5__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2037:1: rule__Constraint__Group__5__Impl : ( ( rule__Constraint__ExpressionAssignment_5 ) ) ;
    public final void rule__Constraint__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2041:1: ( ( ( rule__Constraint__ExpressionAssignment_5 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2042:1: ( ( rule__Constraint__ExpressionAssignment_5 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2042:1: ( ( rule__Constraint__ExpressionAssignment_5 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2043:1: ( rule__Constraint__ExpressionAssignment_5 )
            {
             before(grammarAccess.getConstraintAccess().getExpressionAssignment_5()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2044:1: ( rule__Constraint__ExpressionAssignment_5 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2044:2: rule__Constraint__ExpressionAssignment_5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Constraint__ExpressionAssignment_5_in_rule__Constraint__Group__5__Impl4231);
            rule__Constraint__ExpressionAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getConstraintAccess().getExpressionAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__5__Impl"


    // $ANTLR start "rule__Constraint__Group__6"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2054:1: rule__Constraint__Group__6 : rule__Constraint__Group__6__Impl ;
    public final void rule__Constraint__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2058:1: ( rule__Constraint__Group__6__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2059:2: rule__Constraint__Group__6__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Constraint__Group__6__Impl_in_rule__Constraint__Group__64261);
            rule__Constraint__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__6"


    // $ANTLR start "rule__Constraint__Group__6__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2065:1: rule__Constraint__Group__6__Impl : ( '}' ) ;
    public final void rule__Constraint__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2069:1: ( ( '}' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2070:1: ( '}' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2070:1: ( '}' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2071:1: '}'
            {
             before(grammarAccess.getConstraintAccess().getRightCurlyBracketKeyword_6()); 
            match(input,27,FollowSets000.FOLLOW_27_in_rule__Constraint__Group__6__Impl4289); 
             after(grammarAccess.getConstraintAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__6__Impl"


    // $ANTLR start "rule__Feature__Group__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2098:1: rule__Feature__Group__0 : rule__Feature__Group__0__Impl rule__Feature__Group__1 ;
    public final void rule__Feature__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2102:1: ( rule__Feature__Group__0__Impl rule__Feature__Group__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2103:2: rule__Feature__Group__0__Impl rule__Feature__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group__0__Impl_in_rule__Feature__Group__04334);
            rule__Feature__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group__1_in_rule__Feature__Group__04337);
            rule__Feature__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__0"


    // $ANTLR start "rule__Feature__Group__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2110:1: rule__Feature__Group__0__Impl : ( ( rule__Feature__NameAssignment_0 ) ) ;
    public final void rule__Feature__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2114:1: ( ( ( rule__Feature__NameAssignment_0 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2115:1: ( ( rule__Feature__NameAssignment_0 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2115:1: ( ( rule__Feature__NameAssignment_0 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2116:1: ( rule__Feature__NameAssignment_0 )
            {
             before(grammarAccess.getFeatureAccess().getNameAssignment_0()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2117:1: ( rule__Feature__NameAssignment_0 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2117:2: rule__Feature__NameAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__NameAssignment_0_in_rule__Feature__Group__0__Impl4364);
            rule__Feature__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__0__Impl"


    // $ANTLR start "rule__Feature__Group__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2127:1: rule__Feature__Group__1 : rule__Feature__Group__1__Impl rule__Feature__Group__2 ;
    public final void rule__Feature__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2131:1: ( rule__Feature__Group__1__Impl rule__Feature__Group__2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2132:2: rule__Feature__Group__1__Impl rule__Feature__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group__1__Impl_in_rule__Feature__Group__14394);
            rule__Feature__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group__2_in_rule__Feature__Group__14397);
            rule__Feature__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__1"


    // $ANTLR start "rule__Feature__Group__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2139:1: rule__Feature__Group__1__Impl : ( ( rule__Feature__TypeAssignment_1 ) ) ;
    public final void rule__Feature__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2143:1: ( ( ( rule__Feature__TypeAssignment_1 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2144:1: ( ( rule__Feature__TypeAssignment_1 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2144:1: ( ( rule__Feature__TypeAssignment_1 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2145:1: ( rule__Feature__TypeAssignment_1 )
            {
             before(grammarAccess.getFeatureAccess().getTypeAssignment_1()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2146:1: ( rule__Feature__TypeAssignment_1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2146:2: rule__Feature__TypeAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__TypeAssignment_1_in_rule__Feature__Group__1__Impl4424);
            rule__Feature__TypeAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getTypeAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__1__Impl"


    // $ANTLR start "rule__Feature__Group__2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2156:1: rule__Feature__Group__2 : rule__Feature__Group__2__Impl rule__Feature__Group__3 ;
    public final void rule__Feature__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2160:1: ( rule__Feature__Group__2__Impl rule__Feature__Group__3 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2161:2: rule__Feature__Group__2__Impl rule__Feature__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group__2__Impl_in_rule__Feature__Group__24454);
            rule__Feature__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group__3_in_rule__Feature__Group__24457);
            rule__Feature__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__2"


    // $ANTLR start "rule__Feature__Group__2__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2168:1: rule__Feature__Group__2__Impl : ( ( rule__Feature__Group_2__0 )? ) ;
    public final void rule__Feature__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2172:1: ( ( ( rule__Feature__Group_2__0 )? ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2173:1: ( ( rule__Feature__Group_2__0 )? )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2173:1: ( ( rule__Feature__Group_2__0 )? )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2174:1: ( rule__Feature__Group_2__0 )?
            {
             before(grammarAccess.getFeatureAccess().getGroup_2()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2175:1: ( rule__Feature__Group_2__0 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==32) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2175:2: rule__Feature__Group_2__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_2__0_in_rule__Feature__Group__2__Impl4484);
                    rule__Feature__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFeatureAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__2__Impl"


    // $ANTLR start "rule__Feature__Group__3"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2185:1: rule__Feature__Group__3 : rule__Feature__Group__3__Impl ;
    public final void rule__Feature__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2189:1: ( rule__Feature__Group__3__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2190:2: rule__Feature__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group__3__Impl_in_rule__Feature__Group__34515);
            rule__Feature__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__3"


    // $ANTLR start "rule__Feature__Group__3__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2196:1: rule__Feature__Group__3__Impl : ( ( rule__Feature__Group_3__0 )? ) ;
    public final void rule__Feature__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2200:1: ( ( ( rule__Feature__Group_3__0 )? ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2201:1: ( ( rule__Feature__Group_3__0 )? )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2201:1: ( ( rule__Feature__Group_3__0 )? )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2202:1: ( rule__Feature__Group_3__0 )?
            {
             before(grammarAccess.getFeatureAccess().getGroup_3()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2203:1: ( rule__Feature__Group_3__0 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==26) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2203:2: rule__Feature__Group_3__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_3__0_in_rule__Feature__Group__3__Impl4542);
                    rule__Feature__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFeatureAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__3__Impl"


    // $ANTLR start "rule__Feature__Group_2__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2221:1: rule__Feature__Group_2__0 : rule__Feature__Group_2__0__Impl rule__Feature__Group_2__1 ;
    public final void rule__Feature__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2225:1: ( rule__Feature__Group_2__0__Impl rule__Feature__Group_2__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2226:2: rule__Feature__Group_2__0__Impl rule__Feature__Group_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_2__0__Impl_in_rule__Feature__Group_2__04581);
            rule__Feature__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_2__1_in_rule__Feature__Group_2__04584);
            rule__Feature__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_2__0"


    // $ANTLR start "rule__Feature__Group_2__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2233:1: rule__Feature__Group_2__0__Impl : ( '[' ) ;
    public final void rule__Feature__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2237:1: ( ( '[' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2238:1: ( '[' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2238:1: ( '[' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2239:1: '['
            {
             before(grammarAccess.getFeatureAccess().getLeftSquareBracketKeyword_2_0()); 
            match(input,32,FollowSets000.FOLLOW_32_in_rule__Feature__Group_2__0__Impl4612); 
             after(grammarAccess.getFeatureAccess().getLeftSquareBracketKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_2__0__Impl"


    // $ANTLR start "rule__Feature__Group_2__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2252:1: rule__Feature__Group_2__1 : rule__Feature__Group_2__1__Impl rule__Feature__Group_2__2 ;
    public final void rule__Feature__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2256:1: ( rule__Feature__Group_2__1__Impl rule__Feature__Group_2__2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2257:2: rule__Feature__Group_2__1__Impl rule__Feature__Group_2__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_2__1__Impl_in_rule__Feature__Group_2__14643);
            rule__Feature__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_2__2_in_rule__Feature__Group_2__14646);
            rule__Feature__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_2__1"


    // $ANTLR start "rule__Feature__Group_2__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2264:1: rule__Feature__Group_2__1__Impl : ( ( rule__Feature__MinAssignment_2_1 ) ) ;
    public final void rule__Feature__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2268:1: ( ( ( rule__Feature__MinAssignment_2_1 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2269:1: ( ( rule__Feature__MinAssignment_2_1 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2269:1: ( ( rule__Feature__MinAssignment_2_1 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2270:1: ( rule__Feature__MinAssignment_2_1 )
            {
             before(grammarAccess.getFeatureAccess().getMinAssignment_2_1()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2271:1: ( rule__Feature__MinAssignment_2_1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2271:2: rule__Feature__MinAssignment_2_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__MinAssignment_2_1_in_rule__Feature__Group_2__1__Impl4673);
            rule__Feature__MinAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getMinAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_2__1__Impl"


    // $ANTLR start "rule__Feature__Group_2__2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2281:1: rule__Feature__Group_2__2 : rule__Feature__Group_2__2__Impl rule__Feature__Group_2__3 ;
    public final void rule__Feature__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2285:1: ( rule__Feature__Group_2__2__Impl rule__Feature__Group_2__3 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2286:2: rule__Feature__Group_2__2__Impl rule__Feature__Group_2__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_2__2__Impl_in_rule__Feature__Group_2__24703);
            rule__Feature__Group_2__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_2__3_in_rule__Feature__Group_2__24706);
            rule__Feature__Group_2__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_2__2"


    // $ANTLR start "rule__Feature__Group_2__2__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2293:1: rule__Feature__Group_2__2__Impl : ( '..' ) ;
    public final void rule__Feature__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2297:1: ( ( '..' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2298:1: ( '..' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2298:1: ( '..' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2299:1: '..'
            {
             before(grammarAccess.getFeatureAccess().getFullStopFullStopKeyword_2_2()); 
            match(input,34,FollowSets000.FOLLOW_34_in_rule__Feature__Group_2__2__Impl4734); 
             after(grammarAccess.getFeatureAccess().getFullStopFullStopKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_2__2__Impl"


    // $ANTLR start "rule__Feature__Group_2__3"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2312:1: rule__Feature__Group_2__3 : rule__Feature__Group_2__3__Impl rule__Feature__Group_2__4 ;
    public final void rule__Feature__Group_2__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2316:1: ( rule__Feature__Group_2__3__Impl rule__Feature__Group_2__4 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2317:2: rule__Feature__Group_2__3__Impl rule__Feature__Group_2__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_2__3__Impl_in_rule__Feature__Group_2__34765);
            rule__Feature__Group_2__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_2__4_in_rule__Feature__Group_2__34768);
            rule__Feature__Group_2__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_2__3"


    // $ANTLR start "rule__Feature__Group_2__3__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2324:1: rule__Feature__Group_2__3__Impl : ( ( rule__Feature__MaxAssignment_2_3 ) ) ;
    public final void rule__Feature__Group_2__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2328:1: ( ( ( rule__Feature__MaxAssignment_2_3 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2329:1: ( ( rule__Feature__MaxAssignment_2_3 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2329:1: ( ( rule__Feature__MaxAssignment_2_3 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2330:1: ( rule__Feature__MaxAssignment_2_3 )
            {
             before(grammarAccess.getFeatureAccess().getMaxAssignment_2_3()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2331:1: ( rule__Feature__MaxAssignment_2_3 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2331:2: rule__Feature__MaxAssignment_2_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__MaxAssignment_2_3_in_rule__Feature__Group_2__3__Impl4795);
            rule__Feature__MaxAssignment_2_3();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getMaxAssignment_2_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_2__3__Impl"


    // $ANTLR start "rule__Feature__Group_2__4"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2341:1: rule__Feature__Group_2__4 : rule__Feature__Group_2__4__Impl ;
    public final void rule__Feature__Group_2__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2345:1: ( rule__Feature__Group_2__4__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2346:2: rule__Feature__Group_2__4__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_2__4__Impl_in_rule__Feature__Group_2__44825);
            rule__Feature__Group_2__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_2__4"


    // $ANTLR start "rule__Feature__Group_2__4__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2352:1: rule__Feature__Group_2__4__Impl : ( ']' ) ;
    public final void rule__Feature__Group_2__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2356:1: ( ( ']' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2357:1: ( ']' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2357:1: ( ']' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2358:1: ']'
            {
             before(grammarAccess.getFeatureAccess().getRightSquareBracketKeyword_2_4()); 
            match(input,33,FollowSets000.FOLLOW_33_in_rule__Feature__Group_2__4__Impl4853); 
             after(grammarAccess.getFeatureAccess().getRightSquareBracketKeyword_2_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_2__4__Impl"


    // $ANTLR start "rule__Feature__Group_3__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2381:1: rule__Feature__Group_3__0 : rule__Feature__Group_3__0__Impl rule__Feature__Group_3__1 ;
    public final void rule__Feature__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2385:1: ( rule__Feature__Group_3__0__Impl rule__Feature__Group_3__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2386:2: rule__Feature__Group_3__0__Impl rule__Feature__Group_3__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_3__0__Impl_in_rule__Feature__Group_3__04894);
            rule__Feature__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_3__1_in_rule__Feature__Group_3__04897);
            rule__Feature__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_3__0"


    // $ANTLR start "rule__Feature__Group_3__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2393:1: rule__Feature__Group_3__0__Impl : ( '{' ) ;
    public final void rule__Feature__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2397:1: ( ( '{' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2398:1: ( '{' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2398:1: ( '{' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2399:1: '{'
            {
             before(grammarAccess.getFeatureAccess().getLeftCurlyBracketKeyword_3_0()); 
            match(input,26,FollowSets000.FOLLOW_26_in_rule__Feature__Group_3__0__Impl4925); 
             after(grammarAccess.getFeatureAccess().getLeftCurlyBracketKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_3__0__Impl"


    // $ANTLR start "rule__Feature__Group_3__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2412:1: rule__Feature__Group_3__1 : rule__Feature__Group_3__1__Impl rule__Feature__Group_3__2 ;
    public final void rule__Feature__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2416:1: ( rule__Feature__Group_3__1__Impl rule__Feature__Group_3__2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2417:2: rule__Feature__Group_3__1__Impl rule__Feature__Group_3__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_3__1__Impl_in_rule__Feature__Group_3__14956);
            rule__Feature__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_3__2_in_rule__Feature__Group_3__14959);
            rule__Feature__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_3__1"


    // $ANTLR start "rule__Feature__Group_3__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2424:1: rule__Feature__Group_3__1__Impl : ( ( rule__Feature__GroupsAssignment_3_1 ) ) ;
    public final void rule__Feature__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2428:1: ( ( ( rule__Feature__GroupsAssignment_3_1 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2429:1: ( ( rule__Feature__GroupsAssignment_3_1 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2429:1: ( ( rule__Feature__GroupsAssignment_3_1 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2430:1: ( rule__Feature__GroupsAssignment_3_1 )
            {
             before(grammarAccess.getFeatureAccess().getGroupsAssignment_3_1()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2431:1: ( rule__Feature__GroupsAssignment_3_1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2431:2: rule__Feature__GroupsAssignment_3_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__GroupsAssignment_3_1_in_rule__Feature__Group_3__1__Impl4986);
            rule__Feature__GroupsAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getGroupsAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_3__1__Impl"


    // $ANTLR start "rule__Feature__Group_3__2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2441:1: rule__Feature__Group_3__2 : rule__Feature__Group_3__2__Impl rule__Feature__Group_3__3 ;
    public final void rule__Feature__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2445:1: ( rule__Feature__Group_3__2__Impl rule__Feature__Group_3__3 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2446:2: rule__Feature__Group_3__2__Impl rule__Feature__Group_3__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_3__2__Impl_in_rule__Feature__Group_3__25016);
            rule__Feature__Group_3__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_3__3_in_rule__Feature__Group_3__25019);
            rule__Feature__Group_3__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_3__2"


    // $ANTLR start "rule__Feature__Group_3__2__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2453:1: rule__Feature__Group_3__2__Impl : ( ( rule__Feature__Group_3_2__0 )* ) ;
    public final void rule__Feature__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2457:1: ( ( ( rule__Feature__Group_3_2__0 )* ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2458:1: ( ( rule__Feature__Group_3_2__0 )* )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2458:1: ( ( rule__Feature__Group_3_2__0 )* )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2459:1: ( rule__Feature__Group_3_2__0 )*
            {
             before(grammarAccess.getFeatureAccess().getGroup_3_2()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2460:1: ( rule__Feature__Group_3_2__0 )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==29) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2460:2: rule__Feature__Group_3_2__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_3_2__0_in_rule__Feature__Group_3__2__Impl5046);
            	    rule__Feature__Group_3_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

             after(grammarAccess.getFeatureAccess().getGroup_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_3__2__Impl"


    // $ANTLR start "rule__Feature__Group_3__3"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2470:1: rule__Feature__Group_3__3 : rule__Feature__Group_3__3__Impl ;
    public final void rule__Feature__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2474:1: ( rule__Feature__Group_3__3__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2475:2: rule__Feature__Group_3__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_3__3__Impl_in_rule__Feature__Group_3__35077);
            rule__Feature__Group_3__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_3__3"


    // $ANTLR start "rule__Feature__Group_3__3__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2481:1: rule__Feature__Group_3__3__Impl : ( '}' ) ;
    public final void rule__Feature__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2485:1: ( ( '}' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2486:1: ( '}' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2486:1: ( '}' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2487:1: '}'
            {
             before(grammarAccess.getFeatureAccess().getRightCurlyBracketKeyword_3_3()); 
            match(input,27,FollowSets000.FOLLOW_27_in_rule__Feature__Group_3__3__Impl5105); 
             after(grammarAccess.getFeatureAccess().getRightCurlyBracketKeyword_3_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_3__3__Impl"


    // $ANTLR start "rule__Feature__Group_3_2__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2508:1: rule__Feature__Group_3_2__0 : rule__Feature__Group_3_2__0__Impl rule__Feature__Group_3_2__1 ;
    public final void rule__Feature__Group_3_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2512:1: ( rule__Feature__Group_3_2__0__Impl rule__Feature__Group_3_2__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2513:2: rule__Feature__Group_3_2__0__Impl rule__Feature__Group_3_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_3_2__0__Impl_in_rule__Feature__Group_3_2__05144);
            rule__Feature__Group_3_2__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_3_2__1_in_rule__Feature__Group_3_2__05147);
            rule__Feature__Group_3_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_3_2__0"


    // $ANTLR start "rule__Feature__Group_3_2__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2520:1: rule__Feature__Group_3_2__0__Impl : ( ',' ) ;
    public final void rule__Feature__Group_3_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2524:1: ( ( ',' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2525:1: ( ',' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2525:1: ( ',' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2526:1: ','
            {
             before(grammarAccess.getFeatureAccess().getCommaKeyword_3_2_0()); 
            match(input,29,FollowSets000.FOLLOW_29_in_rule__Feature__Group_3_2__0__Impl5175); 
             after(grammarAccess.getFeatureAccess().getCommaKeyword_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_3_2__0__Impl"


    // $ANTLR start "rule__Feature__Group_3_2__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2539:1: rule__Feature__Group_3_2__1 : rule__Feature__Group_3_2__1__Impl ;
    public final void rule__Feature__Group_3_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2543:1: ( rule__Feature__Group_3_2__1__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2544:2: rule__Feature__Group_3_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__Group_3_2__1__Impl_in_rule__Feature__Group_3_2__15206);
            rule__Feature__Group_3_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_3_2__1"


    // $ANTLR start "rule__Feature__Group_3_2__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2550:1: rule__Feature__Group_3_2__1__Impl : ( ( rule__Feature__GroupsAssignment_3_2_1 ) ) ;
    public final void rule__Feature__Group_3_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2554:1: ( ( ( rule__Feature__GroupsAssignment_3_2_1 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2555:1: ( ( rule__Feature__GroupsAssignment_3_2_1 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2555:1: ( ( rule__Feature__GroupsAssignment_3_2_1 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2556:1: ( rule__Feature__GroupsAssignment_3_2_1 )
            {
             before(grammarAccess.getFeatureAccess().getGroupsAssignment_3_2_1()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2557:1: ( rule__Feature__GroupsAssignment_3_2_1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2557:2: rule__Feature__GroupsAssignment_3_2_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__GroupsAssignment_3_2_1_in_rule__Feature__Group_3_2__1__Impl5233);
            rule__Feature__GroupsAssignment_3_2_1();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getGroupsAssignment_3_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_3_2__1__Impl"


    // $ANTLR start "rule__BOOLEAN__Group__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2571:1: rule__BOOLEAN__Group__0 : rule__BOOLEAN__Group__0__Impl rule__BOOLEAN__Group__1 ;
    public final void rule__BOOLEAN__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2575:1: ( rule__BOOLEAN__Group__0__Impl rule__BOOLEAN__Group__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2576:2: rule__BOOLEAN__Group__0__Impl rule__BOOLEAN__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__BOOLEAN__Group__0__Impl_in_rule__BOOLEAN__Group__05267);
            rule__BOOLEAN__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__BOOLEAN__Group__1_in_rule__BOOLEAN__Group__05270);
            rule__BOOLEAN__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BOOLEAN__Group__0"


    // $ANTLR start "rule__BOOLEAN__Group__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2583:1: rule__BOOLEAN__Group__0__Impl : ( () ) ;
    public final void rule__BOOLEAN__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2587:1: ( ( () ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2588:1: ( () )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2588:1: ( () )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2589:1: ()
            {
             before(grammarAccess.getBOOLEANAccess().getBOOLEANAction_0()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2590:1: ()
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2592:1: 
            {
            }

             after(grammarAccess.getBOOLEANAccess().getBOOLEANAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BOOLEAN__Group__0__Impl"


    // $ANTLR start "rule__BOOLEAN__Group__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2602:1: rule__BOOLEAN__Group__1 : rule__BOOLEAN__Group__1__Impl rule__BOOLEAN__Group__2 ;
    public final void rule__BOOLEAN__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2606:1: ( rule__BOOLEAN__Group__1__Impl rule__BOOLEAN__Group__2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2607:2: rule__BOOLEAN__Group__1__Impl rule__BOOLEAN__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__BOOLEAN__Group__1__Impl_in_rule__BOOLEAN__Group__15328);
            rule__BOOLEAN__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__BOOLEAN__Group__2_in_rule__BOOLEAN__Group__15331);
            rule__BOOLEAN__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BOOLEAN__Group__1"


    // $ANTLR start "rule__BOOLEAN__Group__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2614:1: rule__BOOLEAN__Group__1__Impl : ( 'Boolean' ) ;
    public final void rule__BOOLEAN__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2618:1: ( ( 'Boolean' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2619:1: ( 'Boolean' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2619:1: ( 'Boolean' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2620:1: 'Boolean'
            {
             before(grammarAccess.getBOOLEANAccess().getBooleanKeyword_1()); 
            match(input,35,FollowSets000.FOLLOW_35_in_rule__BOOLEAN__Group__1__Impl5359); 
             after(grammarAccess.getBOOLEANAccess().getBooleanKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BOOLEAN__Group__1__Impl"


    // $ANTLR start "rule__BOOLEAN__Group__2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2633:1: rule__BOOLEAN__Group__2 : rule__BOOLEAN__Group__2__Impl ;
    public final void rule__BOOLEAN__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2637:1: ( rule__BOOLEAN__Group__2__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2638:2: rule__BOOLEAN__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__BOOLEAN__Group__2__Impl_in_rule__BOOLEAN__Group__25390);
            rule__BOOLEAN__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BOOLEAN__Group__2"


    // $ANTLR start "rule__BOOLEAN__Group__2__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2644:1: rule__BOOLEAN__Group__2__Impl : ( ( rule__BOOLEAN__NameAssignment_2 ) ) ;
    public final void rule__BOOLEAN__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2648:1: ( ( ( rule__BOOLEAN__NameAssignment_2 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2649:1: ( ( rule__BOOLEAN__NameAssignment_2 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2649:1: ( ( rule__BOOLEAN__NameAssignment_2 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2650:1: ( rule__BOOLEAN__NameAssignment_2 )
            {
             before(grammarAccess.getBOOLEANAccess().getNameAssignment_2()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2651:1: ( rule__BOOLEAN__NameAssignment_2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2651:2: rule__BOOLEAN__NameAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__BOOLEAN__NameAssignment_2_in_rule__BOOLEAN__Group__2__Impl5417);
            rule__BOOLEAN__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getBOOLEANAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BOOLEAN__Group__2__Impl"


    // $ANTLR start "rule__INTEGER__Group__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2667:1: rule__INTEGER__Group__0 : rule__INTEGER__Group__0__Impl rule__INTEGER__Group__1 ;
    public final void rule__INTEGER__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2671:1: ( rule__INTEGER__Group__0__Impl rule__INTEGER__Group__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2672:2: rule__INTEGER__Group__0__Impl rule__INTEGER__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__INTEGER__Group__0__Impl_in_rule__INTEGER__Group__05453);
            rule__INTEGER__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__INTEGER__Group__1_in_rule__INTEGER__Group__05456);
            rule__INTEGER__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__INTEGER__Group__0"


    // $ANTLR start "rule__INTEGER__Group__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2679:1: rule__INTEGER__Group__0__Impl : ( () ) ;
    public final void rule__INTEGER__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2683:1: ( ( () ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2684:1: ( () )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2684:1: ( () )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2685:1: ()
            {
             before(grammarAccess.getINTEGERAccess().getINTEGERAction_0()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2686:1: ()
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2688:1: 
            {
            }

             after(grammarAccess.getINTEGERAccess().getINTEGERAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__INTEGER__Group__0__Impl"


    // $ANTLR start "rule__INTEGER__Group__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2698:1: rule__INTEGER__Group__1 : rule__INTEGER__Group__1__Impl rule__INTEGER__Group__2 ;
    public final void rule__INTEGER__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2702:1: ( rule__INTEGER__Group__1__Impl rule__INTEGER__Group__2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2703:2: rule__INTEGER__Group__1__Impl rule__INTEGER__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__INTEGER__Group__1__Impl_in_rule__INTEGER__Group__15514);
            rule__INTEGER__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__INTEGER__Group__2_in_rule__INTEGER__Group__15517);
            rule__INTEGER__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__INTEGER__Group__1"


    // $ANTLR start "rule__INTEGER__Group__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2710:1: rule__INTEGER__Group__1__Impl : ( 'Integer' ) ;
    public final void rule__INTEGER__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2714:1: ( ( 'Integer' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2715:1: ( 'Integer' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2715:1: ( 'Integer' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2716:1: 'Integer'
            {
             before(grammarAccess.getINTEGERAccess().getIntegerKeyword_1()); 
            match(input,36,FollowSets000.FOLLOW_36_in_rule__INTEGER__Group__1__Impl5545); 
             after(grammarAccess.getINTEGERAccess().getIntegerKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__INTEGER__Group__1__Impl"


    // $ANTLR start "rule__INTEGER__Group__2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2729:1: rule__INTEGER__Group__2 : rule__INTEGER__Group__2__Impl ;
    public final void rule__INTEGER__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2733:1: ( rule__INTEGER__Group__2__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2734:2: rule__INTEGER__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__INTEGER__Group__2__Impl_in_rule__INTEGER__Group__25576);
            rule__INTEGER__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__INTEGER__Group__2"


    // $ANTLR start "rule__INTEGER__Group__2__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2740:1: rule__INTEGER__Group__2__Impl : ( ( rule__INTEGER__NameAssignment_2 ) ) ;
    public final void rule__INTEGER__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2744:1: ( ( ( rule__INTEGER__NameAssignment_2 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2745:1: ( ( rule__INTEGER__NameAssignment_2 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2745:1: ( ( rule__INTEGER__NameAssignment_2 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2746:1: ( rule__INTEGER__NameAssignment_2 )
            {
             before(grammarAccess.getINTEGERAccess().getNameAssignment_2()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2747:1: ( rule__INTEGER__NameAssignment_2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2747:2: rule__INTEGER__NameAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__INTEGER__NameAssignment_2_in_rule__INTEGER__Group__2__Impl5603);
            rule__INTEGER__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getINTEGERAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__INTEGER__Group__2__Impl"


    // $ANTLR start "rule__ENUM__Group__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2763:1: rule__ENUM__Group__0 : rule__ENUM__Group__0__Impl rule__ENUM__Group__1 ;
    public final void rule__ENUM__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2767:1: ( rule__ENUM__Group__0__Impl rule__ENUM__Group__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2768:2: rule__ENUM__Group__0__Impl rule__ENUM__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ENUM__Group__0__Impl_in_rule__ENUM__Group__05639);
            rule__ENUM__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__ENUM__Group__1_in_rule__ENUM__Group__05642);
            rule__ENUM__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__Group__0"


    // $ANTLR start "rule__ENUM__Group__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2775:1: rule__ENUM__Group__0__Impl : ( () ) ;
    public final void rule__ENUM__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2779:1: ( ( () ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2780:1: ( () )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2780:1: ( () )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2781:1: ()
            {
             before(grammarAccess.getENUMAccess().getENUMAction_0()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2782:1: ()
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2784:1: 
            {
            }

             after(grammarAccess.getENUMAccess().getENUMAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__Group__0__Impl"


    // $ANTLR start "rule__ENUM__Group__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2794:1: rule__ENUM__Group__1 : rule__ENUM__Group__1__Impl rule__ENUM__Group__2 ;
    public final void rule__ENUM__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2798:1: ( rule__ENUM__Group__1__Impl rule__ENUM__Group__2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2799:2: rule__ENUM__Group__1__Impl rule__ENUM__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__ENUM__Group__1__Impl_in_rule__ENUM__Group__15700);
            rule__ENUM__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__ENUM__Group__2_in_rule__ENUM__Group__15703);
            rule__ENUM__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__Group__1"


    // $ANTLR start "rule__ENUM__Group__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2806:1: rule__ENUM__Group__1__Impl : ( ( rule__ENUM__NameAssignment_1 ) ) ;
    public final void rule__ENUM__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2810:1: ( ( ( rule__ENUM__NameAssignment_1 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2811:1: ( ( rule__ENUM__NameAssignment_1 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2811:1: ( ( rule__ENUM__NameAssignment_1 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2812:1: ( rule__ENUM__NameAssignment_1 )
            {
             before(grammarAccess.getENUMAccess().getNameAssignment_1()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2813:1: ( rule__ENUM__NameAssignment_1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2813:2: rule__ENUM__NameAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ENUM__NameAssignment_1_in_rule__ENUM__Group__1__Impl5730);
            rule__ENUM__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getENUMAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__Group__1__Impl"


    // $ANTLR start "rule__ENUM__Group__2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2823:1: rule__ENUM__Group__2 : rule__ENUM__Group__2__Impl rule__ENUM__Group__3 ;
    public final void rule__ENUM__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2827:1: ( rule__ENUM__Group__2__Impl rule__ENUM__Group__3 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2828:2: rule__ENUM__Group__2__Impl rule__ENUM__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__ENUM__Group__2__Impl_in_rule__ENUM__Group__25760);
            rule__ENUM__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__ENUM__Group__3_in_rule__ENUM__Group__25763);
            rule__ENUM__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__Group__2"


    // $ANTLR start "rule__ENUM__Group__2__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2835:1: rule__ENUM__Group__2__Impl : ( '{' ) ;
    public final void rule__ENUM__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2839:1: ( ( '{' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2840:1: ( '{' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2840:1: ( '{' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2841:1: '{'
            {
             before(grammarAccess.getENUMAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,26,FollowSets000.FOLLOW_26_in_rule__ENUM__Group__2__Impl5791); 
             after(grammarAccess.getENUMAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__Group__2__Impl"


    // $ANTLR start "rule__ENUM__Group__3"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2854:1: rule__ENUM__Group__3 : rule__ENUM__Group__3__Impl rule__ENUM__Group__4 ;
    public final void rule__ENUM__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2858:1: ( rule__ENUM__Group__3__Impl rule__ENUM__Group__4 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2859:2: rule__ENUM__Group__3__Impl rule__ENUM__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__ENUM__Group__3__Impl_in_rule__ENUM__Group__35822);
            rule__ENUM__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__ENUM__Group__4_in_rule__ENUM__Group__35825);
            rule__ENUM__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__Group__3"


    // $ANTLR start "rule__ENUM__Group__3__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2866:1: rule__ENUM__Group__3__Impl : ( ( rule__ENUM__EnumValuesAssignment_3 ) ) ;
    public final void rule__ENUM__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2870:1: ( ( ( rule__ENUM__EnumValuesAssignment_3 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2871:1: ( ( rule__ENUM__EnumValuesAssignment_3 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2871:1: ( ( rule__ENUM__EnumValuesAssignment_3 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2872:1: ( rule__ENUM__EnumValuesAssignment_3 )
            {
             before(grammarAccess.getENUMAccess().getEnumValuesAssignment_3()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2873:1: ( rule__ENUM__EnumValuesAssignment_3 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2873:2: rule__ENUM__EnumValuesAssignment_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__ENUM__EnumValuesAssignment_3_in_rule__ENUM__Group__3__Impl5852);
            rule__ENUM__EnumValuesAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getENUMAccess().getEnumValuesAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__Group__3__Impl"


    // $ANTLR start "rule__ENUM__Group__4"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2883:1: rule__ENUM__Group__4 : rule__ENUM__Group__4__Impl rule__ENUM__Group__5 ;
    public final void rule__ENUM__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2887:1: ( rule__ENUM__Group__4__Impl rule__ENUM__Group__5 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2888:2: rule__ENUM__Group__4__Impl rule__ENUM__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__ENUM__Group__4__Impl_in_rule__ENUM__Group__45882);
            rule__ENUM__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__ENUM__Group__5_in_rule__ENUM__Group__45885);
            rule__ENUM__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__Group__4"


    // $ANTLR start "rule__ENUM__Group__4__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2895:1: rule__ENUM__Group__4__Impl : ( ( rule__ENUM__Group_4__0 )* ) ;
    public final void rule__ENUM__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2899:1: ( ( ( rule__ENUM__Group_4__0 )* ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2900:1: ( ( rule__ENUM__Group_4__0 )* )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2900:1: ( ( rule__ENUM__Group_4__0 )* )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2901:1: ( rule__ENUM__Group_4__0 )*
            {
             before(grammarAccess.getENUMAccess().getGroup_4()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2902:1: ( rule__ENUM__Group_4__0 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==29) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2902:2: rule__ENUM__Group_4__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__ENUM__Group_4__0_in_rule__ENUM__Group__4__Impl5912);
            	    rule__ENUM__Group_4__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

             after(grammarAccess.getENUMAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__Group__4__Impl"


    // $ANTLR start "rule__ENUM__Group__5"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2912:1: rule__ENUM__Group__5 : rule__ENUM__Group__5__Impl ;
    public final void rule__ENUM__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2916:1: ( rule__ENUM__Group__5__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2917:2: rule__ENUM__Group__5__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__ENUM__Group__5__Impl_in_rule__ENUM__Group__55943);
            rule__ENUM__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__Group__5"


    // $ANTLR start "rule__ENUM__Group__5__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2923:1: rule__ENUM__Group__5__Impl : ( '}' ) ;
    public final void rule__ENUM__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2927:1: ( ( '}' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2928:1: ( '}' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2928:1: ( '}' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2929:1: '}'
            {
             before(grammarAccess.getENUMAccess().getRightCurlyBracketKeyword_5()); 
            match(input,27,FollowSets000.FOLLOW_27_in_rule__ENUM__Group__5__Impl5971); 
             after(grammarAccess.getENUMAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__Group__5__Impl"


    // $ANTLR start "rule__ENUM__Group_4__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2954:1: rule__ENUM__Group_4__0 : rule__ENUM__Group_4__0__Impl rule__ENUM__Group_4__1 ;
    public final void rule__ENUM__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2958:1: ( rule__ENUM__Group_4__0__Impl rule__ENUM__Group_4__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2959:2: rule__ENUM__Group_4__0__Impl rule__ENUM__Group_4__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ENUM__Group_4__0__Impl_in_rule__ENUM__Group_4__06014);
            rule__ENUM__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__ENUM__Group_4__1_in_rule__ENUM__Group_4__06017);
            rule__ENUM__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__Group_4__0"


    // $ANTLR start "rule__ENUM__Group_4__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2966:1: rule__ENUM__Group_4__0__Impl : ( ',' ) ;
    public final void rule__ENUM__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2970:1: ( ( ',' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2971:1: ( ',' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2971:1: ( ',' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2972:1: ','
            {
             before(grammarAccess.getENUMAccess().getCommaKeyword_4_0()); 
            match(input,29,FollowSets000.FOLLOW_29_in_rule__ENUM__Group_4__0__Impl6045); 
             after(grammarAccess.getENUMAccess().getCommaKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__Group_4__0__Impl"


    // $ANTLR start "rule__ENUM__Group_4__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2985:1: rule__ENUM__Group_4__1 : rule__ENUM__Group_4__1__Impl ;
    public final void rule__ENUM__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2989:1: ( rule__ENUM__Group_4__1__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2990:2: rule__ENUM__Group_4__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__ENUM__Group_4__1__Impl_in_rule__ENUM__Group_4__16076);
            rule__ENUM__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__Group_4__1"


    // $ANTLR start "rule__ENUM__Group_4__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:2996:1: rule__ENUM__Group_4__1__Impl : ( ( rule__ENUM__EnumValuesAssignment_4_1 ) ) ;
    public final void rule__ENUM__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3000:1: ( ( ( rule__ENUM__EnumValuesAssignment_4_1 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3001:1: ( ( rule__ENUM__EnumValuesAssignment_4_1 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3001:1: ( ( rule__ENUM__EnumValuesAssignment_4_1 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3002:1: ( rule__ENUM__EnumValuesAssignment_4_1 )
            {
             before(grammarAccess.getENUMAccess().getEnumValuesAssignment_4_1()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3003:1: ( rule__ENUM__EnumValuesAssignment_4_1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3003:2: rule__ENUM__EnumValuesAssignment_4_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ENUM__EnumValuesAssignment_4_1_in_rule__ENUM__Group_4__1__Impl6103);
            rule__ENUM__EnumValuesAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getENUMAccess().getEnumValuesAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__Group_4__1__Impl"


    // $ANTLR start "rule__EnumValue__Group__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3017:1: rule__EnumValue__Group__0 : rule__EnumValue__Group__0__Impl rule__EnumValue__Group__1 ;
    public final void rule__EnumValue__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3021:1: ( rule__EnumValue__Group__0__Impl rule__EnumValue__Group__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3022:2: rule__EnumValue__Group__0__Impl rule__EnumValue__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__EnumValue__Group__0__Impl_in_rule__EnumValue__Group__06137);
            rule__EnumValue__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EnumValue__Group__1_in_rule__EnumValue__Group__06140);
            rule__EnumValue__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__0"


    // $ANTLR start "rule__EnumValue__Group__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3029:1: rule__EnumValue__Group__0__Impl : ( () ) ;
    public final void rule__EnumValue__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3033:1: ( ( () ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3034:1: ( () )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3034:1: ( () )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3035:1: ()
            {
             before(grammarAccess.getEnumValueAccess().getEnumValueAction_0()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3036:1: ()
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3038:1: 
            {
            }

             after(grammarAccess.getEnumValueAccess().getEnumValueAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__0__Impl"


    // $ANTLR start "rule__EnumValue__Group__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3048:1: rule__EnumValue__Group__1 : rule__EnumValue__Group__1__Impl ;
    public final void rule__EnumValue__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3052:1: ( rule__EnumValue__Group__1__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3053:2: rule__EnumValue__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__EnumValue__Group__1__Impl_in_rule__EnumValue__Group__16198);
            rule__EnumValue__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__1"


    // $ANTLR start "rule__EnumValue__Group__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3059:1: rule__EnumValue__Group__1__Impl : ( ( rule__EnumValue__NameAssignment_1 ) ) ;
    public final void rule__EnumValue__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3063:1: ( ( ( rule__EnumValue__NameAssignment_1 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3064:1: ( ( rule__EnumValue__NameAssignment_1 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3064:1: ( ( rule__EnumValue__NameAssignment_1 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3065:1: ( rule__EnumValue__NameAssignment_1 )
            {
             before(grammarAccess.getEnumValueAccess().getNameAssignment_1()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3066:1: ( rule__EnumValue__NameAssignment_1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3066:2: rule__EnumValue__NameAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__EnumValue__NameAssignment_1_in_rule__EnumValue__Group__1__Impl6225);
            rule__EnumValue__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getEnumValueAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__1__Impl"


    // $ANTLR start "rule__Group__Group__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3080:1: rule__Group__Group__0 : rule__Group__Group__0__Impl rule__Group__Group__1 ;
    public final void rule__Group__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3084:1: ( rule__Group__Group__0__Impl rule__Group__Group__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3085:2: rule__Group__Group__0__Impl rule__Group__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__Group__0__Impl_in_rule__Group__Group__06259);
            rule__Group__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Group__Group__1_in_rule__Group__Group__06262);
            rule__Group__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group__0"


    // $ANTLR start "rule__Group__Group__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3092:1: rule__Group__Group__0__Impl : ( ( rule__Group__NameAssignment_0 ) ) ;
    public final void rule__Group__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3096:1: ( ( ( rule__Group__NameAssignment_0 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3097:1: ( ( rule__Group__NameAssignment_0 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3097:1: ( ( rule__Group__NameAssignment_0 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3098:1: ( rule__Group__NameAssignment_0 )
            {
             before(grammarAccess.getGroupAccess().getNameAssignment_0()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3099:1: ( rule__Group__NameAssignment_0 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3099:2: rule__Group__NameAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__NameAssignment_0_in_rule__Group__Group__0__Impl6289);
            rule__Group__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getGroupAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group__0__Impl"


    // $ANTLR start "rule__Group__Group__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3109:1: rule__Group__Group__1 : rule__Group__Group__1__Impl rule__Group__Group__2 ;
    public final void rule__Group__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3113:1: ( rule__Group__Group__1__Impl rule__Group__Group__2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3114:2: rule__Group__Group__1__Impl rule__Group__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__Group__1__Impl_in_rule__Group__Group__16319);
            rule__Group__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Group__Group__2_in_rule__Group__Group__16322);
            rule__Group__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group__1"


    // $ANTLR start "rule__Group__Group__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3121:1: rule__Group__Group__1__Impl : ( ( rule__Group__Group_1__0 )? ) ;
    public final void rule__Group__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3125:1: ( ( ( rule__Group__Group_1__0 )? ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3126:1: ( ( rule__Group__Group_1__0 )? )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3126:1: ( ( rule__Group__Group_1__0 )? )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3127:1: ( rule__Group__Group_1__0 )?
            {
             before(grammarAccess.getGroupAccess().getGroup_1()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3128:1: ( rule__Group__Group_1__0 )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==32) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3128:2: rule__Group__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Group__Group_1__0_in_rule__Group__Group__1__Impl6349);
                    rule__Group__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getGroupAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group__1__Impl"


    // $ANTLR start "rule__Group__Group__2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3138:1: rule__Group__Group__2 : rule__Group__Group__2__Impl rule__Group__Group__3 ;
    public final void rule__Group__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3142:1: ( rule__Group__Group__2__Impl rule__Group__Group__3 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3143:2: rule__Group__Group__2__Impl rule__Group__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__Group__2__Impl_in_rule__Group__Group__26380);
            rule__Group__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Group__Group__3_in_rule__Group__Group__26383);
            rule__Group__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group__2"


    // $ANTLR start "rule__Group__Group__2__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3150:1: rule__Group__Group__2__Impl : ( '{' ) ;
    public final void rule__Group__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3154:1: ( ( '{' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3155:1: ( '{' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3155:1: ( '{' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3156:1: '{'
            {
             before(grammarAccess.getGroupAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,26,FollowSets000.FOLLOW_26_in_rule__Group__Group__2__Impl6411); 
             after(grammarAccess.getGroupAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group__2__Impl"


    // $ANTLR start "rule__Group__Group__3"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3169:1: rule__Group__Group__3 : rule__Group__Group__3__Impl rule__Group__Group__4 ;
    public final void rule__Group__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3173:1: ( rule__Group__Group__3__Impl rule__Group__Group__4 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3174:2: rule__Group__Group__3__Impl rule__Group__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__Group__3__Impl_in_rule__Group__Group__36442);
            rule__Group__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Group__Group__4_in_rule__Group__Group__36445);
            rule__Group__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group__3"


    // $ANTLR start "rule__Group__Group__3__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3181:1: rule__Group__Group__3__Impl : ( ( rule__Group__FeaturesAssignment_3 ) ) ;
    public final void rule__Group__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3185:1: ( ( ( rule__Group__FeaturesAssignment_3 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3186:1: ( ( rule__Group__FeaturesAssignment_3 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3186:1: ( ( rule__Group__FeaturesAssignment_3 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3187:1: ( rule__Group__FeaturesAssignment_3 )
            {
             before(grammarAccess.getGroupAccess().getFeaturesAssignment_3()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3188:1: ( rule__Group__FeaturesAssignment_3 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3188:2: rule__Group__FeaturesAssignment_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__FeaturesAssignment_3_in_rule__Group__Group__3__Impl6472);
            rule__Group__FeaturesAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getGroupAccess().getFeaturesAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group__3__Impl"


    // $ANTLR start "rule__Group__Group__4"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3198:1: rule__Group__Group__4 : rule__Group__Group__4__Impl rule__Group__Group__5 ;
    public final void rule__Group__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3202:1: ( rule__Group__Group__4__Impl rule__Group__Group__5 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3203:2: rule__Group__Group__4__Impl rule__Group__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__Group__4__Impl_in_rule__Group__Group__46502);
            rule__Group__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Group__Group__5_in_rule__Group__Group__46505);
            rule__Group__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group__4"


    // $ANTLR start "rule__Group__Group__4__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3210:1: rule__Group__Group__4__Impl : ( ( rule__Group__Group_4__0 )* ) ;
    public final void rule__Group__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3214:1: ( ( ( rule__Group__Group_4__0 )* ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3215:1: ( ( rule__Group__Group_4__0 )* )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3215:1: ( ( rule__Group__Group_4__0 )* )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3216:1: ( rule__Group__Group_4__0 )*
            {
             before(grammarAccess.getGroupAccess().getGroup_4()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3217:1: ( rule__Group__Group_4__0 )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==29) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3217:2: rule__Group__Group_4__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Group__Group_4__0_in_rule__Group__Group__4__Impl6532);
            	    rule__Group__Group_4__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

             after(grammarAccess.getGroupAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group__4__Impl"


    // $ANTLR start "rule__Group__Group__5"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3227:1: rule__Group__Group__5 : rule__Group__Group__5__Impl ;
    public final void rule__Group__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3231:1: ( rule__Group__Group__5__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3232:2: rule__Group__Group__5__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__Group__5__Impl_in_rule__Group__Group__56563);
            rule__Group__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group__5"


    // $ANTLR start "rule__Group__Group__5__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3238:1: rule__Group__Group__5__Impl : ( '}' ) ;
    public final void rule__Group__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3242:1: ( ( '}' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3243:1: ( '}' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3243:1: ( '}' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3244:1: '}'
            {
             before(grammarAccess.getGroupAccess().getRightCurlyBracketKeyword_5()); 
            match(input,27,FollowSets000.FOLLOW_27_in_rule__Group__Group__5__Impl6591); 
             after(grammarAccess.getGroupAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group__5__Impl"


    // $ANTLR start "rule__Group__Group_1__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3269:1: rule__Group__Group_1__0 : rule__Group__Group_1__0__Impl rule__Group__Group_1__1 ;
    public final void rule__Group__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3273:1: ( rule__Group__Group_1__0__Impl rule__Group__Group_1__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3274:2: rule__Group__Group_1__0__Impl rule__Group__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__Group_1__0__Impl_in_rule__Group__Group_1__06634);
            rule__Group__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Group__Group_1__1_in_rule__Group__Group_1__06637);
            rule__Group__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group_1__0"


    // $ANTLR start "rule__Group__Group_1__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3281:1: rule__Group__Group_1__0__Impl : ( '[' ) ;
    public final void rule__Group__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3285:1: ( ( '[' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3286:1: ( '[' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3286:1: ( '[' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3287:1: '['
            {
             before(grammarAccess.getGroupAccess().getLeftSquareBracketKeyword_1_0()); 
            match(input,32,FollowSets000.FOLLOW_32_in_rule__Group__Group_1__0__Impl6665); 
             after(grammarAccess.getGroupAccess().getLeftSquareBracketKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group_1__0__Impl"


    // $ANTLR start "rule__Group__Group_1__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3300:1: rule__Group__Group_1__1 : rule__Group__Group_1__1__Impl rule__Group__Group_1__2 ;
    public final void rule__Group__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3304:1: ( rule__Group__Group_1__1__Impl rule__Group__Group_1__2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3305:2: rule__Group__Group_1__1__Impl rule__Group__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__Group_1__1__Impl_in_rule__Group__Group_1__16696);
            rule__Group__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Group__Group_1__2_in_rule__Group__Group_1__16699);
            rule__Group__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group_1__1"


    // $ANTLR start "rule__Group__Group_1__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3312:1: rule__Group__Group_1__1__Impl : ( ( rule__Group__MinAssignment_1_1 ) ) ;
    public final void rule__Group__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3316:1: ( ( ( rule__Group__MinAssignment_1_1 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3317:1: ( ( rule__Group__MinAssignment_1_1 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3317:1: ( ( rule__Group__MinAssignment_1_1 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3318:1: ( rule__Group__MinAssignment_1_1 )
            {
             before(grammarAccess.getGroupAccess().getMinAssignment_1_1()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3319:1: ( rule__Group__MinAssignment_1_1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3319:2: rule__Group__MinAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__MinAssignment_1_1_in_rule__Group__Group_1__1__Impl6726);
            rule__Group__MinAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getGroupAccess().getMinAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group_1__1__Impl"


    // $ANTLR start "rule__Group__Group_1__2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3329:1: rule__Group__Group_1__2 : rule__Group__Group_1__2__Impl rule__Group__Group_1__3 ;
    public final void rule__Group__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3333:1: ( rule__Group__Group_1__2__Impl rule__Group__Group_1__3 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3334:2: rule__Group__Group_1__2__Impl rule__Group__Group_1__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__Group_1__2__Impl_in_rule__Group__Group_1__26756);
            rule__Group__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Group__Group_1__3_in_rule__Group__Group_1__26759);
            rule__Group__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group_1__2"


    // $ANTLR start "rule__Group__Group_1__2__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3341:1: rule__Group__Group_1__2__Impl : ( '...' ) ;
    public final void rule__Group__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3345:1: ( ( '...' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3346:1: ( '...' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3346:1: ( '...' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3347:1: '...'
            {
             before(grammarAccess.getGroupAccess().getFullStopFullStopFullStopKeyword_1_2()); 
            match(input,37,FollowSets000.FOLLOW_37_in_rule__Group__Group_1__2__Impl6787); 
             after(grammarAccess.getGroupAccess().getFullStopFullStopFullStopKeyword_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group_1__2__Impl"


    // $ANTLR start "rule__Group__Group_1__3"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3360:1: rule__Group__Group_1__3 : rule__Group__Group_1__3__Impl rule__Group__Group_1__4 ;
    public final void rule__Group__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3364:1: ( rule__Group__Group_1__3__Impl rule__Group__Group_1__4 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3365:2: rule__Group__Group_1__3__Impl rule__Group__Group_1__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__Group_1__3__Impl_in_rule__Group__Group_1__36818);
            rule__Group__Group_1__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Group__Group_1__4_in_rule__Group__Group_1__36821);
            rule__Group__Group_1__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group_1__3"


    // $ANTLR start "rule__Group__Group_1__3__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3372:1: rule__Group__Group_1__3__Impl : ( ( rule__Group__MaxAssignment_1_3 ) ) ;
    public final void rule__Group__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3376:1: ( ( ( rule__Group__MaxAssignment_1_3 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3377:1: ( ( rule__Group__MaxAssignment_1_3 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3377:1: ( ( rule__Group__MaxAssignment_1_3 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3378:1: ( rule__Group__MaxAssignment_1_3 )
            {
             before(grammarAccess.getGroupAccess().getMaxAssignment_1_3()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3379:1: ( rule__Group__MaxAssignment_1_3 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3379:2: rule__Group__MaxAssignment_1_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__MaxAssignment_1_3_in_rule__Group__Group_1__3__Impl6848);
            rule__Group__MaxAssignment_1_3();

            state._fsp--;


            }

             after(grammarAccess.getGroupAccess().getMaxAssignment_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group_1__3__Impl"


    // $ANTLR start "rule__Group__Group_1__4"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3389:1: rule__Group__Group_1__4 : rule__Group__Group_1__4__Impl ;
    public final void rule__Group__Group_1__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3393:1: ( rule__Group__Group_1__4__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3394:2: rule__Group__Group_1__4__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__Group_1__4__Impl_in_rule__Group__Group_1__46878);
            rule__Group__Group_1__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group_1__4"


    // $ANTLR start "rule__Group__Group_1__4__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3400:1: rule__Group__Group_1__4__Impl : ( ']' ) ;
    public final void rule__Group__Group_1__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3404:1: ( ( ']' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3405:1: ( ']' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3405:1: ( ']' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3406:1: ']'
            {
             before(grammarAccess.getGroupAccess().getRightSquareBracketKeyword_1_4()); 
            match(input,33,FollowSets000.FOLLOW_33_in_rule__Group__Group_1__4__Impl6906); 
             after(grammarAccess.getGroupAccess().getRightSquareBracketKeyword_1_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group_1__4__Impl"


    // $ANTLR start "rule__Group__Group_4__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3429:1: rule__Group__Group_4__0 : rule__Group__Group_4__0__Impl rule__Group__Group_4__1 ;
    public final void rule__Group__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3433:1: ( rule__Group__Group_4__0__Impl rule__Group__Group_4__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3434:2: rule__Group__Group_4__0__Impl rule__Group__Group_4__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__Group_4__0__Impl_in_rule__Group__Group_4__06947);
            rule__Group__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Group__Group_4__1_in_rule__Group__Group_4__06950);
            rule__Group__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group_4__0"


    // $ANTLR start "rule__Group__Group_4__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3441:1: rule__Group__Group_4__0__Impl : ( ',' ) ;
    public final void rule__Group__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3445:1: ( ( ',' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3446:1: ( ',' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3446:1: ( ',' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3447:1: ','
            {
             before(grammarAccess.getGroupAccess().getCommaKeyword_4_0()); 
            match(input,29,FollowSets000.FOLLOW_29_in_rule__Group__Group_4__0__Impl6978); 
             after(grammarAccess.getGroupAccess().getCommaKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group_4__0__Impl"


    // $ANTLR start "rule__Group__Group_4__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3460:1: rule__Group__Group_4__1 : rule__Group__Group_4__1__Impl ;
    public final void rule__Group__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3464:1: ( rule__Group__Group_4__1__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3465:2: rule__Group__Group_4__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__Group_4__1__Impl_in_rule__Group__Group_4__17009);
            rule__Group__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group_4__1"


    // $ANTLR start "rule__Group__Group_4__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3471:1: rule__Group__Group_4__1__Impl : ( ( rule__Group__FeaturesAssignment_4_1 ) ) ;
    public final void rule__Group__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3475:1: ( ( ( rule__Group__FeaturesAssignment_4_1 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3476:1: ( ( rule__Group__FeaturesAssignment_4_1 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3476:1: ( ( rule__Group__FeaturesAssignment_4_1 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3477:1: ( rule__Group__FeaturesAssignment_4_1 )
            {
             before(grammarAccess.getGroupAccess().getFeaturesAssignment_4_1()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3478:1: ( rule__Group__FeaturesAssignment_4_1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3478:2: rule__Group__FeaturesAssignment_4_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Group__FeaturesAssignment_4_1_in_rule__Group__Group_4__1__Impl7036);
            rule__Group__FeaturesAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getGroupAccess().getFeaturesAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Group_4__1__Impl"


    // $ANTLR start "rule__Unary__Group__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3492:1: rule__Unary__Group__0 : rule__Unary__Group__0__Impl rule__Unary__Group__1 ;
    public final void rule__Unary__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3496:1: ( rule__Unary__Group__0__Impl rule__Unary__Group__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3497:2: rule__Unary__Group__0__Impl rule__Unary__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Unary__Group__0__Impl_in_rule__Unary__Group__07070);
            rule__Unary__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Unary__Group__1_in_rule__Unary__Group__07073);
            rule__Unary__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__Group__0"


    // $ANTLR start "rule__Unary__Group__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3504:1: rule__Unary__Group__0__Impl : ( ( rule__Unary__OperatorAssignment_0 ) ) ;
    public final void rule__Unary__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3508:1: ( ( ( rule__Unary__OperatorAssignment_0 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3509:1: ( ( rule__Unary__OperatorAssignment_0 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3509:1: ( ( rule__Unary__OperatorAssignment_0 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3510:1: ( rule__Unary__OperatorAssignment_0 )
            {
             before(grammarAccess.getUnaryAccess().getOperatorAssignment_0()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3511:1: ( rule__Unary__OperatorAssignment_0 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3511:2: rule__Unary__OperatorAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Unary__OperatorAssignment_0_in_rule__Unary__Group__0__Impl7100);
            rule__Unary__OperatorAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getUnaryAccess().getOperatorAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__Group__0__Impl"


    // $ANTLR start "rule__Unary__Group__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3521:1: rule__Unary__Group__1 : rule__Unary__Group__1__Impl ;
    public final void rule__Unary__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3525:1: ( rule__Unary__Group__1__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3526:2: rule__Unary__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Unary__Group__1__Impl_in_rule__Unary__Group__17130);
            rule__Unary__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__Group__1"


    // $ANTLR start "rule__Unary__Group__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3532:1: rule__Unary__Group__1__Impl : ( ( rule__Unary__InnerExpressionAssignment_1 ) ) ;
    public final void rule__Unary__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3536:1: ( ( ( rule__Unary__InnerExpressionAssignment_1 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3537:1: ( ( rule__Unary__InnerExpressionAssignment_1 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3537:1: ( ( rule__Unary__InnerExpressionAssignment_1 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3538:1: ( rule__Unary__InnerExpressionAssignment_1 )
            {
             before(grammarAccess.getUnaryAccess().getInnerExpressionAssignment_1()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3539:1: ( rule__Unary__InnerExpressionAssignment_1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3539:2: rule__Unary__InnerExpressionAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Unary__InnerExpressionAssignment_1_in_rule__Unary__Group__1__Impl7157);
            rule__Unary__InnerExpressionAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getUnaryAccess().getInnerExpressionAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__Group__1__Impl"


    // $ANTLR start "rule__Binary__Group__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3553:1: rule__Binary__Group__0 : rule__Binary__Group__0__Impl rule__Binary__Group__1 ;
    public final void rule__Binary__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3557:1: ( rule__Binary__Group__0__Impl rule__Binary__Group__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3558:2: rule__Binary__Group__0__Impl rule__Binary__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Binary__Group__0__Impl_in_rule__Binary__Group__07191);
            rule__Binary__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Binary__Group__1_in_rule__Binary__Group__07194);
            rule__Binary__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__Group__0"


    // $ANTLR start "rule__Binary__Group__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3565:1: rule__Binary__Group__0__Impl : ( ( rule__Binary__LeftExpressionAssignment_0 ) ) ;
    public final void rule__Binary__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3569:1: ( ( ( rule__Binary__LeftExpressionAssignment_0 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3570:1: ( ( rule__Binary__LeftExpressionAssignment_0 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3570:1: ( ( rule__Binary__LeftExpressionAssignment_0 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3571:1: ( rule__Binary__LeftExpressionAssignment_0 )
            {
             before(grammarAccess.getBinaryAccess().getLeftExpressionAssignment_0()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3572:1: ( rule__Binary__LeftExpressionAssignment_0 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3572:2: rule__Binary__LeftExpressionAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Binary__LeftExpressionAssignment_0_in_rule__Binary__Group__0__Impl7221);
            rule__Binary__LeftExpressionAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getBinaryAccess().getLeftExpressionAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__Group__0__Impl"


    // $ANTLR start "rule__Binary__Group__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3582:1: rule__Binary__Group__1 : rule__Binary__Group__1__Impl rule__Binary__Group__2 ;
    public final void rule__Binary__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3586:1: ( rule__Binary__Group__1__Impl rule__Binary__Group__2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3587:2: rule__Binary__Group__1__Impl rule__Binary__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Binary__Group__1__Impl_in_rule__Binary__Group__17251);
            rule__Binary__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Binary__Group__2_in_rule__Binary__Group__17254);
            rule__Binary__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__Group__1"


    // $ANTLR start "rule__Binary__Group__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3594:1: rule__Binary__Group__1__Impl : ( ( rule__Binary__OperatorAssignment_1 ) ) ;
    public final void rule__Binary__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3598:1: ( ( ( rule__Binary__OperatorAssignment_1 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3599:1: ( ( rule__Binary__OperatorAssignment_1 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3599:1: ( ( rule__Binary__OperatorAssignment_1 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3600:1: ( rule__Binary__OperatorAssignment_1 )
            {
             before(grammarAccess.getBinaryAccess().getOperatorAssignment_1()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3601:1: ( rule__Binary__OperatorAssignment_1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3601:2: rule__Binary__OperatorAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Binary__OperatorAssignment_1_in_rule__Binary__Group__1__Impl7281);
            rule__Binary__OperatorAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBinaryAccess().getOperatorAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__Group__1__Impl"


    // $ANTLR start "rule__Binary__Group__2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3611:1: rule__Binary__Group__2 : rule__Binary__Group__2__Impl ;
    public final void rule__Binary__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3615:1: ( rule__Binary__Group__2__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3616:2: rule__Binary__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Binary__Group__2__Impl_in_rule__Binary__Group__27311);
            rule__Binary__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__Group__2"


    // $ANTLR start "rule__Binary__Group__2__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3622:1: rule__Binary__Group__2__Impl : ( ( rule__Binary__RightExpressionAssignment_2 ) ) ;
    public final void rule__Binary__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3626:1: ( ( ( rule__Binary__RightExpressionAssignment_2 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3627:1: ( ( rule__Binary__RightExpressionAssignment_2 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3627:1: ( ( rule__Binary__RightExpressionAssignment_2 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3628:1: ( rule__Binary__RightExpressionAssignment_2 )
            {
             before(grammarAccess.getBinaryAccess().getRightExpressionAssignment_2()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3629:1: ( rule__Binary__RightExpressionAssignment_2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3629:2: rule__Binary__RightExpressionAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Binary__RightExpressionAssignment_2_in_rule__Binary__Group__2__Impl7338);
            rule__Binary__RightExpressionAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getBinaryAccess().getRightExpressionAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__Group__2__Impl"


    // $ANTLR start "rule__AtomicExpression__Group_2__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3645:1: rule__AtomicExpression__Group_2__0 : rule__AtomicExpression__Group_2__0__Impl rule__AtomicExpression__Group_2__1 ;
    public final void rule__AtomicExpression__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3649:1: ( rule__AtomicExpression__Group_2__0__Impl rule__AtomicExpression__Group_2__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3650:2: rule__AtomicExpression__Group_2__0__Impl rule__AtomicExpression__Group_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__AtomicExpression__Group_2__0__Impl_in_rule__AtomicExpression__Group_2__07374);
            rule__AtomicExpression__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__AtomicExpression__Group_2__1_in_rule__AtomicExpression__Group_2__07377);
            rule__AtomicExpression__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicExpression__Group_2__0"


    // $ANTLR start "rule__AtomicExpression__Group_2__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3657:1: rule__AtomicExpression__Group_2__0__Impl : ( '(' ) ;
    public final void rule__AtomicExpression__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3661:1: ( ( '(' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3662:1: ( '(' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3662:1: ( '(' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3663:1: '('
            {
             before(grammarAccess.getAtomicExpressionAccess().getLeftParenthesisKeyword_2_0()); 
            match(input,38,FollowSets000.FOLLOW_38_in_rule__AtomicExpression__Group_2__0__Impl7405); 
             after(grammarAccess.getAtomicExpressionAccess().getLeftParenthesisKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicExpression__Group_2__0__Impl"


    // $ANTLR start "rule__AtomicExpression__Group_2__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3676:1: rule__AtomicExpression__Group_2__1 : rule__AtomicExpression__Group_2__1__Impl rule__AtomicExpression__Group_2__2 ;
    public final void rule__AtomicExpression__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3680:1: ( rule__AtomicExpression__Group_2__1__Impl rule__AtomicExpression__Group_2__2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3681:2: rule__AtomicExpression__Group_2__1__Impl rule__AtomicExpression__Group_2__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__AtomicExpression__Group_2__1__Impl_in_rule__AtomicExpression__Group_2__17436);
            rule__AtomicExpression__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__AtomicExpression__Group_2__2_in_rule__AtomicExpression__Group_2__17439);
            rule__AtomicExpression__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicExpression__Group_2__1"


    // $ANTLR start "rule__AtomicExpression__Group_2__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3688:1: rule__AtomicExpression__Group_2__1__Impl : ( ruleExpression ) ;
    public final void rule__AtomicExpression__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3692:1: ( ( ruleExpression ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3693:1: ( ruleExpression )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3693:1: ( ruleExpression )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3694:1: ruleExpression
            {
             before(grammarAccess.getAtomicExpressionAccess().getExpressionParserRuleCall_2_1()); 
            pushFollow(FollowSets000.FOLLOW_ruleExpression_in_rule__AtomicExpression__Group_2__1__Impl7466);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getAtomicExpressionAccess().getExpressionParserRuleCall_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicExpression__Group_2__1__Impl"


    // $ANTLR start "rule__AtomicExpression__Group_2__2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3705:1: rule__AtomicExpression__Group_2__2 : rule__AtomicExpression__Group_2__2__Impl ;
    public final void rule__AtomicExpression__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3709:1: ( rule__AtomicExpression__Group_2__2__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3710:2: rule__AtomicExpression__Group_2__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__AtomicExpression__Group_2__2__Impl_in_rule__AtomicExpression__Group_2__27495);
            rule__AtomicExpression__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicExpression__Group_2__2"


    // $ANTLR start "rule__AtomicExpression__Group_2__2__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3716:1: rule__AtomicExpression__Group_2__2__Impl : ( ')' ) ;
    public final void rule__AtomicExpression__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3720:1: ( ( ')' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3721:1: ( ')' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3721:1: ( ')' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3722:1: ')'
            {
             before(grammarAccess.getAtomicExpressionAccess().getRightParenthesisKeyword_2_2()); 
            match(input,39,FollowSets000.FOLLOW_39_in_rule__AtomicExpression__Group_2__2__Impl7523); 
             after(grammarAccess.getAtomicExpressionAccess().getRightParenthesisKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicExpression__Group_2__2__Impl"


    // $ANTLR start "rule__StringConst__Group__0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3741:1: rule__StringConst__Group__0 : rule__StringConst__Group__0__Impl rule__StringConst__Group__1 ;
    public final void rule__StringConst__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3745:1: ( rule__StringConst__Group__0__Impl rule__StringConst__Group__1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3746:2: rule__StringConst__Group__0__Impl rule__StringConst__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__StringConst__Group__0__Impl_in_rule__StringConst__Group__07560);
            rule__StringConst__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__StringConst__Group__1_in_rule__StringConst__Group__07563);
            rule__StringConst__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringConst__Group__0"


    // $ANTLR start "rule__StringConst__Group__0__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3753:1: rule__StringConst__Group__0__Impl : ( '[' ) ;
    public final void rule__StringConst__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3757:1: ( ( '[' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3758:1: ( '[' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3758:1: ( '[' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3759:1: '['
            {
             before(grammarAccess.getStringConstAccess().getLeftSquareBracketKeyword_0()); 
            match(input,32,FollowSets000.FOLLOW_32_in_rule__StringConst__Group__0__Impl7591); 
             after(grammarAccess.getStringConstAccess().getLeftSquareBracketKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringConst__Group__0__Impl"


    // $ANTLR start "rule__StringConst__Group__1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3772:1: rule__StringConst__Group__1 : rule__StringConst__Group__1__Impl rule__StringConst__Group__2 ;
    public final void rule__StringConst__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3776:1: ( rule__StringConst__Group__1__Impl rule__StringConst__Group__2 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3777:2: rule__StringConst__Group__1__Impl rule__StringConst__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__StringConst__Group__1__Impl_in_rule__StringConst__Group__17622);
            rule__StringConst__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__StringConst__Group__2_in_rule__StringConst__Group__17625);
            rule__StringConst__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringConst__Group__1"


    // $ANTLR start "rule__StringConst__Group__1__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3784:1: rule__StringConst__Group__1__Impl : ( ( rule__StringConst__ValueAssignment_1 ) ) ;
    public final void rule__StringConst__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3788:1: ( ( ( rule__StringConst__ValueAssignment_1 ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3789:1: ( ( rule__StringConst__ValueAssignment_1 ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3789:1: ( ( rule__StringConst__ValueAssignment_1 ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3790:1: ( rule__StringConst__ValueAssignment_1 )
            {
             before(grammarAccess.getStringConstAccess().getValueAssignment_1()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3791:1: ( rule__StringConst__ValueAssignment_1 )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3791:2: rule__StringConst__ValueAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__StringConst__ValueAssignment_1_in_rule__StringConst__Group__1__Impl7652);
            rule__StringConst__ValueAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getStringConstAccess().getValueAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringConst__Group__1__Impl"


    // $ANTLR start "rule__StringConst__Group__2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3801:1: rule__StringConst__Group__2 : rule__StringConst__Group__2__Impl ;
    public final void rule__StringConst__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3805:1: ( rule__StringConst__Group__2__Impl )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3806:2: rule__StringConst__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__StringConst__Group__2__Impl_in_rule__StringConst__Group__27682);
            rule__StringConst__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringConst__Group__2"


    // $ANTLR start "rule__StringConst__Group__2__Impl"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3812:1: rule__StringConst__Group__2__Impl : ( ']' ) ;
    public final void rule__StringConst__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3816:1: ( ( ']' ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3817:1: ( ']' )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3817:1: ( ']' )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3818:1: ']'
            {
             before(grammarAccess.getStringConstAccess().getRightSquareBracketKeyword_2()); 
            match(input,33,FollowSets000.FOLLOW_33_in_rule__StringConst__Group__2__Impl7710); 
             after(grammarAccess.getStringConstAccess().getRightSquareBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringConst__Group__2__Impl"


    // $ANTLR start "rule__Configuration__NameAssignment_1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3838:1: rule__Configuration__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__Configuration__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3842:1: ( ( ruleEString ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3843:1: ( ruleEString )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3843:1: ( ruleEString )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3844:1: ruleEString
            {
             before(grammarAccess.getConfigurationAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Configuration__NameAssignment_17752);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__NameAssignment_1"


    // $ANTLR start "rule__Configuration__TypesAssignment_3_2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3853:1: rule__Configuration__TypesAssignment_3_2 : ( ruleType ) ;
    public final void rule__Configuration__TypesAssignment_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3857:1: ( ( ruleType ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3858:1: ( ruleType )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3858:1: ( ruleType )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3859:1: ruleType
            {
             before(grammarAccess.getConfigurationAccess().getTypesTypeParserRuleCall_3_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleType_in_rule__Configuration__TypesAssignment_3_27783);
            ruleType();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getTypesTypeParserRuleCall_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__TypesAssignment_3_2"


    // $ANTLR start "rule__Configuration__TypesAssignment_3_3_1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3868:1: rule__Configuration__TypesAssignment_3_3_1 : ( ruleType ) ;
    public final void rule__Configuration__TypesAssignment_3_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3872:1: ( ( ruleType ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3873:1: ( ruleType )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3873:1: ( ruleType )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3874:1: ruleType
            {
             before(grammarAccess.getConfigurationAccess().getTypesTypeParserRuleCall_3_3_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleType_in_rule__Configuration__TypesAssignment_3_3_17814);
            ruleType();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getTypesTypeParserRuleCall_3_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__TypesAssignment_3_3_1"


    // $ANTLR start "rule__Configuration__RootFeaturesAssignment_4_2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3883:1: rule__Configuration__RootFeaturesAssignment_4_2 : ( ruleFeature ) ;
    public final void rule__Configuration__RootFeaturesAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3887:1: ( ( ruleFeature ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3888:1: ( ruleFeature )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3888:1: ( ruleFeature )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3889:1: ruleFeature
            {
             before(grammarAccess.getConfigurationAccess().getRootFeaturesFeatureParserRuleCall_4_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_rule__Configuration__RootFeaturesAssignment_4_27845);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getRootFeaturesFeatureParserRuleCall_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__RootFeaturesAssignment_4_2"


    // $ANTLR start "rule__Configuration__RootFeaturesAssignment_4_3_1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3898:1: rule__Configuration__RootFeaturesAssignment_4_3_1 : ( ruleFeature ) ;
    public final void rule__Configuration__RootFeaturesAssignment_4_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3902:1: ( ( ruleFeature ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3903:1: ( ruleFeature )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3903:1: ( ruleFeature )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3904:1: ruleFeature
            {
             before(grammarAccess.getConfigurationAccess().getRootFeaturesFeatureParserRuleCall_4_3_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_rule__Configuration__RootFeaturesAssignment_4_3_17876);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getRootFeaturesFeatureParserRuleCall_4_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__RootFeaturesAssignment_4_3_1"


    // $ANTLR start "rule__Configuration__ConstraintsAssignment_5_2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3913:1: rule__Configuration__ConstraintsAssignment_5_2 : ( ruleConstraint ) ;
    public final void rule__Configuration__ConstraintsAssignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3917:1: ( ( ruleConstraint ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3918:1: ( ruleConstraint )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3918:1: ( ruleConstraint )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3919:1: ruleConstraint
            {
             before(grammarAccess.getConfigurationAccess().getConstraintsConstraintParserRuleCall_5_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleConstraint_in_rule__Configuration__ConstraintsAssignment_5_27907);
            ruleConstraint();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getConstraintsConstraintParserRuleCall_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__ConstraintsAssignment_5_2"


    // $ANTLR start "rule__Configuration__ConstraintsAssignment_5_3_1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3928:1: rule__Configuration__ConstraintsAssignment_5_3_1 : ( ruleConstraint ) ;
    public final void rule__Configuration__ConstraintsAssignment_5_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3932:1: ( ( ruleConstraint ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3933:1: ( ruleConstraint )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3933:1: ( ruleConstraint )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3934:1: ruleConstraint
            {
             before(grammarAccess.getConfigurationAccess().getConstraintsConstraintParserRuleCall_5_3_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleConstraint_in_rule__Configuration__ConstraintsAssignment_5_3_17938);
            ruleConstraint();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getConstraintsConstraintParserRuleCall_5_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__ConstraintsAssignment_5_3_1"


    // $ANTLR start "rule__Constraint__NameAssignment_0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3943:1: rule__Constraint__NameAssignment_0 : ( ruleEString ) ;
    public final void rule__Constraint__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3947:1: ( ( ruleEString ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3948:1: ( ruleEString )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3948:1: ( ruleEString )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3949:1: ruleEString
            {
             before(grammarAccess.getConstraintAccess().getNameEStringParserRuleCall_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Constraint__NameAssignment_07969);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getConstraintAccess().getNameEStringParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__NameAssignment_0"


    // $ANTLR start "rule__Constraint__ErrorMessageAssignment_2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3958:1: rule__Constraint__ErrorMessageAssignment_2 : ( ruleEString ) ;
    public final void rule__Constraint__ErrorMessageAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3962:1: ( ( ruleEString ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3963:1: ( ruleEString )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3963:1: ( ruleEString )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3964:1: ruleEString
            {
             before(grammarAccess.getConstraintAccess().getErrorMessageEStringParserRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Constraint__ErrorMessageAssignment_28000);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getConstraintAccess().getErrorMessageEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__ErrorMessageAssignment_2"


    // $ANTLR start "rule__Constraint__ExpressionAssignment_5"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3973:1: rule__Constraint__ExpressionAssignment_5 : ( ruleExpression ) ;
    public final void rule__Constraint__ExpressionAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3977:1: ( ( ruleExpression ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3978:1: ( ruleExpression )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3978:1: ( ruleExpression )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3979:1: ruleExpression
            {
             before(grammarAccess.getConstraintAccess().getExpressionExpressionParserRuleCall_5_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleExpression_in_rule__Constraint__ExpressionAssignment_58031);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getConstraintAccess().getExpressionExpressionParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__ExpressionAssignment_5"


    // $ANTLR start "rule__Feature__NameAssignment_0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3988:1: rule__Feature__NameAssignment_0 : ( ruleEString ) ;
    public final void rule__Feature__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3992:1: ( ( ruleEString ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3993:1: ( ruleEString )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3993:1: ( ruleEString )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:3994:1: ruleEString
            {
             before(grammarAccess.getFeatureAccess().getNameEStringParserRuleCall_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Feature__NameAssignment_08062);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getFeatureAccess().getNameEStringParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__NameAssignment_0"


    // $ANTLR start "rule__Feature__TypeAssignment_1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4003:1: rule__Feature__TypeAssignment_1 : ( ( ruleEString ) ) ;
    public final void rule__Feature__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4007:1: ( ( ( ruleEString ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4008:1: ( ( ruleEString ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4008:1: ( ( ruleEString ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4009:1: ( ruleEString )
            {
             before(grammarAccess.getFeatureAccess().getTypeTypeCrossReference_1_0()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4010:1: ( ruleEString )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4011:1: ruleEString
            {
             before(grammarAccess.getFeatureAccess().getTypeTypeEStringParserRuleCall_1_0_1()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Feature__TypeAssignment_18097);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getFeatureAccess().getTypeTypeEStringParserRuleCall_1_0_1()); 

            }

             after(grammarAccess.getFeatureAccess().getTypeTypeCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__TypeAssignment_1"


    // $ANTLR start "rule__Feature__MinAssignment_2_1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4022:1: rule__Feature__MinAssignment_2_1 : ( ruleEInt ) ;
    public final void rule__Feature__MinAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4026:1: ( ( ruleEInt ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4027:1: ( ruleEInt )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4027:1: ( ruleEInt )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4028:1: ruleEInt
            {
             before(grammarAccess.getFeatureAccess().getMinEIntParserRuleCall_2_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEInt_in_rule__Feature__MinAssignment_2_18132);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getFeatureAccess().getMinEIntParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__MinAssignment_2_1"


    // $ANTLR start "rule__Feature__MaxAssignment_2_3"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4037:1: rule__Feature__MaxAssignment_2_3 : ( ruleEInt ) ;
    public final void rule__Feature__MaxAssignment_2_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4041:1: ( ( ruleEInt ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4042:1: ( ruleEInt )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4042:1: ( ruleEInt )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4043:1: ruleEInt
            {
             before(grammarAccess.getFeatureAccess().getMaxEIntParserRuleCall_2_3_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEInt_in_rule__Feature__MaxAssignment_2_38163);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getFeatureAccess().getMaxEIntParserRuleCall_2_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__MaxAssignment_2_3"


    // $ANTLR start "rule__Feature__GroupsAssignment_3_1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4052:1: rule__Feature__GroupsAssignment_3_1 : ( ruleGroup ) ;
    public final void rule__Feature__GroupsAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4056:1: ( ( ruleGroup ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4057:1: ( ruleGroup )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4057:1: ( ruleGroup )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4058:1: ruleGroup
            {
             before(grammarAccess.getFeatureAccess().getGroupsGroupParserRuleCall_3_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleGroup_in_rule__Feature__GroupsAssignment_3_18194);
            ruleGroup();

            state._fsp--;

             after(grammarAccess.getFeatureAccess().getGroupsGroupParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__GroupsAssignment_3_1"


    // $ANTLR start "rule__Feature__GroupsAssignment_3_2_1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4067:1: rule__Feature__GroupsAssignment_3_2_1 : ( ruleGroup ) ;
    public final void rule__Feature__GroupsAssignment_3_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4071:1: ( ( ruleGroup ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4072:1: ( ruleGroup )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4072:1: ( ruleGroup )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4073:1: ruleGroup
            {
             before(grammarAccess.getFeatureAccess().getGroupsGroupParserRuleCall_3_2_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleGroup_in_rule__Feature__GroupsAssignment_3_2_18225);
            ruleGroup();

            state._fsp--;

             after(grammarAccess.getFeatureAccess().getGroupsGroupParserRuleCall_3_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__GroupsAssignment_3_2_1"


    // $ANTLR start "rule__BOOLEAN__NameAssignment_2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4082:1: rule__BOOLEAN__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__BOOLEAN__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4086:1: ( ( ruleEString ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4087:1: ( ruleEString )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4087:1: ( ruleEString )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4088:1: ruleEString
            {
             before(grammarAccess.getBOOLEANAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__BOOLEAN__NameAssignment_28256);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getBOOLEANAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BOOLEAN__NameAssignment_2"


    // $ANTLR start "rule__INTEGER__NameAssignment_2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4097:1: rule__INTEGER__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__INTEGER__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4101:1: ( ( ruleEString ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4102:1: ( ruleEString )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4102:1: ( ruleEString )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4103:1: ruleEString
            {
             before(grammarAccess.getINTEGERAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__INTEGER__NameAssignment_28287);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getINTEGERAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__INTEGER__NameAssignment_2"


    // $ANTLR start "rule__ENUM__NameAssignment_1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4112:1: rule__ENUM__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__ENUM__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4116:1: ( ( ruleEString ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4117:1: ( ruleEString )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4117:1: ( ruleEString )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4118:1: ruleEString
            {
             before(grammarAccess.getENUMAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__ENUM__NameAssignment_18318);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getENUMAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__NameAssignment_1"


    // $ANTLR start "rule__ENUM__EnumValuesAssignment_3"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4127:1: rule__ENUM__EnumValuesAssignment_3 : ( ruleEnumValue ) ;
    public final void rule__ENUM__EnumValuesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4131:1: ( ( ruleEnumValue ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4132:1: ( ruleEnumValue )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4132:1: ( ruleEnumValue )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4133:1: ruleEnumValue
            {
             before(grammarAccess.getENUMAccess().getEnumValuesEnumValueParserRuleCall_3_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEnumValue_in_rule__ENUM__EnumValuesAssignment_38349);
            ruleEnumValue();

            state._fsp--;

             after(grammarAccess.getENUMAccess().getEnumValuesEnumValueParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__EnumValuesAssignment_3"


    // $ANTLR start "rule__ENUM__EnumValuesAssignment_4_1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4142:1: rule__ENUM__EnumValuesAssignment_4_1 : ( ruleEnumValue ) ;
    public final void rule__ENUM__EnumValuesAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4146:1: ( ( ruleEnumValue ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4147:1: ( ruleEnumValue )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4147:1: ( ruleEnumValue )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4148:1: ruleEnumValue
            {
             before(grammarAccess.getENUMAccess().getEnumValuesEnumValueParserRuleCall_4_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEnumValue_in_rule__ENUM__EnumValuesAssignment_4_18380);
            ruleEnumValue();

            state._fsp--;

             after(grammarAccess.getENUMAccess().getEnumValuesEnumValueParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ENUM__EnumValuesAssignment_4_1"


    // $ANTLR start "rule__EnumValue__NameAssignment_1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4157:1: rule__EnumValue__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__EnumValue__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4161:1: ( ( ruleEString ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4162:1: ( ruleEString )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4162:1: ( ruleEString )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4163:1: ruleEString
            {
             before(grammarAccess.getEnumValueAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__EnumValue__NameAssignment_18411);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEnumValueAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__NameAssignment_1"


    // $ANTLR start "rule__Group__NameAssignment_0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4172:1: rule__Group__NameAssignment_0 : ( ruleEString ) ;
    public final void rule__Group__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4176:1: ( ( ruleEString ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4177:1: ( ruleEString )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4177:1: ( ruleEString )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4178:1: ruleEString
            {
             before(grammarAccess.getGroupAccess().getNameEStringParserRuleCall_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Group__NameAssignment_08442);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getGroupAccess().getNameEStringParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__NameAssignment_0"


    // $ANTLR start "rule__Group__MinAssignment_1_1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4187:1: rule__Group__MinAssignment_1_1 : ( ruleEInt ) ;
    public final void rule__Group__MinAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4191:1: ( ( ruleEInt ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4192:1: ( ruleEInt )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4192:1: ( ruleEInt )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4193:1: ruleEInt
            {
             before(grammarAccess.getGroupAccess().getMinEIntParserRuleCall_1_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEInt_in_rule__Group__MinAssignment_1_18473);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getGroupAccess().getMinEIntParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__MinAssignment_1_1"


    // $ANTLR start "rule__Group__MaxAssignment_1_3"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4202:1: rule__Group__MaxAssignment_1_3 : ( ruleEInt ) ;
    public final void rule__Group__MaxAssignment_1_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4206:1: ( ( ruleEInt ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4207:1: ( ruleEInt )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4207:1: ( ruleEInt )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4208:1: ruleEInt
            {
             before(grammarAccess.getGroupAccess().getMaxEIntParserRuleCall_1_3_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEInt_in_rule__Group__MaxAssignment_1_38504);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getGroupAccess().getMaxEIntParserRuleCall_1_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__MaxAssignment_1_3"


    // $ANTLR start "rule__Group__FeaturesAssignment_3"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4217:1: rule__Group__FeaturesAssignment_3 : ( ruleFeature ) ;
    public final void rule__Group__FeaturesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4221:1: ( ( ruleFeature ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4222:1: ( ruleFeature )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4222:1: ( ruleFeature )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4223:1: ruleFeature
            {
             before(grammarAccess.getGroupAccess().getFeaturesFeatureParserRuleCall_3_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_rule__Group__FeaturesAssignment_38535);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getGroupAccess().getFeaturesFeatureParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__FeaturesAssignment_3"


    // $ANTLR start "rule__Group__FeaturesAssignment_4_1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4232:1: rule__Group__FeaturesAssignment_4_1 : ( ruleFeature ) ;
    public final void rule__Group__FeaturesAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4236:1: ( ( ruleFeature ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4237:1: ( ruleFeature )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4237:1: ( ruleFeature )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4238:1: ruleFeature
            {
             before(grammarAccess.getGroupAccess().getFeaturesFeatureParserRuleCall_4_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_rule__Group__FeaturesAssignment_4_18566);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getGroupAccess().getFeaturesFeatureParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__FeaturesAssignment_4_1"


    // $ANTLR start "rule__Unary__OperatorAssignment_0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4247:1: rule__Unary__OperatorAssignment_0 : ( ruleUnaryOperator ) ;
    public final void rule__Unary__OperatorAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4251:1: ( ( ruleUnaryOperator ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4252:1: ( ruleUnaryOperator )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4252:1: ( ruleUnaryOperator )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4253:1: ruleUnaryOperator
            {
             before(grammarAccess.getUnaryAccess().getOperatorUnaryOperatorEnumRuleCall_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleUnaryOperator_in_rule__Unary__OperatorAssignment_08597);
            ruleUnaryOperator();

            state._fsp--;

             after(grammarAccess.getUnaryAccess().getOperatorUnaryOperatorEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__OperatorAssignment_0"


    // $ANTLR start "rule__Unary__InnerExpressionAssignment_1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4262:1: rule__Unary__InnerExpressionAssignment_1 : ( ruleAtomicExpression ) ;
    public final void rule__Unary__InnerExpressionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4266:1: ( ( ruleAtomicExpression ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4267:1: ( ruleAtomicExpression )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4267:1: ( ruleAtomicExpression )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4268:1: ruleAtomicExpression
            {
             before(grammarAccess.getUnaryAccess().getInnerExpressionAtomicExpressionParserRuleCall_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAtomicExpression_in_rule__Unary__InnerExpressionAssignment_18628);
            ruleAtomicExpression();

            state._fsp--;

             after(grammarAccess.getUnaryAccess().getInnerExpressionAtomicExpressionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__InnerExpressionAssignment_1"


    // $ANTLR start "rule__Binary__LeftExpressionAssignment_0"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4277:1: rule__Binary__LeftExpressionAssignment_0 : ( ruleAtomicExpression ) ;
    public final void rule__Binary__LeftExpressionAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4281:1: ( ( ruleAtomicExpression ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4282:1: ( ruleAtomicExpression )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4282:1: ( ruleAtomicExpression )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4283:1: ruleAtomicExpression
            {
             before(grammarAccess.getBinaryAccess().getLeftExpressionAtomicExpressionParserRuleCall_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAtomicExpression_in_rule__Binary__LeftExpressionAssignment_08659);
            ruleAtomicExpression();

            state._fsp--;

             after(grammarAccess.getBinaryAccess().getLeftExpressionAtomicExpressionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__LeftExpressionAssignment_0"


    // $ANTLR start "rule__Binary__OperatorAssignment_1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4292:1: rule__Binary__OperatorAssignment_1 : ( ruleBinaryOperator ) ;
    public final void rule__Binary__OperatorAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4296:1: ( ( ruleBinaryOperator ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4297:1: ( ruleBinaryOperator )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4297:1: ( ruleBinaryOperator )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4298:1: ruleBinaryOperator
            {
             before(grammarAccess.getBinaryAccess().getOperatorBinaryOperatorEnumRuleCall_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleBinaryOperator_in_rule__Binary__OperatorAssignment_18690);
            ruleBinaryOperator();

            state._fsp--;

             after(grammarAccess.getBinaryAccess().getOperatorBinaryOperatorEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__OperatorAssignment_1"


    // $ANTLR start "rule__Binary__RightExpressionAssignment_2"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4307:1: rule__Binary__RightExpressionAssignment_2 : ( ruleAtomicExpression ) ;
    public final void rule__Binary__RightExpressionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4311:1: ( ( ruleAtomicExpression ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4312:1: ( ruleAtomicExpression )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4312:1: ( ruleAtomicExpression )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4313:1: ruleAtomicExpression
            {
             before(grammarAccess.getBinaryAccess().getRightExpressionAtomicExpressionParserRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAtomicExpression_in_rule__Binary__RightExpressionAssignment_28721);
            ruleAtomicExpression();

            state._fsp--;

             after(grammarAccess.getBinaryAccess().getRightExpressionAtomicExpressionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__RightExpressionAssignment_2"


    // $ANTLR start "rule__IntConst__ValueAssignment"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4322:1: rule__IntConst__ValueAssignment : ( ruleEInt ) ;
    public final void rule__IntConst__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4326:1: ( ( ruleEInt ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4327:1: ( ruleEInt )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4327:1: ( ruleEInt )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4328:1: ruleEInt
            {
             before(grammarAccess.getIntConstAccess().getValueEIntParserRuleCall_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEInt_in_rule__IntConst__ValueAssignment8752);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getIntConstAccess().getValueEIntParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntConst__ValueAssignment"


    // $ANTLR start "rule__StringConst__ValueAssignment_1"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4337:1: rule__StringConst__ValueAssignment_1 : ( ruleEString ) ;
    public final void rule__StringConst__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4341:1: ( ( ruleEString ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4342:1: ( ruleEString )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4342:1: ( ruleEString )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4343:1: ruleEString
            {
             before(grammarAccess.getStringConstAccess().getValueEStringParserRuleCall_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__StringConst__ValueAssignment_18783);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getStringConstAccess().getValueEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringConst__ValueAssignment_1"


    // $ANTLR start "rule__BoolConst__ValueAssignment"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4352:1: rule__BoolConst__ValueAssignment : ( ruleBooolean ) ;
    public final void rule__BoolConst__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4356:1: ( ( ruleBooolean ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4357:1: ( ruleBooolean )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4357:1: ( ruleBooolean )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4358:1: ruleBooolean
            {
             before(grammarAccess.getBoolConstAccess().getValueBoooleanEnumRuleCall_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleBooolean_in_rule__BoolConst__ValueAssignment8814);
            ruleBooolean();

            state._fsp--;

             after(grammarAccess.getBoolConstAccess().getValueBoooleanEnumRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BoolConst__ValueAssignment"


    // $ANTLR start "rule__IdRef__IdentifierAssignment"
    // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4367:1: rule__IdRef__IdentifierAssignment : ( ( ruleEString ) ) ;
    public final void rule__IdRef__IdentifierAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4371:1: ( ( ( ruleEString ) ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4372:1: ( ( ruleEString ) )
            {
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4372:1: ( ( ruleEString ) )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4373:1: ( ruleEString )
            {
             before(grammarAccess.getIdRefAccess().getIdentifierIdentifiableCrossReference_0()); 
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4374:1: ( ruleEString )
            // ../configurator.DSL.ui/src-gen/configurator/ui/contentassist/antlr/internal/InternalDSL.g:4375:1: ruleEString
            {
             before(grammarAccess.getIdRefAccess().getIdentifierIdentifiableEStringParserRuleCall_0_1()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__IdRef__IdentifierAssignment8849);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getIdRefAccess().getIdentifierIdentifiableEStringParserRuleCall_0_1()); 

            }

             after(grammarAccess.getIdRefAccess().getIdentifierIdentifiableCrossReference_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IdRef__IdentifierAssignment"

    // Delegated rules


    protected DFA2 dfa2 = new DFA2(this);
    static final String DFA2_eotS =
        "\20\uffff";
    static final String DFA2_eofS =
        "\2\uffff\2\11\1\12\2\13\7\uffff\1\17\1\uffff";
    static final String DFA2_minS =
        "\1\4\1\uffff\5\14\1\5\4\uffff\2\41\1\14\1\uffff";
    static final String DFA2_maxS =
        "\1\46\1\uffff\5\47\1\6\4\uffff\2\41\1\47\1\uffff";
    static final String DFA2_acceptS =
        "\1\uffff\1\1\6\uffff\1\2\1\3\1\4\1\5\3\uffff\1\6";
    static final String DFA2_specialS =
        "\20\uffff}>";
    static final String[] DFA2_transitionS = {
            "\1\4\1\2\1\3\4\uffff\3\1\12\uffff\1\5\1\6\6\uffff\1\7\5\uffff"+
            "\1\10",
            "",
            "\14\10\3\uffff\1\11\13\uffff\1\11",
            "\14\10\3\uffff\1\11\13\uffff\1\11",
            "\14\10\3\uffff\1\12\13\uffff\1\12",
            "\14\10\3\uffff\1\13\13\uffff\1\13",
            "\14\10\3\uffff\1\13\13\uffff\1\13",
            "\1\14\1\15",
            "",
            "",
            "",
            "",
            "\1\16",
            "\1\16",
            "\14\10\3\uffff\1\17\13\uffff\1\17",
            ""
    };

    static final short[] DFA2_eot = DFA.unpackEncodedString(DFA2_eotS);
    static final short[] DFA2_eof = DFA.unpackEncodedString(DFA2_eofS);
    static final char[] DFA2_min = DFA.unpackEncodedStringToUnsignedChars(DFA2_minS);
    static final char[] DFA2_max = DFA.unpackEncodedStringToUnsignedChars(DFA2_maxS);
    static final short[] DFA2_accept = DFA.unpackEncodedString(DFA2_acceptS);
    static final short[] DFA2_special = DFA.unpackEncodedString(DFA2_specialS);
    static final short[][] DFA2_transition;

    static {
        int numStates = DFA2_transitionS.length;
        DFA2_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA2_transition[i] = DFA.unpackEncodedString(DFA2_transitionS[i]);
        }
    }

    class DFA2 extends DFA {

        public DFA2(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 2;
            this.eot = DFA2_eot;
            this.eof = DFA2_eof;
            this.min = DFA2_min;
            this.max = DFA2_max;
            this.accept = DFA2_accept;
            this.special = DFA2_special;
            this.transition = DFA2_transition;
        }
        public String getDescription() {
            return "707:1: rule__Expression__Alternatives : ( ( ruleUnary ) | ( ruleBinary ) | ( ruleIdRef ) | ( ruleIntConst ) | ( ruleBoolConst ) | ( ruleStringConst ) );";
        }
    }
 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleConfiguration_in_entryRuleConfiguration61 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleConfiguration68 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group__0_in_ruleConfiguration94 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleConstraint_in_entryRuleConstraint121 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleConstraint128 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Constraint__Group__0_in_ruleConstraint154 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleType_in_entryRuleType181 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleType188 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Type__Alternatives_in_ruleType214 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleExpression_in_entryRuleExpression241 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleExpression248 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Expression__Alternatives_in_ruleExpression274 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_entryRuleEString303 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEString310 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EString__Alternatives_in_ruleEString336 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeature_in_entryRuleFeature363 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeature370 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Group__0_in_ruleFeature396 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBOOLEAN_in_entryRuleBOOLEAN423 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleBOOLEAN430 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__BOOLEAN__Group__0_in_ruleBOOLEAN456 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleINTEGER_in_entryRuleINTEGER483 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleINTEGER490 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__INTEGER__Group__0_in_ruleINTEGER516 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleENUM_in_entryRuleENUM543 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleENUM550 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ENUM__Group__0_in_ruleENUM576 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEnumValue_in_entryRuleEnumValue603 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEnumValue610 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EnumValue__Group__0_in_ruleEnumValue636 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleGroup_in_entryRuleGroup663 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleGroup670 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__Group__0_in_ruleGroup696 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEInt_in_entryRuleEInt723 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEInt730 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_ruleEInt756 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleUnary_in_entryRuleUnary782 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleUnary789 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Unary__Group__0_in_ruleUnary815 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBinary_in_entryRuleBinary842 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleBinary849 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Binary__Group__0_in_ruleBinary875 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAtomicExpression_in_entryRuleAtomicExpression902 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAtomicExpression909 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__AtomicExpression__Alternatives_in_ruleAtomicExpression935 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleConstants_in_entryRuleConstants962 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleConstants969 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Constants__Alternatives_in_ruleConstants995 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntConst_in_entryRuleIntConst1022 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleIntConst1029 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__IntConst__ValueAssignment_in_ruleIntConst1055 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringConst_in_entryRuleStringConst1082 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleStringConst1089 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__StringConst__Group__0_in_ruleStringConst1115 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBoolConst_in_entryRuleBoolConst1142 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleBoolConst1149 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__BoolConst__ValueAssignment_in_ruleBoolConst1175 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIdRef_in_entryRuleIdRef1202 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleIdRef1209 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__IdRef__IdentifierAssignment_in_ruleIdRef1235 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__UnaryOperator__Alternatives_in_ruleUnaryOperator1272 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__BinaryOperator__Alternatives_in_ruleBinaryOperator1308 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Booolean__Alternatives_in_ruleBooolean1344 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBOOLEAN_in_rule__Type__Alternatives1379 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleINTEGER_in_rule__Type__Alternatives1396 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleENUM_in_rule__Type__Alternatives1413 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleUnary_in_rule__Expression__Alternatives1445 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBinary_in_rule__Expression__Alternatives1462 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIdRef_in_rule__Expression__Alternatives1479 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntConst_in_rule__Expression__Alternatives1496 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBoolConst_in_rule__Expression__Alternatives1513 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringConst_in_rule__Expression__Alternatives1530 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__EString__Alternatives1563 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__EString__Alternatives1580 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIdRef_in_rule__AtomicExpression__Alternatives1612 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleConstants_in_rule__AtomicExpression__Alternatives1629 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__AtomicExpression__Group_2__0_in_rule__AtomicExpression__Alternatives1646 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntConst_in_rule__Constants__Alternatives1679 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBoolConst_in_rule__Constants__Alternatives1696 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringConst_in_rule__Constants__Alternatives1713 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_11_in_rule__UnaryOperator__Alternatives1746 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_12_in_rule__UnaryOperator__Alternatives1767 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_13_in_rule__UnaryOperator__Alternatives1788 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_13_in_rule__BinaryOperator__Alternatives1824 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_12_in_rule__BinaryOperator__Alternatives1845 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_14_in_rule__BinaryOperator__Alternatives1866 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_15_in_rule__BinaryOperator__Alternatives1887 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_16_in_rule__BinaryOperator__Alternatives1908 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_17_in_rule__BinaryOperator__Alternatives1929 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_18_in_rule__BinaryOperator__Alternatives1950 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_19_in_rule__BinaryOperator__Alternatives1971 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_20_in_rule__BinaryOperator__Alternatives1992 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_21_in_rule__BinaryOperator__Alternatives2013 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_22_in_rule__BinaryOperator__Alternatives2034 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_23_in_rule__BinaryOperator__Alternatives2055 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_24_in_rule__Booolean__Alternatives2091 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_25_in_rule__Booolean__Alternatives2112 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group__0__Impl_in_rule__Configuration__Group__02145 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_rule__Configuration__Group__1_in_rule__Configuration__Group__02148 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group__1__Impl_in_rule__Configuration__Group__12206 = new BitSet(new long[]{0x0000000004000000L});
        public static final BitSet FOLLOW_rule__Configuration__Group__2_in_rule__Configuration__Group__12209 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__NameAssignment_1_in_rule__Configuration__Group__1__Impl2236 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group__2__Impl_in_rule__Configuration__Group__22266 = new BitSet(new long[]{0x00000000D8000000L});
        public static final BitSet FOLLOW_rule__Configuration__Group__3_in_rule__Configuration__Group__22269 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_26_in_rule__Configuration__Group__2__Impl2297 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group__3__Impl_in_rule__Configuration__Group__32328 = new BitSet(new long[]{0x00000000D8000000L});
        public static final BitSet FOLLOW_rule__Configuration__Group__4_in_rule__Configuration__Group__32331 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_3__0_in_rule__Configuration__Group__3__Impl2358 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group__4__Impl_in_rule__Configuration__Group__42389 = new BitSet(new long[]{0x00000000D8000000L});
        public static final BitSet FOLLOW_rule__Configuration__Group__5_in_rule__Configuration__Group__42392 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_4__0_in_rule__Configuration__Group__4__Impl2419 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group__5__Impl_in_rule__Configuration__Group__52450 = new BitSet(new long[]{0x00000000D8000000L});
        public static final BitSet FOLLOW_rule__Configuration__Group__6_in_rule__Configuration__Group__52453 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_5__0_in_rule__Configuration__Group__5__Impl2480 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group__6__Impl_in_rule__Configuration__Group__62511 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_27_in_rule__Configuration__Group__6__Impl2539 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_3__0__Impl_in_rule__Configuration__Group_3__02584 = new BitSet(new long[]{0x0000000004000000L});
        public static final BitSet FOLLOW_rule__Configuration__Group_3__1_in_rule__Configuration__Group_3__02587 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_28_in_rule__Configuration__Group_3__0__Impl2615 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_3__1__Impl_in_rule__Configuration__Group_3__12646 = new BitSet(new long[]{0x0000001800000060L});
        public static final BitSet FOLLOW_rule__Configuration__Group_3__2_in_rule__Configuration__Group_3__12649 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_26_in_rule__Configuration__Group_3__1__Impl2677 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_3__2__Impl_in_rule__Configuration__Group_3__22708 = new BitSet(new long[]{0x0000000028000000L});
        public static final BitSet FOLLOW_rule__Configuration__Group_3__3_in_rule__Configuration__Group_3__22711 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__TypesAssignment_3_2_in_rule__Configuration__Group_3__2__Impl2738 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_3__3__Impl_in_rule__Configuration__Group_3__32768 = new BitSet(new long[]{0x0000000028000000L});
        public static final BitSet FOLLOW_rule__Configuration__Group_3__4_in_rule__Configuration__Group_3__32771 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_3_3__0_in_rule__Configuration__Group_3__3__Impl2798 = new BitSet(new long[]{0x0000000020000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_3__4__Impl_in_rule__Configuration__Group_3__42829 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_27_in_rule__Configuration__Group_3__4__Impl2857 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_3_3__0__Impl_in_rule__Configuration__Group_3_3__02898 = new BitSet(new long[]{0x0000001800000060L});
        public static final BitSet FOLLOW_rule__Configuration__Group_3_3__1_in_rule__Configuration__Group_3_3__02901 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_29_in_rule__Configuration__Group_3_3__0__Impl2929 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_3_3__1__Impl_in_rule__Configuration__Group_3_3__12960 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__TypesAssignment_3_3_1_in_rule__Configuration__Group_3_3__1__Impl2987 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_4__0__Impl_in_rule__Configuration__Group_4__03021 = new BitSet(new long[]{0x0000000004000000L});
        public static final BitSet FOLLOW_rule__Configuration__Group_4__1_in_rule__Configuration__Group_4__03024 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_30_in_rule__Configuration__Group_4__0__Impl3052 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_4__1__Impl_in_rule__Configuration__Group_4__13083 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_rule__Configuration__Group_4__2_in_rule__Configuration__Group_4__13086 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_26_in_rule__Configuration__Group_4__1__Impl3114 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_4__2__Impl_in_rule__Configuration__Group_4__23145 = new BitSet(new long[]{0x0000000028000000L});
        public static final BitSet FOLLOW_rule__Configuration__Group_4__3_in_rule__Configuration__Group_4__23148 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__RootFeaturesAssignment_4_2_in_rule__Configuration__Group_4__2__Impl3175 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_4__3__Impl_in_rule__Configuration__Group_4__33205 = new BitSet(new long[]{0x0000000028000000L});
        public static final BitSet FOLLOW_rule__Configuration__Group_4__4_in_rule__Configuration__Group_4__33208 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_4_3__0_in_rule__Configuration__Group_4__3__Impl3235 = new BitSet(new long[]{0x0000000020000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_4__4__Impl_in_rule__Configuration__Group_4__43266 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_27_in_rule__Configuration__Group_4__4__Impl3294 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_4_3__0__Impl_in_rule__Configuration__Group_4_3__03335 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_rule__Configuration__Group_4_3__1_in_rule__Configuration__Group_4_3__03338 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_29_in_rule__Configuration__Group_4_3__0__Impl3366 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_4_3__1__Impl_in_rule__Configuration__Group_4_3__13397 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__RootFeaturesAssignment_4_3_1_in_rule__Configuration__Group_4_3__1__Impl3424 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_5__0__Impl_in_rule__Configuration__Group_5__03458 = new BitSet(new long[]{0x0000000004000000L});
        public static final BitSet FOLLOW_rule__Configuration__Group_5__1_in_rule__Configuration__Group_5__03461 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_31_in_rule__Configuration__Group_5__0__Impl3489 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_5__1__Impl_in_rule__Configuration__Group_5__13520 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_rule__Configuration__Group_5__2_in_rule__Configuration__Group_5__13523 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_26_in_rule__Configuration__Group_5__1__Impl3551 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_5__2__Impl_in_rule__Configuration__Group_5__23582 = new BitSet(new long[]{0x0000000028000000L});
        public static final BitSet FOLLOW_rule__Configuration__Group_5__3_in_rule__Configuration__Group_5__23585 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__ConstraintsAssignment_5_2_in_rule__Configuration__Group_5__2__Impl3612 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_5__3__Impl_in_rule__Configuration__Group_5__33642 = new BitSet(new long[]{0x0000000028000000L});
        public static final BitSet FOLLOW_rule__Configuration__Group_5__4_in_rule__Configuration__Group_5__33645 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_5_3__0_in_rule__Configuration__Group_5__3__Impl3672 = new BitSet(new long[]{0x0000000020000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_5__4__Impl_in_rule__Configuration__Group_5__43703 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_27_in_rule__Configuration__Group_5__4__Impl3731 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_5_3__0__Impl_in_rule__Configuration__Group_5_3__03772 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_rule__Configuration__Group_5_3__1_in_rule__Configuration__Group_5_3__03775 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_29_in_rule__Configuration__Group_5_3__0__Impl3803 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__Group_5_3__1__Impl_in_rule__Configuration__Group_5_3__13834 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Configuration__ConstraintsAssignment_5_3_1_in_rule__Configuration__Group_5_3__1__Impl3861 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Constraint__Group__0__Impl_in_rule__Constraint__Group__03895 = new BitSet(new long[]{0x0000000100000000L});
        public static final BitSet FOLLOW_rule__Constraint__Group__1_in_rule__Constraint__Group__03898 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Constraint__NameAssignment_0_in_rule__Constraint__Group__0__Impl3925 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Constraint__Group__1__Impl_in_rule__Constraint__Group__13955 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_rule__Constraint__Group__2_in_rule__Constraint__Group__13958 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_32_in_rule__Constraint__Group__1__Impl3986 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Constraint__Group__2__Impl_in_rule__Constraint__Group__24017 = new BitSet(new long[]{0x0000000200000000L});
        public static final BitSet FOLLOW_rule__Constraint__Group__3_in_rule__Constraint__Group__24020 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Constraint__ErrorMessageAssignment_2_in_rule__Constraint__Group__2__Impl4047 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Constraint__Group__3__Impl_in_rule__Constraint__Group__34077 = new BitSet(new long[]{0x0000000004000000L});
        public static final BitSet FOLLOW_rule__Constraint__Group__4_in_rule__Constraint__Group__34080 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_33_in_rule__Constraint__Group__3__Impl4108 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Constraint__Group__4__Impl_in_rule__Constraint__Group__44139 = new BitSet(new long[]{0x0000004103003870L});
        public static final BitSet FOLLOW_rule__Constraint__Group__5_in_rule__Constraint__Group__44142 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_26_in_rule__Constraint__Group__4__Impl4170 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Constraint__Group__5__Impl_in_rule__Constraint__Group__54201 = new BitSet(new long[]{0x0000000008000000L});
        public static final BitSet FOLLOW_rule__Constraint__Group__6_in_rule__Constraint__Group__54204 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Constraint__ExpressionAssignment_5_in_rule__Constraint__Group__5__Impl4231 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Constraint__Group__6__Impl_in_rule__Constraint__Group__64261 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_27_in_rule__Constraint__Group__6__Impl4289 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Group__0__Impl_in_rule__Feature__Group__04334 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_rule__Feature__Group__1_in_rule__Feature__Group__04337 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__NameAssignment_0_in_rule__Feature__Group__0__Impl4364 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Group__1__Impl_in_rule__Feature__Group__14394 = new BitSet(new long[]{0x0000000104000000L});
        public static final BitSet FOLLOW_rule__Feature__Group__2_in_rule__Feature__Group__14397 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__TypeAssignment_1_in_rule__Feature__Group__1__Impl4424 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Group__2__Impl_in_rule__Feature__Group__24454 = new BitSet(new long[]{0x0000000104000000L});
        public static final BitSet FOLLOW_rule__Feature__Group__3_in_rule__Feature__Group__24457 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Group_2__0_in_rule__Feature__Group__2__Impl4484 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Group__3__Impl_in_rule__Feature__Group__34515 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Group_3__0_in_rule__Feature__Group__3__Impl4542 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Group_2__0__Impl_in_rule__Feature__Group_2__04581 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__Feature__Group_2__1_in_rule__Feature__Group_2__04584 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_32_in_rule__Feature__Group_2__0__Impl4612 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Group_2__1__Impl_in_rule__Feature__Group_2__14643 = new BitSet(new long[]{0x0000000400000000L});
        public static final BitSet FOLLOW_rule__Feature__Group_2__2_in_rule__Feature__Group_2__14646 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__MinAssignment_2_1_in_rule__Feature__Group_2__1__Impl4673 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Group_2__2__Impl_in_rule__Feature__Group_2__24703 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__Feature__Group_2__3_in_rule__Feature__Group_2__24706 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_34_in_rule__Feature__Group_2__2__Impl4734 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Group_2__3__Impl_in_rule__Feature__Group_2__34765 = new BitSet(new long[]{0x0000000200000000L});
        public static final BitSet FOLLOW_rule__Feature__Group_2__4_in_rule__Feature__Group_2__34768 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__MaxAssignment_2_3_in_rule__Feature__Group_2__3__Impl4795 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Group_2__4__Impl_in_rule__Feature__Group_2__44825 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_33_in_rule__Feature__Group_2__4__Impl4853 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Group_3__0__Impl_in_rule__Feature__Group_3__04894 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_rule__Feature__Group_3__1_in_rule__Feature__Group_3__04897 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_26_in_rule__Feature__Group_3__0__Impl4925 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Group_3__1__Impl_in_rule__Feature__Group_3__14956 = new BitSet(new long[]{0x0000000028000000L});
        public static final BitSet FOLLOW_rule__Feature__Group_3__2_in_rule__Feature__Group_3__14959 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__GroupsAssignment_3_1_in_rule__Feature__Group_3__1__Impl4986 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Group_3__2__Impl_in_rule__Feature__Group_3__25016 = new BitSet(new long[]{0x0000000028000000L});
        public static final BitSet FOLLOW_rule__Feature__Group_3__3_in_rule__Feature__Group_3__25019 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Group_3_2__0_in_rule__Feature__Group_3__2__Impl5046 = new BitSet(new long[]{0x0000000020000002L});
        public static final BitSet FOLLOW_rule__Feature__Group_3__3__Impl_in_rule__Feature__Group_3__35077 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_27_in_rule__Feature__Group_3__3__Impl5105 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Group_3_2__0__Impl_in_rule__Feature__Group_3_2__05144 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_rule__Feature__Group_3_2__1_in_rule__Feature__Group_3_2__05147 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_29_in_rule__Feature__Group_3_2__0__Impl5175 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Group_3_2__1__Impl_in_rule__Feature__Group_3_2__15206 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__GroupsAssignment_3_2_1_in_rule__Feature__Group_3_2__1__Impl5233 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__BOOLEAN__Group__0__Impl_in_rule__BOOLEAN__Group__05267 = new BitSet(new long[]{0x0000000800000000L});
        public static final BitSet FOLLOW_rule__BOOLEAN__Group__1_in_rule__BOOLEAN__Group__05270 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__BOOLEAN__Group__1__Impl_in_rule__BOOLEAN__Group__15328 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_rule__BOOLEAN__Group__2_in_rule__BOOLEAN__Group__15331 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_35_in_rule__BOOLEAN__Group__1__Impl5359 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__BOOLEAN__Group__2__Impl_in_rule__BOOLEAN__Group__25390 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__BOOLEAN__NameAssignment_2_in_rule__BOOLEAN__Group__2__Impl5417 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__INTEGER__Group__0__Impl_in_rule__INTEGER__Group__05453 = new BitSet(new long[]{0x0000001000000000L});
        public static final BitSet FOLLOW_rule__INTEGER__Group__1_in_rule__INTEGER__Group__05456 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__INTEGER__Group__1__Impl_in_rule__INTEGER__Group__15514 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_rule__INTEGER__Group__2_in_rule__INTEGER__Group__15517 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_36_in_rule__INTEGER__Group__1__Impl5545 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__INTEGER__Group__2__Impl_in_rule__INTEGER__Group__25576 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__INTEGER__NameAssignment_2_in_rule__INTEGER__Group__2__Impl5603 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ENUM__Group__0__Impl_in_rule__ENUM__Group__05639 = new BitSet(new long[]{0x0000001800000060L});
        public static final BitSet FOLLOW_rule__ENUM__Group__1_in_rule__ENUM__Group__05642 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ENUM__Group__1__Impl_in_rule__ENUM__Group__15700 = new BitSet(new long[]{0x0000000004000000L});
        public static final BitSet FOLLOW_rule__ENUM__Group__2_in_rule__ENUM__Group__15703 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ENUM__NameAssignment_1_in_rule__ENUM__Group__1__Impl5730 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ENUM__Group__2__Impl_in_rule__ENUM__Group__25760 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_rule__ENUM__Group__3_in_rule__ENUM__Group__25763 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_26_in_rule__ENUM__Group__2__Impl5791 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ENUM__Group__3__Impl_in_rule__ENUM__Group__35822 = new BitSet(new long[]{0x0000000028000000L});
        public static final BitSet FOLLOW_rule__ENUM__Group__4_in_rule__ENUM__Group__35825 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ENUM__EnumValuesAssignment_3_in_rule__ENUM__Group__3__Impl5852 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ENUM__Group__4__Impl_in_rule__ENUM__Group__45882 = new BitSet(new long[]{0x0000000028000000L});
        public static final BitSet FOLLOW_rule__ENUM__Group__5_in_rule__ENUM__Group__45885 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ENUM__Group_4__0_in_rule__ENUM__Group__4__Impl5912 = new BitSet(new long[]{0x0000000020000002L});
        public static final BitSet FOLLOW_rule__ENUM__Group__5__Impl_in_rule__ENUM__Group__55943 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_27_in_rule__ENUM__Group__5__Impl5971 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ENUM__Group_4__0__Impl_in_rule__ENUM__Group_4__06014 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_rule__ENUM__Group_4__1_in_rule__ENUM__Group_4__06017 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_29_in_rule__ENUM__Group_4__0__Impl6045 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ENUM__Group_4__1__Impl_in_rule__ENUM__Group_4__16076 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ENUM__EnumValuesAssignment_4_1_in_rule__ENUM__Group_4__1__Impl6103 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EnumValue__Group__0__Impl_in_rule__EnumValue__Group__06137 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_rule__EnumValue__Group__1_in_rule__EnumValue__Group__06140 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EnumValue__Group__1__Impl_in_rule__EnumValue__Group__16198 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EnumValue__NameAssignment_1_in_rule__EnumValue__Group__1__Impl6225 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__Group__0__Impl_in_rule__Group__Group__06259 = new BitSet(new long[]{0x0000000104000000L});
        public static final BitSet FOLLOW_rule__Group__Group__1_in_rule__Group__Group__06262 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__NameAssignment_0_in_rule__Group__Group__0__Impl6289 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__Group__1__Impl_in_rule__Group__Group__16319 = new BitSet(new long[]{0x0000000104000000L});
        public static final BitSet FOLLOW_rule__Group__Group__2_in_rule__Group__Group__16322 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__Group_1__0_in_rule__Group__Group__1__Impl6349 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__Group__2__Impl_in_rule__Group__Group__26380 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_rule__Group__Group__3_in_rule__Group__Group__26383 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_26_in_rule__Group__Group__2__Impl6411 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__Group__3__Impl_in_rule__Group__Group__36442 = new BitSet(new long[]{0x0000000028000000L});
        public static final BitSet FOLLOW_rule__Group__Group__4_in_rule__Group__Group__36445 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__FeaturesAssignment_3_in_rule__Group__Group__3__Impl6472 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__Group__4__Impl_in_rule__Group__Group__46502 = new BitSet(new long[]{0x0000000028000000L});
        public static final BitSet FOLLOW_rule__Group__Group__5_in_rule__Group__Group__46505 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__Group_4__0_in_rule__Group__Group__4__Impl6532 = new BitSet(new long[]{0x0000000020000002L});
        public static final BitSet FOLLOW_rule__Group__Group__5__Impl_in_rule__Group__Group__56563 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_27_in_rule__Group__Group__5__Impl6591 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__Group_1__0__Impl_in_rule__Group__Group_1__06634 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__Group__Group_1__1_in_rule__Group__Group_1__06637 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_32_in_rule__Group__Group_1__0__Impl6665 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__Group_1__1__Impl_in_rule__Group__Group_1__16696 = new BitSet(new long[]{0x0000002000000000L});
        public static final BitSet FOLLOW_rule__Group__Group_1__2_in_rule__Group__Group_1__16699 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__MinAssignment_1_1_in_rule__Group__Group_1__1__Impl6726 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__Group_1__2__Impl_in_rule__Group__Group_1__26756 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__Group__Group_1__3_in_rule__Group__Group_1__26759 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_37_in_rule__Group__Group_1__2__Impl6787 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__Group_1__3__Impl_in_rule__Group__Group_1__36818 = new BitSet(new long[]{0x0000000200000000L});
        public static final BitSet FOLLOW_rule__Group__Group_1__4_in_rule__Group__Group_1__36821 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__MaxAssignment_1_3_in_rule__Group__Group_1__3__Impl6848 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__Group_1__4__Impl_in_rule__Group__Group_1__46878 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_33_in_rule__Group__Group_1__4__Impl6906 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__Group_4__0__Impl_in_rule__Group__Group_4__06947 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_rule__Group__Group_4__1_in_rule__Group__Group_4__06950 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_29_in_rule__Group__Group_4__0__Impl6978 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__Group_4__1__Impl_in_rule__Group__Group_4__17009 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Group__FeaturesAssignment_4_1_in_rule__Group__Group_4__1__Impl7036 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Unary__Group__0__Impl_in_rule__Unary__Group__07070 = new BitSet(new long[]{0x0000004103000070L});
        public static final BitSet FOLLOW_rule__Unary__Group__1_in_rule__Unary__Group__07073 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Unary__OperatorAssignment_0_in_rule__Unary__Group__0__Impl7100 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Unary__Group__1__Impl_in_rule__Unary__Group__17130 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Unary__InnerExpressionAssignment_1_in_rule__Unary__Group__1__Impl7157 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Binary__Group__0__Impl_in_rule__Binary__Group__07191 = new BitSet(new long[]{0x0000000000FFF000L});
        public static final BitSet FOLLOW_rule__Binary__Group__1_in_rule__Binary__Group__07194 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Binary__LeftExpressionAssignment_0_in_rule__Binary__Group__0__Impl7221 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Binary__Group__1__Impl_in_rule__Binary__Group__17251 = new BitSet(new long[]{0x0000004103000070L});
        public static final BitSet FOLLOW_rule__Binary__Group__2_in_rule__Binary__Group__17254 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Binary__OperatorAssignment_1_in_rule__Binary__Group__1__Impl7281 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Binary__Group__2__Impl_in_rule__Binary__Group__27311 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Binary__RightExpressionAssignment_2_in_rule__Binary__Group__2__Impl7338 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__AtomicExpression__Group_2__0__Impl_in_rule__AtomicExpression__Group_2__07374 = new BitSet(new long[]{0x0000004103003870L});
        public static final BitSet FOLLOW_rule__AtomicExpression__Group_2__1_in_rule__AtomicExpression__Group_2__07377 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_38_in_rule__AtomicExpression__Group_2__0__Impl7405 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__AtomicExpression__Group_2__1__Impl_in_rule__AtomicExpression__Group_2__17436 = new BitSet(new long[]{0x0000008000000000L});
        public static final BitSet FOLLOW_rule__AtomicExpression__Group_2__2_in_rule__AtomicExpression__Group_2__17439 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleExpression_in_rule__AtomicExpression__Group_2__1__Impl7466 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__AtomicExpression__Group_2__2__Impl_in_rule__AtomicExpression__Group_2__27495 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_39_in_rule__AtomicExpression__Group_2__2__Impl7523 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__StringConst__Group__0__Impl_in_rule__StringConst__Group__07560 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_rule__StringConst__Group__1_in_rule__StringConst__Group__07563 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_32_in_rule__StringConst__Group__0__Impl7591 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__StringConst__Group__1__Impl_in_rule__StringConst__Group__17622 = new BitSet(new long[]{0x0000000200000000L});
        public static final BitSet FOLLOW_rule__StringConst__Group__2_in_rule__StringConst__Group__17625 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__StringConst__ValueAssignment_1_in_rule__StringConst__Group__1__Impl7652 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__StringConst__Group__2__Impl_in_rule__StringConst__Group__27682 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_33_in_rule__StringConst__Group__2__Impl7710 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Configuration__NameAssignment_17752 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleType_in_rule__Configuration__TypesAssignment_3_27783 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleType_in_rule__Configuration__TypesAssignment_3_3_17814 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeature_in_rule__Configuration__RootFeaturesAssignment_4_27845 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeature_in_rule__Configuration__RootFeaturesAssignment_4_3_17876 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleConstraint_in_rule__Configuration__ConstraintsAssignment_5_27907 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleConstraint_in_rule__Configuration__ConstraintsAssignment_5_3_17938 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Constraint__NameAssignment_07969 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Constraint__ErrorMessageAssignment_28000 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleExpression_in_rule__Constraint__ExpressionAssignment_58031 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Feature__NameAssignment_08062 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Feature__TypeAssignment_18097 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEInt_in_rule__Feature__MinAssignment_2_18132 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEInt_in_rule__Feature__MaxAssignment_2_38163 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleGroup_in_rule__Feature__GroupsAssignment_3_18194 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleGroup_in_rule__Feature__GroupsAssignment_3_2_18225 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__BOOLEAN__NameAssignment_28256 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__INTEGER__NameAssignment_28287 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__ENUM__NameAssignment_18318 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEnumValue_in_rule__ENUM__EnumValuesAssignment_38349 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEnumValue_in_rule__ENUM__EnumValuesAssignment_4_18380 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__EnumValue__NameAssignment_18411 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Group__NameAssignment_08442 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEInt_in_rule__Group__MinAssignment_1_18473 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEInt_in_rule__Group__MaxAssignment_1_38504 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeature_in_rule__Group__FeaturesAssignment_38535 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeature_in_rule__Group__FeaturesAssignment_4_18566 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleUnaryOperator_in_rule__Unary__OperatorAssignment_08597 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAtomicExpression_in_rule__Unary__InnerExpressionAssignment_18628 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAtomicExpression_in_rule__Binary__LeftExpressionAssignment_08659 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBinaryOperator_in_rule__Binary__OperatorAssignment_18690 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAtomicExpression_in_rule__Binary__RightExpressionAssignment_28721 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEInt_in_rule__IntConst__ValueAssignment8752 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__StringConst__ValueAssignment_18783 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooolean_in_rule__BoolConst__ValueAssignment8814 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__IdRef__IdentifierAssignment8849 = new BitSet(new long[]{0x0000000000000002L});
    }


}