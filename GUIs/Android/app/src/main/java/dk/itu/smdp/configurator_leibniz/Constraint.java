package dk.itu.smdp.configurator_leibniz;

import android.content.Context;
import android.widget.Toast;

import com.unnamed.b.atv.model.TreeNode;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import dk.itu.smdp.configurator_leibniz.expressions.Expression;
import dk.itu.smdp.configurator_leibniz.holder.interfaces.Violatable;

/**
 * Created by esben on 27-04-2015.
 */
public class Constraint implements Serializable{

    private static String TAG ="Constraint";
    public final String name;
    public final String message;
    public final Expression expr;

    Constraint(String name, String message, Expression expr){
        this.name=name;
        this.message=message;
        this.expr=expr;

    }

    public boolean validate(Context context){
        Object value = expr.getValue();
        if(value instanceof Boolean){
            {
                boolean bVal = (Boolean) value;
                if(!bVal) {
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                    addFailureColor(true);
                    return false;
                }
            }
        }

        addFailureColor(false);
        return true;
    }

    private void addFailureColor(boolean b){
        int color = b ? Violatable.VIOLATED_COLOR : Violatable.VIOLATED_COLOR;

        List<TreeNode> treeNodes = new LinkedList<>();
        expr.collectTreeNodes(treeNodes);

        for(TreeNode tn : treeNodes){

            TreeNode parent = tn;
            while(parent != null){
                if(parent.isRoot() || parent.getValue() == null || parent.getValue().equals("Features"))
                    break;
                ((Violatable) parent.getViewHolder()).markViolated(color);
                parent = parent.getParent();
            }
        }
   }



}
