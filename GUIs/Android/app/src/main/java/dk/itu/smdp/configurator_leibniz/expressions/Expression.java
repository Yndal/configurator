package dk.itu.smdp.configurator_leibniz.expressions;

import com.unnamed.b.atv.model.TreeNode;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lynd on 25/04/15.
 */
public interface Expression  {
    Object getValue();
    void collectTreeNodes(List<TreeNode> list);
}
