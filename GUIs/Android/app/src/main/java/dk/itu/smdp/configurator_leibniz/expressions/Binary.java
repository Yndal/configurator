package dk.itu.smdp.configurator_leibniz.expressions;

import com.unnamed.b.atv.model.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import dk.itu.smdp.configurator_leibniz.holder.SelectableHeaderHolder;
import dk.itu.smdp.configurator_leibniz.holder.SelectableItemHolder;

/**
 * Created by lynd on 25/04/15.
 */
public class Binary implements Expression {
    public enum Operator{
        Add,
        Minus,
        Multiply,
        Divide,
        Equal,
        Different,
        And,
        Or,
        LessThan,
        LessThanOrEqual,
        GreaterThan,
        GreaterThanOrEqual,
        Unkown;

        public static Operator getOperator(String s){
            switch (s.trim()){
                case "ADD":
                    return Add;
                case "MINUS":
                    return Minus;
                case "MULTIPLY":
                    return Multiply;
                case "DIVIDE":
                    return Divide;
                case "EQUAL":
                    return Equal;
                case "DIFFERENT":
                    return Different;
                case "AND":
                    return And;
                case "OR":
                    return Or;
                case "LESSTHAN":
                    return LessThan;
                case "LESSTHANOREQUAL":
                    return LessThanOrEqual;
                case "GREATERTHAN":
                    return GreaterThan;
                case "GREATERTHANOREQUAL":
                    return GreaterThanOrEqual;
                default:
                    return Unkown;

            }
        }
    }

    private final Operator operator;
    private final Expression leftExpr;
    private final Expression rightExpr;

    public Binary(Operator operator, Expression leftExpr, Expression rightExpr){
        this.operator = operator;
        this.leftExpr = leftExpr;
        this.rightExpr = rightExpr;
    }

    @Override
    public Object getValue(){
        Object left = leftExpr.getValue();
        Object right = rightExpr.getValue();

        if(left instanceof Boolean && right instanceof Boolean){
            Boolean bLeft = (Boolean) left;
            Boolean bRight = (Boolean) right;
            switch (operator){
                case Equal:
                    return bLeft.equals(bRight);
                case Different:
                    return !bLeft.equals(bRight);
                case And:
                    return bLeft && bRight;
                case Or:
                    return bLeft || bRight;
                default:
                    //TODO
                    //return bVal;
            }
        } else if (left instanceof Integer && right instanceof Integer){
            List<String> dsf = new LinkedList<>(new ArrayList<String>());
            int leftVal = (Integer) left;
            int rightVal = (Integer) right;
            switch (operator){
                case Add:
                    return leftVal + rightVal;
                case Minus:
                    return leftVal - rightVal;
                case Multiply:
                    return leftVal * rightVal;
                case Divide:
                    return leftVal / rightVal;
                case Equal:
                    return leftVal == rightVal;
                case Different:
                    return leftVal != rightVal;
                case LessThan:
                    return leftVal < rightVal;
                case LessThanOrEqual:
                    return leftVal <= rightVal;
                case GreaterThan:
                    return leftVal > rightVal;
                case GreaterThanOrEqual:
                    return leftVal >= rightVal;
                default:
                    //TODO
            }
        } else if (left instanceof String && right instanceof String){
            switch (operator){
                case Equal:
                    return left.equals(right);
                case Different:
                    return !left.equals(right);
                default:
                    //TODO
            }
        } else if (left instanceof TreeNode && right instanceof TreeNode){
            TreeNode l = (TreeNode) left;
            TreeNode r = (TreeNode) right;
            SelectableItemHolder lView = (SelectableItemHolder) l.getViewHolder();
            SelectableItemHolder rView = (SelectableItemHolder) r.getViewHolder();
            SelectableHeaderHolder f;

            //Both lView and rView should be of same type!
            switch (lView.getType()){
                case SeekBar:
                    switch (operator){
                        case Equal:
                            return lView.getSeekBarValue() == rView.getSeekBarValue();
                        case Different:
                            return lView.getSeekBarValue() != rView.getSeekBarValue();
                        default:
                            //TODO
                    }
                case Spinner:
                    switch (operator){
                        case Equal:
                            return lView.getSpinnerValue() == rView.getSpinnerValue();
                        case Different:
                            return lView.getSpinnerValue() != rView.getSpinnerValue();
                        default:
                            //TODO
                    }

                    break;
                case CheckBox:
                    switch (operator){
                        case Equal:
                            return lView.getCheckBoxValue() == rView.getCheckBoxValue();
                        case Different:
                            return lView.getCheckBoxValue() != rView.getCheckBoxValue();
                        default:
                            //TODO
                    }
            }
        }


        throw new RuntimeException("Unknown types: " + left.toString() + " and " + right.toString());
    }

    @Override
    public void collectTreeNodes(List<TreeNode> list){
        leftExpr.collectTreeNodes(list);
        rightExpr.collectTreeNodes(list);


    }

}
