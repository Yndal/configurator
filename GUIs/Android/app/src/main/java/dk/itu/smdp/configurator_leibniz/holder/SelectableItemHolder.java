package dk.itu.smdp.configurator_leibniz.holder;

import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.johnkil.print.PrintView;
import com.unnamed.b.atv.model.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import dk.itu.smdp.configurator_leibniz.Constraint;
import dk.itu.smdp.configurator_leibniz.R;
import dk.itu.smdp.configurator_leibniz.holder.interfaces.Cardinality;
import dk.itu.smdp.configurator_leibniz.holder.interfaces.Selectable;
import dk.itu.smdp.configurator_leibniz.holder.interfaces.Violatable;


/**
 * Created by Bogdan Melnychuk on 2/15/15.
 */
public class SelectableItemHolder extends TreeNode.BaseNodeViewHolder<String> implements Violatable, Selectable {
    public enum Type{
        SeekBar,
        CheckBox,
        Spinner
    }

    private boolean isSelected = false;

    private static final String INFINITY_SYMBOL = "∞";
    private Type type = null;

    private String none = "None";

    private TreeNode node;
    private TextView tvValue;
    private CheckBox nodeSelector;
    private SeekBar seekBar;
    private TextView seekBarTextView;
    private Spinner spinner;
    private EditText numberTextView;
    private TextView fromTextView;
    private TextView betweenTextView;
    private TextView toTextView;
    private EditText integerEditText;

    private String name;
    private int min;
    private int max;
    private List<String> spinnerValues;
    private int quantity;
    private String spinnerValue = "";
    private boolean valueIsSet = false;
    private int seekBarValue;
    private boolean checkBoxValue;
    private String inputData = "";


    public boolean hasGroup = false;
    public PrintView iconView;
    public PrintView arrowView;

    private List<Constraint> constraints = new LinkedList<>();

    public SelectableItemHolder(Context context) {
        super(context);
    }

    @Override
    public View createNodeView(final TreeNode node, String value) {
        this.node = node;
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_selectable_item, null, false);

        tvValue = (TextView) view.findViewById(R.id.node_value);
        tvValue.setText(value);

        //tView = (View) view.findViewById(R.id.tview)
        tView.setDefaultContainerStyle(R.style.TreeNodeStyleCustom);
        tView.setDefaultViewHolder(IconTreeItemHolder.class);


        nodeSelector = (CheckBox) view.findViewById(R.id.node_selector);
        nodeSelector.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                validateConstraints();
            }
        });
        nodeSelector.setChecked(node.isSelected());

        if (node.isLastChild()) {
            view.findViewById(R.id.bot_line).setVisibility(View.INVISIBLE);
        }

        if(iconView == null) {
            iconView = (PrintView) view.findViewById(R.id.iconItem);
            iconView.setVisibility(View.VISIBLE);
        }

        arrowView = (PrintView) view.findViewById(R.id.arrow_icon);
        if (node.isLeaf()) {
            arrowView.setVisibility(View.GONE);
        }

        //TextView to SeekBar
        seekBarTextView = (TextView) view.findViewById(R.id.node_selector_seekbar_textview);
        seekBarTextView.setVisibility(View.INVISIBLE);
        seekBarTextView.setText(String.valueOf(0));


        //SeekBar
        seekBar = (SeekBar) view.findViewById(R.id.node_selector_seekbar);
        seekBar.setVisibility(View.INVISIBLE);

        integerEditText = (EditText) view.findViewById(R.id.integerEditText);


        spinner = (Spinner) view.findViewById(R.id.node_selector_spinner);
        numberTextView = (EditText) view.findViewById(R.id.numberEditText);
        numberTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (v.getId() == R.id.numberEditText && !hasFocus) {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                    //Rest of lines are copied from the setOnEditorActionListener() just below of here
                    boolean hasNoLimit = toTextView.getText().equals(INFINITY_SYMBOL);
                    //int min = Integer.parseInt(fromTextView.getText() + "");
                    int val = 0;
                    try {
                        val = Integer.parseInt(numberTextView.getText() + "");
                    } catch (NumberFormatException nfe) {
                        //val is already set to 0}
                     }

                    if (!hasNoLimit) {
                        int max = Integer.parseInt(toTextView.getText() + "");
                        if (max < val)
                            numberTextView.setText(String.valueOf(max));
                    }

                    quantity = val;
                    setSelected(0 < val);
                }
            }
        });
        numberTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean hasNoLimit = toTextView.getText().equals(INFINITY_SYMBOL);

                int val = 0;
                try {
                    val = Integer.parseInt(numberTextView.getText() + "");
                } catch (NumberFormatException nfe){
                    //val is already set to 0}
                }

                if(!hasNoLimit){
                    int max = Integer.parseInt(toTextView.getText() + "");
                    if (max < val)
                        numberTextView.setText(String.valueOf(max));
                }

                quantity = val;
                setSelected(0 < val);

                return false;
            }
        });

        if(node.isRoot()
                || node.getParent().isRoot()
                || node.getParent().getParent().isRoot()){
            ((View) view.findViewById(R.id.bot_line)).setVisibility(View.VISIBLE);
            ((View) view.findViewById(R.id.top_line)).setVisibility(View.INVISIBLE);
        }

        fromTextView = (TextView) view.findViewById(R.id.fromTextView);
        betweenTextView = (TextView) view.findViewById(R.id.betweenTextView);
        toTextView = (TextView) view.findViewById(R.id.toTextView);

        fromTextView.setVisibility(View.VISIBLE);
        betweenTextView.setVisibility(View.VISIBLE);
        toTextView.setVisibility(View.VISIBLE);

        fromTextView.setText("0");
        betweenTextView.setText("...");
        toTextView.setText(INFINITY_SYMBOL);


        //Setup as the correct type
        switch (type){
            case Spinner:
                setupAsSpinner();
                if(valueIsSet)
                    setSpinnerValue();
                break;
            case CheckBox:
                setupAsCheckBox();
                if(valueIsSet)
                    setCheckBoxValue();
                break;
            case SeekBar:
                setupAsSeekBar();
                if(valueIsSet)
                    setSeekBarValue();
                break;
        }



        return view;
    }

    public void setSpinner(int quantity, String value) {
        this.valueIsSet = true;
        this.quantity = quantity;
        this.spinnerValue = value;
        this.inputData = String.valueOf(value);
    }

    private void setSpinnerValue(){
        numberTextView.setText(quantity + "");

        int counter = 0;
        while(true){
            if(spinner.getItemAtPosition(counter).equals(spinnerValue)) {
                spinner.setSelection(counter);
                break;
            }
            counter++;
        }
    }


    public void showAsSpinner(String name, int min, int max, List<String> values) {
        this.type = Type.Spinner;
        this.name = name;
        this.min = min;
        this.max = max;
        this.spinnerValues = values;
    }


    private void setupAsSpinner(){
        type = Type.Spinner;
        if(spinnerValues == null)
            spinnerValues = new ArrayList<>();

    /*    if(!values.contains(none))
            values.add(0, none);
*/
        ArrayAdapter arrayAdapter = new ArrayAdapter(context, android.R.layout.simple_list_item_1, spinnerValues);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = (String) spinner.getSelectedItem();
                inputData = selectedItem;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                setSelected(false);
            }
        });

        spinner.setVisibility(View.VISIBLE);

        numberTextView.setText("0");
        numberTextView.setVisibility(View.VISIBLE);


    }

    public void setSeekBar(int value) {
        this.valueIsSet = true;
        this.seekBarValue = value;
        this.inputData = String.valueOf(value);
        this.isSelected = value < min ? false : true;
        this.quantity = value;
    }

    private void setSeekBarValue(){
        seekBar.setProgress(seekBarValue);
        seekBarTextView.setText(seekBarValue + "");
        integerEditText.setText(seekBarValue + "");
    }

    public void showAsSeekBar(String name, final int min, final int max) {
        this.type = Type.SeekBar;
        this.name = name;
        this.min = min;
        this.max = max;
    }

    private void setupAsSeekBar(){
        type = Type.SeekBar;
        if(max == -1){ //If has no upper limit...
            fromTextView.setText(Integer.toString(min));
            integerEditText.setVisibility(View.VISIBLE);
            integerEditText.setText(0 + "");
            integerEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    int val = 0;
                    try {
                        val = Integer.parseInt(integerEditText.getText() + "");
                        if(val < min)
                            val = min;

                        integerEditText.setText(String.valueOf(val));
                        inputData = String.valueOf(val);
                        quantity = val;
                        setSelected(0 < val);
                    } catch (NumberFormatException nfe){
                        integerEditText.setText("0");
                        inputData = "0";
                        quantity = 0;

                    }
                    return false;
                }
            });
            integerEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (v.getId() == R.id.integerEditText && !hasFocus) {
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                        int val = 0;
                        try {
                            val = Integer.parseInt(integerEditText.getText() + "");
                            if (val < min)
                                val = min;

                            integerEditText.setText(min);
                            inputData = String.valueOf(val);
                            quantity = val;
                            setSelected(0 < val);
                        } catch (NumberFormatException nfe) {
                            integerEditText.setText("0");
                            inputData = "0";
                            quantity = 0;

                        }
                    }
                }
            });
        } else {
            seekBarTextView.setText(Integer.toString(0));
            fromTextView.setText(Integer.toString(min));
            toTextView.setText(Integer.toString(max));
            seekBar.setMax(max - min + 1);
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    int value;
                    if (progress == 0) {
                        value = 0;
                    } else {
                        value = min + progress - 1;
                    }

                    seekBarTextView.setText(String.valueOf(value));
                    inputData = String.valueOf(progress);
                    quantity = value;
                    setSelected(value != 0);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    validateConstraints();
                }
            });

            seekBar.setVisibility(View.VISIBLE);
            seekBarTextView.setVisibility(View.VISIBLE);
        }
    }

    public void setCheckBox(boolean isChecked) {
        this.valueIsSet = true;
        this.checkBoxValue = isChecked;
        this.inputData = String.valueOf(isChecked);
        this.quantity = isChecked ? 1 : 0;
    }

    private void setCheckBoxValue(){
        nodeSelector.setChecked(checkBoxValue);
    }

    public void showAsCheckBox(String name){
        this.type = Type.CheckBox;
        this.name = name;
    }

    private void setupAsCheckBox() {
        type = Type.CheckBox;

        toTextView.setVisibility(View.INVISIBLE);
        betweenTextView.setVisibility(View.INVISIBLE);
        fromTextView.setVisibility(View.INVISIBLE);



        nodeSelector.setVisibility(View.VISIBLE);
        nodeSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean val = nodeSelector.isChecked();
                inputData = String.valueOf(val);
                quantity = val ? 1 : 0;
                setSelected(val);
            }
        });
    }

    public void setConstraintsRef(List<Constraint> constraints){
        this.constraints = constraints;
    }

    public void setAsGroup(){
        if(!hasGroup) {
            hasGroup = true;
            iconView.setVisibility(View.GONE);
            arrowView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void toggleSelectionMode(boolean editModeEnabled) {
        nodeSelector.setVisibility(editModeEnabled ? View.VISIBLE : View.GONE);
        nodeSelector.setChecked(mNode.isSelected());
    }

    @Override
    public void toggle(boolean active) {
        if (hasGroup) {
            arrowView.setIconText(context.getResources().getString(active ? R.string.ic_keyboard_arrow_down : R.string.ic_keyboard_arrow_right));
        }
    }

    private void setSelected(boolean isSelected) {
        this.isSelected = isSelected;

        int color = isSelected ? NORMAL_COLOR : VIOLATED_COLOR;

        if (!node.isRoot() && node.getParent().getViewHolder() != null) {
            TreeNode.BaseNodeViewHolder holder = node.getParent().getViewHolder();
            if(holder instanceof Cardinality)
                ((Cardinality) holder).updateCardinality();
        }
    }

    @Override
    public boolean isSelected(){
        return this.isSelected;
    }

    private void validateConstraints(){
        //Toast.makeText(getView().getContext(), "Inside validateContraints", Toast.LENGTH_SHORT).show();
        if(constraints != null)
            for(Constraint c : constraints)
                c.validate(context);
    }

    public void markViolated(int color){
          tvValue.setBackgroundColor(color);
    }

    public Type getType(){
        return type;
    }

    public int getSeekBarValue(){
        return Integer.valueOf(seekBarTextView.getText().toString());
    }

    public boolean getCheckBoxValue(){
        return nodeSelector.isChecked();
    }

    public String getSpinnerValue(){
        return spinner.getSelectedItem().toString();
    }


    public String getName(){
        return name;
    }
    public int getQuantity(){
        return quantity;
    }

    public boolean isIncluded(){
        return isSelected;
    }

    public String getInputData(){
        return inputData;
    }

}

