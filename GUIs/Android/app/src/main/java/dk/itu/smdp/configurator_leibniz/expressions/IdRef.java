package dk.itu.smdp.configurator_leibniz.expressions;

import com.unnamed.b.atv.model.TreeNode;

import java.util.List;

/**
 * Created by lynd on 25/04/15.
 */
public class IdRef implements Expression{
    private final TreeNode ref;

    public IdRef(TreeNode ref){
        this.ref = ref;
    }

    public TreeNode getRef(){
        return ref;
    }

    @Override
    public TreeNode getValue(){
        return getRef();
    }

    @Override
    public void collectTreeNodes(List<TreeNode> list){
        list.add(ref);
    }
}
