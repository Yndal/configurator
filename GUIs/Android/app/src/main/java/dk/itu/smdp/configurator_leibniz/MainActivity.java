package dk.itu.smdp.configurator_leibniz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import dk.itu.smdp.configurator_leibniz.REST.GetConfigurationList;


public class MainActivity extends ActionBarActivity {
    public static final String TAG = "MAIN ACTIVITY";

    private MenuItem loadMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        String url = getString(R.string.JSON_REPO) + getString(R.string.CONFS_FILE_NAME);
        final List<String> urls = getAvailableConfigurations(url);

        final ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, urls);
        ListView listView = (ListView) findViewById(R.id.listview);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);

                String jsonPath = getString(R.string.JSON_REPO) + item;

                Intent intent = new Intent(getBaseContext(), ConfigurationActivity.class);
                intent.putExtra(ConfigurationActivity.JSON_PATH, jsonPath);
                startActivity(intent);

              //  Toast.makeText(getBaseContext(), "Clicked: " + item, Toast.LENGTH_SHORT).show();
            }

        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menuLoadItem) {
            //TODO Add clever stuff here instead
            Toast.makeText(this, "Selected: " + item.getTitle(), Toast.LENGTH_LONG).show();
            String json = PersistenceContainer.load(this);
            Intent loadIntent = new Intent(getBaseContext(), ConfigurationActivity.class);
            loadIntent.putExtra(PersistenceContainer.PERS_CONT, json);
            startActivity(loadIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private List<String> getAvailableConfigurations(String url){
        try{
            return new GetConfigurationList().execute(url).get();
        } catch (Exception ee){ //Catch everything...
        }


        return new LinkedList<>();
    }
}
