package dk.itu.smdp.configurator_leibniz.REST;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by lynd on 06/04/15.
 */
public class GetJson extends AsyncTask<String, Void, String> {
    @Override
    protected String doInBackground(String... urls) {
        if(urls == null || urls.length < 1 || urls[0] == null)
            return "";

        InputStream inputStream = null;
        String result = "";
        try {

            URL url = new URL(urls[0]);

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            inputStream = new BufferedInputStream(urlConnection.getInputStream());

            if(inputStream != null)
                result = convertInputStreamToString(inputStream);

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;

    }

    private static String convertInputStreamToString(InputStream in) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(in));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        in.close();
        return result;
    }

}
