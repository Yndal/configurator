package dk.itu.smdp.configurator_leibniz;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import dk.itu.smdp.configurator_leibniz.REST.GetJson;
import dk.itu.smdp.configurator_leibniz.expressions.Binary;
import dk.itu.smdp.configurator_leibniz.expressions.BoolConst;
import dk.itu.smdp.configurator_leibniz.expressions.Expression;
import dk.itu.smdp.configurator_leibniz.expressions.IdRef;
import dk.itu.smdp.configurator_leibniz.expressions.IntConst;
import dk.itu.smdp.configurator_leibniz.expressions.StringConst;
import dk.itu.smdp.configurator_leibniz.holder.IconTreeItemHolder;
import dk.itu.smdp.configurator_leibniz.holder.SelectableHeaderHolder;
import dk.itu.smdp.configurator_leibniz.holder.SelectableItemHolder;


public class ConfigurationActivity extends ActionBarActivity implements Serializable{

    public static final String TAG = "CONFIGURATION ACTIVITY";
    public static final String JSON_PATH = "JSON_PATH";
    private TextView jsonTextView;
    private String configurationURL;


    private HashMap<String, String> mapTypes = new HashMap<>();
    private HashSet<String> typesBool = new HashSet<>();
    private HashSet<String> typesInt = new HashSet<>();
    private HashMap<String, List<String>> typesEnum = new HashMap<>();
    private HashMap<String, TreeNode> allTreeNodes = new HashMap<>();
    private HashMap<String, TreeNode> mapConstraints = new HashMap<>();
    private HashMap<TreeNode, List<Constraint>> constraints = new HashMap<>();
    private TreeNode root;
    private JSONObject  originalJson;

    //These might not be needed....
    private List<TreeNode> typesStorage = new ArrayList<>();
    private List<TreeNode> constraintsStorage = new ArrayList<>();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_configuration, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menuSaveItem) {
            try {
                String json = treenode2jsonString();
                PersistenceContainer.save(this, json);
            } catch (JSONException je) {
                //...
                Log.d(TAG, je.getMessage());
            }

            return true;
        } else if (id == R.id.sendConfigMenuItem){
            try {
                String config = treenode2jsonString();

                //TODO Set receivers here
                sendMail("lynd@itu.dk", "Leibniz - Configuration", config);
               /* sendMail("guar@itu.dk", "Leibniz - Configuration", config);
                sendMail("hbsl@itu.dk", "Leibniz - Configuration", config);
                sendMail("esfr@itu.dk", "Leibniz - Configuration", config);*/
            } catch (JSONException je) {
                Toast.makeText(this, "Unable to create configuration before sending", Toast.LENGTH_SHORT).show();
            }

        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String pc = getIntent().getExtras().getString(PersistenceContainer.PERS_CONT);
        if(pc == null) {

            configurationURL = getIntent().getExtras().getString(JSON_PATH);
            String json = getJson(configurationURL);

            if (json != null) {
                root = json2TreeView(json);


            } else {
                jsonTextView.setText("Unable to collect configuration from the Internet...");
            }
        } else {
           root = json2TreeView(pc);
            Log.e(TAG, "Loaded root value: " + ((root == null) ? "Null" : "Not Null") );
        }
        AndroidTreeView tView = new AndroidTreeView(this, root);
        setContentView(tView.getView());
        //setContentView(R.layout.activity_configuration);
        jsonTextView = (TextView) findViewById(R.id.jsonTextView);
    }



    private String getJson(String url){
        String json;
        try{
            json = new GetJson().execute(url).get();
        } catch (Exception ee){ //Catch everything...
            json = "";
        }
        return json;
    }


    private TreeNode json2TreeView(String json){
        try {
            JSONObject topElement = new JSONObject(json);

            Iterator<String> it = topElement.keys();
            List<String> keys = new ArrayList<>();
            while(it.hasNext())
                keys.add(it.next());

            if(keys.contains("OriginalConfig")){
               JSONObject config = topElement.getJSONObject("OriginalConfig");
                originalJson = config;

                TreeNode result = extractConfiguration(config);

                for(TreeNode tn : allTreeNodes.values())
                    //This will force the validation of all TreeNodes, without having RuntimeExceptions
                    tn.setExpanded(true);

                if(keys.contains("Saves")) {
                    JSONArray saves = topElement.getJSONArray("Saves");
                    JSONObject firstSave = saves.getJSONObject(0); //TODO Doesn't support multiple saves yet

                    extractSave(firstSave);
                }


                return result;
            } else {
                originalJson = topElement;
                TreeNode result = extractConfiguration(topElement);

                for(TreeNode tn : allTreeNodes.values())
                    //This will force the validation of all TreeNodes, without having RuntimeExceptions
                    tn.setExpanded(true);

                return result;
            }



           /* switch(""){
                case "OriginalConfig":

                    break;
                case "FeatureInstances":

                    break;

            }*/

         //   return result;
        } catch (JSONException je){
            Log.d(TAG, je.getMessage());
        }

        return TreeNode.root();
    }

    private TreeNode extractConfiguration(JSONObject o) throws JSONException{
        Iterator<String> keys = o.keys();
        String name = "";
        TreeNode types = null;
        TreeNode features = null;
        TreeNode constraints = null;
        root = TreeNode.root();

        while(keys.hasNext()) {
            String key = keys.next();

            switch (key){
                case "Name":
                    name = o.getString(key);
                    break;
                case "Types":
                    types = new TreeNode(key);
                    extractTypes(o.getJSONArray(key), types);
                    break;
                case "Features":
                    features = TreeNode.root();
                    extractFeatures(o.getJSONArray(key), features);
                    break;
                case "Constraints":
                    constraints = new TreeNode(key);
                    constraints = extractConstraints(o.getJSONArray(key), constraints);
                    break;
                default:
                    //Ignoring anything else...
                    break;
            }
        }

        setTitle("Configure your: " + name);

        root.addChild(features);
        typesStorage.add(types);
        constraintsStorage.add(constraints);

        return features;
    }

    private void extractTypes(JSONArray array, TreeNode parent){
        try{
            for(int i=0; i<array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                String name = object.getString("Name");
                String type = object.getString("Type");
                switch (type) {
                    case "Boolean":
                        TreeNode bType = new TreeNode(name + "==>" + type);
                        typesBool.add(name);

                        mapTypes.put(name, type);
                        parent.addChild(bType);
                        break;
                    case "Integer":
                        TreeNode iType = new TreeNode(name + "==>" + type);
                        typesInt.add(name);
                        mapTypes.put(name, type);
                        parent.addChild(iType);
                        break;
                    case "Enum":
                        TreeNode eType = new TreeNode(name + "==>" + type);
                        mapTypes.put(name, type);

                        JSONArray enumArr = object.getJSONArray("EnumValues");
                        List<String> enumList = new ArrayList<>();
                        for (int ii = 0; ii < enumArr.length(); ii++) {
                            String enumType = enumArr.getString(ii);
                            enumList.add(enumType);
                            eType.addChild(new TreeNode(enumType));
                        }
                        typesEnum.put(name, enumList);
                        parent.addChild(eType);
                        break;
                    default:
                        //....
                        break;
                }

            }
        } catch (JSONException js){
            Log.d(TAG, js.getMessage());
        }

    }

    private void extractFeatures(JSONArray features, TreeNode parent) throws JSONException{
        for(int i=0; i<features.length(); i++){
            JSONObject feature = features.getJSONObject(i);

            parent.addChild(extractFeature(feature));
        }
    }

    private TreeNode extractFeature(JSONObject o){
        try {
            Iterator<String> keys = o.keys();

            String name = "";
            String type = "";
            int min = -2;
            int max = -2;
            JSONArray groups = null;

            while(keys.hasNext()) {
                String key = keys.next();

                switch (key) {
                    case "Name":
                        name = o.getString(key);
                    case "Type":
                        type = o.getString(key);
                        break;
                    case "Min":
                        min = o.getInt(key);
                        break;
                    case "Max":
                        max = o.getInt(key);
                        break;
                    case "FeatureGroups":
                        groups = o.getJSONArray(key);
                    default:
                        //Ignoring anything else
                        break;
                }
            }
            //If any of the criteria for creating a group fails, do...
            if (name.equals("") || type.equals("") || min == -2 || max == -2)
                return createErrorTreeNode();

            final String fName = name;
            final String fType = type;
            final int fMin = min;
            final int fMax = max;
            final boolean hasGroups = groups == null ? false : true;
            SelectableItemHolder holder = new SelectableItemHolder(this);

            final TreeNode parent = new TreeNode(name);
            String val = mapTypes.get(type);
            if(val != null){
                switch (val){
                    case "Boolean":
                        holder = new SelectableItemHolder(this){
                            @Override
                            public View createNodeView(final TreeNode node, String value) {
                                View view = super.createNodeView(node, value);

                                if(hasGroups)
                                    setAsGroup();

                                return view;
                            }

                        };
                        holder.showAsCheckBox(fName);

                        break;
                    case "Integer":
                        holder = new SelectableItemHolder(this){
                            @Override
                            public View createNodeView(final TreeNode node, String value) {
                                View view = super.createNodeView(node, value);

                                if(hasGroups)
                                    setAsGroup();

                                return view;
                            }
                        };
                        holder.showAsSeekBar(fName, fMin, fMax);

                        break;
                    case "Enum":
                        holder = new SelectableItemHolder(this){
                            @Override
                            public View createNodeView(final TreeNode node, String value) {
                                View view = super.createNodeView(node, value);
                                setConstraintsRef(constraints.get(parent));
                                if(hasGroups)
                                    setAsGroup();


                                return view;
                            }
                        };
                        List<String> list = typesEnum.get(fType);
                        holder.showAsSpinner(fName, fMin, fMax, list);

                        break;
                }
            }

            parent.setViewHolder(holder);
           // holder.setAsGroup();
            if(!constraints.containsKey(parent))
                constraints.put(parent, new LinkedList<Constraint>());
            holder.setConstraintsRef(constraints.get(parent));
            allTreeNodes.put(name, parent);



            // TreeNode parent = new TreeNode(name + "(Min:" + min + ", Max: "  + max + ", Type: " + type + ")");
            //parent = new TreeNode(key);//.addChild(child);


            /****************
             * Get groups
             ***************/
            if(groups != null) {
                for (int i = 0; i < groups.length(); i++) {
                    JSONObject group = groups.getJSONObject(i);
                    parent.addChild(extractGroup(group));
                }
            }

            return parent;

        } catch (JSONException js){
            Log.d(TAG, js.getMessage());
        }

        return createErrorTreeNode();
    }



    private TreeNode extractGroup(JSONObject o){
        try {
            Iterator<String> keys = o.keys();

            String name = "";
            int min = -2;
            int max = -2;
            JSONArray features = null;

            //Should only contain a single key
            while(keys.hasNext()) {
                String key = keys.next();

                switch (key) {
                    case "Name":
                        name = o.getString(key);
                        break;
                    case "Min":
                        min = o.getInt(key);
                        break;
                    case "Max":
                        max = o.getInt(key);
                        break;
                    case "Features":
                        features = o.getJSONArray(key);
                        break;
                    default:
                        //Something went wrong, but we're not handling it...
                        break;
                }
            }


            //If any of the criteria for creating features fails, do...
            if (min == -2 || max == -2 || features == null)
                return createErrorTreeNode();

            SelectableHeaderHolder holder = new SelectableHeaderHolder(this){
                @Override
                public View createNodeView(final TreeNode node, IconTreeItemHolder.IconTreeItem value){
                    View view = super.createNodeView(node, value);
                    updateCardinality();
                    return view;
                }
            };
            holder.setMinMax(min, max);
            TreeNode parent = new TreeNode(new IconTreeItemHolder.IconTreeItem(R.string.ic_folder,  name)).setViewHolder(holder);
            allTreeNodes.put(name, parent);
//            TreeNode parent = new TreeNode(name + "(Min:" + min + ", Max: "  + max + ")");


            /****************
             * Get features
             ***************/
            // JSONArray types = o.keys().getJSONArray("Types");
            for (int i = 0; i < features.length(); i++) {
                JSONObject feature = features.getJSONObject(i);
                //extractFeature(feature, parent);
                parent.addChild(extractFeature(feature));
            }

            return parent;

        } catch (JSONException js){
            Log.d(TAG, js.getMessage());
            return createErrorTreeNode();
        }
    }

    private TreeNode extractConstraints(JSONArray array, TreeNode parent){
        try {
            for(int i=0; i<array.length(); i++){
                JSONObject constraint = array.getJSONObject(i);

                parent.addChild(extractConstraint(constraint));
            }
        } catch (JSONException js){
            Log.d(TAG, js.getMessage());
        }

        return parent;
    }

    private TreeNode extractConstraint(JSONObject jsonConstraint){
        try{
            String name = "";
            String msg = "";
            JSONObject jsonExpression = null;

            Iterator<String> keys = jsonConstraint.keys();
            while(keys.hasNext()){
                String key = keys.next();
                switch(key) {
                    case "Name":
                        name = jsonConstraint.getString(key);
                        break;
                    case "Msg":
                        msg = jsonConstraint.getString(key);
                        break;
                    case "Expression":
                        jsonExpression = jsonConstraint.getJSONObject(key);
                        break;
                    default:
                        //Tough luck...
                        break;
                }
            }

            if(jsonExpression == null)
                return createErrorTreeNode();

            //For-loop should only containt a single item
            TreeNode parent = new TreeNode(name + "(Msg: " + msg + ")");

            Expression expression = extractExpression(jsonExpression);
            Constraint constraint = new Constraint(name, msg, expression);


            List<TreeNode> list = new LinkedList<>();
            expression.collectTreeNodes(list);

            for(TreeNode tn : list){
                if(!constraints.containsKey(tn))
                    constraints.put(tn, new LinkedList<Constraint>());
                constraints.get(tn).add(constraint);
            }
            //constraints.put(tn, list);
            //parent.addChild(extractExpression(jsonExpression));


            return parent;

        } catch (JSONException je){
            //Not handling this
        }

        return createErrorTreeNode();
    }

    private Expression extractExpression(JSONObject exprObj){
        try {
            Iterator<String> keys = exprObj.keys();
            while(keys.hasNext()){
                String key = keys.next();
                switch (key){
                    case "Type":
                        String type = exprObj.getString(key);

                        switch(type){
                            case "Unary":
                                return extractUnary(exprObj);
                            case "Binary":
                                return extractBinary(exprObj);
                            case "StringConst":
                            case "IntConst":
                            case "BoolConst":
                                return extractConst(exprObj);
                            case "IdRef":
                                return extractIdRef(exprObj);
                            default:
                                //Ignore this
                                break;
                        }
                        break;
                }
            }

        } catch (JSONException je){
            //Ignore
        }

        throw new RuntimeException("Unable to recognize Expression: " + exprObj);
        //return createErrorTreeNode();
    }


    private Expression extractUnary(JSONObject unary) {
        try {
            Iterator<String> keys = unary.keys();
            String type = ""; //Should be "Unary", but is for checking
            String operator = "";
            JSONObject jsonExpr = null;

            while (keys.hasNext()) {
                String key = keys.next();
                switch (key) {
                    case "Type":
                        type = unary.getString(key);
                        break;
                    case "Operator":
                        operator = unary.getString(key);
                        break;
                    case "Expression":
                        jsonExpr = unary.getJSONObject(key);
                        break;
                    default:
                        break;

                }
            }

            if(type.equals("") || operator.equals("") || jsonExpr == null)
                throw new RuntimeException("Type, operator or jsonExpr is not set (type: " + type + ", operator: " + operator + ", jsonExpr: " + jsonExpr + ")");
            // return createErrorTreeNode();

            TreeNode parent = new TreeNode(type + "(op: " + operator + ")");
            Expression expression = extractExpression(jsonExpr);
            //parent.addChild(extractExpression(jsonExpr));
        } catch (JSONException je){
            //Do nothing
        }

        throw new RuntimeException("Got to unreachable point...");
        //return createErrorTreeNode();
    }

    private Expression extractBinary(JSONObject jsonBinary){
        try {
            Iterator<String> keys = jsonBinary.keys();
            String type = ""; //Should be "Binary", but is for checking
            String operator = "";
            JSONObject left = null;
            JSONObject right = null;

            while (keys.hasNext()) {
                String key = keys.next();
                switch (key) {
                    case "Type":
                        type = jsonBinary.getString(key);
                        break;
                    case "Operator":
                        operator = jsonBinary.getString(key);
                        break;
                    case "Left":
                        left = jsonBinary.getJSONObject(key);
                        break;
                    case "Right":
                        right = jsonBinary.getJSONObject(key);
                    default:
                        break;

                }
            }

            if(type.equals("") || operator.equals("") || left == null || right == null)
                throw new RuntimeException("Type, operator, leftExpr or rightExpr  is not set (type: " + type + ", operator: " + operator + ", left: " + left + ", right: " + right + ")");
                //return createErrorTreeNode();

            TreeNode parent = new TreeNode(type + "(Op: " + operator + ")");
            Expression leftExpr = extractExpression(left);
            Expression rightExpr = extractExpression(right);
            return new Binary(Binary.Operator.getOperator(operator), leftExpr, rightExpr);

            //parent.addChild(extractExpression(left));
           // parent.addChild(extractExpression(right));

           // return parent;
        } catch (JSONException je){
            //Do nothing
        }

        throw new RuntimeException("Got to unreachable point...");
        //return createErrorTreeNode();
    }

    private Expression extractConst(JSONObject sConst){
        try {
            Iterator<String> keys = sConst.keys();
            String type = "";
            String value = "";

            while (keys.hasNext()) {
                String key = keys.next();
                switch (key) {
                    case "Type":
                        type = sConst.getString(key);
                        break;
                    case "Value":
                        value = sConst.getString(key);
                        break;
                    default:
                        break;
                }
            }

            if(type.equals("") || value.equals(""))
                throw new RuntimeException("Type or value is not set (type: " + type + ", value: " + value + ")");// + return createErrorTreeNode();
                // return createErrorTreeNode();

            //TreeNode parent = new TreeNode(type + "(Value: " + value + ")");
            switch (type){
                case "StringConst":
                    return new StringConst(value);
                case "IntConst":
                    return new IntConst(Integer.valueOf(value));
                case "BoolConst":
                    return new BoolConst(Boolean.valueOf(value));
                default:
                    //Tough luck...
                    break;
            }


           // return parent;
        } catch (JSONException je){
            //Do nothing
        }

        throw new RuntimeException("Got to unreachable point...");
        //return createErrorTreeNode();
    }

    private Expression extractIdRef(JSONObject jsonIdRef){
        try {
            Iterator<String> keys = jsonIdRef.keys();
            String type = "";
            String ref = "";

            while (keys.hasNext()) {
                String key = keys.next();
                switch (key) {
                    case "Type":
                        type = jsonIdRef.getString(key);
                        break;
                    case "Ref":
                        ref = jsonIdRef.getString(key);
                        break;
                    default:
                        break;
                }
            }

            if(type.equals("") || ref.equals(""))
                throw new RuntimeException("Type or ref is not set (type: " + type + ", ref: " + ref + ")");// + return createErrorTreeNode();

            TreeNode parent = new TreeNode(type + "(Ref: " + ref + ")");
            return new IdRef(allTreeNodes.get(ref));

            //return parent;
        } catch (JSONException je){
            //Do nothing
        }

        throw new RuntimeException("Got to unreachable point...");
//        return createErrorTreeNode();
    }


    private void extractSave(JSONObject save) throws JSONException{
        Iterator<String> keys = save.keys();

        JSONArray FIs = new JSONArray();
        String savedBy = "";
        String savedOn = "";


        while(keys.hasNext()) {
            String key = keys.next();

            switch (key){
                case "FeatureInstances":
                    FIs = save.getJSONArray(key);
                    break;
                case "SavedOn":
                    savedOn = save.getString(key);
                    break;
                case "SavedBy":
                    savedBy = save.getString(key);
                    break;
                default:
                    //Ignore
                    break;
            }
        }
        extractFeatureInstances(FIs);
        Toast.makeText(this, "Load configuration saved by " + savedBy + " at " + savedOn, Toast.LENGTH_SHORT).show();

    }



    private void extractFeatureInstances(JSONArray array){
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);

                Iterator<String> keys = object.keys();

                String name = null;
                //JSONObject feature = null;
                int quantity = -2;
                boolean included = false;
                String inputData = null;


                while (keys.hasNext()) {
                    String key = keys.next();

                    switch (key) {
                        case "Name":
                            name = object.getString(key);
                            break;
                       /* case "Feature":
                            feature = object.getJSONObject(key);
                            break;
                        */case "Quantity":
                            String val = object.getString(key);
                            if(val.equals(""))
                                break;
                            quantity = Integer.valueOf(val);
                            break;
                        case "Included":
                            included = object.getBoolean(key);
                            break;
                        case "InputData":
                            inputData = object.getString(key);
                            break;
                    }
                }


                TreeNode treeNode = allTreeNodes.get(name);
                SelectableItemHolder itemHolder = (SelectableItemHolder) treeNode.getViewHolder();
                SelectableItemHolder.Type type = itemHolder.getType();
                if(type != null) {
                    switch (type) {
                        case SeekBar:
                            if(inputData == null)
                                break;
                            itemHolder.setSeekBar(Integer.valueOf(inputData));
                            break;
                        case CheckBox:
                            if(quantity == -2)
                                break;
                            itemHolder.setCheckBox(quantity == 1 && included);
                            break;
                        case Spinner:
                            if(inputData == null || quantity == -2)
                                break;
                            itemHolder.setSpinner(quantity, inputData);
                            break;
                        default:
                            //Ignore
                            break;
                    }
                }
            }
        } catch (JSONException je) {
            //To bad...
        }
    }


    private static TreeNode createErrorTreeNode(){
        return new TreeNode("Missing functionality... An error occurred.");
    }


    private String treenode2jsonString() throws JSONException{
        JSONObject root = new JSONObject();
        root.put("OriginalConfig", originalJson);


        JSONArray featInst = new JSONArray();
        Set<String> keySet = allTreeNodes.keySet();
        for(String key : keySet){
            TreeNode tn = allTreeNodes.get(key);
            if(tn.getViewHolder() instanceof SelectableHeaderHolder)
                continue;

            SelectableItemHolder holder = (SelectableItemHolder) tn.getViewHolder();
            String name =  holder.getName();
            int quantity = holder.getQuantity();
            boolean included = holder.isIncluded();
            String inputData = holder.getInputData();

            JSONObject object = new JSONObject();
            object.put("Name", name);
            object.put("Quantity", quantity);
            object.put("Included", included);
            object.put("InputData", inputData);

            featInst.put(object);
        }
        JSONObject save1 = new JSONObject();
        save1.put("FeatureInstances", featInst);
        save1.put("SavedOn", getDate());
        save1.put("SavedBy", "Android himself");

        JSONArray saves = new JSONArray();
        saves.put(save1);

        root.put("Saves", saves);

//        String debug = root.toString();

        return root.toString();
    }

    private String getDate(){
        return android.text.format.DateFormat.format("yyyy-MM-dd", new java.util.Date()).toString();
        /*Date date = new Date(location.getTime());
        DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
        mTimeText.setText("Time: " + dateFormat.format(date));*/

    }

    /***********************************************************************************************************************
     * From this point, the code is kindly borrowed from the QuizApp created in the Model Driven Development course at ITU
     ***********************************************************************************************************************/


    /**
     * These are static defined for Google SMTP. If you want to use another
     * e-mail server, change these accordingly
     * @return Session for sending e-mails
     */
    private Session createSessionObject() {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.office365.com");
        properties.put("mail.smtp.port", "587");

        return Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("lynd@itu.dk", "password"); //TODO Add credentials
            }
        });
    }

    private Message createMessage(String email, String subject, String messageBody, Session session) throws MessagingException, UnsupportedEncodingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress("lynd@itu.dk", "Leibniz Configuration")); // this e-mail address does not have to be valid :)
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(email, email));
        message.setSubject(subject);
        message.setText(messageBody);
        return message;
    }
    /*
     * Send e-mail functionality. Takes as arguments an e-mail address as a String,
     * a subject as a String and a message body as a String.
     *
     */
    private void sendMail(String email, String subject, String messageBody) {
        Session session = createSessionObject();
        try {
            Message message = createMessage(email, subject, messageBody, session);
            new SendMailTask().execute(message);
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    /*
     * The standard way of creating Threads for different tasks in Android.
     * In the doInBackground the stuff happens in a different thread than the UI thread.
     * It is not recommended to have this kind of functionality on the UI thread.
     * Always use an AsyncTask for these kind of things.
     *
     */
    private class SendMailTask extends AsyncTask<Message, Void, Void> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(ConfigurationActivity.this, "Please wait", "Sending configuration", true, false);
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();

        }

        protected Void doInBackground(Message... messages) {
            try {
                Transport.send(messages[0]);
                Toast.makeText(getApplicationContext(), "Configuration has been send", Toast.LENGTH_LONG).show();
            } catch (MessagingException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Unable to send configuration: " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
            return null;
        }
    }

    /***********************************************************************************************************************
     * End of borrowed code
     ***********************************************************************************************************************/
}
