package dk.itu.smdp.configurator_leibniz.expressions;

import com.unnamed.b.atv.model.TreeNode;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lynd on 25/04/15.
 */
public class StringConst implements Expression {
    private final String value;

    public StringConst(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public void collectTreeNodes(List<TreeNode> list){
    }

}
