package dk.itu.smdp.configurator_leibniz.holder.interfaces;

import android.graphics.Color;

/**
 * Created by lynd on 27/04/15.
 */
public interface Violatable{
    public static final int VIOLATED_COLOR = Color.RED;
    public static final int NORMAL_COLOR = 0x00000000;

    void markViolated(int color);
}