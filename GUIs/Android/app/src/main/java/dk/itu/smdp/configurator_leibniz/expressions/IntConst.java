package dk.itu.smdp.configurator_leibniz.expressions;

import com.unnamed.b.atv.model.TreeNode;

import java.util.List;

/**
 * Created by lynd on 25/04/15.
 */
public class IntConst implements Expression {
    private final int value;

    public IntConst(int value){
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public void collectTreeNodes(List<TreeNode> list){
    }

}
