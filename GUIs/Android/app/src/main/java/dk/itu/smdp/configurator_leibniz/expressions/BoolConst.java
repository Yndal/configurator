package dk.itu.smdp.configurator_leibniz.expressions;

import com.unnamed.b.atv.model.TreeNode;

import java.util.List;

/**
 * Created by lynd on 25/04/15.
 */
public class BoolConst implements Expression {
    private final boolean value;

    public BoolConst(boolean value){
        this.value = value;
    }

    @Override
    public Boolean getValue(){
        return value;
    }

    @Override
    public void collectTreeNodes(List<TreeNode> list){
    }

}
