package dk.itu.smdp.configurator_leibniz;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


/**
 * Created by esben on 27-04-2015.
 */
public class PersistenceContainer implements Serializable{
    private static final String FILE_PATH = "saveFile.ser";

    public static final String PERS_CONT ="PersistenceContainer";
    private static final String TAG = "PersistenceContainer";

    public static String load(Context context){
        try (
                FileInputStream file = context.openFileInput(FILE_PATH);
                BufferedInputStream buffer =new BufferedInputStream(file);
                ObjectInputStream input = new ObjectInputStream(buffer);
        ) {
            String json = (String) input.readObject();
            Toast.makeText(context, "Loaded previous configuration", Toast.LENGTH_LONG).show();
            return json;

        }
        catch (FileNotFoundException e){Log.e(TAG, "FileNotFoundExeption: Can not load");}
        catch(ClassNotFoundException e){Log.e(TAG, "ClassClassNotFoundException");}
        catch (IOException e){Log.e(TAG, "IOException");}
        return "";
    }

    public static void save(Context context, String json){
        try (
            FileOutputStream file = context.openFileOutput(FILE_PATH, Context.MODE_PRIVATE);//new FileOutputStream( "saveFile.ser");
            BufferedOutputStream buffer = new BufferedOutputStream(file);
            ObjectOutputStream output = new ObjectOutputStream(buffer);
        ){
            output.writeObject(json);
            Toast.makeText(context, "Configuration saved!", Toast.LENGTH_LONG).show();
            return;
        }
        catch (FileNotFoundException e){
            Log.e(TAG, "FileNotFoundExeption: Can not save");
        }
        catch (IOException e){
            Log.e(TAG, "IOException. Counter is ");
        }
        Toast.makeText(context, "Unable to save configuration", Toast.LENGTH_LONG).show();
    }
}
