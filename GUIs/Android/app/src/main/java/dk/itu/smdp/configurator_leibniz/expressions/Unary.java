package dk.itu.smdp.configurator_leibniz.expressions;

import com.unnamed.b.atv.model.TreeNode;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lynd on 25/04/15.
 */
public class Unary implements Expression{
    public enum Operator{
        Not,
        Plus,
        Minus
    }

    private final Operator operator;
    private final Expression expr;


    public Unary(Operator operator, Expression expression) {
        this.operator = operator;
        this.expr = expression;

   //     validate();
    }

    @Override
    public Object getValue(){
        Object val = expr.getValue();

        if(val instanceof Boolean){
            Boolean bVal = (Boolean) val;
            switch (operator){
                case Not:
                    return !bVal;
                default:
                    return bVal;
            }
        } else if (val instanceof Integer){
            int iVal = (Integer) val;
            switch (operator){
                case Minus:
                    return iVal * -1;
                default:
                    return iVal;
            }
        } else if (val instanceof String ||
                   val instanceof TreeNode){
            return val;
        } else
            throw new RuntimeException("Unknown type: " + val.toString());
    }

    public Expression getExpression(){
        return expr;
    }



    private Object evaluate(Unary un){
        Object val = un.getValue();

        if(val instanceof Boolean){
            Boolean bVal = (Boolean) val;
            switch (operator){
                case Not:
                    return !bVal;
                default:
                    return bVal;
            }
        } else if (val instanceof Integer){
            int iVal = (Integer) val;
            switch (operator){
                case Minus:
                    return iVal * -1;
                default:
                    return iVal;
            }
        } else if (val instanceof String){
            return ((StringConst) val).getValue();
        } else if (val instanceof TreeNode){
            return ((TreeNode) val).getValue();
        } else
            throw new RuntimeException("Unknown type: " + val.toString());
    }

    @Override
    public void collectTreeNodes(List<TreeNode> list){
        expr.collectTreeNodes(list);
    }

}
