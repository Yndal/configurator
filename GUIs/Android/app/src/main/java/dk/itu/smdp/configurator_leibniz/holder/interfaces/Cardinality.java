package dk.itu.smdp.configurator_leibniz.holder.interfaces;

/**
 * Created by lynd on 01/05/15.
 */
public interface Cardinality {
    boolean updateCardinality();
}
