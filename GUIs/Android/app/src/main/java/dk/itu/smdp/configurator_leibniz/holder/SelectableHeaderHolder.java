package dk.itu.smdp.configurator_leibniz.holder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.github.johnkil.print.PrintView;
import com.unnamed.b.atv.model.TreeNode;

import dk.itu.smdp.configurator_leibniz.R;
import dk.itu.smdp.configurator_leibniz.holder.interfaces.Cardinality;
import dk.itu.smdp.configurator_leibniz.holder.interfaces.Selectable;
import dk.itu.smdp.configurator_leibniz.holder.interfaces.Violatable;


/**
 * Created by Bogdan Melnychuk on 2/15/15.
 */
public class SelectableHeaderHolder extends TreeNode.BaseNodeViewHolder<IconTreeItemHolder.IconTreeItem> implements Violatable, Cardinality {
    private TreeNode node;
    private TextView tvValue;
    private PrintView arrowView;
    private CheckBox nodeSelector;
    private TextView fromTextView;
    private TextView betweenTextView;
    private TextView toTextView;

    private int min;
    private int max;
    private boolean setMinMax = false;

    private static Toast minViolatedToast;
    private static Toast maxViolatedToast;

    public SelectableHeaderHolder(Context context) {
        super(context);
    }

    @Override
    public View createNodeView(final TreeNode node, IconTreeItemHolder.IconTreeItem value) {
        this.node = node;
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_selectable_header, null, false);

        tvValue = (TextView) view.findViewById(R.id.node_value);
        tvValue.setText(value.text);

        final PrintView iconView = (PrintView) view.findViewById(R.id.icon);
        iconView.setIconText(context.getResources().getString(value.icon));

        arrowView = (PrintView) view.findViewById(R.id.arrow_icon);
        if (node.isLeaf()) {
            arrowView.setVisibility(View.GONE);
        }

        nodeSelector = (CheckBox) view.findViewById(R.id.node_selector);
        nodeSelector.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                node.setSelected(isChecked);
                for (TreeNode n : node.getChildren()) {
                    getTreeView().selectNode(n, isChecked);
                }
            }
        });
        nodeSelector.setChecked(node.isSelected());

        fromTextView = (TextView) view.findViewById(R.id.fromTextView);
        betweenTextView = (TextView) view.findViewById(R.id.betweenTextView);
        toTextView = (TextView) view.findViewById(R.id.toTextView);

        fromTextView.setVisibility(View.INVISIBLE);
        betweenTextView.setVisibility(View.INVISIBLE);
        toTextView.setVisibility(View.INVISIBLE);

        fromTextView.setText(Integer.toString(min));
        betweenTextView.setText("...");
        toTextView.setText(Integer.toString(max));

        if (setMinMax) {
            fromTextView.setVisibility(View.VISIBLE);
            betweenTextView.setVisibility(View.VISIBLE);
            toTextView.setVisibility(View.VISIBLE);

        } else {
            fromTextView.setVisibility(View.INVISIBLE);
            betweenTextView.setVisibility(View.INVISIBLE);
            toTextView.setVisibility(View.INVISIBLE);

        }

        updateCardinality();

        return view;
    }

    @Override
    public void toggle(boolean active) {
        arrowView.setIconText(context.getResources().getString(active ? R.string.ic_keyboard_arrow_down : R.string.ic_keyboard_arrow_right));
    }

    @Override
    public void toggleSelectionMode(boolean editModeEnabled) {
        nodeSelector.setVisibility(editModeEnabled ? View.VISIBLE : View.GONE);
        nodeSelector.setChecked(mNode.isSelected());
    }

    public void setMinMax(int min, int max) {
        this.min = min;
        this.max = max;
        this.setMinMax = true;
    }

    @Override
    public void markViolated(int color) {
        tvValue.setBackgroundColor(color);

        if (!node.isRoot())
            ((Violatable) node.getParent().getViewHolder()).markViolated(color);
    }


    public boolean updateCardinality() {
        int counter = 0;
        for (TreeNode tn : node.getChildren()) {
            if (((Selectable) tn.getViewHolder()).isSelected())
                counter++;
        }

        boolean result;
        if (counter < min) {
            result = false;

            if(minViolatedToast == null)
                minViolatedToast = Toast.makeText(context, tvValue.getText() + " must have at least " + min + " selected feature(s)!", Toast.LENGTH_LONG);

            if (!minViolatedToast.getView().isShown()) {
                minViolatedToast = Toast.makeText(context, tvValue.getText() + " must have at least " + min + " selected feature(s)!", Toast.LENGTH_LONG);
                minViolatedToast.show();
            }

            markMinViolated(true);
            markViolated(VIOLATED_COLOR);
        } else if (max < counter) {
            result = false;

            if(maxViolatedToast == null)
                maxViolatedToast = Toast.makeText(context, tvValue.getText() + " can't have more than " + max + " selected feature(s)!", Toast.LENGTH_LONG);

            if (!maxViolatedToast.getView().isShown()) {
                maxViolatedToast = Toast.makeText(context, tvValue.getText() + " can't have more than " + max + " selected feature(s)!", Toast.LENGTH_LONG);
                maxViolatedToast.show();
            }


            markMaxViolated(true);
            markViolated(VIOLATED_COLOR);
        } else {
            result = true;
            markMinViolated(false);
            markMaxViolated(false);
            markViolated(NORMAL_COLOR);
        }

        TreeNode parent = node.getParent();
        int color = result ? Violatable.NORMAL_COLOR : Violatable.VIOLATED_COLOR;
        while(parent != null && !(parent.isRoot() || parent.getValue() == null || parent.getValue().equals("Features"))){
            if(parent.getViewHolder() instanceof SelectableHeaderHolder) {
                boolean notViolated =/* true;/*/((SelectableHeaderHolder) parent.getViewHolder()).updateCardinality();
                if(notViolated && !result){
                    ((SelectableHeaderHolder) parent.getViewHolder()).markViolated(color);
                }
            }

            parent = parent.getParent();
        }

        return result;
    }

    private void markMinViolated(boolean isViolated){
        if(isViolated) {
            fromTextView.setBackgroundColor(VIOLATED_COLOR);
        } else {
            fromTextView.setBackgroundColor(NORMAL_COLOR);
        }
    }

    private void markMaxViolated(boolean isViolated){
        if(isViolated) {
            toTextView.setBackgroundColor(VIOLATED_COLOR);
        } else {
            toTextView.setBackgroundColor(NORMAL_COLOR);
        }
    }

    public int getMin(){
        return min;
    }

}
