package configurator.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import configurator.BOOLEAN;
import configurator.Binary;
import configurator.BoolConst;
import configurator.Configuration;
import configurator.ConfiguratorPackage;
import configurator.Constraint;
import configurator.ENUM;
import configurator.EnumValue;
import configurator.Feature;
import configurator.Group;
import configurator.INTEGER;
import configurator.IdRef;
import configurator.IntConst;
import configurator.StringConst;
import configurator.Unary;
import configurator.services.DSLGrammarAccess;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;

@SuppressWarnings("all")
public abstract class AbstractDSLSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private DSLGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == ConfiguratorPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case ConfiguratorPackage.BOOLEAN:
				if(context == grammarAccess.getBOOLEANRule() ||
				   context == grammarAccess.getTypeRule()) {
					sequence_BOOLEAN(context, (BOOLEAN) semanticObject); 
					return; 
				}
				else break;
			case ConfiguratorPackage.BINARY:
				if(context == grammarAccess.getAtomicExpressionRule() ||
				   context == grammarAccess.getBinaryRule() ||
				   context == grammarAccess.getExpressionRule()) {
					sequence_Binary(context, (Binary) semanticObject); 
					return; 
				}
				else break;
			case ConfiguratorPackage.BOOL_CONST:
				if(context == grammarAccess.getAtomicExpressionRule() ||
				   context == grammarAccess.getBoolConstRule() ||
				   context == grammarAccess.getConstantsRule() ||
				   context == grammarAccess.getExpressionRule()) {
					sequence_BoolConst(context, (BoolConst) semanticObject); 
					return; 
				}
				else break;
			case ConfiguratorPackage.CONFIGURATION:
				if(context == grammarAccess.getConfigurationRule()) {
					sequence_Configuration(context, (Configuration) semanticObject); 
					return; 
				}
				else break;
			case ConfiguratorPackage.CONSTRAINT:
				if(context == grammarAccess.getConstraintRule()) {
					sequence_Constraint(context, (Constraint) semanticObject); 
					return; 
				}
				else break;
			case ConfiguratorPackage.ENUM:
				if(context == grammarAccess.getENUMRule() ||
				   context == grammarAccess.getTypeRule()) {
					sequence_ENUM(context, (ENUM) semanticObject); 
					return; 
				}
				else break;
			case ConfiguratorPackage.ENUM_VALUE:
				if(context == grammarAccess.getEnumValueRule() ||
				   context == grammarAccess.getIdentifiableRule()) {
					sequence_EnumValue(context, (EnumValue) semanticObject); 
					return; 
				}
				else break;
			case ConfiguratorPackage.FEATURE:
				if(context == grammarAccess.getFeatureRule() ||
				   context == grammarAccess.getIdentifiableRule()) {
					sequence_Feature(context, (Feature) semanticObject); 
					return; 
				}
				else break;
			case ConfiguratorPackage.GROUP:
				if(context == grammarAccess.getGroupRule()) {
					sequence_Group(context, (Group) semanticObject); 
					return; 
				}
				else break;
			case ConfiguratorPackage.INTEGER:
				if(context == grammarAccess.getINTEGERRule() ||
				   context == grammarAccess.getTypeRule()) {
					sequence_INTEGER(context, (INTEGER) semanticObject); 
					return; 
				}
				else break;
			case ConfiguratorPackage.ID_REF:
				if(context == grammarAccess.getAtomicExpressionRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getIdRefRule()) {
					sequence_IdRef(context, (IdRef) semanticObject); 
					return; 
				}
				else break;
			case ConfiguratorPackage.INT_CONST:
				if(context == grammarAccess.getAtomicExpressionRule() ||
				   context == grammarAccess.getConstantsRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getIntConstRule()) {
					sequence_IntConst(context, (IntConst) semanticObject); 
					return; 
				}
				else break;
			case ConfiguratorPackage.STRING_CONST:
				if(context == grammarAccess.getAtomicExpressionRule() ||
				   context == grammarAccess.getConstantsRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getStringConstRule()) {
					sequence_StringConst(context, (StringConst) semanticObject); 
					return; 
				}
				else break;
			case ConfiguratorPackage.UNARY:
				if(context == grammarAccess.getAtomicExpressionRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getUnaryRule()) {
					sequence_Unary(context, (Unary) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     name=EString
	 */
	protected void sequence_BOOLEAN(EObject context, BOOLEAN semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, ConfiguratorPackage.Literals.NAMED_ELEMENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ConfiguratorPackage.Literals.NAMED_ELEMENT__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getBOOLEANAccess().getNameEStringParserRuleCall_2_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (leftExpression=AtomicExpression operator=BinaryOperator rightExpression=AtomicExpression)
	 */
	protected void sequence_Binary(EObject context, Binary semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, ConfiguratorPackage.Literals.BINARY__LEFT_EXPRESSION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ConfiguratorPackage.Literals.BINARY__LEFT_EXPRESSION));
			if(transientValues.isValueTransient(semanticObject, ConfiguratorPackage.Literals.BINARY__OPERATOR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ConfiguratorPackage.Literals.BINARY__OPERATOR));
			if(transientValues.isValueTransient(semanticObject, ConfiguratorPackage.Literals.BINARY__RIGHT_EXPRESSION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ConfiguratorPackage.Literals.BINARY__RIGHT_EXPRESSION));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getBinaryAccess().getLeftExpressionAtomicExpressionParserRuleCall_0_0(), semanticObject.getLeftExpression());
		feeder.accept(grammarAccess.getBinaryAccess().getOperatorBinaryOperatorEnumRuleCall_1_0(), semanticObject.getOperator());
		feeder.accept(grammarAccess.getBinaryAccess().getRightExpressionAtomicExpressionParserRuleCall_2_0(), semanticObject.getRightExpression());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     value=Booolean
	 */
	protected void sequence_BoolConst(EObject context, BoolConst semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, ConfiguratorPackage.Literals.BOOL_CONST__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ConfiguratorPackage.Literals.BOOL_CONST__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getBoolConstAccess().getValueBoooleanEnumRuleCall_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name=EString (types+=Type types+=Type*)? (rootFeatures+=Feature rootFeatures+=Feature*)? (constraints+=Constraint constraints+=Constraint*)?)
	 */
	protected void sequence_Configuration(EObject context, Configuration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=EString errorMessage=EString expression=Expression)
	 */
	protected void sequence_Constraint(EObject context, Constraint semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, ConfiguratorPackage.Literals.NAMED_ELEMENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ConfiguratorPackage.Literals.NAMED_ELEMENT__NAME));
			if(transientValues.isValueTransient(semanticObject, ConfiguratorPackage.Literals.CONSTRAINT__EXPRESSION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ConfiguratorPackage.Literals.CONSTRAINT__EXPRESSION));
			if(transientValues.isValueTransient(semanticObject, ConfiguratorPackage.Literals.CONSTRAINT__ERROR_MESSAGE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ConfiguratorPackage.Literals.CONSTRAINT__ERROR_MESSAGE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getConstraintAccess().getNameEStringParserRuleCall_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getConstraintAccess().getErrorMessageEStringParserRuleCall_2_0(), semanticObject.getErrorMessage());
		feeder.accept(grammarAccess.getConstraintAccess().getExpressionExpressionParserRuleCall_5_0(), semanticObject.getExpression());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name=EString enumValues+=EnumValue enumValues+=EnumValue*)
	 */
	protected void sequence_ENUM(EObject context, ENUM semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name=EString
	 */
	protected void sequence_EnumValue(EObject context, EnumValue semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, ConfiguratorPackage.Literals.NAMED_ELEMENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ConfiguratorPackage.Literals.NAMED_ELEMENT__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getEnumValueAccess().getNameEStringParserRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name=EString type=[Type|EString] (min=EInt max=EInt)? (groups+=Group groups+=Group*)?)
	 */
	protected void sequence_Feature(EObject context, Feature semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=EString (min=EInt max=EInt)? features+=Feature features+=Feature*)
	 */
	protected void sequence_Group(EObject context, Group semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name=EString
	 */
	protected void sequence_INTEGER(EObject context, INTEGER semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, ConfiguratorPackage.Literals.NAMED_ELEMENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ConfiguratorPackage.Literals.NAMED_ELEMENT__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getINTEGERAccess().getNameEStringParserRuleCall_2_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     identifier=[Identifiable|EString]
	 */
	protected void sequence_IdRef(EObject context, IdRef semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, ConfiguratorPackage.Literals.ID_REF__IDENTIFIER) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ConfiguratorPackage.Literals.ID_REF__IDENTIFIER));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getIdRefAccess().getIdentifierIdentifiableEStringParserRuleCall_0_1(), semanticObject.getIdentifier());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     value=EInt
	 */
	protected void sequence_IntConst(EObject context, IntConst semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, ConfiguratorPackage.Literals.INT_CONST__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ConfiguratorPackage.Literals.INT_CONST__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getIntConstAccess().getValueEIntParserRuleCall_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     value=EString
	 */
	protected void sequence_StringConst(EObject context, StringConst semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, ConfiguratorPackage.Literals.STRING_CONST__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ConfiguratorPackage.Literals.STRING_CONST__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getStringConstAccess().getValueEStringParserRuleCall_1_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (operator=UnaryOperator innerExpression=AtomicExpression)
	 */
	protected void sequence_Unary(EObject context, Unary semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, ConfiguratorPackage.Literals.UNARY__INNER_EXPRESSION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ConfiguratorPackage.Literals.UNARY__INNER_EXPRESSION));
			if(transientValues.isValueTransient(semanticObject, ConfiguratorPackage.Literals.UNARY__OPERATOR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ConfiguratorPackage.Literals.UNARY__OPERATOR));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getUnaryAccess().getOperatorUnaryOperatorEnumRuleCall_0_0(), semanticObject.getOperator());
		feeder.accept(grammarAccess.getUnaryAccess().getInnerExpressionAtomicExpressionParserRuleCall_1_0(), semanticObject.getInnerExpression());
		feeder.finish();
	}
}
