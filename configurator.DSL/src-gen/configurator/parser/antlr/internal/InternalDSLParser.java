package configurator.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import configurator.services.DSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDSLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'{'", "'Types'", "','", "'}'", "'Features'", "'Constraints'", "'['", "']'", "'..'", "'Boolean'", "'Integer'", "'...'", "'('", "')'", "'not'", "'-'", "'+'", "'*'", "'/'", "'=='", "'!='", "' and '", "' or '", "'>'", "'>='", "'<'", "'<='", "'true'", "'false'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDSLParser.tokenNames; }
    public String getGrammarFileName() { return "../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g"; }



     	private DSLGrammarAccess grammarAccess;
     	
        public InternalDSLParser(TokenStream input, DSLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Configuration";	
       	}
       	
       	@Override
       	protected DSLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleConfiguration"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:68:1: entryRuleConfiguration returns [EObject current=null] : iv_ruleConfiguration= ruleConfiguration EOF ;
    public final EObject entryRuleConfiguration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConfiguration = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:69:2: (iv_ruleConfiguration= ruleConfiguration EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:70:2: iv_ruleConfiguration= ruleConfiguration EOF
            {
             newCompositeNode(grammarAccess.getConfigurationRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleConfiguration_in_entryRuleConfiguration75);
            iv_ruleConfiguration=ruleConfiguration();

            state._fsp--;

             current =iv_ruleConfiguration; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleConfiguration85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConfiguration"


    // $ANTLR start "ruleConfiguration"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:77:1: ruleConfiguration returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'Types' otherlv_4= '{' ( (lv_types_5_0= ruleType ) ) (otherlv_6= ',' ( (lv_types_7_0= ruleType ) ) )* otherlv_8= '}' )? (otherlv_9= 'Features' otherlv_10= '{' ( (lv_rootFeatures_11_0= ruleFeature ) ) (otherlv_12= ',' ( (lv_rootFeatures_13_0= ruleFeature ) ) )* otherlv_14= '}' )? (otherlv_15= 'Constraints' otherlv_16= '{' ( (lv_constraints_17_0= ruleConstraint ) ) (otherlv_18= ',' ( (lv_constraints_19_0= ruleConstraint ) ) )* otherlv_20= '}' )? otherlv_21= '}' ) ;
    public final EObject ruleConfiguration() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_types_5_0 = null;

        EObject lv_types_7_0 = null;

        EObject lv_rootFeatures_11_0 = null;

        EObject lv_rootFeatures_13_0 = null;

        EObject lv_constraints_17_0 = null;

        EObject lv_constraints_19_0 = null;


         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:80:28: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'Types' otherlv_4= '{' ( (lv_types_5_0= ruleType ) ) (otherlv_6= ',' ( (lv_types_7_0= ruleType ) ) )* otherlv_8= '}' )? (otherlv_9= 'Features' otherlv_10= '{' ( (lv_rootFeatures_11_0= ruleFeature ) ) (otherlv_12= ',' ( (lv_rootFeatures_13_0= ruleFeature ) ) )* otherlv_14= '}' )? (otherlv_15= 'Constraints' otherlv_16= '{' ( (lv_constraints_17_0= ruleConstraint ) ) (otherlv_18= ',' ( (lv_constraints_19_0= ruleConstraint ) ) )* otherlv_20= '}' )? otherlv_21= '}' ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:81:1: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'Types' otherlv_4= '{' ( (lv_types_5_0= ruleType ) ) (otherlv_6= ',' ( (lv_types_7_0= ruleType ) ) )* otherlv_8= '}' )? (otherlv_9= 'Features' otherlv_10= '{' ( (lv_rootFeatures_11_0= ruleFeature ) ) (otherlv_12= ',' ( (lv_rootFeatures_13_0= ruleFeature ) ) )* otherlv_14= '}' )? (otherlv_15= 'Constraints' otherlv_16= '{' ( (lv_constraints_17_0= ruleConstraint ) ) (otherlv_18= ',' ( (lv_constraints_19_0= ruleConstraint ) ) )* otherlv_20= '}' )? otherlv_21= '}' )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:81:1: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'Types' otherlv_4= '{' ( (lv_types_5_0= ruleType ) ) (otherlv_6= ',' ( (lv_types_7_0= ruleType ) ) )* otherlv_8= '}' )? (otherlv_9= 'Features' otherlv_10= '{' ( (lv_rootFeatures_11_0= ruleFeature ) ) (otherlv_12= ',' ( (lv_rootFeatures_13_0= ruleFeature ) ) )* otherlv_14= '}' )? (otherlv_15= 'Constraints' otherlv_16= '{' ( (lv_constraints_17_0= ruleConstraint ) ) (otherlv_18= ',' ( (lv_constraints_19_0= ruleConstraint ) ) )* otherlv_20= '}' )? otherlv_21= '}' )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:81:2: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'Types' otherlv_4= '{' ( (lv_types_5_0= ruleType ) ) (otherlv_6= ',' ( (lv_types_7_0= ruleType ) ) )* otherlv_8= '}' )? (otherlv_9= 'Features' otherlv_10= '{' ( (lv_rootFeatures_11_0= ruleFeature ) ) (otherlv_12= ',' ( (lv_rootFeatures_13_0= ruleFeature ) ) )* otherlv_14= '}' )? (otherlv_15= 'Constraints' otherlv_16= '{' ( (lv_constraints_17_0= ruleConstraint ) ) (otherlv_18= ',' ( (lv_constraints_19_0= ruleConstraint ) ) )* otherlv_20= '}' )? otherlv_21= '}'
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:81:2: ()
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:82:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getConfigurationAccess().getConfigurationAction_0(),
                        current);
                

            }

            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:87:2: ( (lv_name_1_0= ruleEString ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:88:1: (lv_name_1_0= ruleEString )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:88:1: (lv_name_1_0= ruleEString )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:89:3: lv_name_1_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getConfigurationAccess().getNameEStringParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleConfiguration140);
            lv_name_1_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getConfigurationRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,11,FollowSets000.FOLLOW_11_in_ruleConfiguration152); 

                	newLeafNode(otherlv_2, grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_2());
                
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:109:1: (otherlv_3= 'Types' otherlv_4= '{' ( (lv_types_5_0= ruleType ) ) (otherlv_6= ',' ( (lv_types_7_0= ruleType ) ) )* otherlv_8= '}' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==12) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:109:3: otherlv_3= 'Types' otherlv_4= '{' ( (lv_types_5_0= ruleType ) ) (otherlv_6= ',' ( (lv_types_7_0= ruleType ) ) )* otherlv_8= '}'
                    {
                    otherlv_3=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleConfiguration165); 

                        	newLeafNode(otherlv_3, grammarAccess.getConfigurationAccess().getTypesKeyword_3_0());
                        
                    otherlv_4=(Token)match(input,11,FollowSets000.FOLLOW_11_in_ruleConfiguration177); 

                        	newLeafNode(otherlv_4, grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_3_1());
                        
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:117:1: ( (lv_types_5_0= ruleType ) )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:118:1: (lv_types_5_0= ruleType )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:118:1: (lv_types_5_0= ruleType )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:119:3: lv_types_5_0= ruleType
                    {
                     
                    	        newCompositeNode(grammarAccess.getConfigurationAccess().getTypesTypeParserRuleCall_3_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleType_in_ruleConfiguration198);
                    lv_types_5_0=ruleType();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getConfigurationRule());
                    	        }
                           		add(
                           			current, 
                           			"types",
                            		lv_types_5_0, 
                            		"Type");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:135:2: (otherlv_6= ',' ( (lv_types_7_0= ruleType ) ) )*
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( (LA1_0==13) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:135:4: otherlv_6= ',' ( (lv_types_7_0= ruleType ) )
                    	    {
                    	    otherlv_6=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleConfiguration211); 

                    	        	newLeafNode(otherlv_6, grammarAccess.getConfigurationAccess().getCommaKeyword_3_3_0());
                    	        
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:139:1: ( (lv_types_7_0= ruleType ) )
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:140:1: (lv_types_7_0= ruleType )
                    	    {
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:140:1: (lv_types_7_0= ruleType )
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:141:3: lv_types_7_0= ruleType
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getConfigurationAccess().getTypesTypeParserRuleCall_3_3_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleType_in_ruleConfiguration232);
                    	    lv_types_7_0=ruleType();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getConfigurationRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"types",
                    	            		lv_types_7_0, 
                    	            		"Type");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop1;
                        }
                    } while (true);

                    otherlv_8=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleConfiguration246); 

                        	newLeafNode(otherlv_8, grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_3_4());
                        

                    }
                    break;

            }

            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:161:3: (otherlv_9= 'Features' otherlv_10= '{' ( (lv_rootFeatures_11_0= ruleFeature ) ) (otherlv_12= ',' ( (lv_rootFeatures_13_0= ruleFeature ) ) )* otherlv_14= '}' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==15) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:161:5: otherlv_9= 'Features' otherlv_10= '{' ( (lv_rootFeatures_11_0= ruleFeature ) ) (otherlv_12= ',' ( (lv_rootFeatures_13_0= ruleFeature ) ) )* otherlv_14= '}'
                    {
                    otherlv_9=(Token)match(input,15,FollowSets000.FOLLOW_15_in_ruleConfiguration261); 

                        	newLeafNode(otherlv_9, grammarAccess.getConfigurationAccess().getFeaturesKeyword_4_0());
                        
                    otherlv_10=(Token)match(input,11,FollowSets000.FOLLOW_11_in_ruleConfiguration273); 

                        	newLeafNode(otherlv_10, grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_4_1());
                        
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:169:1: ( (lv_rootFeatures_11_0= ruleFeature ) )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:170:1: (lv_rootFeatures_11_0= ruleFeature )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:170:1: (lv_rootFeatures_11_0= ruleFeature )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:171:3: lv_rootFeatures_11_0= ruleFeature
                    {
                     
                    	        newCompositeNode(grammarAccess.getConfigurationAccess().getRootFeaturesFeatureParserRuleCall_4_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleFeature_in_ruleConfiguration294);
                    lv_rootFeatures_11_0=ruleFeature();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getConfigurationRule());
                    	        }
                           		add(
                           			current, 
                           			"rootFeatures",
                            		lv_rootFeatures_11_0, 
                            		"Feature");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:187:2: (otherlv_12= ',' ( (lv_rootFeatures_13_0= ruleFeature ) ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==13) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:187:4: otherlv_12= ',' ( (lv_rootFeatures_13_0= ruleFeature ) )
                    	    {
                    	    otherlv_12=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleConfiguration307); 

                    	        	newLeafNode(otherlv_12, grammarAccess.getConfigurationAccess().getCommaKeyword_4_3_0());
                    	        
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:191:1: ( (lv_rootFeatures_13_0= ruleFeature ) )
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:192:1: (lv_rootFeatures_13_0= ruleFeature )
                    	    {
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:192:1: (lv_rootFeatures_13_0= ruleFeature )
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:193:3: lv_rootFeatures_13_0= ruleFeature
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getConfigurationAccess().getRootFeaturesFeatureParserRuleCall_4_3_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleFeature_in_ruleConfiguration328);
                    	    lv_rootFeatures_13_0=ruleFeature();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getConfigurationRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"rootFeatures",
                    	            		lv_rootFeatures_13_0, 
                    	            		"Feature");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleConfiguration342); 

                        	newLeafNode(otherlv_14, grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_4_4());
                        

                    }
                    break;

            }

            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:213:3: (otherlv_15= 'Constraints' otherlv_16= '{' ( (lv_constraints_17_0= ruleConstraint ) ) (otherlv_18= ',' ( (lv_constraints_19_0= ruleConstraint ) ) )* otherlv_20= '}' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==16) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:213:5: otherlv_15= 'Constraints' otherlv_16= '{' ( (lv_constraints_17_0= ruleConstraint ) ) (otherlv_18= ',' ( (lv_constraints_19_0= ruleConstraint ) ) )* otherlv_20= '}'
                    {
                    otherlv_15=(Token)match(input,16,FollowSets000.FOLLOW_16_in_ruleConfiguration357); 

                        	newLeafNode(otherlv_15, grammarAccess.getConfigurationAccess().getConstraintsKeyword_5_0());
                        
                    otherlv_16=(Token)match(input,11,FollowSets000.FOLLOW_11_in_ruleConfiguration369); 

                        	newLeafNode(otherlv_16, grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_5_1());
                        
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:221:1: ( (lv_constraints_17_0= ruleConstraint ) )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:222:1: (lv_constraints_17_0= ruleConstraint )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:222:1: (lv_constraints_17_0= ruleConstraint )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:223:3: lv_constraints_17_0= ruleConstraint
                    {
                     
                    	        newCompositeNode(grammarAccess.getConfigurationAccess().getConstraintsConstraintParserRuleCall_5_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleConstraint_in_ruleConfiguration390);
                    lv_constraints_17_0=ruleConstraint();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getConfigurationRule());
                    	        }
                           		add(
                           			current, 
                           			"constraints",
                            		lv_constraints_17_0, 
                            		"Constraint");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:239:2: (otherlv_18= ',' ( (lv_constraints_19_0= ruleConstraint ) ) )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==13) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:239:4: otherlv_18= ',' ( (lv_constraints_19_0= ruleConstraint ) )
                    	    {
                    	    otherlv_18=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleConfiguration403); 

                    	        	newLeafNode(otherlv_18, grammarAccess.getConfigurationAccess().getCommaKeyword_5_3_0());
                    	        
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:243:1: ( (lv_constraints_19_0= ruleConstraint ) )
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:244:1: (lv_constraints_19_0= ruleConstraint )
                    	    {
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:244:1: (lv_constraints_19_0= ruleConstraint )
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:245:3: lv_constraints_19_0= ruleConstraint
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getConfigurationAccess().getConstraintsConstraintParserRuleCall_5_3_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleConstraint_in_ruleConfiguration424);
                    	    lv_constraints_19_0=ruleConstraint();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getConfigurationRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"constraints",
                    	            		lv_constraints_19_0, 
                    	            		"Constraint");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                    otherlv_20=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleConfiguration438); 

                        	newLeafNode(otherlv_20, grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_5_4());
                        

                    }
                    break;

            }

            otherlv_21=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleConfiguration452); 

                	newLeafNode(otherlv_21, grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConfiguration"


    // $ANTLR start "entryRuleConstraint"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:277:1: entryRuleConstraint returns [EObject current=null] : iv_ruleConstraint= ruleConstraint EOF ;
    public final EObject entryRuleConstraint() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstraint = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:278:2: (iv_ruleConstraint= ruleConstraint EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:279:2: iv_ruleConstraint= ruleConstraint EOF
            {
             newCompositeNode(grammarAccess.getConstraintRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleConstraint_in_entryRuleConstraint488);
            iv_ruleConstraint=ruleConstraint();

            state._fsp--;

             current =iv_ruleConstraint; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleConstraint498); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstraint"


    // $ANTLR start "ruleConstraint"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:286:1: ruleConstraint returns [EObject current=null] : ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '[' ( (lv_errorMessage_2_0= ruleEString ) ) otherlv_3= ']' otherlv_4= '{' ( (lv_expression_5_0= ruleExpression ) ) otherlv_6= '}' ) ;
    public final EObject ruleConstraint() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;

        AntlrDatatypeRuleToken lv_errorMessage_2_0 = null;

        EObject lv_expression_5_0 = null;


         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:289:28: ( ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '[' ( (lv_errorMessage_2_0= ruleEString ) ) otherlv_3= ']' otherlv_4= '{' ( (lv_expression_5_0= ruleExpression ) ) otherlv_6= '}' ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:290:1: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '[' ( (lv_errorMessage_2_0= ruleEString ) ) otherlv_3= ']' otherlv_4= '{' ( (lv_expression_5_0= ruleExpression ) ) otherlv_6= '}' )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:290:1: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '[' ( (lv_errorMessage_2_0= ruleEString ) ) otherlv_3= ']' otherlv_4= '{' ( (lv_expression_5_0= ruleExpression ) ) otherlv_6= '}' )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:290:2: ( (lv_name_0_0= ruleEString ) ) otherlv_1= '[' ( (lv_errorMessage_2_0= ruleEString ) ) otherlv_3= ']' otherlv_4= '{' ( (lv_expression_5_0= ruleExpression ) ) otherlv_6= '}'
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:290:2: ( (lv_name_0_0= ruleEString ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:291:1: (lv_name_0_0= ruleEString )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:291:1: (lv_name_0_0= ruleEString )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:292:3: lv_name_0_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getConstraintAccess().getNameEStringParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleConstraint544);
            lv_name_0_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getConstraintRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleConstraint556); 

                	newLeafNode(otherlv_1, grammarAccess.getConstraintAccess().getLeftSquareBracketKeyword_1());
                
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:312:1: ( (lv_errorMessage_2_0= ruleEString ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:313:1: (lv_errorMessage_2_0= ruleEString )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:313:1: (lv_errorMessage_2_0= ruleEString )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:314:3: lv_errorMessage_2_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getConstraintAccess().getErrorMessageEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleConstraint577);
            lv_errorMessage_2_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getConstraintRule());
            	        }
                   		set(
                   			current, 
                   			"errorMessage",
                    		lv_errorMessage_2_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleConstraint589); 

                	newLeafNode(otherlv_3, grammarAccess.getConstraintAccess().getRightSquareBracketKeyword_3());
                
            otherlv_4=(Token)match(input,11,FollowSets000.FOLLOW_11_in_ruleConstraint601); 

                	newLeafNode(otherlv_4, grammarAccess.getConstraintAccess().getLeftCurlyBracketKeyword_4());
                
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:338:1: ( (lv_expression_5_0= ruleExpression ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:339:1: (lv_expression_5_0= ruleExpression )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:339:1: (lv_expression_5_0= ruleExpression )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:340:3: lv_expression_5_0= ruleExpression
            {
             
            	        newCompositeNode(grammarAccess.getConstraintAccess().getExpressionExpressionParserRuleCall_5_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleExpression_in_ruleConstraint622);
            lv_expression_5_0=ruleExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getConstraintRule());
            	        }
                   		set(
                   			current, 
                   			"expression",
                    		lv_expression_5_0, 
                    		"Expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_6=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleConstraint634); 

                	newLeafNode(otherlv_6, grammarAccess.getConstraintAccess().getRightCurlyBracketKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstraint"


    // $ANTLR start "entryRuleType"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:368:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:369:2: (iv_ruleType= ruleType EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:370:2: iv_ruleType= ruleType EOF
            {
             newCompositeNode(grammarAccess.getTypeRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleType_in_entryRuleType670);
            iv_ruleType=ruleType();

            state._fsp--;

             current =iv_ruleType; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleType680); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:377:1: ruleType returns [EObject current=null] : (this_BOOLEAN_0= ruleBOOLEAN | this_INTEGER_1= ruleINTEGER | this_ENUM_2= ruleENUM ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        EObject this_BOOLEAN_0 = null;

        EObject this_INTEGER_1 = null;

        EObject this_ENUM_2 = null;


         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:380:28: ( (this_BOOLEAN_0= ruleBOOLEAN | this_INTEGER_1= ruleINTEGER | this_ENUM_2= ruleENUM ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:381:1: (this_BOOLEAN_0= ruleBOOLEAN | this_INTEGER_1= ruleINTEGER | this_ENUM_2= ruleENUM )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:381:1: (this_BOOLEAN_0= ruleBOOLEAN | this_INTEGER_1= ruleINTEGER | this_ENUM_2= ruleENUM )
            int alt7=3;
            switch ( input.LA(1) ) {
            case 20:
                {
                alt7=1;
                }
                break;
            case 21:
                {
                alt7=2;
                }
                break;
            case RULE_STRING:
            case RULE_ID:
                {
                alt7=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:382:5: this_BOOLEAN_0= ruleBOOLEAN
                    {
                     
                            newCompositeNode(grammarAccess.getTypeAccess().getBOOLEANParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleBOOLEAN_in_ruleType727);
                    this_BOOLEAN_0=ruleBOOLEAN();

                    state._fsp--;

                     
                            current = this_BOOLEAN_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:392:5: this_INTEGER_1= ruleINTEGER
                    {
                     
                            newCompositeNode(grammarAccess.getTypeAccess().getINTEGERParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleINTEGER_in_ruleType754);
                    this_INTEGER_1=ruleINTEGER();

                    state._fsp--;

                     
                            current = this_INTEGER_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:402:5: this_ENUM_2= ruleENUM
                    {
                     
                            newCompositeNode(grammarAccess.getTypeAccess().getENUMParserRuleCall_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleENUM_in_ruleType781);
                    this_ENUM_2=ruleENUM();

                    state._fsp--;

                     
                            current = this_ENUM_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleExpression"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:418:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:419:2: (iv_ruleExpression= ruleExpression EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:420:2: iv_ruleExpression= ruleExpression EOF
            {
             newCompositeNode(grammarAccess.getExpressionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleExpression_in_entryRuleExpression816);
            iv_ruleExpression=ruleExpression();

            state._fsp--;

             current =iv_ruleExpression; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleExpression826); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:427:1: ruleExpression returns [EObject current=null] : (this_Unary_0= ruleUnary | this_Binary_1= ruleBinary | this_IdRef_2= ruleIdRef | this_IntConst_3= ruleIntConst | this_BoolConst_4= ruleBoolConst | this_StringConst_5= ruleStringConst ) ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_Unary_0 = null;

        EObject this_Binary_1 = null;

        EObject this_IdRef_2 = null;

        EObject this_IntConst_3 = null;

        EObject this_BoolConst_4 = null;

        EObject this_StringConst_5 = null;


         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:430:28: ( (this_Unary_0= ruleUnary | this_Binary_1= ruleBinary | this_IdRef_2= ruleIdRef | this_IntConst_3= ruleIntConst | this_BoolConst_4= ruleBoolConst | this_StringConst_5= ruleStringConst ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:431:1: (this_Unary_0= ruleUnary | this_Binary_1= ruleBinary | this_IdRef_2= ruleIdRef | this_IntConst_3= ruleIntConst | this_BoolConst_4= ruleBoolConst | this_StringConst_5= ruleStringConst )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:431:1: (this_Unary_0= ruleUnary | this_Binary_1= ruleBinary | this_IdRef_2= ruleIdRef | this_IntConst_3= ruleIntConst | this_BoolConst_4= ruleBoolConst | this_StringConst_5= ruleStringConst )
            int alt8=6;
            alt8 = dfa8.predict(input);
            switch (alt8) {
                case 1 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:432:5: this_Unary_0= ruleUnary
                    {
                     
                            newCompositeNode(grammarAccess.getExpressionAccess().getUnaryParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleUnary_in_ruleExpression873);
                    this_Unary_0=ruleUnary();

                    state._fsp--;

                     
                            current = this_Unary_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:442:5: this_Binary_1= ruleBinary
                    {
                     
                            newCompositeNode(grammarAccess.getExpressionAccess().getBinaryParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleBinary_in_ruleExpression900);
                    this_Binary_1=ruleBinary();

                    state._fsp--;

                     
                            current = this_Binary_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:452:5: this_IdRef_2= ruleIdRef
                    {
                     
                            newCompositeNode(grammarAccess.getExpressionAccess().getIdRefParserRuleCall_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleIdRef_in_ruleExpression927);
                    this_IdRef_2=ruleIdRef();

                    state._fsp--;

                     
                            current = this_IdRef_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:462:5: this_IntConst_3= ruleIntConst
                    {
                     
                            newCompositeNode(grammarAccess.getExpressionAccess().getIntConstParserRuleCall_3()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleIntConst_in_ruleExpression954);
                    this_IntConst_3=ruleIntConst();

                    state._fsp--;

                     
                            current = this_IntConst_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:472:5: this_BoolConst_4= ruleBoolConst
                    {
                     
                            newCompositeNode(grammarAccess.getExpressionAccess().getBoolConstParserRuleCall_4()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleBoolConst_in_ruleExpression981);
                    this_BoolConst_4=ruleBoolConst();

                    state._fsp--;

                     
                            current = this_BoolConst_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 6 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:482:5: this_StringConst_5= ruleStringConst
                    {
                     
                            newCompositeNode(grammarAccess.getExpressionAccess().getStringConstParserRuleCall_5()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleStringConst_in_ruleExpression1008);
                    this_StringConst_5=ruleStringConst();

                    state._fsp--;

                     
                            current = this_StringConst_5; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleEString"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:500:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:501:2: (iv_ruleEString= ruleEString EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:502:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_entryRuleEString1046);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEString1057); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:509:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;

         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:512:28: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:513:1: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:513:1: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_STRING) ) {
                alt9=1;
            }
            else if ( (LA9_0==RULE_ID) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:513:6: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleEString1097); 

                    		current.merge(this_STRING_0);
                        
                     
                        newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                        

                    }
                    break;
                case 2 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:521:10: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleEString1123); 

                    		current.merge(this_ID_1);
                        
                     
                        newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleFeature"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:536:1: entryRuleFeature returns [EObject current=null] : iv_ruleFeature= ruleFeature EOF ;
    public final EObject entryRuleFeature() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeature = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:537:2: (iv_ruleFeature= ruleFeature EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:538:2: iv_ruleFeature= ruleFeature EOF
            {
             newCompositeNode(grammarAccess.getFeatureRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_entryRuleFeature1168);
            iv_ruleFeature=ruleFeature();

            state._fsp--;

             current =iv_ruleFeature; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeature1178); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:545:1: ruleFeature returns [EObject current=null] : ( ( (lv_name_0_0= ruleEString ) ) ( ( ruleEString ) ) (otherlv_2= '[' ( (lv_min_3_0= ruleEInt ) ) otherlv_4= '..' ( (lv_max_5_0= ruleEInt ) ) otherlv_6= ']' )? (otherlv_7= '{' ( (lv_groups_8_0= ruleGroup ) ) (otherlv_9= ',' ( (lv_groups_10_0= ruleGroup ) ) )* otherlv_11= '}' )? ) ;
    public final EObject ruleFeature() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;

        AntlrDatatypeRuleToken lv_min_3_0 = null;

        AntlrDatatypeRuleToken lv_max_5_0 = null;

        EObject lv_groups_8_0 = null;

        EObject lv_groups_10_0 = null;


         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:548:28: ( ( ( (lv_name_0_0= ruleEString ) ) ( ( ruleEString ) ) (otherlv_2= '[' ( (lv_min_3_0= ruleEInt ) ) otherlv_4= '..' ( (lv_max_5_0= ruleEInt ) ) otherlv_6= ']' )? (otherlv_7= '{' ( (lv_groups_8_0= ruleGroup ) ) (otherlv_9= ',' ( (lv_groups_10_0= ruleGroup ) ) )* otherlv_11= '}' )? ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:549:1: ( ( (lv_name_0_0= ruleEString ) ) ( ( ruleEString ) ) (otherlv_2= '[' ( (lv_min_3_0= ruleEInt ) ) otherlv_4= '..' ( (lv_max_5_0= ruleEInt ) ) otherlv_6= ']' )? (otherlv_7= '{' ( (lv_groups_8_0= ruleGroup ) ) (otherlv_9= ',' ( (lv_groups_10_0= ruleGroup ) ) )* otherlv_11= '}' )? )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:549:1: ( ( (lv_name_0_0= ruleEString ) ) ( ( ruleEString ) ) (otherlv_2= '[' ( (lv_min_3_0= ruleEInt ) ) otherlv_4= '..' ( (lv_max_5_0= ruleEInt ) ) otherlv_6= ']' )? (otherlv_7= '{' ( (lv_groups_8_0= ruleGroup ) ) (otherlv_9= ',' ( (lv_groups_10_0= ruleGroup ) ) )* otherlv_11= '}' )? )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:549:2: ( (lv_name_0_0= ruleEString ) ) ( ( ruleEString ) ) (otherlv_2= '[' ( (lv_min_3_0= ruleEInt ) ) otherlv_4= '..' ( (lv_max_5_0= ruleEInt ) ) otherlv_6= ']' )? (otherlv_7= '{' ( (lv_groups_8_0= ruleGroup ) ) (otherlv_9= ',' ( (lv_groups_10_0= ruleGroup ) ) )* otherlv_11= '}' )?
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:549:2: ( (lv_name_0_0= ruleEString ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:550:1: (lv_name_0_0= ruleEString )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:550:1: (lv_name_0_0= ruleEString )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:551:3: lv_name_0_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getFeatureAccess().getNameEStringParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleFeature1224);
            lv_name_0_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFeatureRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:567:2: ( ( ruleEString ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:568:1: ( ruleEString )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:568:1: ( ruleEString )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:569:3: ruleEString
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getFeatureRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getFeatureAccess().getTypeTypeCrossReference_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleFeature1247);
            ruleEString();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:582:2: (otherlv_2= '[' ( (lv_min_3_0= ruleEInt ) ) otherlv_4= '..' ( (lv_max_5_0= ruleEInt ) ) otherlv_6= ']' )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==17) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:582:4: otherlv_2= '[' ( (lv_min_3_0= ruleEInt ) ) otherlv_4= '..' ( (lv_max_5_0= ruleEInt ) ) otherlv_6= ']'
                    {
                    otherlv_2=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleFeature1260); 

                        	newLeafNode(otherlv_2, grammarAccess.getFeatureAccess().getLeftSquareBracketKeyword_2_0());
                        
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:586:1: ( (lv_min_3_0= ruleEInt ) )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:587:1: (lv_min_3_0= ruleEInt )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:587:1: (lv_min_3_0= ruleEInt )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:588:3: lv_min_3_0= ruleEInt
                    {
                     
                    	        newCompositeNode(grammarAccess.getFeatureAccess().getMinEIntParserRuleCall_2_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEInt_in_ruleFeature1281);
                    lv_min_3_0=ruleEInt();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFeatureRule());
                    	        }
                           		set(
                           			current, 
                           			"min",
                            		lv_min_3_0, 
                            		"EInt");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_4=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleFeature1293); 

                        	newLeafNode(otherlv_4, grammarAccess.getFeatureAccess().getFullStopFullStopKeyword_2_2());
                        
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:608:1: ( (lv_max_5_0= ruleEInt ) )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:609:1: (lv_max_5_0= ruleEInt )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:609:1: (lv_max_5_0= ruleEInt )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:610:3: lv_max_5_0= ruleEInt
                    {
                     
                    	        newCompositeNode(grammarAccess.getFeatureAccess().getMaxEIntParserRuleCall_2_3_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEInt_in_ruleFeature1314);
                    lv_max_5_0=ruleEInt();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFeatureRule());
                    	        }
                           		set(
                           			current, 
                           			"max",
                            		lv_max_5_0, 
                            		"EInt");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_6=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleFeature1326); 

                        	newLeafNode(otherlv_6, grammarAccess.getFeatureAccess().getRightSquareBracketKeyword_2_4());
                        

                    }
                    break;

            }

            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:630:3: (otherlv_7= '{' ( (lv_groups_8_0= ruleGroup ) ) (otherlv_9= ',' ( (lv_groups_10_0= ruleGroup ) ) )* otherlv_11= '}' )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==11) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:630:5: otherlv_7= '{' ( (lv_groups_8_0= ruleGroup ) ) (otherlv_9= ',' ( (lv_groups_10_0= ruleGroup ) ) )* otherlv_11= '}'
                    {
                    otherlv_7=(Token)match(input,11,FollowSets000.FOLLOW_11_in_ruleFeature1341); 

                        	newLeafNode(otherlv_7, grammarAccess.getFeatureAccess().getLeftCurlyBracketKeyword_3_0());
                        
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:634:1: ( (lv_groups_8_0= ruleGroup ) )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:635:1: (lv_groups_8_0= ruleGroup )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:635:1: (lv_groups_8_0= ruleGroup )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:636:3: lv_groups_8_0= ruleGroup
                    {
                     
                    	        newCompositeNode(grammarAccess.getFeatureAccess().getGroupsGroupParserRuleCall_3_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleGroup_in_ruleFeature1362);
                    lv_groups_8_0=ruleGroup();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFeatureRule());
                    	        }
                           		add(
                           			current, 
                           			"groups",
                            		lv_groups_8_0, 
                            		"Group");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:652:2: (otherlv_9= ',' ( (lv_groups_10_0= ruleGroup ) ) )*
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( (LA11_0==13) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:652:4: otherlv_9= ',' ( (lv_groups_10_0= ruleGroup ) )
                    	    {
                    	    otherlv_9=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleFeature1375); 

                    	        	newLeafNode(otherlv_9, grammarAccess.getFeatureAccess().getCommaKeyword_3_2_0());
                    	        
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:656:1: ( (lv_groups_10_0= ruleGroup ) )
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:657:1: (lv_groups_10_0= ruleGroup )
                    	    {
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:657:1: (lv_groups_10_0= ruleGroup )
                    	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:658:3: lv_groups_10_0= ruleGroup
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getFeatureAccess().getGroupsGroupParserRuleCall_3_2_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleGroup_in_ruleFeature1396);
                    	    lv_groups_10_0=ruleGroup();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getFeatureRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"groups",
                    	            		lv_groups_10_0, 
                    	            		"Group");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop11;
                        }
                    } while (true);

                    otherlv_11=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleFeature1410); 

                        	newLeafNode(otherlv_11, grammarAccess.getFeatureAccess().getRightCurlyBracketKeyword_3_3());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleBOOLEAN"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:686:1: entryRuleBOOLEAN returns [EObject current=null] : iv_ruleBOOLEAN= ruleBOOLEAN EOF ;
    public final EObject entryRuleBOOLEAN() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBOOLEAN = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:687:2: (iv_ruleBOOLEAN= ruleBOOLEAN EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:688:2: iv_ruleBOOLEAN= ruleBOOLEAN EOF
            {
             newCompositeNode(grammarAccess.getBOOLEANRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleBOOLEAN_in_entryRuleBOOLEAN1448);
            iv_ruleBOOLEAN=ruleBOOLEAN();

            state._fsp--;

             current =iv_ruleBOOLEAN; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleBOOLEAN1458); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBOOLEAN"


    // $ANTLR start "ruleBOOLEAN"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:695:1: ruleBOOLEAN returns [EObject current=null] : ( () otherlv_1= 'Boolean' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleBOOLEAN() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;


         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:698:28: ( ( () otherlv_1= 'Boolean' ( (lv_name_2_0= ruleEString ) ) ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:699:1: ( () otherlv_1= 'Boolean' ( (lv_name_2_0= ruleEString ) ) )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:699:1: ( () otherlv_1= 'Boolean' ( (lv_name_2_0= ruleEString ) ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:699:2: () otherlv_1= 'Boolean' ( (lv_name_2_0= ruleEString ) )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:699:2: ()
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:700:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getBOOLEANAccess().getBOOLEANAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,20,FollowSets000.FOLLOW_20_in_ruleBOOLEAN1504); 

                	newLeafNode(otherlv_1, grammarAccess.getBOOLEANAccess().getBooleanKeyword_1());
                
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:709:1: ( (lv_name_2_0= ruleEString ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:710:1: (lv_name_2_0= ruleEString )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:710:1: (lv_name_2_0= ruleEString )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:711:3: lv_name_2_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getBOOLEANAccess().getNameEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleBOOLEAN1525);
            lv_name_2_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBOOLEANRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_2_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBOOLEAN"


    // $ANTLR start "entryRuleINTEGER"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:735:1: entryRuleINTEGER returns [EObject current=null] : iv_ruleINTEGER= ruleINTEGER EOF ;
    public final EObject entryRuleINTEGER() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleINTEGER = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:736:2: (iv_ruleINTEGER= ruleINTEGER EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:737:2: iv_ruleINTEGER= ruleINTEGER EOF
            {
             newCompositeNode(grammarAccess.getINTEGERRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleINTEGER_in_entryRuleINTEGER1561);
            iv_ruleINTEGER=ruleINTEGER();

            state._fsp--;

             current =iv_ruleINTEGER; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleINTEGER1571); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleINTEGER"


    // $ANTLR start "ruleINTEGER"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:744:1: ruleINTEGER returns [EObject current=null] : ( () otherlv_1= 'Integer' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleINTEGER() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;


         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:747:28: ( ( () otherlv_1= 'Integer' ( (lv_name_2_0= ruleEString ) ) ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:748:1: ( () otherlv_1= 'Integer' ( (lv_name_2_0= ruleEString ) ) )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:748:1: ( () otherlv_1= 'Integer' ( (lv_name_2_0= ruleEString ) ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:748:2: () otherlv_1= 'Integer' ( (lv_name_2_0= ruleEString ) )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:748:2: ()
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:749:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getINTEGERAccess().getINTEGERAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleINTEGER1617); 

                	newLeafNode(otherlv_1, grammarAccess.getINTEGERAccess().getIntegerKeyword_1());
                
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:758:1: ( (lv_name_2_0= ruleEString ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:759:1: (lv_name_2_0= ruleEString )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:759:1: (lv_name_2_0= ruleEString )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:760:3: lv_name_2_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getINTEGERAccess().getNameEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleINTEGER1638);
            lv_name_2_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getINTEGERRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_2_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleINTEGER"


    // $ANTLR start "entryRuleENUM"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:784:1: entryRuleENUM returns [EObject current=null] : iv_ruleENUM= ruleENUM EOF ;
    public final EObject entryRuleENUM() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleENUM = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:785:2: (iv_ruleENUM= ruleENUM EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:786:2: iv_ruleENUM= ruleENUM EOF
            {
             newCompositeNode(grammarAccess.getENUMRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleENUM_in_entryRuleENUM1674);
            iv_ruleENUM=ruleENUM();

            state._fsp--;

             current =iv_ruleENUM; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleENUM1684); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleENUM"


    // $ANTLR start "ruleENUM"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:793:1: ruleENUM returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( (lv_enumValues_3_0= ruleEnumValue ) ) (otherlv_4= ',' ( (lv_enumValues_5_0= ruleEnumValue ) ) )* otherlv_6= '}' ) ;
    public final EObject ruleENUM() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_enumValues_3_0 = null;

        EObject lv_enumValues_5_0 = null;


         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:796:28: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( (lv_enumValues_3_0= ruleEnumValue ) ) (otherlv_4= ',' ( (lv_enumValues_5_0= ruleEnumValue ) ) )* otherlv_6= '}' ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:797:1: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( (lv_enumValues_3_0= ruleEnumValue ) ) (otherlv_4= ',' ( (lv_enumValues_5_0= ruleEnumValue ) ) )* otherlv_6= '}' )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:797:1: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( (lv_enumValues_3_0= ruleEnumValue ) ) (otherlv_4= ',' ( (lv_enumValues_5_0= ruleEnumValue ) ) )* otherlv_6= '}' )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:797:2: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( (lv_enumValues_3_0= ruleEnumValue ) ) (otherlv_4= ',' ( (lv_enumValues_5_0= ruleEnumValue ) ) )* otherlv_6= '}'
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:797:2: ()
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:798:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getENUMAccess().getENUMAction_0(),
                        current);
                

            }

            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:803:2: ( (lv_name_1_0= ruleEString ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:804:1: (lv_name_1_0= ruleEString )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:804:1: (lv_name_1_0= ruleEString )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:805:3: lv_name_1_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getENUMAccess().getNameEStringParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleENUM1739);
            lv_name_1_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getENUMRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,11,FollowSets000.FOLLOW_11_in_ruleENUM1751); 

                	newLeafNode(otherlv_2, grammarAccess.getENUMAccess().getLeftCurlyBracketKeyword_2());
                
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:825:1: ( (lv_enumValues_3_0= ruleEnumValue ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:826:1: (lv_enumValues_3_0= ruleEnumValue )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:826:1: (lv_enumValues_3_0= ruleEnumValue )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:827:3: lv_enumValues_3_0= ruleEnumValue
            {
             
            	        newCompositeNode(grammarAccess.getENUMAccess().getEnumValuesEnumValueParserRuleCall_3_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEnumValue_in_ruleENUM1772);
            lv_enumValues_3_0=ruleEnumValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getENUMRule());
            	        }
                   		add(
                   			current, 
                   			"enumValues",
                    		lv_enumValues_3_0, 
                    		"EnumValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:843:2: (otherlv_4= ',' ( (lv_enumValues_5_0= ruleEnumValue ) ) )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==13) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:843:4: otherlv_4= ',' ( (lv_enumValues_5_0= ruleEnumValue ) )
            	    {
            	    otherlv_4=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleENUM1785); 

            	        	newLeafNode(otherlv_4, grammarAccess.getENUMAccess().getCommaKeyword_4_0());
            	        
            	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:847:1: ( (lv_enumValues_5_0= ruleEnumValue ) )
            	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:848:1: (lv_enumValues_5_0= ruleEnumValue )
            	    {
            	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:848:1: (lv_enumValues_5_0= ruleEnumValue )
            	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:849:3: lv_enumValues_5_0= ruleEnumValue
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getENUMAccess().getEnumValuesEnumValueParserRuleCall_4_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleEnumValue_in_ruleENUM1806);
            	    lv_enumValues_5_0=ruleEnumValue();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getENUMRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"enumValues",
            	            		lv_enumValues_5_0, 
            	            		"EnumValue");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            otherlv_6=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleENUM1820); 

                	newLeafNode(otherlv_6, grammarAccess.getENUMAccess().getRightCurlyBracketKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleENUM"


    // $ANTLR start "entryRuleEnumValue"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:877:1: entryRuleEnumValue returns [EObject current=null] : iv_ruleEnumValue= ruleEnumValue EOF ;
    public final EObject entryRuleEnumValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumValue = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:878:2: (iv_ruleEnumValue= ruleEnumValue EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:879:2: iv_ruleEnumValue= ruleEnumValue EOF
            {
             newCompositeNode(grammarAccess.getEnumValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEnumValue_in_entryRuleEnumValue1856);
            iv_ruleEnumValue=ruleEnumValue();

            state._fsp--;

             current =iv_ruleEnumValue; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEnumValue1866); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumValue"


    // $ANTLR start "ruleEnumValue"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:886:1: ruleEnumValue returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) ) ;
    public final EObject ruleEnumValue() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_1_0 = null;


         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:889:28: ( ( () ( (lv_name_1_0= ruleEString ) ) ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:890:1: ( () ( (lv_name_1_0= ruleEString ) ) )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:890:1: ( () ( (lv_name_1_0= ruleEString ) ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:890:2: () ( (lv_name_1_0= ruleEString ) )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:890:2: ()
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:891:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getEnumValueAccess().getEnumValueAction_0(),
                        current);
                

            }

            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:896:2: ( (lv_name_1_0= ruleEString ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:897:1: (lv_name_1_0= ruleEString )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:897:1: (lv_name_1_0= ruleEString )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:898:3: lv_name_1_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getEnumValueAccess().getNameEStringParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleEnumValue1921);
            lv_name_1_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getEnumValueRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumValue"


    // $ANTLR start "entryRuleGroup"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:922:1: entryRuleGroup returns [EObject current=null] : iv_ruleGroup= ruleGroup EOF ;
    public final EObject entryRuleGroup() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGroup = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:923:2: (iv_ruleGroup= ruleGroup EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:924:2: iv_ruleGroup= ruleGroup EOF
            {
             newCompositeNode(grammarAccess.getGroupRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleGroup_in_entryRuleGroup1957);
            iv_ruleGroup=ruleGroup();

            state._fsp--;

             current =iv_ruleGroup; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleGroup1967); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGroup"


    // $ANTLR start "ruleGroup"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:931:1: ruleGroup returns [EObject current=null] : ( ( (lv_name_0_0= ruleEString ) ) (otherlv_1= '[' ( (lv_min_2_0= ruleEInt ) ) otherlv_3= '...' ( (lv_max_4_0= ruleEInt ) ) otherlv_5= ']' )? otherlv_6= '{' ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ',' ( (lv_features_9_0= ruleFeature ) ) )* otherlv_10= '}' ) ;
    public final EObject ruleGroup() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;

        AntlrDatatypeRuleToken lv_min_2_0 = null;

        AntlrDatatypeRuleToken lv_max_4_0 = null;

        EObject lv_features_7_0 = null;

        EObject lv_features_9_0 = null;


         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:934:28: ( ( ( (lv_name_0_0= ruleEString ) ) (otherlv_1= '[' ( (lv_min_2_0= ruleEInt ) ) otherlv_3= '...' ( (lv_max_4_0= ruleEInt ) ) otherlv_5= ']' )? otherlv_6= '{' ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ',' ( (lv_features_9_0= ruleFeature ) ) )* otherlv_10= '}' ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:935:1: ( ( (lv_name_0_0= ruleEString ) ) (otherlv_1= '[' ( (lv_min_2_0= ruleEInt ) ) otherlv_3= '...' ( (lv_max_4_0= ruleEInt ) ) otherlv_5= ']' )? otherlv_6= '{' ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ',' ( (lv_features_9_0= ruleFeature ) ) )* otherlv_10= '}' )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:935:1: ( ( (lv_name_0_0= ruleEString ) ) (otherlv_1= '[' ( (lv_min_2_0= ruleEInt ) ) otherlv_3= '...' ( (lv_max_4_0= ruleEInt ) ) otherlv_5= ']' )? otherlv_6= '{' ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ',' ( (lv_features_9_0= ruleFeature ) ) )* otherlv_10= '}' )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:935:2: ( (lv_name_0_0= ruleEString ) ) (otherlv_1= '[' ( (lv_min_2_0= ruleEInt ) ) otherlv_3= '...' ( (lv_max_4_0= ruleEInt ) ) otherlv_5= ']' )? otherlv_6= '{' ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ',' ( (lv_features_9_0= ruleFeature ) ) )* otherlv_10= '}'
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:935:2: ( (lv_name_0_0= ruleEString ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:936:1: (lv_name_0_0= ruleEString )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:936:1: (lv_name_0_0= ruleEString )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:937:3: lv_name_0_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getGroupAccess().getNameEStringParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleGroup2013);
            lv_name_0_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getGroupRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:953:2: (otherlv_1= '[' ( (lv_min_2_0= ruleEInt ) ) otherlv_3= '...' ( (lv_max_4_0= ruleEInt ) ) otherlv_5= ']' )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==17) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:953:4: otherlv_1= '[' ( (lv_min_2_0= ruleEInt ) ) otherlv_3= '...' ( (lv_max_4_0= ruleEInt ) ) otherlv_5= ']'
                    {
                    otherlv_1=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleGroup2026); 

                        	newLeafNode(otherlv_1, grammarAccess.getGroupAccess().getLeftSquareBracketKeyword_1_0());
                        
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:957:1: ( (lv_min_2_0= ruleEInt ) )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:958:1: (lv_min_2_0= ruleEInt )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:958:1: (lv_min_2_0= ruleEInt )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:959:3: lv_min_2_0= ruleEInt
                    {
                     
                    	        newCompositeNode(grammarAccess.getGroupAccess().getMinEIntParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEInt_in_ruleGroup2047);
                    lv_min_2_0=ruleEInt();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getGroupRule());
                    	        }
                           		set(
                           			current, 
                           			"min",
                            		lv_min_2_0, 
                            		"EInt");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_3=(Token)match(input,22,FollowSets000.FOLLOW_22_in_ruleGroup2059); 

                        	newLeafNode(otherlv_3, grammarAccess.getGroupAccess().getFullStopFullStopFullStopKeyword_1_2());
                        
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:979:1: ( (lv_max_4_0= ruleEInt ) )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:980:1: (lv_max_4_0= ruleEInt )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:980:1: (lv_max_4_0= ruleEInt )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:981:3: lv_max_4_0= ruleEInt
                    {
                     
                    	        newCompositeNode(grammarAccess.getGroupAccess().getMaxEIntParserRuleCall_1_3_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEInt_in_ruleGroup2080);
                    lv_max_4_0=ruleEInt();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getGroupRule());
                    	        }
                           		set(
                           			current, 
                           			"max",
                            		lv_max_4_0, 
                            		"EInt");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_5=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleGroup2092); 

                        	newLeafNode(otherlv_5, grammarAccess.getGroupAccess().getRightSquareBracketKeyword_1_4());
                        

                    }
                    break;

            }

            otherlv_6=(Token)match(input,11,FollowSets000.FOLLOW_11_in_ruleGroup2106); 

                	newLeafNode(otherlv_6, grammarAccess.getGroupAccess().getLeftCurlyBracketKeyword_2());
                
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1005:1: ( (lv_features_7_0= ruleFeature ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1006:1: (lv_features_7_0= ruleFeature )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1006:1: (lv_features_7_0= ruleFeature )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1007:3: lv_features_7_0= ruleFeature
            {
             
            	        newCompositeNode(grammarAccess.getGroupAccess().getFeaturesFeatureParserRuleCall_3_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_ruleGroup2127);
            lv_features_7_0=ruleFeature();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getGroupRule());
            	        }
                   		add(
                   			current, 
                   			"features",
                    		lv_features_7_0, 
                    		"Feature");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1023:2: (otherlv_8= ',' ( (lv_features_9_0= ruleFeature ) ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==13) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1023:4: otherlv_8= ',' ( (lv_features_9_0= ruleFeature ) )
            	    {
            	    otherlv_8=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleGroup2140); 

            	        	newLeafNode(otherlv_8, grammarAccess.getGroupAccess().getCommaKeyword_4_0());
            	        
            	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1027:1: ( (lv_features_9_0= ruleFeature ) )
            	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1028:1: (lv_features_9_0= ruleFeature )
            	    {
            	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1028:1: (lv_features_9_0= ruleFeature )
            	    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1029:3: lv_features_9_0= ruleFeature
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getGroupAccess().getFeaturesFeatureParserRuleCall_4_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleFeature_in_ruleGroup2161);
            	    lv_features_9_0=ruleFeature();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getGroupRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"features",
            	            		lv_features_9_0, 
            	            		"Feature");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            otherlv_10=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleGroup2175); 

                	newLeafNode(otherlv_10, grammarAccess.getGroupAccess().getRightCurlyBracketKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGroup"


    // $ANTLR start "entryRuleEInt"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1057:1: entryRuleEInt returns [String current=null] : iv_ruleEInt= ruleEInt EOF ;
    public final String entryRuleEInt() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEInt = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1058:2: (iv_ruleEInt= ruleEInt EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1059:2: iv_ruleEInt= ruleEInt EOF
            {
             newCompositeNode(grammarAccess.getEIntRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEInt_in_entryRuleEInt2212);
            iv_ruleEInt=ruleEInt();

            state._fsp--;

             current =iv_ruleEInt.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEInt2223); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1066:1: ruleEInt returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_INT_0= RULE_INT ;
    public final AntlrDatatypeRuleToken ruleEInt() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;

         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1069:28: (this_INT_0= RULE_INT )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1070:5: this_INT_0= RULE_INT
            {
            this_INT_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_ruleEInt2262); 

            		current.merge(this_INT_0);
                
             
                newLeafNode(this_INT_0, grammarAccess.getEIntAccess().getINTTerminalRuleCall()); 
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "entryRuleUnary"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1085:1: entryRuleUnary returns [EObject current=null] : iv_ruleUnary= ruleUnary EOF ;
    public final EObject entryRuleUnary() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnary = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1086:2: (iv_ruleUnary= ruleUnary EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1087:2: iv_ruleUnary= ruleUnary EOF
            {
             newCompositeNode(grammarAccess.getUnaryRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleUnary_in_entryRuleUnary2306);
            iv_ruleUnary=ruleUnary();

            state._fsp--;

             current =iv_ruleUnary; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleUnary2316); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnary"


    // $ANTLR start "ruleUnary"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1094:1: ruleUnary returns [EObject current=null] : ( ( (lv_operator_0_0= ruleUnaryOperator ) ) ( (lv_innerExpression_1_0= ruleAtomicExpression ) ) ) ;
    public final EObject ruleUnary() throws RecognitionException {
        EObject current = null;

        Enumerator lv_operator_0_0 = null;

        EObject lv_innerExpression_1_0 = null;


         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1097:28: ( ( ( (lv_operator_0_0= ruleUnaryOperator ) ) ( (lv_innerExpression_1_0= ruleAtomicExpression ) ) ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1098:1: ( ( (lv_operator_0_0= ruleUnaryOperator ) ) ( (lv_innerExpression_1_0= ruleAtomicExpression ) ) )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1098:1: ( ( (lv_operator_0_0= ruleUnaryOperator ) ) ( (lv_innerExpression_1_0= ruleAtomicExpression ) ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1098:2: ( (lv_operator_0_0= ruleUnaryOperator ) ) ( (lv_innerExpression_1_0= ruleAtomicExpression ) )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1098:2: ( (lv_operator_0_0= ruleUnaryOperator ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1099:1: (lv_operator_0_0= ruleUnaryOperator )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1099:1: (lv_operator_0_0= ruleUnaryOperator )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1100:3: lv_operator_0_0= ruleUnaryOperator
            {
             
            	        newCompositeNode(grammarAccess.getUnaryAccess().getOperatorUnaryOperatorEnumRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleUnaryOperator_in_ruleUnary2362);
            lv_operator_0_0=ruleUnaryOperator();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getUnaryRule());
            	        }
                   		set(
                   			current, 
                   			"operator",
                    		lv_operator_0_0, 
                    		"UnaryOperator");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1116:2: ( (lv_innerExpression_1_0= ruleAtomicExpression ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1117:1: (lv_innerExpression_1_0= ruleAtomicExpression )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1117:1: (lv_innerExpression_1_0= ruleAtomicExpression )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1118:3: lv_innerExpression_1_0= ruleAtomicExpression
            {
             
            	        newCompositeNode(grammarAccess.getUnaryAccess().getInnerExpressionAtomicExpressionParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleAtomicExpression_in_ruleUnary2383);
            lv_innerExpression_1_0=ruleAtomicExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getUnaryRule());
            	        }
                   		set(
                   			current, 
                   			"innerExpression",
                    		lv_innerExpression_1_0, 
                    		"AtomicExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnary"


    // $ANTLR start "entryRuleBinary"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1142:1: entryRuleBinary returns [EObject current=null] : iv_ruleBinary= ruleBinary EOF ;
    public final EObject entryRuleBinary() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBinary = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1143:2: (iv_ruleBinary= ruleBinary EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1144:2: iv_ruleBinary= ruleBinary EOF
            {
             newCompositeNode(grammarAccess.getBinaryRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleBinary_in_entryRuleBinary2419);
            iv_ruleBinary=ruleBinary();

            state._fsp--;

             current =iv_ruleBinary; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleBinary2429); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBinary"


    // $ANTLR start "ruleBinary"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1151:1: ruleBinary returns [EObject current=null] : ( ( (lv_leftExpression_0_0= ruleAtomicExpression ) ) ( (lv_operator_1_0= ruleBinaryOperator ) ) ( (lv_rightExpression_2_0= ruleAtomicExpression ) ) ) ;
    public final EObject ruleBinary() throws RecognitionException {
        EObject current = null;

        EObject lv_leftExpression_0_0 = null;

        Enumerator lv_operator_1_0 = null;

        EObject lv_rightExpression_2_0 = null;


         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1154:28: ( ( ( (lv_leftExpression_0_0= ruleAtomicExpression ) ) ( (lv_operator_1_0= ruleBinaryOperator ) ) ( (lv_rightExpression_2_0= ruleAtomicExpression ) ) ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1155:1: ( ( (lv_leftExpression_0_0= ruleAtomicExpression ) ) ( (lv_operator_1_0= ruleBinaryOperator ) ) ( (lv_rightExpression_2_0= ruleAtomicExpression ) ) )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1155:1: ( ( (lv_leftExpression_0_0= ruleAtomicExpression ) ) ( (lv_operator_1_0= ruleBinaryOperator ) ) ( (lv_rightExpression_2_0= ruleAtomicExpression ) ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1155:2: ( (lv_leftExpression_0_0= ruleAtomicExpression ) ) ( (lv_operator_1_0= ruleBinaryOperator ) ) ( (lv_rightExpression_2_0= ruleAtomicExpression ) )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1155:2: ( (lv_leftExpression_0_0= ruleAtomicExpression ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1156:1: (lv_leftExpression_0_0= ruleAtomicExpression )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1156:1: (lv_leftExpression_0_0= ruleAtomicExpression )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1157:3: lv_leftExpression_0_0= ruleAtomicExpression
            {
             
            	        newCompositeNode(grammarAccess.getBinaryAccess().getLeftExpressionAtomicExpressionParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleAtomicExpression_in_ruleBinary2475);
            lv_leftExpression_0_0=ruleAtomicExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBinaryRule());
            	        }
                   		set(
                   			current, 
                   			"leftExpression",
                    		lv_leftExpression_0_0, 
                    		"AtomicExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1173:2: ( (lv_operator_1_0= ruleBinaryOperator ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1174:1: (lv_operator_1_0= ruleBinaryOperator )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1174:1: (lv_operator_1_0= ruleBinaryOperator )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1175:3: lv_operator_1_0= ruleBinaryOperator
            {
             
            	        newCompositeNode(grammarAccess.getBinaryAccess().getOperatorBinaryOperatorEnumRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleBinaryOperator_in_ruleBinary2496);
            lv_operator_1_0=ruleBinaryOperator();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBinaryRule());
            	        }
                   		set(
                   			current, 
                   			"operator",
                    		lv_operator_1_0, 
                    		"BinaryOperator");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1191:2: ( (lv_rightExpression_2_0= ruleAtomicExpression ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1192:1: (lv_rightExpression_2_0= ruleAtomicExpression )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1192:1: (lv_rightExpression_2_0= ruleAtomicExpression )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1193:3: lv_rightExpression_2_0= ruleAtomicExpression
            {
             
            	        newCompositeNode(grammarAccess.getBinaryAccess().getRightExpressionAtomicExpressionParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleAtomicExpression_in_ruleBinary2517);
            lv_rightExpression_2_0=ruleAtomicExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBinaryRule());
            	        }
                   		set(
                   			current, 
                   			"rightExpression",
                    		lv_rightExpression_2_0, 
                    		"AtomicExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinary"


    // $ANTLR start "entryRuleAtomicExpression"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1217:1: entryRuleAtomicExpression returns [EObject current=null] : iv_ruleAtomicExpression= ruleAtomicExpression EOF ;
    public final EObject entryRuleAtomicExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAtomicExpression = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1218:2: (iv_ruleAtomicExpression= ruleAtomicExpression EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1219:2: iv_ruleAtomicExpression= ruleAtomicExpression EOF
            {
             newCompositeNode(grammarAccess.getAtomicExpressionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAtomicExpression_in_entryRuleAtomicExpression2553);
            iv_ruleAtomicExpression=ruleAtomicExpression();

            state._fsp--;

             current =iv_ruleAtomicExpression; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAtomicExpression2563); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAtomicExpression"


    // $ANTLR start "ruleAtomicExpression"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1226:1: ruleAtomicExpression returns [EObject current=null] : (this_IdRef_0= ruleIdRef | this_Constants_1= ruleConstants | (otherlv_2= '(' this_Expression_3= ruleExpression otherlv_4= ')' ) ) ;
    public final EObject ruleAtomicExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject this_IdRef_0 = null;

        EObject this_Constants_1 = null;

        EObject this_Expression_3 = null;


         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1229:28: ( (this_IdRef_0= ruleIdRef | this_Constants_1= ruleConstants | (otherlv_2= '(' this_Expression_3= ruleExpression otherlv_4= ')' ) ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1230:1: (this_IdRef_0= ruleIdRef | this_Constants_1= ruleConstants | (otherlv_2= '(' this_Expression_3= ruleExpression otherlv_4= ')' ) )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1230:1: (this_IdRef_0= ruleIdRef | this_Constants_1= ruleConstants | (otherlv_2= '(' this_Expression_3= ruleExpression otherlv_4= ')' ) )
            int alt16=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
            case RULE_ID:
                {
                alt16=1;
                }
                break;
            case RULE_INT:
            case 17:
            case 38:
            case 39:
                {
                alt16=2;
                }
                break;
            case 23:
                {
                alt16=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }

            switch (alt16) {
                case 1 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1231:5: this_IdRef_0= ruleIdRef
                    {
                     
                            newCompositeNode(grammarAccess.getAtomicExpressionAccess().getIdRefParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleIdRef_in_ruleAtomicExpression2610);
                    this_IdRef_0=ruleIdRef();

                    state._fsp--;

                     
                            current = this_IdRef_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1241:5: this_Constants_1= ruleConstants
                    {
                     
                            newCompositeNode(grammarAccess.getAtomicExpressionAccess().getConstantsParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleConstants_in_ruleAtomicExpression2637);
                    this_Constants_1=ruleConstants();

                    state._fsp--;

                     
                            current = this_Constants_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1250:6: (otherlv_2= '(' this_Expression_3= ruleExpression otherlv_4= ')' )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1250:6: (otherlv_2= '(' this_Expression_3= ruleExpression otherlv_4= ')' )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1250:8: otherlv_2= '(' this_Expression_3= ruleExpression otherlv_4= ')'
                    {
                    otherlv_2=(Token)match(input,23,FollowSets000.FOLLOW_23_in_ruleAtomicExpression2655); 

                        	newLeafNode(otherlv_2, grammarAccess.getAtomicExpressionAccess().getLeftParenthesisKeyword_2_0());
                        
                     
                            newCompositeNode(grammarAccess.getAtomicExpressionAccess().getExpressionParserRuleCall_2_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleExpression_in_ruleAtomicExpression2677);
                    this_Expression_3=ruleExpression();

                    state._fsp--;

                     
                            current = this_Expression_3; 
                            afterParserOrEnumRuleCall();
                        
                    otherlv_4=(Token)match(input,24,FollowSets000.FOLLOW_24_in_ruleAtomicExpression2688); 

                        	newLeafNode(otherlv_4, grammarAccess.getAtomicExpressionAccess().getRightParenthesisKeyword_2_2());
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAtomicExpression"


    // $ANTLR start "entryRuleConstants"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1275:1: entryRuleConstants returns [EObject current=null] : iv_ruleConstants= ruleConstants EOF ;
    public final EObject entryRuleConstants() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstants = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1276:2: (iv_ruleConstants= ruleConstants EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1277:2: iv_ruleConstants= ruleConstants EOF
            {
             newCompositeNode(grammarAccess.getConstantsRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleConstants_in_entryRuleConstants2725);
            iv_ruleConstants=ruleConstants();

            state._fsp--;

             current =iv_ruleConstants; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleConstants2735); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstants"


    // $ANTLR start "ruleConstants"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1284:1: ruleConstants returns [EObject current=null] : (this_IntConst_0= ruleIntConst | this_BoolConst_1= ruleBoolConst | this_StringConst_2= ruleStringConst ) ;
    public final EObject ruleConstants() throws RecognitionException {
        EObject current = null;

        EObject this_IntConst_0 = null;

        EObject this_BoolConst_1 = null;

        EObject this_StringConst_2 = null;


         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1287:28: ( (this_IntConst_0= ruleIntConst | this_BoolConst_1= ruleBoolConst | this_StringConst_2= ruleStringConst ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1288:1: (this_IntConst_0= ruleIntConst | this_BoolConst_1= ruleBoolConst | this_StringConst_2= ruleStringConst )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1288:1: (this_IntConst_0= ruleIntConst | this_BoolConst_1= ruleBoolConst | this_StringConst_2= ruleStringConst )
            int alt17=3;
            switch ( input.LA(1) ) {
            case RULE_INT:
                {
                alt17=1;
                }
                break;
            case 38:
            case 39:
                {
                alt17=2;
                }
                break;
            case 17:
                {
                alt17=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }

            switch (alt17) {
                case 1 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1289:5: this_IntConst_0= ruleIntConst
                    {
                     
                            newCompositeNode(grammarAccess.getConstantsAccess().getIntConstParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleIntConst_in_ruleConstants2782);
                    this_IntConst_0=ruleIntConst();

                    state._fsp--;

                     
                            current = this_IntConst_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1299:5: this_BoolConst_1= ruleBoolConst
                    {
                     
                            newCompositeNode(grammarAccess.getConstantsAccess().getBoolConstParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleBoolConst_in_ruleConstants2809);
                    this_BoolConst_1=ruleBoolConst();

                    state._fsp--;

                     
                            current = this_BoolConst_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1309:5: this_StringConst_2= ruleStringConst
                    {
                     
                            newCompositeNode(grammarAccess.getConstantsAccess().getStringConstParserRuleCall_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleStringConst_in_ruleConstants2836);
                    this_StringConst_2=ruleStringConst();

                    state._fsp--;

                     
                            current = this_StringConst_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstants"


    // $ANTLR start "entryRuleIntConst"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1325:1: entryRuleIntConst returns [EObject current=null] : iv_ruleIntConst= ruleIntConst EOF ;
    public final EObject entryRuleIntConst() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntConst = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1326:2: (iv_ruleIntConst= ruleIntConst EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1327:2: iv_ruleIntConst= ruleIntConst EOF
            {
             newCompositeNode(grammarAccess.getIntConstRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleIntConst_in_entryRuleIntConst2871);
            iv_ruleIntConst=ruleIntConst();

            state._fsp--;

             current =iv_ruleIntConst; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleIntConst2881); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntConst"


    // $ANTLR start "ruleIntConst"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1334:1: ruleIntConst returns [EObject current=null] : ( (lv_value_0_0= ruleEInt ) ) ;
    public final EObject ruleIntConst() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1337:28: ( ( (lv_value_0_0= ruleEInt ) ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1338:1: ( (lv_value_0_0= ruleEInt ) )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1338:1: ( (lv_value_0_0= ruleEInt ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1339:1: (lv_value_0_0= ruleEInt )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1339:1: (lv_value_0_0= ruleEInt )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1340:3: lv_value_0_0= ruleEInt
            {
             
            	        newCompositeNode(grammarAccess.getIntConstAccess().getValueEIntParserRuleCall_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEInt_in_ruleIntConst2926);
            lv_value_0_0=ruleEInt();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIntConstRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"EInt");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntConst"


    // $ANTLR start "entryRuleStringConst"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1364:1: entryRuleStringConst returns [EObject current=null] : iv_ruleStringConst= ruleStringConst EOF ;
    public final EObject entryRuleStringConst() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringConst = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1365:2: (iv_ruleStringConst= ruleStringConst EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1366:2: iv_ruleStringConst= ruleStringConst EOF
            {
             newCompositeNode(grammarAccess.getStringConstRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleStringConst_in_entryRuleStringConst2961);
            iv_ruleStringConst=ruleStringConst();

            state._fsp--;

             current =iv_ruleStringConst; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleStringConst2971); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringConst"


    // $ANTLR start "ruleStringConst"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1373:1: ruleStringConst returns [EObject current=null] : (otherlv_0= '[' ( (lv_value_1_0= ruleEString ) ) otherlv_2= ']' ) ;
    public final EObject ruleStringConst() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_value_1_0 = null;


         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1376:28: ( (otherlv_0= '[' ( (lv_value_1_0= ruleEString ) ) otherlv_2= ']' ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1377:1: (otherlv_0= '[' ( (lv_value_1_0= ruleEString ) ) otherlv_2= ']' )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1377:1: (otherlv_0= '[' ( (lv_value_1_0= ruleEString ) ) otherlv_2= ']' )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1377:3: otherlv_0= '[' ( (lv_value_1_0= ruleEString ) ) otherlv_2= ']'
            {
            otherlv_0=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleStringConst3008); 

                	newLeafNode(otherlv_0, grammarAccess.getStringConstAccess().getLeftSquareBracketKeyword_0());
                
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1381:1: ( (lv_value_1_0= ruleEString ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1382:1: (lv_value_1_0= ruleEString )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1382:1: (lv_value_1_0= ruleEString )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1383:3: lv_value_1_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getStringConstAccess().getValueEStringParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleStringConst3029);
            lv_value_1_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getStringConstRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_1_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleStringConst3041); 

                	newLeafNode(otherlv_2, grammarAccess.getStringConstAccess().getRightSquareBracketKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringConst"


    // $ANTLR start "entryRuleBoolConst"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1411:1: entryRuleBoolConst returns [EObject current=null] : iv_ruleBoolConst= ruleBoolConst EOF ;
    public final EObject entryRuleBoolConst() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBoolConst = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1412:2: (iv_ruleBoolConst= ruleBoolConst EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1413:2: iv_ruleBoolConst= ruleBoolConst EOF
            {
             newCompositeNode(grammarAccess.getBoolConstRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleBoolConst_in_entryRuleBoolConst3077);
            iv_ruleBoolConst=ruleBoolConst();

            state._fsp--;

             current =iv_ruleBoolConst; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleBoolConst3087); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBoolConst"


    // $ANTLR start "ruleBoolConst"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1420:1: ruleBoolConst returns [EObject current=null] : ( (lv_value_0_0= ruleBooolean ) ) ;
    public final EObject ruleBoolConst() throws RecognitionException {
        EObject current = null;

        Enumerator lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1423:28: ( ( (lv_value_0_0= ruleBooolean ) ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1424:1: ( (lv_value_0_0= ruleBooolean ) )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1424:1: ( (lv_value_0_0= ruleBooolean ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1425:1: (lv_value_0_0= ruleBooolean )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1425:1: (lv_value_0_0= ruleBooolean )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1426:3: lv_value_0_0= ruleBooolean
            {
             
            	        newCompositeNode(grammarAccess.getBoolConstAccess().getValueBoooleanEnumRuleCall_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleBooolean_in_ruleBoolConst3132);
            lv_value_0_0=ruleBooolean();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBoolConstRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"Booolean");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBoolConst"


    // $ANTLR start "entryRuleIdRef"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1450:1: entryRuleIdRef returns [EObject current=null] : iv_ruleIdRef= ruleIdRef EOF ;
    public final EObject entryRuleIdRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIdRef = null;


        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1451:2: (iv_ruleIdRef= ruleIdRef EOF )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1452:2: iv_ruleIdRef= ruleIdRef EOF
            {
             newCompositeNode(grammarAccess.getIdRefRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleIdRef_in_entryRuleIdRef3167);
            iv_ruleIdRef=ruleIdRef();

            state._fsp--;

             current =iv_ruleIdRef; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleIdRef3177); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIdRef"


    // $ANTLR start "ruleIdRef"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1459:1: ruleIdRef returns [EObject current=null] : ( ( ruleEString ) ) ;
    public final EObject ruleIdRef() throws RecognitionException {
        EObject current = null;

         enterRule(); 
            
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1462:28: ( ( ( ruleEString ) ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1463:1: ( ( ruleEString ) )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1463:1: ( ( ruleEString ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1464:1: ( ruleEString )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1464:1: ( ruleEString )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1465:3: ruleEString
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getIdRefRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getIdRefAccess().getIdentifierIdentifiableCrossReference_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleIdRef3224);
            ruleEString();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIdRef"


    // $ANTLR start "ruleUnaryOperator"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1486:1: ruleUnaryOperator returns [Enumerator current=null] : ( (enumLiteral_0= 'not' ) | (enumLiteral_1= '-' ) | (enumLiteral_2= '+' ) ) ;
    public final Enumerator ruleUnaryOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;

         enterRule(); 
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1488:28: ( ( (enumLiteral_0= 'not' ) | (enumLiteral_1= '-' ) | (enumLiteral_2= '+' ) ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1489:1: ( (enumLiteral_0= 'not' ) | (enumLiteral_1= '-' ) | (enumLiteral_2= '+' ) )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1489:1: ( (enumLiteral_0= 'not' ) | (enumLiteral_1= '-' ) | (enumLiteral_2= '+' ) )
            int alt18=3;
            switch ( input.LA(1) ) {
            case 25:
                {
                alt18=1;
                }
                break;
            case 26:
                {
                alt18=2;
                }
                break;
            case 27:
                {
                alt18=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }

            switch (alt18) {
                case 1 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1489:2: (enumLiteral_0= 'not' )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1489:2: (enumLiteral_0= 'not' )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1489:4: enumLiteral_0= 'not'
                    {
                    enumLiteral_0=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleUnaryOperator3273); 

                            current = grammarAccess.getUnaryOperatorAccess().getNOTEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getUnaryOperatorAccess().getNOTEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1495:6: (enumLiteral_1= '-' )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1495:6: (enumLiteral_1= '-' )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1495:8: enumLiteral_1= '-'
                    {
                    enumLiteral_1=(Token)match(input,26,FollowSets000.FOLLOW_26_in_ruleUnaryOperator3290); 

                            current = grammarAccess.getUnaryOperatorAccess().getNEGATIVEEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getUnaryOperatorAccess().getNEGATIVEEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1501:6: (enumLiteral_2= '+' )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1501:6: (enumLiteral_2= '+' )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1501:8: enumLiteral_2= '+'
                    {
                    enumLiteral_2=(Token)match(input,27,FollowSets000.FOLLOW_27_in_ruleUnaryOperator3307); 

                            current = grammarAccess.getUnaryOperatorAccess().getPOSITIVEEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getUnaryOperatorAccess().getPOSITIVEEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryOperator"


    // $ANTLR start "ruleBinaryOperator"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1511:1: ruleBinaryOperator returns [Enumerator current=null] : ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) | (enumLiteral_2= '*' ) | (enumLiteral_3= '/' ) | (enumLiteral_4= '==' ) | (enumLiteral_5= '!=' ) | (enumLiteral_6= ' and ' ) | (enumLiteral_7= ' or ' ) | (enumLiteral_8= '>' ) | (enumLiteral_9= '>=' ) | (enumLiteral_10= '<' ) | (enumLiteral_11= '<=' ) ) ;
    public final Enumerator ruleBinaryOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;
        Token enumLiteral_8=null;
        Token enumLiteral_9=null;
        Token enumLiteral_10=null;
        Token enumLiteral_11=null;

         enterRule(); 
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1513:28: ( ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) | (enumLiteral_2= '*' ) | (enumLiteral_3= '/' ) | (enumLiteral_4= '==' ) | (enumLiteral_5= '!=' ) | (enumLiteral_6= ' and ' ) | (enumLiteral_7= ' or ' ) | (enumLiteral_8= '>' ) | (enumLiteral_9= '>=' ) | (enumLiteral_10= '<' ) | (enumLiteral_11= '<=' ) ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1514:1: ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) | (enumLiteral_2= '*' ) | (enumLiteral_3= '/' ) | (enumLiteral_4= '==' ) | (enumLiteral_5= '!=' ) | (enumLiteral_6= ' and ' ) | (enumLiteral_7= ' or ' ) | (enumLiteral_8= '>' ) | (enumLiteral_9= '>=' ) | (enumLiteral_10= '<' ) | (enumLiteral_11= '<=' ) )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1514:1: ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) | (enumLiteral_2= '*' ) | (enumLiteral_3= '/' ) | (enumLiteral_4= '==' ) | (enumLiteral_5= '!=' ) | (enumLiteral_6= ' and ' ) | (enumLiteral_7= ' or ' ) | (enumLiteral_8= '>' ) | (enumLiteral_9= '>=' ) | (enumLiteral_10= '<' ) | (enumLiteral_11= '<=' ) )
            int alt19=12;
            switch ( input.LA(1) ) {
            case 27:
                {
                alt19=1;
                }
                break;
            case 26:
                {
                alt19=2;
                }
                break;
            case 28:
                {
                alt19=3;
                }
                break;
            case 29:
                {
                alt19=4;
                }
                break;
            case 30:
                {
                alt19=5;
                }
                break;
            case 31:
                {
                alt19=6;
                }
                break;
            case 32:
                {
                alt19=7;
                }
                break;
            case 33:
                {
                alt19=8;
                }
                break;
            case 34:
                {
                alt19=9;
                }
                break;
            case 35:
                {
                alt19=10;
                }
                break;
            case 36:
                {
                alt19=11;
                }
                break;
            case 37:
                {
                alt19=12;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }

            switch (alt19) {
                case 1 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1514:2: (enumLiteral_0= '+' )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1514:2: (enumLiteral_0= '+' )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1514:4: enumLiteral_0= '+'
                    {
                    enumLiteral_0=(Token)match(input,27,FollowSets000.FOLLOW_27_in_ruleBinaryOperator3352); 

                            current = grammarAccess.getBinaryOperatorAccess().getADDEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getBinaryOperatorAccess().getADDEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1520:6: (enumLiteral_1= '-' )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1520:6: (enumLiteral_1= '-' )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1520:8: enumLiteral_1= '-'
                    {
                    enumLiteral_1=(Token)match(input,26,FollowSets000.FOLLOW_26_in_ruleBinaryOperator3369); 

                            current = grammarAccess.getBinaryOperatorAccess().getMINUSEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getBinaryOperatorAccess().getMINUSEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1526:6: (enumLiteral_2= '*' )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1526:6: (enumLiteral_2= '*' )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1526:8: enumLiteral_2= '*'
                    {
                    enumLiteral_2=(Token)match(input,28,FollowSets000.FOLLOW_28_in_ruleBinaryOperator3386); 

                            current = grammarAccess.getBinaryOperatorAccess().getMULTIPLYEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getBinaryOperatorAccess().getMULTIPLYEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;
                case 4 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1532:6: (enumLiteral_3= '/' )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1532:6: (enumLiteral_3= '/' )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1532:8: enumLiteral_3= '/'
                    {
                    enumLiteral_3=(Token)match(input,29,FollowSets000.FOLLOW_29_in_ruleBinaryOperator3403); 

                            current = grammarAccess.getBinaryOperatorAccess().getDIVIDEEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_3, grammarAccess.getBinaryOperatorAccess().getDIVIDEEnumLiteralDeclaration_3()); 
                        

                    }


                    }
                    break;
                case 5 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1538:6: (enumLiteral_4= '==' )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1538:6: (enumLiteral_4= '==' )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1538:8: enumLiteral_4= '=='
                    {
                    enumLiteral_4=(Token)match(input,30,FollowSets000.FOLLOW_30_in_ruleBinaryOperator3420); 

                            current = grammarAccess.getBinaryOperatorAccess().getEQUALEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_4, grammarAccess.getBinaryOperatorAccess().getEQUALEnumLiteralDeclaration_4()); 
                        

                    }


                    }
                    break;
                case 6 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1544:6: (enumLiteral_5= '!=' )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1544:6: (enumLiteral_5= '!=' )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1544:8: enumLiteral_5= '!='
                    {
                    enumLiteral_5=(Token)match(input,31,FollowSets000.FOLLOW_31_in_ruleBinaryOperator3437); 

                            current = grammarAccess.getBinaryOperatorAccess().getDIFFERENTEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_5, grammarAccess.getBinaryOperatorAccess().getDIFFERENTEnumLiteralDeclaration_5()); 
                        

                    }


                    }
                    break;
                case 7 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1550:6: (enumLiteral_6= ' and ' )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1550:6: (enumLiteral_6= ' and ' )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1550:8: enumLiteral_6= ' and '
                    {
                    enumLiteral_6=(Token)match(input,32,FollowSets000.FOLLOW_32_in_ruleBinaryOperator3454); 

                            current = grammarAccess.getBinaryOperatorAccess().getANDEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_6, grammarAccess.getBinaryOperatorAccess().getANDEnumLiteralDeclaration_6()); 
                        

                    }


                    }
                    break;
                case 8 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1556:6: (enumLiteral_7= ' or ' )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1556:6: (enumLiteral_7= ' or ' )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1556:8: enumLiteral_7= ' or '
                    {
                    enumLiteral_7=(Token)match(input,33,FollowSets000.FOLLOW_33_in_ruleBinaryOperator3471); 

                            current = grammarAccess.getBinaryOperatorAccess().getOREnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_7, grammarAccess.getBinaryOperatorAccess().getOREnumLiteralDeclaration_7()); 
                        

                    }


                    }
                    break;
                case 9 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1562:6: (enumLiteral_8= '>' )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1562:6: (enumLiteral_8= '>' )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1562:8: enumLiteral_8= '>'
                    {
                    enumLiteral_8=(Token)match(input,34,FollowSets000.FOLLOW_34_in_ruleBinaryOperator3488); 

                            current = grammarAccess.getBinaryOperatorAccess().getGREATERTHANEnumLiteralDeclaration_8().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_8, grammarAccess.getBinaryOperatorAccess().getGREATERTHANEnumLiteralDeclaration_8()); 
                        

                    }


                    }
                    break;
                case 10 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1568:6: (enumLiteral_9= '>=' )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1568:6: (enumLiteral_9= '>=' )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1568:8: enumLiteral_9= '>='
                    {
                    enumLiteral_9=(Token)match(input,35,FollowSets000.FOLLOW_35_in_ruleBinaryOperator3505); 

                            current = grammarAccess.getBinaryOperatorAccess().getGREATERTHANOREQUALEnumLiteralDeclaration_9().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_9, grammarAccess.getBinaryOperatorAccess().getGREATERTHANOREQUALEnumLiteralDeclaration_9()); 
                        

                    }


                    }
                    break;
                case 11 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1574:6: (enumLiteral_10= '<' )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1574:6: (enumLiteral_10= '<' )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1574:8: enumLiteral_10= '<'
                    {
                    enumLiteral_10=(Token)match(input,36,FollowSets000.FOLLOW_36_in_ruleBinaryOperator3522); 

                            current = grammarAccess.getBinaryOperatorAccess().getLESSTHANEnumLiteralDeclaration_10().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_10, grammarAccess.getBinaryOperatorAccess().getLESSTHANEnumLiteralDeclaration_10()); 
                        

                    }


                    }
                    break;
                case 12 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1580:6: (enumLiteral_11= '<=' )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1580:6: (enumLiteral_11= '<=' )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1580:8: enumLiteral_11= '<='
                    {
                    enumLiteral_11=(Token)match(input,37,FollowSets000.FOLLOW_37_in_ruleBinaryOperator3539); 

                            current = grammarAccess.getBinaryOperatorAccess().getLESSTHANOREQUALEnumLiteralDeclaration_11().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_11, grammarAccess.getBinaryOperatorAccess().getLESSTHANOREQUALEnumLiteralDeclaration_11()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinaryOperator"


    // $ANTLR start "ruleBooolean"
    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1590:1: ruleBooolean returns [Enumerator current=null] : ( (enumLiteral_0= 'true' ) | (enumLiteral_1= 'false' ) ) ;
    public final Enumerator ruleBooolean() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1592:28: ( ( (enumLiteral_0= 'true' ) | (enumLiteral_1= 'false' ) ) )
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1593:1: ( (enumLiteral_0= 'true' ) | (enumLiteral_1= 'false' ) )
            {
            // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1593:1: ( (enumLiteral_0= 'true' ) | (enumLiteral_1= 'false' ) )
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==38) ) {
                alt20=1;
            }
            else if ( (LA20_0==39) ) {
                alt20=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }
            switch (alt20) {
                case 1 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1593:2: (enumLiteral_0= 'true' )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1593:2: (enumLiteral_0= 'true' )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1593:4: enumLiteral_0= 'true'
                    {
                    enumLiteral_0=(Token)match(input,38,FollowSets000.FOLLOW_38_in_ruleBooolean3584); 

                            current = grammarAccess.getBoooleanAccess().getTrueEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getBoooleanAccess().getTrueEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1599:6: (enumLiteral_1= 'false' )
                    {
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1599:6: (enumLiteral_1= 'false' )
                    // ../configurator.DSL/src-gen/configurator/parser/antlr/internal/InternalDSL.g:1599:8: enumLiteral_1= 'false'
                    {
                    enumLiteral_1=(Token)match(input,39,FollowSets000.FOLLOW_39_in_ruleBooolean3601); 

                            current = grammarAccess.getBoooleanAccess().getFalseEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getBoooleanAccess().getFalseEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooolean"

    // Delegated rules


    protected DFA8 dfa8 = new DFA8(this);
    static final String DFA8_eotS =
        "\20\uffff";
    static final String DFA8_eofS =
        "\2\uffff\2\11\1\12\2\13\7\uffff\1\17\1\uffff";
    static final String DFA8_minS =
        "\1\4\1\uffff\5\16\1\4\4\uffff\2\22\1\16\1\uffff";
    static final String DFA8_maxS =
        "\1\47\1\uffff\5\45\1\5\4\uffff\2\22\1\45\1\uffff";
    static final String DFA8_acceptS =
        "\1\uffff\1\1\6\uffff\1\2\1\3\1\4\1\5\3\uffff\1\6";
    static final String DFA8_specialS =
        "\20\uffff}>";
    static final String[] DFA8_transitionS = {
            "\1\2\1\3\1\4\12\uffff\1\7\5\uffff\1\10\1\uffff\3\1\12\uffff"+
            "\1\5\1\6",
            "",
            "\1\11\11\uffff\1\11\1\uffff\14\10",
            "\1\11\11\uffff\1\11\1\uffff\14\10",
            "\1\12\11\uffff\1\12\1\uffff\14\10",
            "\1\13\11\uffff\1\13\1\uffff\14\10",
            "\1\13\11\uffff\1\13\1\uffff\14\10",
            "\1\14\1\15",
            "",
            "",
            "",
            "",
            "\1\16",
            "\1\16",
            "\1\17\11\uffff\1\17\1\uffff\14\10",
            ""
    };

    static final short[] DFA8_eot = DFA.unpackEncodedString(DFA8_eotS);
    static final short[] DFA8_eof = DFA.unpackEncodedString(DFA8_eofS);
    static final char[] DFA8_min = DFA.unpackEncodedStringToUnsignedChars(DFA8_minS);
    static final char[] DFA8_max = DFA.unpackEncodedStringToUnsignedChars(DFA8_maxS);
    static final short[] DFA8_accept = DFA.unpackEncodedString(DFA8_acceptS);
    static final short[] DFA8_special = DFA.unpackEncodedString(DFA8_specialS);
    static final short[][] DFA8_transition;

    static {
        int numStates = DFA8_transitionS.length;
        DFA8_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA8_transition[i] = DFA.unpackEncodedString(DFA8_transitionS[i]);
        }
    }

    class DFA8 extends DFA {

        public DFA8(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 8;
            this.eot = DFA8_eot;
            this.eof = DFA8_eof;
            this.min = DFA8_min;
            this.max = DFA8_max;
            this.accept = DFA8_accept;
            this.special = DFA8_special;
            this.transition = DFA8_transition;
        }
        public String getDescription() {
            return "431:1: (this_Unary_0= ruleUnary | this_Binary_1= ruleBinary | this_IdRef_2= ruleIdRef | this_IntConst_3= ruleIntConst | this_BoolConst_4= ruleBoolConst | this_StringConst_5= ruleStringConst )";
        }
    }
 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleConfiguration_in_entryRuleConfiguration75 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleConfiguration85 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleConfiguration140 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_11_in_ruleConfiguration152 = new BitSet(new long[]{0x000000000001D000L});
        public static final BitSet FOLLOW_12_in_ruleConfiguration165 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_11_in_ruleConfiguration177 = new BitSet(new long[]{0x0000000000300030L});
        public static final BitSet FOLLOW_ruleType_in_ruleConfiguration198 = new BitSet(new long[]{0x0000000000006000L});
        public static final BitSet FOLLOW_13_in_ruleConfiguration211 = new BitSet(new long[]{0x0000000000300030L});
        public static final BitSet FOLLOW_ruleType_in_ruleConfiguration232 = new BitSet(new long[]{0x0000000000006000L});
        public static final BitSet FOLLOW_14_in_ruleConfiguration246 = new BitSet(new long[]{0x000000000001C000L});
        public static final BitSet FOLLOW_15_in_ruleConfiguration261 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_11_in_ruleConfiguration273 = new BitSet(new long[]{0x0000000000300030L});
        public static final BitSet FOLLOW_ruleFeature_in_ruleConfiguration294 = new BitSet(new long[]{0x0000000000006000L});
        public static final BitSet FOLLOW_13_in_ruleConfiguration307 = new BitSet(new long[]{0x0000000000300030L});
        public static final BitSet FOLLOW_ruleFeature_in_ruleConfiguration328 = new BitSet(new long[]{0x0000000000006000L});
        public static final BitSet FOLLOW_14_in_ruleConfiguration342 = new BitSet(new long[]{0x0000000000014000L});
        public static final BitSet FOLLOW_16_in_ruleConfiguration357 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_11_in_ruleConfiguration369 = new BitSet(new long[]{0x0000000000300030L});
        public static final BitSet FOLLOW_ruleConstraint_in_ruleConfiguration390 = new BitSet(new long[]{0x0000000000006000L});
        public static final BitSet FOLLOW_13_in_ruleConfiguration403 = new BitSet(new long[]{0x0000000000300030L});
        public static final BitSet FOLLOW_ruleConstraint_in_ruleConfiguration424 = new BitSet(new long[]{0x0000000000006000L});
        public static final BitSet FOLLOW_14_in_ruleConfiguration438 = new BitSet(new long[]{0x0000000000004000L});
        public static final BitSet FOLLOW_14_in_ruleConfiguration452 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleConstraint_in_entryRuleConstraint488 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleConstraint498 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleConstraint544 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleConstraint556 = new BitSet(new long[]{0x0000000000300030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleConstraint577 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_18_in_ruleConstraint589 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_11_in_ruleConstraint601 = new BitSet(new long[]{0x000000C00EB20070L});
        public static final BitSet FOLLOW_ruleExpression_in_ruleConstraint622 = new BitSet(new long[]{0x0000000000004000L});
        public static final BitSet FOLLOW_14_in_ruleConstraint634 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleType_in_entryRuleType670 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleType680 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBOOLEAN_in_ruleType727 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleINTEGER_in_ruleType754 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleENUM_in_ruleType781 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleExpression_in_entryRuleExpression816 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleExpression826 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleUnary_in_ruleExpression873 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBinary_in_ruleExpression900 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIdRef_in_ruleExpression927 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntConst_in_ruleExpression954 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBoolConst_in_ruleExpression981 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringConst_in_ruleExpression1008 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_entryRuleEString1046 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEString1057 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleEString1097 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleEString1123 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeature_in_entryRuleFeature1168 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeature1178 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleFeature1224 = new BitSet(new long[]{0x0000000000300030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleFeature1247 = new BitSet(new long[]{0x0000000000020802L});
        public static final BitSet FOLLOW_17_in_ruleFeature1260 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_ruleEInt_in_ruleFeature1281 = new BitSet(new long[]{0x0000000000080000L});
        public static final BitSet FOLLOW_19_in_ruleFeature1293 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_ruleEInt_in_ruleFeature1314 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_18_in_ruleFeature1326 = new BitSet(new long[]{0x0000000000000802L});
        public static final BitSet FOLLOW_11_in_ruleFeature1341 = new BitSet(new long[]{0x0000000000300030L});
        public static final BitSet FOLLOW_ruleGroup_in_ruleFeature1362 = new BitSet(new long[]{0x0000000000006000L});
        public static final BitSet FOLLOW_13_in_ruleFeature1375 = new BitSet(new long[]{0x0000000000300030L});
        public static final BitSet FOLLOW_ruleGroup_in_ruleFeature1396 = new BitSet(new long[]{0x0000000000006000L});
        public static final BitSet FOLLOW_14_in_ruleFeature1410 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBOOLEAN_in_entryRuleBOOLEAN1448 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleBOOLEAN1458 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_20_in_ruleBOOLEAN1504 = new BitSet(new long[]{0x0000000000300030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleBOOLEAN1525 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleINTEGER_in_entryRuleINTEGER1561 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleINTEGER1571 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_21_in_ruleINTEGER1617 = new BitSet(new long[]{0x0000000000300030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleINTEGER1638 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleENUM_in_entryRuleENUM1674 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleENUM1684 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleENUM1739 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_11_in_ruleENUM1751 = new BitSet(new long[]{0x0000000000300030L});
        public static final BitSet FOLLOW_ruleEnumValue_in_ruleENUM1772 = new BitSet(new long[]{0x0000000000006000L});
        public static final BitSet FOLLOW_13_in_ruleENUM1785 = new BitSet(new long[]{0x0000000000300030L});
        public static final BitSet FOLLOW_ruleEnumValue_in_ruleENUM1806 = new BitSet(new long[]{0x0000000000006000L});
        public static final BitSet FOLLOW_14_in_ruleENUM1820 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEnumValue_in_entryRuleEnumValue1856 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEnumValue1866 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleEnumValue1921 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleGroup_in_entryRuleGroup1957 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleGroup1967 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleGroup2013 = new BitSet(new long[]{0x0000000000020800L});
        public static final BitSet FOLLOW_17_in_ruleGroup2026 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_ruleEInt_in_ruleGroup2047 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_22_in_ruleGroup2059 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_ruleEInt_in_ruleGroup2080 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_18_in_ruleGroup2092 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_11_in_ruleGroup2106 = new BitSet(new long[]{0x0000000000300030L});
        public static final BitSet FOLLOW_ruleFeature_in_ruleGroup2127 = new BitSet(new long[]{0x0000000000006000L});
        public static final BitSet FOLLOW_13_in_ruleGroup2140 = new BitSet(new long[]{0x0000000000300030L});
        public static final BitSet FOLLOW_ruleFeature_in_ruleGroup2161 = new BitSet(new long[]{0x0000000000006000L});
        public static final BitSet FOLLOW_14_in_ruleGroup2175 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEInt_in_entryRuleEInt2212 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEInt2223 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_ruleEInt2262 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleUnary_in_entryRuleUnary2306 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleUnary2316 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleUnaryOperator_in_ruleUnary2362 = new BitSet(new long[]{0x000000C000B20070L});
        public static final BitSet FOLLOW_ruleAtomicExpression_in_ruleUnary2383 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBinary_in_entryRuleBinary2419 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleBinary2429 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAtomicExpression_in_ruleBinary2475 = new BitSet(new long[]{0x0000003FFC000000L});
        public static final BitSet FOLLOW_ruleBinaryOperator_in_ruleBinary2496 = new BitSet(new long[]{0x000000C000B20070L});
        public static final BitSet FOLLOW_ruleAtomicExpression_in_ruleBinary2517 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAtomicExpression_in_entryRuleAtomicExpression2553 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAtomicExpression2563 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIdRef_in_ruleAtomicExpression2610 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleConstants_in_ruleAtomicExpression2637 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_23_in_ruleAtomicExpression2655 = new BitSet(new long[]{0x000000C00EB20070L});
        public static final BitSet FOLLOW_ruleExpression_in_ruleAtomicExpression2677 = new BitSet(new long[]{0x0000000001000000L});
        public static final BitSet FOLLOW_24_in_ruleAtomicExpression2688 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleConstants_in_entryRuleConstants2725 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleConstants2735 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntConst_in_ruleConstants2782 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBoolConst_in_ruleConstants2809 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringConst_in_ruleConstants2836 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntConst_in_entryRuleIntConst2871 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleIntConst2881 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEInt_in_ruleIntConst2926 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringConst_in_entryRuleStringConst2961 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleStringConst2971 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_17_in_ruleStringConst3008 = new BitSet(new long[]{0x0000000000300030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleStringConst3029 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_18_in_ruleStringConst3041 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBoolConst_in_entryRuleBoolConst3077 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleBoolConst3087 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooolean_in_ruleBoolConst3132 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIdRef_in_entryRuleIdRef3167 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleIdRef3177 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleIdRef3224 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_25_in_ruleUnaryOperator3273 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_26_in_ruleUnaryOperator3290 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_27_in_ruleUnaryOperator3307 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_27_in_ruleBinaryOperator3352 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_26_in_ruleBinaryOperator3369 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_28_in_ruleBinaryOperator3386 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_29_in_ruleBinaryOperator3403 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_30_in_ruleBinaryOperator3420 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_31_in_ruleBinaryOperator3437 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_32_in_ruleBinaryOperator3454 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_33_in_ruleBinaryOperator3471 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_34_in_ruleBinaryOperator3488 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_35_in_ruleBinaryOperator3505 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_36_in_ruleBinaryOperator3522 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_37_in_ruleBinaryOperator3539 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_38_in_ruleBooolean3584 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_39_in_ruleBooolean3601 = new BitSet(new long[]{0x0000000000000002L});
    }


}