package configurator.validation

import configurator.Binary
import configurator.BinaryOperator
import configurator.BoolConst
import configurator.ConfiguratorPackage
import configurator.Constraint
import configurator.EnumValue
import configurator.Expression
import configurator.Feature
import configurator.Group
import configurator.IdRef
import configurator.IntConst
import configurator.StringConst
import configurator.Unary
import configurator.UnaryOperator
import org.eclipse.xtext.validation.Check
import configurator.BOOLEAN
import configurator.INTEGER
import configurator.Type
import configurator.Configuration
import org.eclipse.emf.ecore.EObject
import java.util.ArrayList
import configurator.ENUM

class DSLValidator extends AbstractDSLValidator {

	public static val INVALID_OPERATOR = "invalid operator"
	public static val MAX_LIMIT = 1000
	
	public static val BinaryOperator[] booleanBinaryOperators = #{
					BinaryOperator.EQUAL,
					BinaryOperator.DIFFERENT,
					BinaryOperator.GREATERTHAN,
					BinaryOperator.GREATERTHANOREQUAL,
					BinaryOperator.LESSTHAN,
					BinaryOperator.LESSTHANOREQUAL,
					BinaryOperator.AND,
					BinaryOperator.OR
				};
	
	public static val BinaryOperator[] integerBinaryOperators = #{
					BinaryOperator.ADD,
					BinaryOperator.MINUS,
					BinaryOperator.DIVIDE,
					BinaryOperator.MULTIPLY
				};
	
	
	//Utility methods
	def String getInterfaceName(Expression expression) {
		if (expression instanceof StringConst)
			return "StringConst"
		if (expression instanceof IntConst)
			return "IntConst"
		if (expression instanceof INTEGER)
			return "INTEGER" 
		if (expression instanceof BoolConst)
			return "BoolConst"
		if (expression instanceof BOOLEAN)
			return "BOOLEAN"
		if (expression instanceof IdRef) {
			var idRef = expression as IdRef
			if (idRef.identifier instanceof Feature)
				return "Feature"
			if (idRef.identifier instanceof EnumValue)
				return "EnumValue"
		}
		if (expression instanceof Binary) 
			return "Binary"
		
		if (expression instanceof Unary)
			return "Unary"

	}

	def getReturnTypeName(Expression expression) {
		switch (expression.interfaceName) {
			case "StringConst":
				return "String"
			case "IntConst":
				return "Integer"
			case "BoolConst":
				return "Boolean"
			case "Feature": {
				if (expression instanceof IdRef){
				var t =((expression as IdRef).identifier as Feature).type
				if(t instanceof BOOLEAN)
					return "Boolean"
				if(t instanceof INTEGER)
					return "Integer"
				if(t instanceof ENUM)
					return "EnumType"
				}
			}			
			case "EnumValue":
				return "EnumType"
			
			case "Binary": {
				var binary = expression as Binary
				if (binary.matchingExpressionTypes){
					if (booleanBinaryOperators.contains(binary.operator)){
						return "Boolean"
					} 
					else {
						return "Integer"
					}
				}
			}
			case "Unary":{
				var unary = expression as Unary
				return unary.innerExpression.returnTypeName
			}
			default:
				return "Boolean"
			}	
				
		
	}
	
		

	def matchingExpressionTypes(Binary binary) {
		if (binary.leftExpression == null || binary.rightExpression == null)
			return false
		binary.leftExpression.returnTypeName == binary.rightExpression.returnTypeName
	}

	//Validation methods
	//Validation for Types
	@Check
	def noDuplicateNamesAllowedForType(Configuration config) {
		val EObject[] allNames = config.eContents
		val allNamesType = new ArrayList<String>();
		val allNamesFeature = new ArrayList<String>();
		for (EObject e : allNames) 
		{
			if (e instanceof Type) 
			{
				var strType = (e as Type).name
				if (allNamesType.contains(strType)) 
				{
					var msg = "Duplicate names in types are not allowed!\nThe type " + strType + " already exists!"
					error(msg, ConfiguratorPackage.Literals.NAMED_ELEMENT__NAME)
				} else
				{
					allNamesType.add(strType.toString)
					//println("Adding typename: " + strType)	
				}
			}
			if (e instanceof Feature) 
			{
				allNamesFeature.addAll(addSubFeatureNames(allNamesFeature, e))
				var strFeature = (e as Feature).name
				if (allNamesFeature.contains(strFeature)) 
				{
					var msg = "Duplicate names in feature are not allowed!\nThe feature " + strFeature + " already exists!"
					error(msg, ConfiguratorPackage.Literals.NAMED_ELEMENT__NAME)
				} 
				else
				{
					allNamesFeature.add(strFeature.toString)
					//println("Adding featurename: " + strFeature)	
				}
			}
		}
	}
	
	//recursive helper function which add feature names to the array of feature names.
	def ArrayList<String> addSubFeatureNames(ArrayList<String> allNamesSubFeature, Feature feature)
	{
		var Group[] allGroups = feature.groups
		for (Group g : allGroups)
		{
			var Feature[] allFeaturesInGroup = g.features
			for (Feature f : allFeaturesInGroup)
			{
				if (allNamesSubFeature.contains(f.name))
				{
					error("Duplicate name on subfeature " + f.name, ConfiguratorPackage.Literals.NAMED_ELEMENT__NAME)
				}
				else
				{
					allNamesSubFeature.add(f.name)
				}
				allNamesSubFeature.addAll(addSubFeatureNames(allNamesSubFeature, f))
			}
		}
		return allNamesSubFeature
	}
	
	@Check
	def checkGroupHasEnoughFeatures(Group group)
	{
		//Validation that there is enough features to match the number specified by min
		val EObject[] allSubs = group.features
		var iCount = 0
		for (EObject o : allSubs)
		{
			if (o instanceof Feature)
				iCount++
		}
		//println("The group contains " + allSubs.size + " size. Or when counted: " + iCount)
		if (group.min > 0)
			if (group.min > iCount)
				error("You will need to create enough features to satisfy you group min constraint!", ConfiguratorPackage.Literals.NAMED_ELEMENT__NAME)
		if (group.max > 0)
			if (group.max > iCount)
				error("You can never reach the maximum allowed number of features in this group!", ConfiguratorPackage.Literals.NAMED_ELEMENT__NAME)		
	}

	//Validation for Features
	@Check
	def checkMinNotGreaterThanMax(Feature feature) {

		//println("Inside the check on feature")
		var startMsg = "Cardinalities on feature: "
		if (feature.min != -1 && feature.max != -1) {
			if (feature.min > feature.max)
				error(startMsg + "max must be greater or equal to min.", ConfiguratorPackage.Literals.FEATURE__MAX)
			if (feature.min < 0)
				error(startMsg + "min cannot be negative.", ConfiguratorPackage.Literals.FEATURE__MIN)
			if (feature.max < 0)
				error(startMsg + "max cannot be negative.", ConfiguratorPackage.Literals.FEATURE__MAX)
		}
	}

	@Check
	def checkMinNotGreaterThanMax(Group group) {

		//println("Inside the check on group")
		var startMsg = "Cardinalities on group: "
		if (group.min != -1 && group.max != -1) {
			if (group.min > group.max)
				error(startMsg + "max must be greater or equal to min.", ConfiguratorPackage.Literals.GROUP__MAX)
			if (group.min < 0)
				error(startMsg + "min cannot be negative.", ConfiguratorPackage.Literals.GROUP__MIN)
			if (group.max < 0)
				error(startMsg + "max cannot be negative.", ConfiguratorPackage.Literals.GROUP__MAX)
		}
	}

	//Validation for expressions
	@Check
	def checkTopExpressionBool(Expression expression) {
		var startMsg = "Constraint top expression: "
		if (expression.eContainer instanceof Constraint) {
			if (expression instanceof Unary) {
				if (expression.operator != UnaryOperator.NOT)
					error(startMsg + "Only unary operator 'not' can resolve to a bool",
						ConfiguratorPackage.Literals.UNARY__INNER_EXPRESSION)
			}
			if (expression instanceof Binary) {
				val BinaryOperator[] validOperators = #{
					BinaryOperator.EQUAL,
					BinaryOperator.DIFFERENT,
					BinaryOperator.GREATERTHAN,
					BinaryOperator.GREATERTHANOREQUAL,
					BinaryOperator.LESSTHAN,
					BinaryOperator.LESSTHANOREQUAL,
					BinaryOperator.AND,
					BinaryOperator.OR
				};
				//println(expression.operator)
				if (!validOperators.contains(expression.operator))
					error(startMsg + "Not valid binary operator", ConfiguratorPackage.Literals.BINARY__LEFT_EXPRESSION)
			}
			if (expression instanceof IdRef)
				error(startMsg + "Does not evaluate to bool", ConfiguratorPackage.Literals.ID_REF__IDENTIFIER)
			if (expression instanceof IntConst)
				error(startMsg + "Does not evaluate to bool", ConfiguratorPackage.Literals.INT_CONST__VALUE)
			if (expression instanceof StringConst)
				error(startMsg + "Does not evaluate to bool", ConfiguratorPackage.Literals.STRING_CONST__VALUE)
			if (expression instanceof BoolConst)
				info("Boolean selected", ConfiguratorPackage.Literals.BOOL_CONST__VALUE) // do nothing
		}
	}

	@Check
	def checkBinaryExpression(Binary binary) {
		var startMsg = "Binary Expression: "

		//Matching expression types
		if (!binary.matchingExpressionTypes)
			error(
				startMsg + "The types of both left and right expression should be the same " + "[" +
					binary.leftExpression.returnTypeName + ", " + binary.rightExpression.returnTypeName + "]",
				ConfiguratorPackage.Literals.BINARY__OPERATOR)

		//ADD operator
		if (binary.leftExpression instanceof IdRef && (binary.leftExpression as IdRef).identifier instanceof EnumValue &&
			binary.operator == BinaryOperator.ADD)
			error(startMsg + "Cannot use the '+' operator on enum values", ConfiguratorPackage.Literals.BINARY__OPERATOR)

		if (binary.leftExpression instanceof BoolConst && binary.operator == BinaryOperator.ADD)
			error(startMsg + "Cannot use the '+' operator on boolean values",
				ConfiguratorPackage.Literals.BINARY__OPERATOR)

		//MINUS operator
		if (binary.leftExpression instanceof StringConst && binary.operator == BinaryOperator.MINUS)
			error(startMsg + "Cannot use the '-' operator on strings", ConfiguratorPackage.Literals.BINARY__OPERATOR)
		if (binary.leftExpression instanceof IdRef && (binary.leftExpression as IdRef).identifier instanceof EnumValue &&
			binary.operator == BinaryOperator.MINUS)
			error(startMsg + "Cannot use the '-' operator on enum values", ConfiguratorPackage.Literals.BINARY__OPERATOR)

		if (binary.leftExpression instanceof BoolConst && binary.operator == BinaryOperator.MINUS)
			error(startMsg + "Cannot use the '-' operator on boolean values",
				ConfiguratorPackage.Literals.BINARY__OPERATOR)

		//MULTIPLY operator
		if (binary.leftExpression instanceof StringConst && binary.operator == BinaryOperator.MULTIPLY)
			error(startMsg + "Cannot use the '*' operator on strings", ConfiguratorPackage.Literals.BINARY__OPERATOR)
		if (binary.leftExpression instanceof IdRef && (binary.leftExpression as IdRef).identifier instanceof EnumValue &&
			binary.operator == BinaryOperator.MULTIPLY)
			error(startMsg + "Cannot use the '*' operator on enum values", ConfiguratorPackage.Literals.BINARY__OPERATOR)

		if (binary.leftExpression instanceof BoolConst && binary.operator == BinaryOperator.MULTIPLY)
			error(startMsg + "Cannot use the '*' operator on boolean values",
				ConfiguratorPackage.Literals.BINARY__OPERATOR)

		//DIVIDE operator
		if (binary.leftExpression instanceof StringConst && binary.operator == BinaryOperator.DIVIDE)
			error(startMsg + "Cannot use the '/' operator on strings", ConfiguratorPackage.Literals.BINARY__OPERATOR)
		if (binary.leftExpression instanceof IdRef && (binary.leftExpression as IdRef).identifier instanceof EnumValue &&
			binary.operator == BinaryOperator.DIVIDE)
			error(startMsg + "Cannot use the '/' operator on enum values", ConfiguratorPackage.Literals.BINARY__OPERATOR)

		if (binary.leftExpression instanceof BoolConst && binary.operator == BinaryOperator.DIVIDE)
			error(startMsg + "Cannot use the '/' operator on boolean values",
				ConfiguratorPackage.Literals.BINARY__OPERATOR)

		//EQUAL - empty
		//DIFFERENT - empty
		//AND
		if (binary.leftExpression instanceof StringConst && binary.operator == BinaryOperator.AND)
			error(startMsg + "Cannot use the 'and' operator on strings", ConfiguratorPackage.Literals.BINARY__OPERATOR)
		if (binary.leftExpression instanceof IntConst && binary.operator == BinaryOperator.AND)
			error(startMsg + "Cannot use the 'and' operator on integers", ConfiguratorPackage.Literals.BINARY__OPERATOR)

		if (binary.leftExpression instanceof IdRef && (binary.leftExpression as IdRef).identifier instanceof EnumValue &&
			binary.operator == BinaryOperator.AND)
			error(startMsg + "Cannot use the 'and' operator on enum values",
				ConfiguratorPackage.Literals.BINARY__OPERATOR)

		//OR
		if (binary.leftExpression instanceof StringConst && binary.operator == BinaryOperator.OR)
			error(startMsg + "Cannot use the 'or' operator on strings", ConfiguratorPackage.Literals.BINARY__OPERATOR)
		if (binary.leftExpression instanceof IntConst && binary.operator == BinaryOperator.OR)
			error(startMsg + "Cannot use the 'or' operator on integers", ConfiguratorPackage.Literals.BINARY__OPERATOR)

		if (binary.leftExpression instanceof IdRef && (binary.leftExpression as IdRef).identifier instanceof EnumValue &&
			binary.operator == BinaryOperator.OR)
			error(startMsg + "Cannot use the 'or' operator on enum values",
				ConfiguratorPackage.Literals.BINARY__OPERATOR)

		//GREATERTHAN
		if (binary.leftExpression instanceof StringConst && binary.operator == BinaryOperator.GREATERTHAN)
			error(startMsg + "Cannot use the '>' operator on strings", ConfiguratorPackage.Literals.BINARY__OPERATOR)
		if (binary.leftExpression instanceof IdRef && (binary.leftExpression as IdRef).identifier instanceof EnumValue &&
			binary.operator == BinaryOperator.GREATERTHAN)
			error(startMsg + "Cannot use the '>' operator on enum values", ConfiguratorPackage.Literals.BINARY__OPERATOR)

		if (binary.leftExpression instanceof BoolConst && binary.operator == BinaryOperator.GREATERTHAN)
			error(startMsg + "Cannot use the '>' operator on boolean values",
				ConfiguratorPackage.Literals.BINARY__OPERATOR)

		//GREATERTHANOREQUAL
		if (binary.leftExpression instanceof StringConst && binary.operator == BinaryOperator.GREATERTHANOREQUAL)
			error(startMsg + "Cannot use the '>=' operator on strings", ConfiguratorPackage.Literals.BINARY__OPERATOR)
		if (binary.leftExpression instanceof IdRef && (binary.leftExpression as IdRef).identifier instanceof EnumValue &&
			binary.operator == BinaryOperator.GREATERTHANOREQUAL)
			error(startMsg + "Cannot use the '>=' operator on enum values",
				ConfiguratorPackage.Literals.BINARY__OPERATOR)

		if (binary.leftExpression instanceof BoolConst && binary.operator == BinaryOperator.GREATERTHANOREQUAL)
			error(startMsg + "Cannot use the '>=' operator on boolean values",
				ConfiguratorPackage.Literals.BINARY__OPERATOR)

		//LESSTHAN
		if (binary.leftExpression instanceof StringConst && binary.operator == BinaryOperator.LESSTHAN)
			error(startMsg + "Cannot use the '<' operator on strings", ConfiguratorPackage.Literals.BINARY__OPERATOR)
		if (binary.leftExpression instanceof IdRef && (binary.leftExpression as IdRef).identifier instanceof EnumValue &&
			binary.operator == BinaryOperator.LESSTHAN)
			error(startMsg + "Cannot use the '<' operator on enum values", ConfiguratorPackage.Literals.BINARY__OPERATOR)

		if (binary.leftExpression instanceof BoolConst && binary.operator == BinaryOperator.LESSTHAN)
			error(startMsg + "Cannot use the '<' operator on boolean values",
				ConfiguratorPackage.Literals.BINARY__OPERATOR)

		//LESSTHANOREQUAL
		if (binary.leftExpression instanceof StringConst && binary.operator == BinaryOperator.LESSTHANOREQUAL)
			error(startMsg + "Cannot use the '<=' operator on strings", ConfiguratorPackage.Literals.BINARY__OPERATOR)
		if (binary.leftExpression instanceof IdRef && (binary.leftExpression as IdRef).identifier instanceof EnumValue &&
			binary.operator == BinaryOperator.LESSTHANOREQUAL)
			error(startMsg + "Cannot use the '<=' operator on enum values",
				ConfiguratorPackage.Literals.BINARY__OPERATOR)

		if (binary.leftExpression instanceof BoolConst && binary.operator == BinaryOperator.LESSTHANOREQUAL)
			error(startMsg + "Cannot use the '<=' operator on boolean values",
				ConfiguratorPackage.Literals.BINARY__OPERATOR)
	}

	@Check
	def checkUnaryExpression(Unary unary) {
		var startMsg = "Unary Expression: "
		if (unary.innerExpression instanceof StringConst && unary.operator != null)
			error(startMsg + "StringConst can not have an operator in front of it",
				ConfiguratorPackage.Literals.UNARY__OPERATOR, INVALID_OPERATOR)
		if (unary.innerExpression instanceof IntConst && unary.operator == UnaryOperator.NOT)
			error(startMsg + "IntConst can not have '!' in front of it", ConfiguratorPackage.Literals.UNARY__OPERATOR,
				INVALID_OPERATOR)
		if (unary.innerExpression instanceof BoolConst &&
			(unary.operator == UnaryOperator.POSITIVE || unary.operator == UnaryOperator.NEGATIVE))
			error(startMsg + "BoolConst can not have '+' or '-' in front of it",
				ConfiguratorPackage.Literals.UNARY__OPERATOR, INVALID_OPERATOR)
	}

//Old methods
/*
def adjustFeatureCardinalities(Feature feature) {
//Non-set limits
if (feature.min == -1)
feature.min = 0
if (feature.max == -1) {
if (feature.eContainer instanceof Group) {
var group = feature.eContainer as Group
var otherFeatures = group.features.filter[!identityEquals(feature)]
var otherFeaturesWithMaxLimits = otherFeatures.filter[max != -1]
var presetMaxLimit = 0
for (i: 0..otherFeaturesWithMaxLimits.size)
presetMaxLimit += otherFeaturesWithMaxLimits.get(i).max
var otherFeaturesWithNoMaxLimits = otherFeatures.filter[max > -1]
var nFeaturesWithNoMaxLimits = otherFeaturesWithNoMaxLimits.size
var deltaMax = group.max - presetMaxLimit
var deltaMaxForEach = deltaMax / nFeaturesWithNoMaxLimits
feature.max = deltaMaxForEach
}
else {
feature.max = MAX_LIMIT
}
}
return feature
}
def adjustGroupCardinalities(Group group) {
//Non-set limits
if (group.min == -1)
group.min = 0
if (group.max == -1)
group.max = MAX_LIMIT
return group
}
@Check
def checkFeatureCardinality(Feature feature) {
var startMsg = "Cardinality error: "
//Pre-adjust, if non-set limits
var feature2 = feature.adjustFeatureCardinalities
//Grouped feature?
if (feature2.eContainer instanceof Group) {
var group = feature2.eContainer as Group
//Single-contained feature.min > containing group.max
if (feature2.min > group.max) {
error(startMsg + "The feature minimum limit is higher than the maximum limit of the containing group",
ConfiguratorPackage.Literals.GROUP__FEATURES)
}
}
}
@Check
def checkGroupCardinality(Group group) {
var startMsg = "Cardinality error: "
//Pre-adjust, if non-set limits
var group2 = group.adjustGroupCardinalities
//Group min/max < 0
if (group2.min < 0 || group.max < 0)
error(startMsg + "Group limits cannot be negative", ConfiguratorPackage.Literals.GROUP__MIN)
//Group min vs max constraint
if (group2.min > group2.max)
error(startMsg + "The group min (" + group2.min + ") cannot be greater than the group max (" + group2.max + ")", 
ConfiguratorPackage.Literals.GROUP__MAX)
//SUM(Feature.min) > group.max
var featuresMin = 0
for (i : 0 .. group2.features.size - 1)
featuresMin += group2.features.get(i).min
if (featuresMin > group2.max)
error(startMsg + "The sum of minimum limits in the grouped features is larger than the maximum limit allowed in the group", 
ConfiguratorPackage.Literals.GROUP__MAX)
//SUM(Feature.max) < group.min
var featuresMax = 0
for (i : 0 .. group2.features.size - 1)
featuresMax += group2.features.get(i).max
if (featuresMax < group2.min)
error(startMsg + "The sum of maximum limits in the grouped features is smaller than the minimum limit allowed in the group", 
ConfiguratorPackage.Literals.GROUP__MIN)
}
*/
}
