package configurator.validation

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import configurator.ConfiguratorPackage

class Main {
	static val filenames = #[
		"filename1.xmi",
		"filename2.xmi"]
		
	//static val filename = "test-files/test-00.xmi"
	static val fileExtension = "xmi"

	/*
	 * This method is based on the from Andrzej's repo' on BitBucket
	 */
	def static void main(String[] args) {

		// register the file extension to be read as XMI
		Resource.Factory.Registry::INSTANCE.extensionToFactoryMap.put(fileExtension, new XMIResourceFactoryImpl)

		// register our meta-model package
		ConfiguratorPackage.eINSTANCE.eClass()

		// load the file 
		val resourceSet = new ResourceSetImpl

		// change file name here to try other files
		for(String filename : filenames){
			val uri = URI::createURI(filename)
			val resource = resourceSet.getResource(uri, true)
	
			// check constraints
			if (EcoreUtil.getAllProperContents(resource, false).forall [Constraints.constraint(it)])
				println("All constraints are satisfied!")
			else
				println("Some constraint is violated")
		}
	}
}