package configurator.generator

import configurator.BOOLEAN
import configurator.Binary
import configurator.BoolConst
import configurator.Configuration
import configurator.Constraint
import configurator.ENUM
import configurator.Expression
import configurator.Feature
import configurator.Group
import configurator.INTEGER
import configurator.IdRef
import configurator.IntConst
import configurator.StringConst
import configurator.Unary
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator

class DSLGenerator implements IGenerator {

	def static toJSON(Configuration it) {
'''
{
  "Name": "«it.name»",
  "Types": [
    «FOR type : types SEPARATOR ","»
    {
      "Name": "«type.name»",
      "Type": «IF (type instanceof BOOLEAN)»"Boolean"«ELSEIF (type instanceof INTEGER)»"Integer"«ELSEIF (type instanceof ENUM)»"Enum",
      "EnumValues": [
        «FOR e : (type as ENUM).enumValues SEPARATOR ","»
        "«e.name»"
        «ENDFOR»
      ]«ENDIF»
    }
    «ENDFOR»
  ],
  "Features": [
    «FOR feature : rootFeatures SEPARATOR ","»«generateFeature(feature)»«ENDFOR»
  ],
  "Constraints": [
    «FOR constraint : constraints SEPARATOR ","»«generateConstraint(constraint)»«ENDFOR»
  ]
}
'''
	}
	
	def static generateFeature(Feature it) {
'''
{
  "Name": "«it.name»",
  «IF(it.type != null)»"Type": "«it.type.name»", «ENDIF»
  "Min": "«min»",
  "Max": "«max»"«IF (it.groups != null && 0 < it.groups.size())»,
  "FeatureGroups": [
    «FOR group : it.groups SEPARATOR ","» «generateGroup(group)»«ENDFOR»
  ]«ENDIF»
}
'''
	}
	
	def static generateGroup(Group it) {
'''
{
  "Name": "«name»",
  "Min": "«min»",
  "Max": "«max»",
  "Features": [
    «FOR f : features SEPARATOR ","»«generateFeature(f)»«ENDFOR»
  ]
}
'''
	}
	
	def static generateConstraint(Constraint it) {
'''
{
  "Name": "«name»",
  "Msg": "«errorMessage»",
  "Expression": «generateExpression(expression)»
}
'''
	}
	
	def static generateExpression(Expression expr) {
'''
  «IF(expr instanceof Unary)»
  «generateExpression(expr.innerExpression)»
  «ELSEIF(expr instanceof Binary)»
  «generateBinaryExpr(expr as Binary)»
  «ELSEIF(expr instanceof StringConst)»
  «generateStringConst(expr as StringConst)»
  «ELSEIF(expr instanceof IntConst)»
  «generateIntConst(expr as IntConst)»
  «ELSEIF(expr instanceof BoolConst)»
  «generateBoolConst(expr as BoolConst)»
  «ELSEIF(expr instanceof IdRef)»
  «generateIdRef(expr as IdRef)»
  «ENDIF»
'''
	}
	
	def static generateUnaryExpr(Unary it) {
'''
{
  "Type": "Unary",
  "Operator": "«operator»",
  "Expression": «generateExpression(innerExpression)»
}
'''
	}
	
def static generateBinaryExpr(Binary it) {
'''
{
  "Type": "Binary",
  "Operator": "«operator»",
  "Left": «generateExpression(leftExpression)»,
  "Right": «generateExpression(rightExpression)»
}
'''
}
	
	def static generateStringConst(StringConst it) {
'''
{
  "Type": "StringConst",
  "Value": "«value»"
}
'''
	}
	
	def static generateIntConst(IntConst it) {
'''
{
  "Type": "IntConst",
  "Value": "«value»"
}
'''
	}
	
	def static generateBoolConst(BoolConst it) {
'''
{
  "Type": "BoolConst",
  "Value": "«value»"
}
'''
	}
	
	def static generateIdRef(IdRef it) {
'''
{
  "Type": "IdRef",
  "Ref": "«identifier.name»"
}
'''
	}
	
	override void doGenerate(Resource resource, IFileSystemAccess fsa) {
		resource.allContents.toIterable.filter(typeof(Configuration)). // FiniteStateMachine.class
			forEach [ Configuration it |
				val fname = it.name.toFirstUpper
				fsa.generateFile("configurations/" + fname + ".json", it.toJSON)
			]
	}
}
