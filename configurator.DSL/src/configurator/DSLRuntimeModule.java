/*
 * generated by Xtext
 */
package configurator;

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
public class DSLRuntimeModule extends configurator.AbstractDSLRuntimeModule {

}
