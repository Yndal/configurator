package configurator.generator;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import configurator.BOOLEAN;
import configurator.Binary;
import configurator.BinaryOperator;
import configurator.BoolConst;
import configurator.Booolean;
import configurator.Configuration;
import configurator.Constraint;
import configurator.ENUM;
import configurator.EnumValue;
import configurator.Expression;
import configurator.Feature;
import configurator.Group;
import configurator.INTEGER;
import configurator.IdRef;
import configurator.Identifiable;
import configurator.IntConst;
import configurator.StringConst;
import configurator.Type;
import configurator.Unary;
import configurator.UnaryOperator;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class DSLGenerator implements IGenerator {
  public static CharSequence toJSON(final Configuration it) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("{");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("\"Name\": \"");
    String _name = it.getName();
    _builder.append(_name, "  ");
    _builder.append("\",");
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    _builder.append("\"Types\": [");
    _builder.newLine();
    {
      EList<Type> _types = it.getTypes();
      boolean _hasElements = false;
      for(final Type type : _types) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(",", "    ");
        }
        _builder.append("    ");
        _builder.append("{");
        _builder.newLine();
        _builder.append("    ");
        _builder.append("  ");
        _builder.append("\"Name\": \"");
        String _name_1 = type.getName();
        _builder.append(_name_1, "      ");
        _builder.append("\",");
        _builder.newLineIfNotEmpty();
        _builder.append("    ");
        _builder.append("  ");
        _builder.append("\"Type\": ");
        {
          if ((type instanceof BOOLEAN)) {
            _builder.append("\"Boolean\"");
          } else {
            if ((type instanceof INTEGER)) {
              _builder.append("\"Integer\"");
            } else {
              if ((type instanceof ENUM)) {
                _builder.append("\"Enum\",");
                _builder.newLineIfNotEmpty();
                _builder.append("    ");
                _builder.append("  ");
                _builder.append("\"EnumValues\": [");
                _builder.newLine();
                {
                  EList<EnumValue> _enumValues = ((ENUM) type).getEnumValues();
                  boolean _hasElements_1 = false;
                  for(final EnumValue e : _enumValues) {
                    if (!_hasElements_1) {
                      _hasElements_1 = true;
                    } else {
                      _builder.appendImmediate(",", "        ");
                    }
                    _builder.append("    ");
                    _builder.append("  ");
                    _builder.append("  ");
                    _builder.append("\"");
                    String _name_2 = e.getName();
                    _builder.append(_name_2, "        ");
                    _builder.append("\"");
                    _builder.newLineIfNotEmpty();
                  }
                }
                _builder.append("    ");
                _builder.append("  ");
                _builder.append("]");
              }
            }
          }
        }
        _builder.newLineIfNotEmpty();
        _builder.append("    ");
        _builder.append("}");
        _builder.newLine();
      }
    }
    _builder.append("  ");
    _builder.append("],");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("\"Features\": [");
    _builder.newLine();
    _builder.append("    ");
    {
      EList<Feature> _rootFeatures = it.getRootFeatures();
      boolean _hasElements_2 = false;
      for(final Feature feature : _rootFeatures) {
        if (!_hasElements_2) {
          _hasElements_2 = true;
        } else {
          _builder.appendImmediate(",", "    ");
        }
        CharSequence _generateFeature = DSLGenerator.generateFeature(feature);
        _builder.append(_generateFeature, "    ");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    _builder.append("],");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("\"Constraints\": [");
    _builder.newLine();
    _builder.append("    ");
    {
      EList<Constraint> _constraints = it.getConstraints();
      boolean _hasElements_3 = false;
      for(final Constraint constraint : _constraints) {
        if (!_hasElements_3) {
          _hasElements_3 = true;
        } else {
          _builder.appendImmediate(",", "    ");
        }
        CharSequence _generateConstraint = DSLGenerator.generateConstraint(constraint);
        _builder.append(_generateConstraint, "    ");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    _builder.append("]");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public static CharSequence generateFeature(final Feature it) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("{");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("\"Name\": \"");
    String _name = it.getName();
    _builder.append(_name, "  ");
    _builder.append("\",");
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    {
      Type _type = it.getType();
      boolean _notEquals = (!Objects.equal(_type, null));
      if (_notEquals) {
        _builder.append("\"Type\": \"");
        Type _type_1 = it.getType();
        String _name_1 = _type_1.getName();
        _builder.append(_name_1, "  ");
        _builder.append("\", ");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    _builder.append("\"Min\": \"");
    int _min = it.getMin();
    _builder.append(_min, "  ");
    _builder.append("\",");
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    _builder.append("\"Max\": \"");
    int _max = it.getMax();
    _builder.append(_max, "  ");
    _builder.append("\"");
    {
      boolean _and = false;
      EList<Group> _groups = it.getGroups();
      boolean _notEquals_1 = (!Objects.equal(_groups, null));
      if (!_notEquals_1) {
        _and = false;
      } else {
        EList<Group> _groups_1 = it.getGroups();
        int _size = _groups_1.size();
        boolean _lessThan = (0 < _size);
        _and = _lessThan;
      }
      if (_and) {
        _builder.append(",");
        _builder.newLineIfNotEmpty();
        _builder.append("  ");
        _builder.append("\"FeatureGroups\": [");
        _builder.newLine();
        _builder.append("  ");
        _builder.append("  ");
        {
          EList<Group> _groups_2 = it.getGroups();
          boolean _hasElements = false;
          for(final Group group : _groups_2) {
            if (!_hasElements) {
              _hasElements = true;
            } else {
              _builder.appendImmediate(",", "    ");
            }
            _builder.append(" ");
            CharSequence _generateGroup = DSLGenerator.generateGroup(group);
            _builder.append(_generateGroup, "    ");
          }
        }
        _builder.newLineIfNotEmpty();
        _builder.append("  ");
        _builder.append("]");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public static CharSequence generateGroup(final Group it) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("{");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("\"Name\": \"");
    String _name = it.getName();
    _builder.append(_name, "  ");
    _builder.append("\",");
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    _builder.append("\"Min\": \"");
    int _min = it.getMin();
    _builder.append(_min, "  ");
    _builder.append("\",");
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    _builder.append("\"Max\": \"");
    int _max = it.getMax();
    _builder.append(_max, "  ");
    _builder.append("\",");
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    _builder.append("\"Features\": [");
    _builder.newLine();
    _builder.append("    ");
    {
      EList<Feature> _features = it.getFeatures();
      boolean _hasElements = false;
      for(final Feature f : _features) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(",", "    ");
        }
        Object _generateFeature = DSLGenerator.generateFeature(f);
        _builder.append(_generateFeature, "    ");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    _builder.append("]");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public static CharSequence generateConstraint(final Constraint it) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("{");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("\"Name\": \"");
    String _name = it.getName();
    _builder.append(_name, "  ");
    _builder.append("\",");
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    _builder.append("\"Msg\": \"");
    String _errorMessage = it.getErrorMessage();
    _builder.append(_errorMessage, "  ");
    _builder.append("\",");
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    _builder.append("\"Expression\": ");
    Expression _expression = it.getExpression();
    CharSequence _generateExpression = DSLGenerator.generateExpression(_expression);
    _builder.append(_generateExpression, "  ");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public static CharSequence generateExpression(final Expression expr) {
    StringConcatenation _builder = new StringConcatenation();
    {
      if ((expr instanceof Unary)) {
        Expression _innerExpression = ((Unary)expr).getInnerExpression();
        Object _generateExpression = DSLGenerator.generateExpression(_innerExpression);
        _builder.append(_generateExpression, "");
        _builder.newLineIfNotEmpty();
      } else {
        if ((expr instanceof Binary)) {
          CharSequence _generateBinaryExpr = DSLGenerator.generateBinaryExpr(((Binary) expr));
          _builder.append(_generateBinaryExpr, "");
          _builder.newLineIfNotEmpty();
        } else {
          if ((expr instanceof StringConst)) {
            CharSequence _generateStringConst = DSLGenerator.generateStringConst(((StringConst) expr));
            _builder.append(_generateStringConst, "");
            _builder.newLineIfNotEmpty();
          } else {
            if ((expr instanceof IntConst)) {
              CharSequence _generateIntConst = DSLGenerator.generateIntConst(((IntConst) expr));
              _builder.append(_generateIntConst, "");
              _builder.newLineIfNotEmpty();
            } else {
              if ((expr instanceof BoolConst)) {
                CharSequence _generateBoolConst = DSLGenerator.generateBoolConst(((BoolConst) expr));
                _builder.append(_generateBoolConst, "");
                _builder.newLineIfNotEmpty();
              } else {
                if ((expr instanceof IdRef)) {
                  CharSequence _generateIdRef = DSLGenerator.generateIdRef(((IdRef) expr));
                  _builder.append(_generateIdRef, "");
                  _builder.newLineIfNotEmpty();
                }
              }
            }
          }
        }
      }
    }
    return _builder;
  }
  
  public static CharSequence generateUnaryExpr(final Unary it) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("{");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("\"Type\": \"Unary\",");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("\"Operator\": \"");
    UnaryOperator _operator = it.getOperator();
    _builder.append(_operator, "  ");
    _builder.append("\",");
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    _builder.append("\"Expression\": ");
    Expression _innerExpression = it.getInnerExpression();
    CharSequence _generateExpression = DSLGenerator.generateExpression(_innerExpression);
    _builder.append(_generateExpression, "  ");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public static CharSequence generateBinaryExpr(final Binary it) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("{");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("\"Type\": \"Binary\",");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("\"Operator\": \"");
    BinaryOperator _operator = it.getOperator();
    _builder.append(_operator, "  ");
    _builder.append("\",");
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    _builder.append("\"Left\": ");
    Expression _leftExpression = it.getLeftExpression();
    Object _generateExpression = DSLGenerator.generateExpression(_leftExpression);
    _builder.append(_generateExpression, "  ");
    _builder.append(",");
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    _builder.append("\"Right\": ");
    Expression _rightExpression = it.getRightExpression();
    Object _generateExpression_1 = DSLGenerator.generateExpression(_rightExpression);
    _builder.append(_generateExpression_1, "  ");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public static CharSequence generateStringConst(final StringConst it) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("{");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("\"Type\": \"StringConst\",");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("\"Value\": \"");
    String _value = it.getValue();
    _builder.append(_value, "  ");
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public static CharSequence generateIntConst(final IntConst it) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("{");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("\"Type\": \"IntConst\",");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("\"Value\": \"");
    int _value = it.getValue();
    _builder.append(_value, "  ");
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public static CharSequence generateBoolConst(final BoolConst it) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("{");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("\"Type\": \"BoolConst\",");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("\"Value\": \"");
    Booolean _value = it.getValue();
    _builder.append(_value, "  ");
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public static CharSequence generateIdRef(final IdRef it) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("{");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("\"Type\": \"IdRef\",");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("\"Ref\": \"");
    Identifiable _identifier = it.getIdentifier();
    String _name = _identifier.getName();
    _builder.append(_name, "  ");
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public void doGenerate(final Resource resource, final IFileSystemAccess fsa) {
    TreeIterator<EObject> _allContents = resource.getAllContents();
    Iterable<EObject> _iterable = IteratorExtensions.<EObject>toIterable(_allContents);
    Iterable<Configuration> _filter = Iterables.<Configuration>filter(_iterable, Configuration.class);
    final Consumer<Configuration> _function = new Consumer<Configuration>() {
      public void accept(final Configuration it) {
        String _name = it.getName();
        final String fname = StringExtensions.toFirstUpper(_name);
        CharSequence _jSON = DSLGenerator.toJSON(it);
        fsa.generateFile((("configurations/" + fname) + ".json"), _jSON);
      }
    };
    _filter.forEach(_function);
  }
}
