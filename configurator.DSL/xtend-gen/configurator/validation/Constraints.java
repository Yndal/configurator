package configurator.validation;

import configurator.Group;
import java.util.Arrays;
import org.eclipse.emf.ecore.EObject;

@SuppressWarnings("all")
public class Constraints {
  protected static boolean _constraint(final Group it) {
    return true;
  }
  
  /**
   * Fall back for all types that are not constrained - Copied from Andrzej's repo'
   */
  protected static boolean _constraint(final EObject it) {
    return true;
  }
  
  public static boolean constraint(final EObject it) {
    if (it instanceof Group) {
      return _constraint((Group)it);
    } else if (it != null) {
      return _constraint(it);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(it).toString());
    }
  }
}
