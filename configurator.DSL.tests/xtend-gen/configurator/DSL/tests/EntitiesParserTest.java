package configurator.DSL.tests;

import configurator.Configuration;
import configurator.DSL.tests.JSONHelper;
import configurator.DSLInjectorProvider;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.generator.InMemoryFileSystemAccess;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.eclipse.xtext.junit4.util.ParseHelper;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(XtextRunner.class)
@InjectWith(DSLInjectorProvider.class)
@SuppressWarnings("all")
public class EntitiesParserTest {
  @Inject
  @Extension
  private ParseHelper<Configuration> _parseHelper;
  
  @Inject
  private IGenerator DSLGenerator;
  
  @Test
  public void generatorTester() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("TestConfiguration {");
      _builder.newLine();
      _builder.append("Types {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Integer i1,");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Integer i2,");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Boolean b1,");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Boolean b2,");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("VingummeTyper {Banan,Appelsin,Vindrue},");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Lakrids {Skipper,Saltbombe}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      Configuration generatorTestDSL = this._parseHelper.parse(_builder);
      final InMemoryFileSystemAccess fsa = new InMemoryFileSystemAccess();
      Resource _eResource = generatorTestDSL.eResource();
      this.DSLGenerator.doGenerate(_eResource, fsa);
      final Map<String, CharSequence> m = fsa.getTextFiles();
      Set<Map.Entry<String, CharSequence>> _entrySet = m.entrySet();
      final Iterator iterator = _entrySet.iterator();
      boolean _hasNext = iterator.hasNext();
      boolean _while = _hasNext;
      while (_while) {
        {
          Object _next = iterator.next();
          final Map.Entry e = ((Map.Entry) _next);
          Object _value = e.getValue();
          final String JSON = _value.toString();
          boolean _parse = JSONHelper.parse(JSON);
          Assert.assertEquals(Boolean.valueOf(_parse), Boolean.valueOf(true));
        }
        boolean _hasNext_1 = iterator.hasNext();
        _while = _hasNext_1;
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
