package configurator.DSL.tests;

import configurator.Binary;
import configurator.BoolConst;
import configurator.ConfiguratorFactory;
import configurator.DSLInjectorProvider;
import configurator.IntConst;
import configurator.StringConst;
import configurator.Unary;
import configurator.validation.DSLValidator;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function0;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(XtextRunner.class)
@InjectWith(DSLInjectorProvider.class)
@SuppressWarnings("all")
public class ValidationTests {
  private DSLValidator validator = new Function0<DSLValidator>() {
    public DSLValidator apply() {
      try {
        DSLValidator _newInstance = DSLValidator.class.newInstance();
        return _newInstance;
      } catch (Throwable _e) {
        throw Exceptions.sneakyThrow(_e);
      }
    }
  }.apply();
  
  @Test
  public void testAdjustFeatureCardinalities() {
  }
  
  @Test
  public void testAdjustGroupCardinalities() {
  }
  
  @Test
  public void testGetReturnTypeName() {
    StringConst _createStringConst = ConfiguratorFactory.eINSTANCE.createStringConst();
    String _returnTypeName = this.validator.getReturnTypeName(_createStringConst);
    Assert.assertEquals(_returnTypeName, "String");
    StringConst _createStringConst_1 = ConfiguratorFactory.eINSTANCE.createStringConst();
    String _returnTypeName_1 = this.validator.getReturnTypeName(_createStringConst_1);
    Assert.assertNotEquals(_returnTypeName_1, "Integer");
    StringConst _createStringConst_2 = ConfiguratorFactory.eINSTANCE.createStringConst();
    String _returnTypeName_2 = this.validator.getReturnTypeName(_createStringConst_2);
    Assert.assertNotEquals(_returnTypeName_2, "Boolean");
    IntConst _createIntConst = ConfiguratorFactory.eINSTANCE.createIntConst();
    String _returnTypeName_3 = this.validator.getReturnTypeName(_createIntConst);
    Assert.assertEquals(_returnTypeName_3, "Integer");
    IntConst _createIntConst_1 = ConfiguratorFactory.eINSTANCE.createIntConst();
    String _returnTypeName_4 = this.validator.getReturnTypeName(_createIntConst_1);
    Assert.assertNotEquals(_returnTypeName_4, "String");
    IntConst _createIntConst_2 = ConfiguratorFactory.eINSTANCE.createIntConst();
    String _returnTypeName_5 = this.validator.getReturnTypeName(_createIntConst_2);
    Assert.assertNotEquals(_returnTypeName_5, "Boolean");
    BoolConst _createBoolConst = ConfiguratorFactory.eINSTANCE.createBoolConst();
    String _returnTypeName_6 = this.validator.getReturnTypeName(_createBoolConst);
    Assert.assertEquals(_returnTypeName_6, "Boolean");
    BoolConst _createBoolConst_1 = ConfiguratorFactory.eINSTANCE.createBoolConst();
    String _returnTypeName_7 = this.validator.getReturnTypeName(_createBoolConst_1);
    Assert.assertNotEquals(_returnTypeName_7, "String");
    BoolConst _createBoolConst_2 = ConfiguratorFactory.eINSTANCE.createBoolConst();
    String _returnTypeName_8 = this.validator.getReturnTypeName(_createBoolConst_2);
    Assert.assertNotEquals(_returnTypeName_8, "Integer");
    Unary unaryExpression = ConfiguratorFactory.eINSTANCE.createUnary();
    String _returnTypeName_9 = this.validator.getReturnTypeName(unaryExpression);
    Assert.assertNull(_returnTypeName_9);
    StringConst _createStringConst_3 = ConfiguratorFactory.eINSTANCE.createStringConst();
    unaryExpression.setInnerExpression(_createStringConst_3);
    String _returnTypeName_10 = this.validator.getReturnTypeName(unaryExpression);
    Assert.assertEquals(_returnTypeName_10, "String");
    Binary binaryExpression = ConfiguratorFactory.eINSTANCE.createBinary();
    String _returnTypeName_11 = this.validator.getReturnTypeName(binaryExpression);
    Assert.assertNull(_returnTypeName_11);
    StringConst _createStringConst_4 = ConfiguratorFactory.eINSTANCE.createStringConst();
    binaryExpression.setLeftExpression(_createStringConst_4);
    String _returnTypeName_12 = this.validator.getReturnTypeName(binaryExpression);
    Assert.assertNull(_returnTypeName_12);
    StringConst _createStringConst_5 = ConfiguratorFactory.eINSTANCE.createStringConst();
    binaryExpression.setLeftExpression(_createStringConst_5);
    IntConst _createIntConst_3 = ConfiguratorFactory.eINSTANCE.createIntConst();
    binaryExpression.setRightExpression(_createIntConst_3);
    String _returnTypeName_13 = this.validator.getReturnTypeName(binaryExpression);
    Assert.assertNull(_returnTypeName_13);
    StringConst _createStringConst_6 = ConfiguratorFactory.eINSTANCE.createStringConst();
    binaryExpression.setLeftExpression(_createStringConst_6);
    StringConst _createStringConst_7 = ConfiguratorFactory.eINSTANCE.createStringConst();
    binaryExpression.setRightExpression(_createStringConst_7);
    String _returnTypeName_14 = this.validator.getReturnTypeName(binaryExpression);
    Assert.assertEquals(_returnTypeName_14, "String");
  }
  
  @Test
  public void testGetInterfaceName() {
    StringConst _createStringConst = ConfiguratorFactory.eINSTANCE.createStringConst();
    String _interfaceName = this.validator.getInterfaceName(_createStringConst);
    Assert.assertEquals(_interfaceName, "StringConst");
    StringConst _createStringConst_1 = ConfiguratorFactory.eINSTANCE.createStringConst();
    String _interfaceName_1 = this.validator.getInterfaceName(_createStringConst_1);
    Assert.assertNotEquals(_interfaceName_1, "IntConst");
    StringConst _createStringConst_2 = ConfiguratorFactory.eINSTANCE.createStringConst();
    String _interfaceName_2 = this.validator.getInterfaceName(_createStringConst_2);
    Assert.assertNotEquals(_interfaceName_2, "BoolConst");
    IntConst _createIntConst = ConfiguratorFactory.eINSTANCE.createIntConst();
    String _interfaceName_3 = this.validator.getInterfaceName(_createIntConst);
    Assert.assertEquals(_interfaceName_3, "IntConst");
    IntConst _createIntConst_1 = ConfiguratorFactory.eINSTANCE.createIntConst();
    String _interfaceName_4 = this.validator.getInterfaceName(_createIntConst_1);
    Assert.assertNotEquals(_interfaceName_4, "StringConst");
    IntConst _createIntConst_2 = ConfiguratorFactory.eINSTANCE.createIntConst();
    String _interfaceName_5 = this.validator.getInterfaceName(_createIntConst_2);
    Assert.assertNotEquals(_interfaceName_5, "BoolConst");
    BoolConst _createBoolConst = ConfiguratorFactory.eINSTANCE.createBoolConst();
    String _interfaceName_6 = this.validator.getInterfaceName(_createBoolConst);
    Assert.assertEquals(_interfaceName_6, "BoolConst");
    BoolConst _createBoolConst_1 = ConfiguratorFactory.eINSTANCE.createBoolConst();
    String _interfaceName_7 = this.validator.getInterfaceName(_createBoolConst_1);
    Assert.assertNotEquals(_interfaceName_7, "StringConst");
    BoolConst _createBoolConst_2 = ConfiguratorFactory.eINSTANCE.createBoolConst();
    String _interfaceName_8 = this.validator.getInterfaceName(_createBoolConst_2);
    Assert.assertNotEquals(_interfaceName_8, "IntConst");
    Unary unaryExpression = ConfiguratorFactory.eINSTANCE.createUnary();
    String _interfaceName_9 = this.validator.getInterfaceName(unaryExpression);
    Assert.assertNull(_interfaceName_9);
    StringConst _createStringConst_3 = ConfiguratorFactory.eINSTANCE.createStringConst();
    unaryExpression.setInnerExpression(_createStringConst_3);
    String _interfaceName_10 = this.validator.getInterfaceName(unaryExpression);
    Assert.assertEquals(_interfaceName_10, "StringConst");
    Binary binaryExpression = ConfiguratorFactory.eINSTANCE.createBinary();
    String _interfaceName_11 = this.validator.getInterfaceName(binaryExpression);
    Assert.assertNull(_interfaceName_11);
    StringConst _createStringConst_4 = ConfiguratorFactory.eINSTANCE.createStringConst();
    binaryExpression.setLeftExpression(_createStringConst_4);
    StringConst _createStringConst_5 = ConfiguratorFactory.eINSTANCE.createStringConst();
    binaryExpression.setRightExpression(_createStringConst_5);
    String _interfaceName_12 = this.validator.getInterfaceName(binaryExpression);
    Assert.assertEquals(_interfaceName_12, "StringConst");
    StringConst _createStringConst_6 = ConfiguratorFactory.eINSTANCE.createStringConst();
    binaryExpression.setLeftExpression(_createStringConst_6);
    IntConst _createIntConst_3 = ConfiguratorFactory.eINSTANCE.createIntConst();
    binaryExpression.setRightExpression(_createIntConst_3);
    String _interfaceName_13 = this.validator.getInterfaceName(binaryExpression);
    Assert.assertNull(_interfaceName_13);
  }
  
  @Test
  public void testMatchingExpressionTypes() {
    Binary binaryExpression = ConfiguratorFactory.eINSTANCE.createBinary();
    boolean _matchingExpressionTypes = this.validator.matchingExpressionTypes(binaryExpression);
    Assert.assertFalse(_matchingExpressionTypes);
    StringConst _createStringConst = ConfiguratorFactory.eINSTANCE.createStringConst();
    binaryExpression.setLeftExpression(_createStringConst);
    boolean _matchingExpressionTypes_1 = this.validator.matchingExpressionTypes(binaryExpression);
    Assert.assertFalse(_matchingExpressionTypes_1);
    StringConst _createStringConst_1 = ConfiguratorFactory.eINSTANCE.createStringConst();
    binaryExpression.setLeftExpression(_createStringConst_1);
    IntConst _createIntConst = ConfiguratorFactory.eINSTANCE.createIntConst();
    binaryExpression.setRightExpression(_createIntConst);
    boolean _matchingExpressionTypes_2 = this.validator.matchingExpressionTypes(binaryExpression);
    Assert.assertFalse(_matchingExpressionTypes_2);
    StringConst _createStringConst_2 = ConfiguratorFactory.eINSTANCE.createStringConst();
    binaryExpression.setLeftExpression(_createStringConst_2);
    StringConst _createStringConst_3 = ConfiguratorFactory.eINSTANCE.createStringConst();
    binaryExpression.setRightExpression(_createStringConst_3);
    boolean _matchingExpressionTypes_3 = this.validator.matchingExpressionTypes(binaryExpression);
    Assert.assertTrue(_matchingExpressionTypes_3);
  }
  
  @Test
  public void testCheckFeatureCardinality() {
  }
}
