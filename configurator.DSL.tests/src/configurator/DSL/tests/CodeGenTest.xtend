package configurator.DSL.tests

import configurator.Configuration
import configurator.DSLInjectorProvider
import java.util.Iterator
import java.util.Map
import javax.inject.Inject
import org.eclipse.xtext.generator.IGenerator
import org.eclipse.xtext.generator.InMemoryFileSystemAccess
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Assert

@RunWith(typeof(XtextRunner))
@InjectWith(typeof(DSLInjectorProvider))

class CodeGenTest {
	@Inject extension ParseHelper<Configuration>
	@Inject IGenerator DSLGenerator

	//Testing basic parsing of Types - no errors
	@Test
	def void generatorTester() {
		var generatorTestDSL = 
		'''
		VolksWagen {
			Types {
				carType{Small, Medium, Big},
				Boolean hasHook,
				Boolean hasSpinners,
				Boolean hasSolarFilm,
				Boolean hasRadio,
				Boolean hasAntenna,
				Boolean hasCDPlayer,
				Integer speakers,
				rims { Alu, Chrome, Steal}, 
				tyres {Winter, Summer, AllSeasons},
				wheelSize {size16, size17, size18, size19, size20},
				wheel{FrontLeft, FrontRight, BackLeft, BackRight},
				engineType {Electrical, Diesel, Benzin},
				engineSize {l1400, l1600, l1800, l2000}
				}
			Features {
				CompleteCar carType {
					wheels [5...5]{
					Wheel wheel{
						wheelPartsMandatory [3...3] {
							Rims rims,
							Tyres tyres,
							WheelSize wheelSize
						},
						wheelPartsOptional [0...1]{
							Spinners hasSpinners
						}
					}
				},
				Engines [1...2]{
					Primary engineType,
					Secondary engineType
					
				},
				optionals [0...3]{
					Hook hasHook,
					SolarFilm hasSolarFilm,
					Radio hasRadio{
						radioParts [3...3]{
							Antenna hasAntenna,
							CDPlayer hasCDPlayer,
							Speakers speakers
						}
					}
				}
			}
		}
		Constraints {
			engineConstraint ["Engines must be of different types"]{
				Primary != Secondary
			},
			expressionTest["Unary and binary"]{
				8  == ((5 + 5) - 2) 
			}
		}
		} 
		'''.parse
		
		val fsa = new InMemoryFileSystemAccess()
        DSLGenerator.doGenerate(generatorTestDSL.eResource, fsa)
        val Map<String, CharSequence> m = fsa.textFiles
        val Iterator iterator = m.entrySet().iterator();
		while (iterator.hasNext()) {
        	val Map.Entry e = iterator.next as Map.Entry
        	val String JSON = e.value.toString
        	println(JSON)
        	Assert::assertEquals(JSONHelper.parse(JSON), true)
		}
	}
}

