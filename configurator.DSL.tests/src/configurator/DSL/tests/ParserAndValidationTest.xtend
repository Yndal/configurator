package configurator.DSL.tests

import configurator.Configuration
import configurator.ConfiguratorPackage
import configurator.DSLInjectorProvider
import javax.inject.Inject
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.eclipse.xtext.junit4.validation.ValidationTestHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(typeof(XtextRunner))
@InjectWith(typeof(DSLInjectorProvider))

class EntitiesParserTest {
	@Inject extension ParseHelper<Configuration>
	@Inject extension ValidationTestHelper

	//Testing basic parsing of Types - no errors
	@Test
	def void testParsingTypes() {
		var testConfigNoErrors = 
		'''
		TestConfiguration {
		Types {
			Integer i1,
			Integer i2,
			Boolean b1,
			Boolean b2,
			VingummeTyper {Banan,Appelsin,Vindrue},
			Lakrids {Skipper,Saltbombe}
		}
		}
		'''.parse
		
		testConfigNoErrors.assertNoErrors
		}
	
	//Testing basic parsing of Types - errors missing commas grammar error!
	@Test
	def void testParsingTypesErr() {
		var testConfigErrors = 	
		'''
		TestConfiguration {
		Types {
			Integer i1,
			Integer i2
			Boolean b1,
			Boolean b2,
			VingummeTyper {Banan,Appelsin,Vindrue},
			Lakrids {Skipper,Saltbombe}
		}
		}
		'''.parse
		
		testConfigErrors.assertError(ConfiguratorPackage::eINSTANCE.type, null, 'mismatched')
		}
	
	//Testing specific values
	@Test
	def void testValue() {
		val testConfg = 
		'''
		TestConfiguration {
		Types {
			VingummiTyper {Banan,Appelsin,Vindrue},
			Lakrids {Skipper,Saltbombe}
		}
		}
		'''.parse
		val type = testConfg.types.get(0)
		Assert::assertEquals("VingummiTyper", type.name);
		}
		
	//Testing cardinalities positive test
	@Test
	def void testCardinalitiesPos()
	{
		val testConfiguration = '''
		MyFirstConfiguration {
				Types
				{
					Integer i1,
					Integer i2,
					Boolean b1,
					Boolean b2,
					myEnum {val1, val2, val3}
				} 
				Features
				{
					MyRoot i1
						{
							Group1 [1...1]
							{
								F1 i1 [2..2],
								F2 i1 [1..1]
							}
						}
				}			
				Constraints
				{
					MyConstraint ['This is not good!!!'] 
					{
						F1 < F2
					},
					MyConstraint2 ['This is not good!!!'] 
					{
						F1 > (F2 == F1)
					} 	
				}
		}
		'''.parse
		
		testConfiguration.assertNoErrors
	}
	
	//Testing cardinalities negative test feature
	@Test
	def void testCardinalitiesNeg()
	{
		val testConfiguration = '''
		MyFirstConfiguration {
				Types
				{
					Integer i1,
					Integer i2,
					Boolean b1,
					Boolean b2,
					myEnum {val1, val2, val3}
				} 
				Features
				{
					MyRoot i1
						{
							Group1 [1...1]
							{
								F1 i1 [3..2],
								F2 i1 [1..1]
							}
						}
				}			
				Constraints
				{
					MyConstraint ['This is not good!!!'] 
					{
						F1 < F2
					},
					MyConstraint2 ['This is not good!!!'] 
					{
						F1 > (F2 == F1)
					} 	
				}
		}
		'''.parse
		
		testConfiguration.assertError(ConfiguratorPackage::eINSTANCE.feature, null, 'Cardinalities')
	}
	
	//Testing cardinalities negative test group
	@Test
	def void testCardinalitiesGroupNeg()
	{
		val testConfiguration = '''
		MyFirstConfiguration {
				Types
				{
					Integer i1,
					Integer i2,
					Boolean b1,
					Boolean b2,
					myEnum {val1, val2, val3}
				} 
				Features
				{
					MyRoot i1
						{
							Group1 [3...1]
							{
								F1 i1 [2..2],
								F2 i1 [1..1]
							}
						}
				}			
				Constraints
				{
					MyConstraint ['This is not good!!!'] 
					{
						F1 < F2
					},
					MyConstraint2 ['This is not good!!!'] 
					{
						F1 > (F2 == F1)
					} 	
				}
		}
		'''.parse
		
		testConfiguration.assertError(ConfiguratorPackage::eINSTANCE.group, null, 'Cardinalities')
	}
	
	//Testing constraint root validator
	@Test
	def void testConstraintRootIsBool()
	{
		val testConfiguration = '''
		MyFirstConfiguration {
				Types
				{
					Integer i1,
					Integer i2,
					Boolean b1,
					Boolean b2,
					myEnum {val1, val2, val3}
				} 
				Features
				{
					MyRoot i1
						{
							Group1 [1...1]
							{
								F1 i1 [2..2],
								F2 i1 [1..1]
							}
						}
				}			
				Constraints
				{
					MyConstraint ['This is not good!!!'] 
					{
						-(F1 < F2)
					},
					MyConstraint2 ['This is not good!!!'] 
					{
						F1 > (F2 == F1)
					} 	
				}
		}
		'''.parse
		
		testConfiguration.assertError(ConfiguratorPackage::eINSTANCE.expression, null, 'bool')
	}
	
	//Testing expressionTypes 
	@Test
	def void testExpressionTypes()
	{
		val testConfiguration = '''
		MyFirstConfiguration {
				Types
				{
					Integer i1,
					Integer i2,
					Boolean b1,
					Boolean b2,
					myEnum {val1, val2, val3}
				} 
				Features
				{
					MyRoot i1
						{
							Group1 [1...1]
							{
								F1 i1 [2..2],
								F2 i1 [1..1],
								F3 myEnum
							}
						}
				}			
				Constraints
				{
					MyConstraint ['This is not good!!!'] 
					{
						F1 < F2
					},
					MyConstraint2 ['This is not good!!!'] 
					{
						F1 > (F2 == F1)
					} 	
				}
		}
		'''.parse
		
		testConfiguration.assertNoErrors
		//testConfiguration.assertError(ConfiguratorPackage::eINSTANCE.expression, null, 'same')
	}
}

