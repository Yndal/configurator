package configurator.DSL.tests

import configurator.ConfiguratorFactory
import configurator.DSLInjectorProvider
import configurator.validation.DSLValidator
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import configurator.Group

@RunWith(typeof(XtextRunner))
@InjectWith(typeof(DSLInjectorProvider))

class ValidationTests {
	
	var DSLValidator validator = DSLValidator.newInstance 
	
	@Test
	def void testAdjustFeatureCardinalities() {
		
	}

	@Test
	def void testAdjustGroupCardinalities() {
		
	}
		
	@Test
	def void testGetReturnTypeName() {
		//StringConst == String
		Assert::assertEquals(validator.getReturnTypeName(ConfiguratorFactory.eINSTANCE.createStringConst), "String")
		//StringConst != Integer
		Assert::assertNotEquals(validator.getReturnTypeName(ConfiguratorFactory.eINSTANCE.createStringConst), "Integer")
		//StringConst != Boolean
		Assert::assertNotEquals(validator.getReturnTypeName(ConfiguratorFactory.eINSTANCE.createStringConst), "Boolean")
		//IntConst == Integer
		Assert::assertEquals(validator.getReturnTypeName(ConfiguratorFactory.eINSTANCE.createIntConst), "Integer")
		//IntConst != String
		Assert::assertNotEquals(validator.getReturnTypeName(ConfiguratorFactory.eINSTANCE.createIntConst), "String")
		//IntConst != Boolean
		Assert::assertNotEquals(validator.getReturnTypeName(ConfiguratorFactory.eINSTANCE.createIntConst), "Boolean")
		//BoolConst == Boolean
		Assert::assertEquals(validator.getReturnTypeName(ConfiguratorFactory.eINSTANCE.createBoolConst), "Boolean")
		//BoolConst == String
		Assert::assertNotEquals(validator.getReturnTypeName(ConfiguratorFactory.eINSTANCE.createBoolConst), "String")
		//BoolConst == Integer
		Assert::assertNotEquals(validator.getReturnTypeName(ConfiguratorFactory.eINSTANCE.createBoolConst), "Integer")
		//Unary - inner = null
		var unaryExpression = ConfiguratorFactory.eINSTANCE.createUnary
		Assert::assertNull(validator.getReturnTypeName(unaryExpression))
		//Unary - inner = StringConst
		unaryExpression.innerExpression = ConfiguratorFactory.eINSTANCE.createStringConst
		Assert::assertEquals(validator.getReturnTypeName(unaryExpression), "String")
		//Binary - left = null & right = null
		var binaryExpression = ConfiguratorFactory.eINSTANCE.createBinary
		Assert::assertNull(validator.getReturnTypeName(binaryExpression))
		//Binary - left = StringConst & right = null
		binaryExpression.leftExpression = ConfiguratorFactory.eINSTANCE.createStringConst
		Assert::assertNull(validator.getReturnTypeName(binaryExpression))
		//Binary - left = StringConst & right = IntConst
		binaryExpression.leftExpression = ConfiguratorFactory.eINSTANCE.createStringConst
		binaryExpression.rightExpression = ConfiguratorFactory.eINSTANCE.createIntConst
		Assert::assertNull(validator.getReturnTypeName(binaryExpression))
		//Binary - left = StringConst & right = StringConst
		binaryExpression.leftExpression = ConfiguratorFactory.eINSTANCE.createStringConst
		binaryExpression.rightExpression = ConfiguratorFactory.eINSTANCE.createStringConst
		Assert::assertEquals(validator.getReturnTypeName(binaryExpression), "String")
	}
	
	@Test
	def void testGetInterfaceName() {
		//StringConst == StringConst
		Assert::assertEquals(validator.getInterfaceName(ConfiguratorFactory.eINSTANCE.createStringConst), "StringConst")		
		//StringConst != IntConst
		Assert::assertNotEquals(validator.getInterfaceName(ConfiguratorFactory.eINSTANCE.createStringConst), "IntConst")
		//StringConst != BoolConst
		Assert::assertNotEquals(validator.getInterfaceName(ConfiguratorFactory.eINSTANCE.createStringConst), "BoolConst")
		//IntConst == IntConst
		Assert::assertEquals(validator.getInterfaceName(ConfiguratorFactory.eINSTANCE.createIntConst), "IntConst")		
		//IntConst != StringConst
		Assert::assertNotEquals(validator.getInterfaceName(ConfiguratorFactory.eINSTANCE.createIntConst), "StringConst")
		//IntConst != BoolConst
		Assert::assertNotEquals(validator.getInterfaceName(ConfiguratorFactory.eINSTANCE.createIntConst), "BoolConst")
		//BoolConst == BoolConst
		Assert::assertEquals(validator.getInterfaceName(ConfiguratorFactory.eINSTANCE.createBoolConst), "BoolConst")		
		//BoolConst != StringConst
		Assert::assertNotEquals(validator.getInterfaceName(ConfiguratorFactory.eINSTANCE.createBoolConst), "StringConst")
		//BoolConst != IntConst
		Assert::assertNotEquals(validator.getInterfaceName(ConfiguratorFactory.eINSTANCE.createBoolConst), "IntConst")
		//Unary - inner = null
		var unaryExpression = ConfiguratorFactory.eINSTANCE.createUnary
		Assert::assertNull(validator.getInterfaceName(unaryExpression))		
		//Unary - inner = StringConst
		unaryExpression.innerExpression = ConfiguratorFactory.eINSTANCE.createStringConst
		Assert::assertEquals(validator.getInterfaceName(unaryExpression), "StringConst")		
		//Binary - left & right = null
		var binaryExpression = ConfiguratorFactory.eINSTANCE.createBinary
		Assert::assertNull(validator.getInterfaceName(binaryExpression))		
		//Binary - left & right = StringConst
		binaryExpression.leftExpression = ConfiguratorFactory.eINSTANCE.createStringConst
		binaryExpression.rightExpression = ConfiguratorFactory.eINSTANCE.createStringConst
		Assert::assertEquals(validator.getInterfaceName(binaryExpression), "StringConst")		
		//Binary - left = StringConst & right = IntConst
		binaryExpression.leftExpression = ConfiguratorFactory.eINSTANCE.createStringConst
		binaryExpression.rightExpression = ConfiguratorFactory.eINSTANCE.createIntConst
		Assert::assertNull(validator.getInterfaceName(binaryExpression))
	}
	
	@Test
	def void testMatchingExpressionTypes() {
		//Binary, left & right = null
		var binaryExpression = ConfiguratorFactory.eINSTANCE.createBinary
		Assert::assertFalse(validator.matchingExpressionTypes(binaryExpression))
		//Binary, left = StringConst & right = null
		binaryExpression.leftExpression = ConfiguratorFactory.eINSTANCE.createStringConst
		Assert::assertFalse(validator.matchingExpressionTypes(binaryExpression))
		//Binary, left = StringConst & right = IntConst
		binaryExpression.leftExpression = ConfiguratorFactory.eINSTANCE.createStringConst
		binaryExpression.rightExpression = ConfiguratorFactory.eINSTANCE.createIntConst
		Assert::assertFalse(validator.matchingExpressionTypes(binaryExpression))
		//Binary, left = StringConst & right = StringConst
		binaryExpression.leftExpression = ConfiguratorFactory.eINSTANCE.createStringConst
		binaryExpression.rightExpression = ConfiguratorFactory.eINSTANCE.createStringConst
		Assert::assertTrue(validator.matchingExpressionTypes(binaryExpression))
	}
	
	@Test
	def void testCheckFeatureCardinality() {
		
	}
	
	/*
	@Test
	def void testCheckGroupCardinality() {
		//Group.min < 0
		var group = ConfiguratorFactory.eINSTANCE.createGroup
		group.min = -1
		validator.checkGroupCardinality(group)
	}
	*/
}

