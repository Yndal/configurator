package configurator.DSL.tests;
import com.eclipsesource.json.*;

public class JSONHelper {
	
	static JsonObject json;
	
	public JSONHelper()
	{
	
	}
	
	public static boolean parse(String jsonString)
	{
		try
		{
			json = JsonObject.readFrom(jsonString);
			return true;
		}
		catch (ParseException parseEx)
		{
			return false;
		}
	}
}
