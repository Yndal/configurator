/**
 */
package configurator.tests;

import configurator.BOOLEAN;
import configurator.ConfiguratorFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>BOOLEAN</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class BOOLEANTest extends TypeTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(BOOLEANTest.class);
	}

	/**
	 * Constructs a new BOOLEAN test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BOOLEANTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this BOOLEAN test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected BOOLEAN getFixture() {
		return (BOOLEAN)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConfiguratorFactory.eINSTANCE.createBOOLEAN());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //BOOLEANTest
