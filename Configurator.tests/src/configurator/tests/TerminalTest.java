/**
 */
package configurator.tests;

import configurator.Terminal;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Terminal</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class TerminalTest extends ExpressionTest {

	/**
	 * Constructs a new Terminal test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TerminalTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Terminal test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Terminal getFixture() {
		return (Terminal)fixture;
	}

} //TerminalTest
