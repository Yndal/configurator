/**
 */
package configurator.tests;

import configurator.ConfiguratorFactory;
import configurator.INTEGER;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>INTEGER</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class INTEGERTest extends TypeTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(INTEGERTest.class);
	}

	/**
	 * Constructs a new INTEGER test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public INTEGERTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this INTEGER test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected INTEGER getFixture() {
		return (INTEGER)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConfiguratorFactory.eINSTANCE.createINTEGER());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //INTEGERTest
