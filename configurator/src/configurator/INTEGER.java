/**
 */
package configurator;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>INTEGER</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see configurator.ConfiguratorPackage#getINTEGER()
 * @model
 * @generated
 */
public interface INTEGER extends Type {
} // INTEGER
