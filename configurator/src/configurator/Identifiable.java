/**
 */
package configurator;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Identifiable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see configurator.ConfiguratorPackage#getIdentifiable()
 * @model abstract="true"
 * @generated
 */
public interface Identifiable extends NamedElement {
} // Identifiable
