/**
 */
package configurator;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Id Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link configurator.IdRef#getIdentifier <em>Identifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see configurator.ConfiguratorPackage#getIdRef()
 * @model
 * @generated
 */
public interface IdRef extends Terminal {
	/**
	 * Returns the value of the '<em><b>Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identifier</em>' reference.
	 * @see #setIdentifier(Identifiable)
	 * @see configurator.ConfiguratorPackage#getIdRef_Identifier()
	 * @model required="true"
	 * @generated
	 */
	Identifiable getIdentifier();

	/**
	 * Sets the value of the '{@link configurator.IdRef#getIdentifier <em>Identifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Identifier</em>' reference.
	 * @see #getIdentifier()
	 * @generated
	 */
	void setIdentifier(Identifiable value);

} // IdRef
