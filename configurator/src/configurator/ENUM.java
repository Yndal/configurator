/**
 */
package configurator;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ENUM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link configurator.ENUM#getEnumValues <em>Enum Values</em>}</li>
 * </ul>
 * </p>
 *
 * @see configurator.ConfiguratorPackage#getENUM()
 * @model
 * @generated
 */
public interface ENUM extends Type {
	/**
	 * Returns the value of the '<em><b>Enum Values</b></em>' containment reference list.
	 * The list contents are of type {@link configurator.EnumValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enum Values</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enum Values</em>' containment reference list.
	 * @see configurator.ConfiguratorPackage#getENUM_EnumValues()
	 * @model containment="true"
	 * @generated
	 */
	EList<EnumValue> getEnumValues();

} // ENUM
