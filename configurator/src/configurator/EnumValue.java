/**
 */
package configurator;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum Value</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see configurator.ConfiguratorPackage#getEnumValue()
 * @model
 * @generated
 */
public interface EnumValue extends Identifiable {

} // EnumValue
