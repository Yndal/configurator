/**
 */
package configurator;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Terminal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see configurator.ConfiguratorPackage#getTerminal()
 * @model abstract="true"
 * @generated
 */
public interface Terminal extends Expression {
} // Terminal
