/**
 */
package configurator;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bool Const</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link configurator.BoolConst#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see configurator.ConfiguratorPackage#getBoolConst()
 * @model
 * @generated
 */
public interface BoolConst extends Constant {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * The literals are from the enumeration {@link configurator.Booolean}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see configurator.Booolean
	 * @see #setValue(Booolean)
	 * @see configurator.ConfiguratorPackage#getBoolConst_Value()
	 * @model default="false" required="true"
	 * @generated
	 */
	Booolean getValue();

	/**
	 * Sets the value of the '{@link configurator.BoolConst#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see configurator.Booolean
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Booolean value);

} // BoolConst
