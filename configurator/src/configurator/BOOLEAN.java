/**
 */
package configurator;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BOOLEAN</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see configurator.ConfiguratorPackage#getBOOLEAN()
 * @model
 * @generated
 */
public interface BOOLEAN extends Type {
} // BOOLEAN
