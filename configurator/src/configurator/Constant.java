/**
 */
package configurator;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constant</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see configurator.ConfiguratorPackage#getConstant()
 * @model abstract="true"
 * @generated
 */
public interface Constant extends Terminal {
} // Constant
